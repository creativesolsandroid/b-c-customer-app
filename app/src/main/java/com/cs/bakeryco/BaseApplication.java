package com.cs.bakeryco;

import android.app.Application;

import com.cs.bakeryco.widgets.FontsOverride;


/**
 * Created by CS on 26-05-2016.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        FontsOverride.setDefaultFont(this, "SERIF", "avenirnext_medium.ttf");
    }
}
