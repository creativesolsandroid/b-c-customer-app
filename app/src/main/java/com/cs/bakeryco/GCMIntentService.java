//package com.cs.bakeryco;
//
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.os.StrictMode;
//import android.support.v4.app.NotificationCompat;
//import android.support.v4.app.TaskStackBuilder;
//import android.util.Log;
//
//
//
//public class GCMIntentService extends GcmListenerService {
//    public static final int NOTIFICATION_ID = 1;
//    private NotificationManager mNotificationManager;
//    NotificationCompat.Builder builder;
//    String mPhoto_url;
//    String fromUser,msg,type,category,photoId,largeIcon,toUser;
//    boolean mAlreadyFollowing = false;
//    private SharedPreferences pref;
//    boolean likes_pref,comment_pref,follow_pref,stuff_pref,weekly_pref,feature_pref;
//
//
//    public GCMIntentService() {
////        super("GcmIntentService");
//
//        if (android.os.Build.VERSION.SDK_INT > 9) {
// 			StrictMode.ThreadPolicy policy =
// 			        new StrictMode.ThreadPolicy.Builder().permitAll().build();
// 			StrictMode.setThreadPolicy(policy);
// 			}
//    }
//
//    @Override
//    public void onMessageReceived(String from, Bundle data) {
//        super.onMessageReceived(from, data);
////    }
////
////    @Override
////    protected void onHandleIntent(Intent intent) {
//        Bundle extras = data;
//        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
//        // The getMessageType() intent parameter must be the intent you received
//        // in your BroadcastReceiver.
//       // String messageType = gcm.getMessageType(intent);
//
//        if(extras != null){
//        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
//            /*
//             * Filter messages based on message type. Since it is likely that GCM
//             * will be extended in the future with new message types, just ignore
//             * any message types you're not interested in, or that you don't
//             * recognize.
//             */
////            if (GoogleCloudMessaging.
////                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
////               // sendNotification("Send error: " + extras.toString());
////            } else if (GoogleCloudMessaging.
////                    MESSAGE_TYPE_DELETED.equals(messageType)) {
////               // sendNotification("Deleted messages on server: " +
////               //         extras.toString());
////            // If it's a regular GCM message, do some work.
////            } else if (GoogleCloudMessaging.
////                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//
//                Log.i("TAG", "Received: " + extras.toString());
//
//                // Post notification of received message.
//                msg = extras.getString("message");
////                type = extras.getString("type");
////                fromUser = extras.getString("toUser");
////                category = extras.getString("category");
////                photoId = extras.getString("photo");
//
//            sendNotification(msg);
//
//
////            }
//        }
//    }
//        // Release the wake lock provided by the WakefulBroadcastReceiver.
////        GcmBroadcastReceiver.completeWakefulIntent(intent);
//    }
//
//    // Put the message into a notification and post it.
//    // This is just one simple example of what you might choose to do with
//    // a GCM message.
//    private void sendNotification(String msg) {
//    	Bitmap remote_picture = null;
//    	Bitmap large_icon = null;
//
//        mNotificationManager = (NotificationManager)
//                this.getSystemService(Context.NOTIFICATION_SERVICE);
////
////
////        Notification myNotification = new NotificationCompat.Builder(this)
////                .setSmallIcon(R.drawable.more_aboutus)
////                .setAutoCancel(true)
////                .setContentTitle("dr.CAFE")
////                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
////                .setContentText(msg)
////				.setTicker(msg)
////                .setDefaults(NotificationCompat.DEFAULT_ALL).build();
////
////        mNotificationManager.notify(NOTIFICATION_ID, myNotification);
//
//
//
//        // Creates an explicit intent for an ResultActivity to receive.
//        Intent resultIntent = new Intent(this, MainActivity.class);
//        resultIntent.putExtra("push", true);
//
//        // This ensures that the back button follows the recommended
//        // convention for the back key.
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//
//        // Adds the back stack for the Intent (but not the Intent itself).
//        stackBuilder.addParentStack(MainActivity.class);
//
//        // Adds the Intent that starts the Activity to the top of the stack.
//        stackBuilder.addNextIntent(resultIntent);
//        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
//                0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Notification myNotification = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.notification_icon)
//                .setColor(Color.parseColor("#34684D"))
//                .setAutoCancel(true)
//                .setContentIntent(resultPendingIntent)
//                .setContentTitle("BAkery & Company")
//                .setContentText(msg)
//                .setTicker(msg)
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setDefaults(NotificationCompat.DEFAULT_ALL)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
//                .build();
//
//        mNotificationManager.notify(NOTIFICATION_ID, myNotification);
//
//
//
//    }
//
//
//}
//
//
