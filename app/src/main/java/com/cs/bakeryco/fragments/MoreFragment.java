package com.cs.bakeryco.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.AboutUs;
import com.cs.bakeryco.activities.AddressActivity;
import com.cs.bakeryco.activities.EditProfile;
import com.cs.bakeryco.activities.FavoriteOrderActivity;
import com.cs.bakeryco.activities.LanguageActivity;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.activities.MoreWebView;
import com.cs.bakeryco.activities.MyAddressActivity;
import com.cs.bakeryco.model.FavouriteOrder;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CS on 14-09-2016.
 */
public class MoreFragment extends Fragment implements View.OnClickListener {

    private static final int PROFILE_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    private static final int FAV_REQUEST = 3;
    String mLoginStatus;
    //    CardView myProfile, manageAddress;
//    TextView langEnglish, langArabic;
//    CardView aboutUs, contactUs;
    LinearLayout profile_layout, share_layout, rate_layout;
    //  LinearLayout  login_layout;
//        share_layout, rate_layout;
//    ImageView twitter, instagram, facebook, snap, youtube;
    RelativeLayout back_btn, fav_order_layout, manageAddress_layout, location_layout, langauge_layout, setting;
    //    TextView no_of_fav_orders;
    ImageView bag;
    TextView bag_count;
    View fav_view, manage_view;
    DataBaseHelper myDbHelper;

    private ArrayList<FavouriteOrder> favoritesList = new ArrayList<>();

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language, userId;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.moe_fragment, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.moe_fragment_arabic, container,
                    false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        mLoginStatus = userPrefs.getString("login_status", "");

        myDbHelper = new DataBaseHelper(getActivity());

//        about_us_layout = rootView.findViewById(R.id.about_us_layout);
        profile_layout = rootView.findViewById(R.id.profile_layout);
        share_layout = rootView.findViewById(R.id.share_layout);
        rate_layout = rootView.findViewById(R.id.rate_layout);
        bag = (ImageView) rootView.findViewById(R.id.bag);
        bag_count = (TextView) rootView.findViewById(R.id.bag_count);

//        twitter = rootView.findViewById(R.id.twitter);
//        instagram = rootView.findViewById(R.id.instagram);
//        facebook = rootView.findViewById(R.id.facebook);
//        snap = rootView.findViewById(R.id.snap);
//        youtube = rootView.findViewById(R.id.youtube);

        back_btn = rootView.findViewById(R.id.back_btn);
        fav_order_layout = rootView.findViewById(R.id.fav_order_layout);
        manageAddress_layout = rootView.findViewById(R.id.manage_address_layout);
//        login_layout = rootView.findViewById(R.id.login_layout);
//        location_layout = rootView.findViewById(R.id.location);
        langauge_layout = rootView.findViewById(R.id.language);
//        setting = rootView.findViewById(R.id.setting);
//        no_of_fav_orders = rootView.findViewById(R.id.no_of_favourites);

        fav_view = rootView.findViewById(R.id.view_fav);
        manage_view = rootView.findViewById(R.id.view_manage_address);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) getActivity()).menuClick();


            }
        });

//        if (myDbHelper.getTotalOrderQty() == 0) {
//
//            bag_count.setVisibility(View.GONE);
//
//        } else {
//
//            bag_count.setVisibility(View.VISIBLE);
//            bag_count.setText("" + myDbHelper.getTotalOrderQty());
//
//        }

//        if (mLoginStatus.equalsIgnoreCase("loggedin")){
//
//            fav_order_layout.setVisibility(View.VISIBLE);
//            manageAddress_layout.setVisibility(View.VISIBLE);
//            profile_layout.setVisibility(View.VISIBLE);
//            fav_view.setVisibility(View.VISIBLE);
//            manage_view.setVisibility(View.VISIBLE);
////            login_layout.setVisibility(View.GONE);
//
//        } else {
//
//            fav_order_layout.setVisibility(View.GONE);
//            manageAddress_layout.setVisibility(View.GONE);
//            profile_layout.setVisibility(View.GONE);
//            fav_view.setVisibility(View.GONE);
//            manage_view.setVisibility(View.GONE);
////            login_layout.setVisibility(View.VISIBLE);
//
//        }

//        login_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                startActivity(intent);
//
//            }
//        });

//        if (mLoginStatus.equalsIgnoreCase("loggedin")){
//
//            new GetFavoriteOrderDetails().execute(Constants.GET_FAVORITE_ORDERS_URL+userId);
//
//        } else {
//
//            no_of_fav_orders.setText("0");
//
//        }

        fav_order_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    ((MainActivity) getActivity()).selectItem(2);
                } else {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, FAV_REQUEST);
                }

            }
        });

        profile_layout.setOnClickListener(this);
        manageAddress_layout.setOnClickListener(this);
//        facebook.setOnClickListener(this);
//        twitter.setOnClickListener(this);
//        youtube.setOnClickListener(this);
//        instagram.setOnClickListener(this);
//        moreKeek.setOnClickListener(this);
        share_layout.setOnClickListener(this);
        rate_layout.setOnClickListener(this);
//        about_us_layout.setOnClickListener(this);
        langauge_layout.setOnClickListener(this);

//        langEnglish.setOnClickListener(this);
//        langArabic.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_layout:
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    ((MainActivity) getActivity()).Profile();
                } else {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, PROFILE_REQUEST);
                }
                break;

            case R.id.manage_address_layout:
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), MyAddressActivity.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, ADDRESS_REQUEST);
                }

                break;

//            case R.id.facebook:
//                Intent fbIntent = new Intent(getActivity(), MoreWebView.class);
//                fbIntent.putExtra("title", "Facebook");
//                fbIntent.putExtra("url","https://www.facebook.com/bakeryand.coksa");
//                startActivity(fbIntent);
//                break;
//
//            case R.id.twitter:
//                Intent twitterIntent = new Intent(getActivity(), MoreWebView.class);
//                twitterIntent.putExtra("title", "Twitter");
//                twitterIntent.putExtra("url","https://twitter.com/bakeryandcoksa");
//                startActivity(twitterIntent);
//                break;

//            case R.id.more_keek:
//                Intent keekIntent = new Intent(getActivity(), MoreWebView.class);
//                keekIntent.putExtra("title", "KeeK");
//                keekIntent.putExtra("url","https://www.keek.com/profile/bakeryandcoksa");
//                startActivity(keekIntent);
//                break;

//            case R.id.youtube:
//                Intent youtubeIntent = new Intent(getActivity(), MoreWebView.class);
//                youtubeIntent.putExtra("title", "Youtube");
//                youtubeIntent.putExtra("url","http://www.youtube.com/user/bakeryandcoksa");
//                startActivity(youtubeIntent);
//                break;
//
//            case R.id.instagram:
//                Intent instagramIntent = new Intent(getActivity(), MoreWebView.class);
//                instagramIntent.putExtra("title", "Instagram");
//                instagramIntent.putExtra("url","https://www.instagram.com/bakery.coksa/");
//                startActivity(instagramIntent);
//                break;

            case R.id.share_layout:

                new Handler().postDelayed(new Runnable() {

                    // Using handler with postDelayed called runnable run method

                    @Override
                    public void run() {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT,
                                "https://play.google.com/store/apps/details?id=com.cs.bakeryco&hl=en");
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                                "Check out Bakery & Company App !");
                        startActivity(Intent.createChooser(intent, "Share"));
                    }
                }, 200);
                break;

            case R.id.rate_layout:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.bakeryco")));
                break;

//            case R.id.about_us_layout:
//                ((MainActivity) getActivity()).selectItem(4);
//                break;
//            case R.id.more_contactus:
////                String email = "info@bakery-co.com";
////                Intent intent = new Intent(Intent.ACTION_SEND);
////                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                intent.setType("plain/text");
////                intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
////                intent.putExtra(Intent.EXTRA_SUBJECT, "");
//////                intent.putExtra(Intent.EXTRA_TITLE, "Bakery & Company Experiance");
////                intent.putExtra(Intent.EXTRA_EMAIL,new String[] {email});
////                getActivity().startActivity(intent);
//                Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("plain/text");
//                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@bakery-co.com"});
//                i.putExtra(Intent.EXTRA_SUBJECT, "Bakery & Company Experience");
//                i.putExtra(Intent.EXTRA_TITLE  , "Bakery & Company Experience");
//                final PackageManager pm = getActivity().getPackageManager();
//                final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
//                String className = null;
//                for (final ResolveInfo info : matches) {
//                    if (info.activityInfo.packageName.equals("com.google.android.gm")) {
//                        className = info.activityInfo.name;
//
//                        if(className != null && !className.isEmpty()){
//                            break;
//                        }
//                    }
//                }
//                i.setClassName("com.google.android.gm", className);
//                try {
//                    getActivity().startActivity(Intent.createChooser(i, "Send mail..."));
//                } catch (android.content.ActivityNotFoundException ex) {
//                    Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
//                }
//                break;
            case R.id.language:
//                languagePrefsEditor.putString("language","En");
//                languagePrefsEditor.commit();
////                langEnglish.setBackgroundColor(Color.parseColor("#40815F"));
////                langArabic.setBackgroundColor(Color.parseColor("#00000000"));
////                langEnglish.setTextColor(Color.parseColor("#FFFFFF"));
////                langArabic.setTextColor(Color.parseColor("#000000"));
//                getActivity().recreate();
                ((MainActivity) getActivity()).selectItem(5);
                break;
//            case R.id.lang_arabic:
//                languagePrefsEditor.putString("language","Ar");
//                languagePrefsEditor.commit();
////                langArabic.setBackgroundColor(Color.parseColor("#40815F"));
////                langEnglish.setBackgroundColor(Color.parseColor("#00000000"));
////                langArabic.setTextColor(Color.parseColor("#FFFFFF"));
////                langEnglish.setTextColor(Color.parseColor("#000000"));
//                getActivity().recreate();
//                break;
        }
    }

    public class GetFavoriteOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading...");
            favoritesList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", "user response:" + response);
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    FavouriteOrder fo = new FavouriteOrder();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String orderId = jo1.getString("odrId");
                                    String favoriteName = jo1.getString("Fname");
                                    String orderDate = jo1.getString("Odate");
                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
                                    String total_Price = jo1.getString("TotPrice");
                                    String orderType = jo1.getString("OrderType");
                                    String userAddress = jo1.getString("UserAddress");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for (int j = 0; j < ja1.length(); j++) {
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if (itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")) {
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        } else {
                                            itemDetails = itemDetails + ", " + jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr + ", " + jo2.getString("ItmName_ar");
                                        }
                                    }


                                    fo.setOrderId(orderId);
                                    fo.setFavoriteName(favoriteName);
                                    fo.setOrderDate(orderDate);
                                    fo.setStoreName(storeName);
                                    fo.setStoreName_ar(storeName_ar);
                                    fo.setTotalPrice(total_Price);
                                    fo.setItemDetails(itemDetails);
                                    fo.setItemDetails_ar(itemDetailsAr);
                                    fo.setOrdertype(orderType);
                                    fo.setUseraddress(userAddress);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    favoritesList.add(fo);

//                                    no_of_fav_orders.setText("" + favoritesList.size());

                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
//                                Toast.makeText(FavoriteOrdersActivity.this, "Sorry You Don't Have any Favorite Orders", Toast.LENGTH_SHORT).show();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }
}
