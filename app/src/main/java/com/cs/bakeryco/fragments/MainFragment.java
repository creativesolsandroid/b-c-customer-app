package com.cs.bakeryco.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.MenuPop;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.activities.LoginRegistration;
import com.cs.bakeryco.activities.OrderConfirmationNew;
import com.cs.bakeryco.activities.OrderHistoryActivity;
import com.cs.bakeryco.activities.SplashActivity;
import com.cs.bakeryco.activities.TrackOrderActivity;
import com.cs.bakeryco.adapters.BannersAdapter;
import com.cs.bakeryco.adapters.HomeMainCatList;
import com.cs.bakeryco.adapters.SpecialOffersAdapter;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.MainCategories;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.OrderHistory;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubCategories;
import com.cs.bakeryco.model.SubItems;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by CS on 08-09-2016.
 */
public class MainFragment extends Fragment {
    private static final int TRACK_ORDER_REQUEST = 1;
    //    LinearLayout orderLayout, trackOrderLayout, cateringLayout, aboutusLayout, corporateLayout, menu_layout, occasionLayout;
    SharedPreferences userPrefs;
    String mLoginStatus;
    RecyclerView special_offers_list;
    SpecialOffersAdapter specialOffersAdapter;
    ArrayList<ItemsList.Item> itemArrayList = new ArrayList<>();
    //    RelativeLayout morder_cnt;
//    TextView morder_count;
    TextView view_all, view_more;
    RecyclerView main_cat_list;
    //    ImageView partries_img, cake_img, sandwich_img, catring_img;
//    TextView  view_all, pastries_txt, cake_txt, sandwich_txt, catring_txt;
    DataBaseHelper myDbHelper;
    ListView xyz;
    ArrayList<Order> orderList = new ArrayList<>();
    int position;
    String language;
    String response;
    ArrayList<ItemsList.Items> itemsArrayList = new ArrayList<>();
    ArrayList<ItemsList.Banners> bannersArrayList = new ArrayList<>();
    SharedPreferences languagePrefs;
    View rootView;
    //    ImageView mbread;
    Typeface lightTypeface;
    ImageView menu;
    private AutoScrollViewPager bannersViewpage;
    Context context;
    TextView my_order;
    BannersAdapter mBanners;


    ArrayList<String> main_cat_name = new ArrayList<>();
    ArrayList<String> main_cat_name_ar = new ArrayList<>();
    ArrayList<Integer> main_cat_img = new ArrayList<>();

    HomeMainCatList mainCatList;

    ImageView bag;
    TextView bag_count;

    int overallXScroll = 0;
    int myCellWidth;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.main_fragment, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.main_fragment_arabic, container,
                    false);
        }

        if (language.equalsIgnoreCase("Ar")) {
            lightTypeface = Typeface.createFromAsset(getActivity().getAssets(),
                    "helveticaneue_regular.ttf");
        }

        main_cat_name.clear();
        main_cat_img.clear();

//        itemsArrayList = (ArrayList<ItemsList.Items>) getArguments().getSerializable("itemArraylist");
//        bannersArrayList = (ArrayList<ItemsList.Banners>) getArguments().getSerializable("bannersArraylist");

        itemsArrayList = SplashActivity.itemsArrayList;
        bannersArrayList = SplashActivity.bannersArrayList;

        Log.i("TAG", "itemslistsize: " + itemsArrayList.size());
        Log.i("TAG", "bannerslistsize: " + bannersArrayList.size());

//        order_now = (TextView) rootView.findViewById(R.id.order_now);
//        pastries_txt = (TextView) rootView.findViewById(R.id.pastries_txt);
//        cake_txt = (TextView) rootView.findViewById(R.id.cake_txt);
//        sandwich_txt = (TextView) rootView.findViewById(R.id.sandwich_txt);
//        catring_txt = (TextView) rootView.findViewById(R.id.catering_txt);
//        partries_img = (ImageView) rootView.findViewById(R.id.pastries_img);
//        cake_img = (ImageView) rootView.findViewById(R.id.cake_img);
//        sandwich_img = (ImageView) rootView.findViewById(R.id.sandwich_img);
//        catring_img = (ImageView) rootView.findViewById(R.id.catering_img);

        main_cat_list = rootView.findViewById(R.id.main_cat_list);

        bannersViewpage = (AutoScrollViewPager) rootView.findViewById(R.id.home_banner_list);

        bag = (ImageView) rootView.findViewById(R.id.bag);
        bag_count = (TextView) rootView.findViewById(R.id.bag_count);

        special_offers_list = rootView.findViewById(R.id.special_offer_list);

        my_order = rootView.findViewById(R.id.my_order);

        menu = rootView.findViewById(R.id.menu);

        view_all = (TextView) rootView.findViewById(R.id.view_all);
        view_more = rootView.findViewById(R.id.view_more);

//        home_order = (ImageView) rootView.findViewById(R.id.home_order);

        myDbHelper = new DataBaseHelper(getActivity());

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        if (language.equalsIgnoreCase("En")) {

            main_cat_name.add("CAKES");
            main_cat_name.add("PASTRIES");
            main_cat_name.add("MACAROONS");
            main_cat_name.add("BREADS");
            main_cat_name.add("SANDWICHES");
            main_cat_name.add("CHEF TASTING");
            main_cat_name.add("SPECIAL CAKES");
            main_cat_name.add("OFFERS");
            main_cat_name.add("CATERING");

        } else {

            main_cat_name.add("كيكة");
            main_cat_name.add("معجنات");
            main_cat_name.add("ماكارون");
            main_cat_name.add("الخبز الدولي");
            main_cat_name.add("ساندويتشات");
            main_cat_name.add("اختيارات الشيف");
            main_cat_name.add("كيكات خاص");
            main_cat_name.add("العروض");
            main_cat_name.add("طلبات التموين");

        }


        main_cat_img.add(R.drawable.menu_cake);
        main_cat_img.add(R.drawable.menu_pastries);
        main_cat_img.add(R.drawable.menu_macarons);
        main_cat_img.add(R.drawable.menu_breads);
        main_cat_img.add(R.drawable.menu_sandwiches);
        main_cat_img.add(R.drawable.menu_chefteastin);
        main_cat_img.add(R.drawable.menu_specialcake);
        main_cat_img.add(R.drawable.menu_offers);
        main_cat_img.add(R.drawable.menu_catering);


        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        main_cat_list.setLayoutManager(layoutManager1);

        mainCatList = new HomeMainCatList(getActivity(), main_cat_name, main_cat_img, itemsArrayList, language);
        main_cat_list.setAdapter(mainCatList);


        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderConfirmationNew.class);
                startActivity(a);

            }
        });

        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {

                    Intent i = new Intent(getActivity(), OrderHistoryActivity.class);
                    startActivity(i);

                } else {

                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);

                }

            }
        });

//        catring_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent a = new Intent(getActivity(), MenuPop.class);
//                a.putExtra("itemsArraylist", itemsArrayList);
//                startActivity(a);
//
//            }
//        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

//        order_now.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (myDbHelper.getTotalOrderQty()==0) {
////                    ((MainActivity) getActivity()).selectItem(1);
////                } else {
//                Intent intent = new Intent(getActivity(), OrderFragment.class);
//                startActivity(intent);
////                }
//            }
//        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        special_offers_list.setLayoutManager(layoutManager);

        specialOffersAdapter = new SpecialOffersAdapter(getActivity(), itemsArrayList.get(6).getSubCategoryItems(), itemsArrayList, language);
        special_offers_list.setAdapter(specialOffersAdapter);

        special_offers_list.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {

            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {

            }
        });


        special_offers_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int pos;
                pos = special_offers_list.SCROLL_STATE_IDLE;

                Log.i("TAG", "onScrollStateChanged: " + pos);

            }
        });

        special_offers_list.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                myCellWidth = special_offers_list.getChildAt(1).getMeasuredWidth();

                final int offset = special_offers_list.computeHorizontalScrollOffset();
//                if (offset % myCellWidth == 0) {
                final int position = offset / myCellWidth;
//                    Log.i("TAG","overall->" + position);
//                }


            }
        });

        view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).selectItem(1);

            }
        });

//        home_order.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent cateringIntent = new Intent(getActivity(), OrderFragment.class);
//                cateringIntent.putExtra("mainList", itemsArrayList);
//                startActivity(cateringIntent);
//            }
//        });


        view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
                breadIntent.putExtra("catId", 14);
                breadIntent.putExtra("mainList", itemsArrayList);
                startActivity(breadIntent);

            }
        });

//        pastries_txt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
//                breadIntent.putExtra("catId", 1);
//                breadIntent.putExtra("mainList", itemsArrayList);
//                startActivity(breadIntent);
//            }
//        });

//        cake_txt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
//                breadIntent.putExtra("catId", 2);
//                breadIntent.putExtra("mainList", itemsArrayList);
//                startActivity(breadIntent);
//            }
//        });
//
//        sandwich_txt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
//                breadIntent.putExtra("catId", 3);
//                breadIntent.putExtra("mainList", itemsArrayList);
//                startActivity(breadIntent);
//            }
//        });
//
//        partries_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
//                breadIntent.putExtra("catId", 1);
//                breadIntent.putExtra("mainList", itemsArrayList);
//                startActivity(breadIntent);
//            }
//        });
//        cake_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
//                breadIntent.putExtra("catId", 2);
//                breadIntent.putExtra("mainList", itemsArrayList);
//                startActivity(breadIntent);
//            }
//        });
//
//        sandwich_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
//                breadIntent.putExtra("catId", 3);
//                breadIntent.putExtra("mainList", itemsArrayList);
//                startActivity(breadIntent);
//            }
//        });

//        new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL);

        mBanners = new BannersAdapter(getContext(), itemsArrayList, bannersArrayList, language);
        bannersViewpage.setAdapter(mBanners);
        mBanners.notifyDataSetChanged();

        bannersViewpage.setInterval(3000);
        bannersViewpage.setCycle(true);
        bannersViewpage.startAutoScroll();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if (requestCode == TRACK_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent i = new Intent(getActivity(), TrackOrderActivity.class);
            i.putExtra("orderId", "-1");
            startActivity(i);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }
    }

//    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String networkStatus;
//        ProgressDialog dialog;
//
//        @Override
//        protected void onPreExecute() {
//            mainCatList.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading items...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    if (result.equals("")) {
//                        Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        try {
//                            JSONObject jo = new JSONObject(result);
//                            JSONArray ja = jo.getJSONArray("Success");
//
//                            for (int i=0;i<ja.length();i++){
//
//                                MainCategories maincat =new MainCategories();
//                                ArrayList<SubCategories> subcatList = new ArrayList<>();
//
//                                JSONObject jo1 = ja.getJSONObject(i);
//
//                                maincat.setMaincatid(jo1.getString("MainCategoryId"));
//                                maincat.setMaincatname(jo1.getString("MainCategoryName"));
//                                maincat.setMaincatname_ar(jo1.getString("MainCategoryName_Ar"));
//                                maincat.setMaincatdesc(jo1.getString("MainCategoryDesc"));
//                                maincat.setMaincatdesc_ar(jo1.getString("MainCategoryDesc_Ar"));
//                                maincat.setMainimg(jo1.getString("Simg"));
//
//                                JSONArray ja1 =jo1.getJSONArray("SubCategoryItems");
//
//                                for (int k=0;k<ja1.length();k++){
//
//                                    SubCategories subcat = new SubCategories();
//                                    ArrayList<Section> sectionList = new ArrayList<>();
//
//                                    JSONObject jo2 = ja1.getJSONObject(k);
//                                    subcat.setCatId(jo2.getString("SubCategoryId"));
//                                    subcat.setSubCat(jo2.getString("SubCategoryName"));
//                                    subcat.setSubCatAr(jo2.getString("SubCategoryName_Ar"));
//                                    subcat.setDescription(jo2.getString("SubCategoryDesc"));
//                                    subcat.setDescriptionAr(jo2.getString("SubCategoryDesc_Ar"));
//                                    subcat.setImages(jo2.getString("SubCategoryImage"));
//                                    subcat.setSubCatSeq(jo2.getString("SubCategorySeq"));
//                                    subcat.setMaincatid(jo2.getString("MainCategoryId"));
//
//
//                                    JSONArray ja2 = jo2.getJSONArray("Sections");
//
//                                    for (int l=0;l<ja2.length();l++){
//
//                                        Section section =new Section();
//                                        ArrayList<Items> itemlist =new ArrayList<>();
//
//                                        JSONObject jo3 = ja2.getJSONObject(l);
//                                        section.setSectionid(jo3.getString("SectionId"));
//                                        section.setSectionname(jo3.getString("SectionName"));
//                                        section.setSectionname_ar(jo3.getString("SectionName_Ar"));
//                                        section.setSectionsubcat(jo3.getString("SectionSubCategory"));
//
//                                        JSONArray ja3 = jo3.getJSONArray("Items");
//
//                                        for (int m=0;m<ja3.length();m++){
//
//                                            Items item =new Items();
//                                            ArrayList<Price> priceList = new ArrayList<>();
//                                            ArrayList<SubItems> subitemsList =new ArrayList<>();
//
//                                            JSONObject jo4 = ja3.getJSONObject(m);
//                                            item.setItemId(jo4.getString("ItemId"));
//                                            item.setSubCatId(jo4.getString("SubCategoryId"));
//                                            item.setSectionid(jo4.getString("SectionId"));
//                                            item.setItemName(jo4.getString("ItemName"));
//                                            item.setItemNameAr(jo4.getString("ItemName_Ar"));
//                                            item.setDescription(jo4.getString("ItemDesc"));
//                                            item.setDescriptionAr(jo4.getString("ItemDesc_Ar"));
//                                            item.setImage(jo4.getString("ItemImage"));
//                                            item.setItemtypeIds(jo4.getString("ItemTypeIds"));
//                                            item.setItemseq(jo4.getString("ItemSequence"));
//                                            item.setBuildingType(jo4.getString("BindingType"));
//                                            item.setIsdeliver(jo4.getString("IsDeliver"));
//                                            item.setNo_of_items(jo4.getString("NoOfItems"));
//                                            item.setDisplayname(jo4.getString("DisplayName"));
//                                            item.setDisplayname_ar(jo4.getString("DisplayName_Ar"));
//
//                                            JSONArray ja4 = jo4.getJSONArray("Price");
//
//                                            for (int n=0;n<ja4.length();n++){
//                                                Price price = new Price();
//
//                                                JSONObject jo5 = ja4.getJSONObject(n);
//                                                price.setPrice(jo5.getString("price"));
//                                                price.setSizeid(jo5.getString("sizeId"));
//                                                price.setSize(jo5.getString("size"));
//                                                price.setSizeName_Ar(jo5.getString("SizeName_Ar"));
//                                                price.setItemsizedesc(jo5.getString("ItemSizeDescription"));
//                                                price.setItemSizeDescription_Ar(jo5.getString("ItemSizeDescription_Ar"));
//                                                price.setPriceId(jo5.getString("priceId"));
//
//                                                Log.i("TAG","desc "+jo5.getString("ItemSizeDescription"));
//
//                                                priceList.add(price);
//                                            }
//
//                                            JSONArray ja5 = jo4.getJSONArray("SubItems");
//
//                                            try {
//                                                for (int o=0;o<ja5.length();o++){
//                                                    SubItems subItems =new SubItems();
//
//                                                    JSONObject jo6 = ja5.getJSONObject(o);
//                                                    subItems.setItemid(jo6.getString("ItemId"));
//                                                    subItems.setSubitemid(jo6.getString("SubItemId"));
//                                                    subItems.setSubitemName(jo6.getString("SubItemName"));
//                                                    subItems.setSubItemName_Ar(jo6.getString("SubItemName_Ar"));
//                                                    subItems.setSubItemDesc(jo6.getString("SubItemDesc"));
//                                                    subItems.setSubItemDesc_Ar(jo6.getString("SubItemDesc_Ar"));
//                                                    subItems.setSubItemimage(jo6.getString("SubItemImage"));
//                                                    subItems.setSubItemseq(jo6.getString("SubItemSequence"));
//                                                    subItems.setSubItemIsdeliver(jo6.getString("SubItemIsDeliver"));
//
//                                                    subitemsList.add(subItems);
//
//                                                }
//                                            } catch (JSONException e) {
//                                                e.printStackTrace();
//                                            }
//
//                                            item.setSubItems(subitemsList);
//                                            item.setPriceList(priceList);
//                                            itemlist.add(item);
//                                        }
//                                        section.setChildItems(itemlist);
//                                        sectionList.add(section);
//                                    }
//                                    subcat.setSections(sectionList);
//                                    subcatList.add(subcat);
//
//                                }
//                                maincat.setSubCategories(subcatList);
//                                mainCatList.add(maincat);
//                                Log.e("TAG" , "maincat " +mainCatList.size());
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }else {
//                Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }
////            mAdapter.notifyDataSetChanged();
//
////            if(subCatList.size()>0) {
////                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, subCatList, language);
////                mGridView.setAdapter(mSubMenuAdapter);
////                mSubMenuAdapter.notifyDataSetChanged();
////            }
////            int count =  mAdapter.getGroupCount();
////            for (int i = 0; i <count ; i++) {
////                mExpListView.collapseGroup(i);
////            }
//            super.onPostExecute(result);
//        }
//    }
}
