package com.cs.bakeryco.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.ImageLayout;
import com.cs.bakeryco.activities.OrderConfirmationNew;
import com.cs.bakeryco.activities.SpecialCakeActivity;
import com.cs.bakeryco.activities.SplashActivity;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.MainCategories;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubCategories;
import com.cs.bakeryco.model.SubItems;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 08-09-2016.
 */
public class OrderFragment extends Fragment implements View.OnClickListener {
    DataBaseHelper myDbHelper;
    String language;
    SharedPreferences languagePrefs;
    RelativeLayout back_btn;
    ImageView partries_img, cake_img, sandwich_img, bread_img, macaron_img, offer_img, special_cake_img, chef_teasting_img, catring_img;
    TextView pastries_txt, cake_txt, sandwich_txt, bread_txt, macaron_txt, offer_txt, special_cake_txt, chef_teasting_txt, catring_txt;

    ImageView bag;
    TextView bag_count;

    String response;
    ArrayList<ItemsList.Banners> bannersArrayList = new ArrayList<>();
    ArrayList<ItemsList.Items> mainCatList = new ArrayList<>();
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_fragment, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.order_fragment_arabic, container,
                    false);
        }

//        mainCatList = (ArrayList<ItemsList.Items>) getArguments().getSerializable("itemArraylist");
//        bannersArrayList = (ArrayList<ItemsList.Banners>) getArguments().getSerializable("bannersArraylist");

        mainCatList = SplashActivity.itemsArrayList;
        bannersArrayList = SplashActivity.bannersArrayList;

        myDbHelper = new DataBaseHelper(getActivity());
        pastries_txt = (TextView) rootView.findViewById(R.id.pastries_txt);
        cake_txt = (TextView) rootView.findViewById(R.id.cake_txt);
        sandwich_txt = (TextView) rootView.findViewById(R.id.sandwich_txt);
        bread_txt = (TextView) rootView.findViewById(R.id.bread_txt);
        macaron_txt = (TextView) rootView.findViewById(R.id.macaron_txt);
        offer_txt = (TextView) rootView.findViewById(R.id.offers_txt);
        special_cake_txt = (TextView) rootView.findViewById(R.id.special_cake_txt);
        chef_teasting_txt = (TextView) rootView.findViewById(R.id.chef_teasting_txt);
        catring_txt = (TextView) rootView.findViewById(R.id.catering_txt);

        partries_img = (ImageView) rootView.findViewById(R.id.pastries_img);
        cake_img = (ImageView) rootView.findViewById(R.id.cake_img);
        sandwich_img = (ImageView) rootView.findViewById(R.id.sandwich_img);
        bread_img = (ImageView) rootView.findViewById(R.id.bread_img);
        macaron_img = (ImageView) rootView.findViewById(R.id.macaron_img);
        offer_img = (ImageView) rootView.findViewById(R.id.offer_img);
        special_cake_img = (ImageView) rootView.findViewById(R.id.special_cakes_img);
        chef_teasting_img = (ImageView) rootView.findViewById(R.id.chef_teasting_img);
        catring_img = (ImageView) rootView.findViewById(R.id.catering_img);

        bag = (ImageView) rootView.findViewById(R.id.bag);
        bag_count = (TextView) rootView.findViewById(R.id.bag_count);

        back_btn = (RelativeLayout) rootView.findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).menuClick();

            }
        });

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderConfirmationNew.class);
                startActivity(a);

            }
        });

//        checkOut.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (myDbHelper.getTotalOrderQty() == 0) {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("There is no items in your order? To proceed checkout please add the items")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
//                    Intent checkoutIntent = new Intent(getActivity(), CheckoutActivity.class);
//                    startActivity(checkoutIntent);
//                }
//            }
//        });

//        new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL);

        pastries_txt.setOnClickListener(this);
        cake_txt.setOnClickListener(this);
        sandwich_txt.setOnClickListener(this);
        bread_txt.setOnClickListener(this);
        macaron_txt.setOnClickListener(this);
        offer_txt.setOnClickListener(this);
        special_cake_txt.setOnClickListener(this);
        chef_teasting_txt.setOnClickListener(this);
        catring_txt.setOnClickListener(this);

        partries_img.setOnClickListener(this);
        cake_img.setOnClickListener(this);
        sandwich_img.setOnClickListener(this);
        bread_img.setOnClickListener(this);
        macaron_img.setOnClickListener(this);
        offer_img.setOnClickListener(this);
        special_cake_img.setOnClickListener(this);
        chef_teasting_img.setOnClickListener(this);
        catring_img.setOnClickListener(this);

        double number;
        number = myDbHelper.getTotalOrderPrice();
        DecimalFormat decim = new DecimalFormat("0.00");
//        if (myDbHelper.getTotalOrderQty() == 0) {
//            orderPrice.setText("-");
//            orderQuantity.setText("-");
//        } else {
//            orderPrice.setText("" + decim.format(number));
//            orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//        }
//    }


        return rootView;

    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                getActivity().onBackPressed();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pastries_txt:
                Intent pastryIntent = new Intent(getActivity(), CategoriesListActivity.class);
                pastryIntent.putExtra("catId", 1);
                pastryIntent.putExtra("mainList", mainCatList);
//                pastryIntent.putExtra("subcatList", mainCatList.get(0).getSubCategories());
                Log.e("TAG", "size " + mainCatList.size());
                startActivity(pastryIntent);
                break;
            case R.id.pastries_img:
                Intent pastryIntent1 = new Intent(getActivity(), CategoriesListActivity.class);
                pastryIntent1.putExtra("catId", 1);
                pastryIntent1.putExtra("mainList", mainCatList);
//                pastryIntent.putExtra("subcatList", mainCatList.get(0).getSubCategories());
                Log.e("TAG", "size " + mainCatList.size());
                startActivity(pastryIntent1);
                break;
            case R.id.cake_img:
                Intent cakeIntent = new Intent(getActivity(), CategoriesListActivity.class);
                cakeIntent.putExtra("catId", 2);
                cakeIntent.putExtra("mainList", mainCatList);
                startActivity(cakeIntent);
                break;
            case R.id.cake_txt:
                Intent cakeIntent1 = new Intent(getActivity(), CategoriesListActivity.class);
                cakeIntent1.putExtra("catId", 2);
                cakeIntent1.putExtra("mainList", mainCatList);
                startActivity(cakeIntent1);
                break;
            case R.id.bread_img:
                Intent breadIntent = new Intent(getActivity(), CategoriesListActivity.class);
                breadIntent.putExtra("catId", 6);
                breadIntent.putExtra("mainList", mainCatList);
                startActivity(breadIntent);
                break;
            case R.id.bread_txt:
                Intent breadIntent1 = new Intent(getActivity(), CategoriesListActivity.class);
                breadIntent1.putExtra("catId", 6);
                breadIntent1.putExtra("mainList", mainCatList);
                startActivity(breadIntent1);
                break;
            case R.id.sandwich_img:
                Intent sandwichIntent = new Intent(getActivity(), CategoriesListActivity.class);
                sandwichIntent.putExtra("catId", 3);
                sandwichIntent.putExtra("mainList", mainCatList);
                startActivity(sandwichIntent);
                break;
            case R.id.sandwich_txt:
                Intent sandwichIntent1 = new Intent(getActivity(), CategoriesListActivity.class);
                sandwichIntent1.putExtra("catId", 3);
                sandwichIntent1.putExtra("mainList", mainCatList);
                startActivity(sandwichIntent1);
                break;

            case R.id.offers_txt:
                Intent saladsIntent = new Intent(getActivity(), CategoriesListActivity.class);
                saladsIntent.putExtra("catId", 14);
                saladsIntent.putExtra("mainList", mainCatList);
                startActivity(saladsIntent);
                break;
            case R.id.offer_img:
                Intent saladsIntent1 = new Intent(getActivity(), CategoriesListActivity.class);
                saladsIntent1.putExtra("catId", 14);
                saladsIntent1.putExtra("mainList", mainCatList);
                startActivity(saladsIntent1);
                break;
            case R.id.macaron_img:
                Intent macaronsIntent = new Intent(getActivity(), CategoriesListActivity.class);
                macaronsIntent.putExtra("catId", 4);
                macaronsIntent.putExtra("mainList", mainCatList);
                startActivity(macaronsIntent);
                break;
            case R.id.macaron_txt:
                Intent macaronsIntent1 = new Intent(getActivity(), CategoriesListActivity.class);
                macaronsIntent1.putExtra("catId", 4);
                macaronsIntent1.putExtra("mainList", mainCatList);
                startActivity(macaronsIntent1);
                break;
            case R.id.chef_teasting_img:
                Intent hotmealsIntent = new Intent(getActivity(), CategoriesListActivity.class);
                hotmealsIntent.putExtra("catId", 13);
                hotmealsIntent.putExtra("mainList", mainCatList);
                startActivity(hotmealsIntent);
                break;
            case R.id.chef_teasting_txt:
                Intent hotmealsIntent1 = new Intent(getActivity(), CategoriesListActivity.class);
                hotmealsIntent1.putExtra("catId", 13);
                hotmealsIntent1.putExtra("mainList", mainCatList);
                startActivity(hotmealsIntent1);
                break;
            case R.id.catering_img:
                Intent a = new Intent(getActivity(), ImageLayout.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("title", "Catering");
                } else {
                    a.putExtra("title", "التموين وخدمات الطعام");
                }
                startActivity(a);
                break;
            case R.id.catering_txt:
                Intent b = new Intent(getActivity(), ImageLayout.class);
                if (language.equalsIgnoreCase("En")) {
                    b.putExtra("title", "Catering");
                } else {
                    b.putExtra("title", "التموين وخدمات الطعام");
                }
                startActivity(b);
                break;
            case R.id.special_cakes_img:
                Intent c = new Intent(getActivity(), SpecialCakeActivity.class);
                startActivity(c);
                break;
            case R.id.special_cake_txt:
                Intent intent = new Intent(getActivity(), SpecialCakeActivity.class);
                startActivity(intent);
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

//        DecimalFormat decimalFormat = new DecimalFormat("0.00");
//        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//        if (myDbHelper.getTotalOrderQty() == 0) {
//            orderPrice.setText("-");
//            orderQuantity.setText("-");
//        } else {
//            orderPrice.setText("" + decimalFormat.format(myDbHelper.getTotalOrderPrice()));
//            orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//        }
//
//
//        mcount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (myDbHelper.getTotalOrderQty() == 0) {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("There is no items in your order? To proceed checkout please add the items")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
////                     set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
////                     set dialog message
//                        alertDialogBuilder
//                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
//
////
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
////
////                    // show it
//                    alertDialog.show();
//                } else {
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
//                                .setCancelable(false)
//                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        Intent a = new Intent(getActivity(), CheckoutActivity.class);
//                                        startActivity(a);
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        myDbHelper.deleteOrderTable();
////                                    CheckoutActivity.orderQuantity.setText("0");
////                                    CheckoutActivity.orderPrice.setText("0SR");
////                                    CheckoutActivity.mcount_basket.setText("0");
////                                        CategoriesListActivity.orderQuantity.setText("0");
////                                        CategoriesListActivity.orderPrice.setText("0.00SR");
////                                        CategoriesListActivity.mcount_basket.setText("0");
//                                        OrderFragment.orderQuantity.setText("-");
//                                        OrderFragment.orderPrice.setText("-");
//                                        OrderFragment.mcount_basket.setText("0");
//                                        setQty();
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
//                                .setCancelable(false)
//                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        myDbHelper.deleteOrderTable();
////                                    CheckoutActivity.orderQuantity.setText("0");
////                                    CheckoutActivity.orderPrice.setText("0SR");
////                                    CheckoutActivity.mcount_basket.setText("0");
////                                        CategoriesListActivity.orderQuantity.setText("0");
////                                        CategoriesListActivity.orderPrice.setText("0.00SR");
////                                        CategoriesListActivity.mcount_basket.setText("0");
//                                        OrderFragment.orderQuantity.setText("0");
//                                        OrderFragment.orderPrice.setText("-");
//                                        OrderFragment.mcount_basket.setText("-");
//                                        setQty();
//                                        dialog.dismiss();
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        Intent a = new Intent(getActivity(), CheckoutActivity.class);
//                                        startActivity(a);
//                                    }
//                                });
//                    }
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }
//
//            }
//        });
//
//        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//
//        setQty();
    }

//    @Override
//    public void onBackPressed() {
//        if (myDbHelper.getTotalOrderQty() > 0) {
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));
//            String title = "", msg = "", posBtn = "", negBtn = "";
////            if(language.equalsIgnoreCase("En")){
////                posBtn = "Yes";
////                negBtn = "No";
////                title = "dr.CAFE";
//            msg = "You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear. Do you want to continue?";
////            }else if(language.equalsIgnoreCase("Ar")){
////                posBtn = "نعم";
////                negBtn = "لا";
////                title = "د. كيف";
////                msg = "لديك " + myDbHelper.getTotalOrderQty()+ "  منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات . هل تود الاستمرار";
////            }
//            // set title
//            alertDialogBuilder.setTitle(title);
//
//            // set dialog message
//            alertDialogBuilder
//                    .setMessage(msg)
//                    .setCancelable(false)
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.dismiss();
//                            myDbHelper.deleteOrderTable();
//                            onBackPressed();
//                        }
//                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//
//            // create alert dialog
//            AlertDialog alertDialog = alertDialogBuilder.create();
//
//            // show it
//            alertDialog.show();
//
//
//        } else {
//            super.onBackPressed();
//        }
//    }
}
