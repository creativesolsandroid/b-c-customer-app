package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CakeInnerActivity1;
import com.cs.bakeryco.model.CakeDetailslist;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Section;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.PastriesSelection.catId;

/**
 * Created by CS on 3/7/2018.
 */

public class CakeListAdapter extends BaseAdapter {

    Context context;
    public LayoutInflater inflater;
    public static ArrayList<Section> orderList = new ArrayList<>();
    ArrayList<Order> orderDB = new ArrayList<>();
    String language;
    public static Activity parentActivity;
    private static DataBaseHelper myDbHelper;
    public static int value;
    ArrayList<CakeDetailslist> value1 = new ArrayList<>();
    public static int Qty = 0;
    public static float orderPrice = 0;
    String itemid;
    int size;
    public static int pos;
    public static int count;
    public static String itemname;


    public CakeListAdapter(Context context, ArrayList<Section> groups, int value, ArrayList<CakeDetailslist> value1, String language,
                           Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.value = value;
        this.value1 = value1;
        myDbHelper = new DataBaseHelper(context);
    }

    @Override
    public int getCount() {
        return orderList.get(value).getChildItems().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static class ViewHolder {
        TextView caketitle, itemQty, price;
        ImageView itemImage, minus, plus;
        LinearLayout grid_layout;
        View sideLine, topline;
        RelativeLayout mcakelayout;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup viewGroup) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.cake_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.caketitle = (TextView) convertView.findViewById(R.id.cake_txt);
            holder.itemQty = (TextView) convertView.findViewById(R.id.order_quantity);
//            holder.itemQty = (TextView) convertView.findViewById(R.id.donut_qty);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.cake_img);
//            holder.minus = (ImageView) convertView.findViewById(R.id.donut_minus);
//            holder.plus = (ImageView) convertView.findViewById(R.id.donut_plus);
//            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);
//            holder.price = (TextView) convertView.findViewById(R.id.item_price);
            holder.mcakelayout = (RelativeLayout) convertView.findViewById(R.id.cakelayout);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        size = orderList.get(value).getChildItems().size();

//        if (size == 1 || size == 2 || size == 3){
//
//            holder.topline.setVisibility(View.VISIBLE);
//        }else {
//
//            holder.topline.setVisibility(View.GONE);
//
//        }

//        pos = size/2;
//
//        if(position == (pos-1) || (position+1) == size){
//            holder.sideLine.setVisibility(View.INVISIBLE);
//        }
//        else{
//            holder.sideLine.setVisibility(View.VISIBLE);
//        }

//        holder.sideLine.setVisibility(View.VISIBLE);
        if (language.equalsIgnoreCase("En")) {
            holder.caketitle.setText(orderList.get(value).getChildItems().get(position).getItemName());
        }else   if (language.equalsIgnoreCase("Ar")) {
            holder.caketitle.setText(orderList.get(value).getChildItems().get(position).getItemNameAr());
        }

        Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(position).getImage()).into(holder.itemImage);

        Log.i("TAG","size "+value1.size());

        if (myDbHelper.getItemOrderCount(orderList.get(value).getChildItems().get(position).getItemId())==0){
            holder.itemQty.setVisibility(View.GONE);
            notifyDataSetChanged();
        }else {
            holder.itemQty.setVisibility(View.VISIBLE);
            holder.itemQty.setText(""+myDbHelper.getItemOrderCount(orderList.get(value).getChildItems().get(position).getItemId()));
            notifyDataSetChanged();
        }

        Log.i("TAG","value "+value1.get(0).getCakedesc());

        Log.i("TAG","catid" + catId);
        if (language.equalsIgnoreCase("Ar")){
            holder.mcakelayout.setRotationY(180);
        }
        holder.mcakelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0;i<value1.size();i++) {
                    Intent a = new Intent(context, CakeInnerActivity1.class);
//                a.putExtra("subitemlist", orderList.get(value).getChildItems().get(value1).getSubItems());
                    a.putExtra("catid", catId);
                    a.putExtra("subcatid", orderList.get(value).getChildItems().get(position).getSubCatId());
                    a.putExtra("itemid", orderList.get(value).getChildItems().get(position).getItemId());
                    a.putExtra("ItemTypeIds",orderList.get(value).getChildItems().get(position).getItemtypeIds());
                    a.putExtra("pos", i);
                    a.putExtra("pricelist", orderList.get(value).getChildItems().get(position).getPriceList());
                    a.putExtra("Image", orderList.get(value).getChildItems().get(position).getImage());
                    a.putExtra("ItemName", orderList.get(value).getChildItems().get(position).getItemName());
                    a.putExtra("ItemName_Ar", orderList.get(value).getChildItems().get(position).getItemNameAr());
                    a.putExtra("Desc", orderList.get(value).getChildItems().get(position).getDescription());
                    a.putExtra("Desc_Ar", orderList.get(value).getChildItems().get(position).getDescriptionAr());
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(a);
                }
            }
        });

        return convertView;
    }
}
