package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.activities.ItemsBoxActivity;
import com.cs.bakeryco.activities.ItemsIndiviualActivity;
import com.cs.bakeryco.model.ItemsList;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.ItemsActivity.indivialcount;
import static com.cs.bakeryco.activities.ItemsActivity.itemsListAdapter;
import static com.cs.bakeryco.activities.ItemsActivity.items_list;
import static com.cs.bakeryco.activities.ItemsActivity.selection_pos;

public class SpecialOffersAdapter extends RecyclerView.Adapter<SpecialOffersAdapter.MyViewHolder> {

    Context context;
    ArrayList<ItemsList.SubCategoryItems> sections = new ArrayList<>();
    ArrayList<ItemsList.Items> itemsArrayList = new ArrayList<>();

    Activity activity;
    LayoutInflater inflater;
    String userid, language, title;
    boolean select = false;

    public SpecialOffersAdapter(Context context, ArrayList<ItemsList.SubCategoryItems> sections, ArrayList<ItemsList.Items> itemsArrayList, String language) {
        this.context = context;
        this.sections = sections;
        this.itemsArrayList = itemsArrayList;
        this.language = language;
        this.title = title;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_special_offer_items, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_special_offer_items_arabic, parent, false);
        }

        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if (language.equalsIgnoreCase("Ar")) {

            holder.layout.setRotation(180);

        }


        if (language.equalsIgnoreCase("En")) {
            holder.name.setText("" + sections.get(0).getSections().get(0).getItems().get(position).getItemName());
            holder.desc.setText("" + sections.get(0).getSections().get(0).getItems().get(position).getItemDesc());
        } else {
            holder.name.setText("" + sections.get(0).getSections().get(0).getItems().get(position).getItemName_Ar());
            holder.desc.setText("" + sections.get(0).getSections().get(0).getItems().get(position).getItemDesc_Ar());
        }
        double price = Double.parseDouble(sections.get(0).getSections().get(0).getItems().get(position).getPrice().get(0).getPrice());
        holder.price.setText("" + Constants.decimalFormat.format(price) + " SR");
        Glide.with(context).load(Constants.IMAGE_URL + sections.get(0).getSections().get(0).getItems().get(position).getItemImage()).into(holder.img);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sections.get(0).getSections().get(0).getSectionId() == 1 || sections.get(0).getSections().get(0).getSectionId() == 5 || sections.get(0).getSections().get(0).getSectionId() == 6) {
                    Intent a = new Intent(context, ItemsIndiviualActivity.class);
                    a.putExtra("desc", sections.get(0).getSections().get(0).getItems().get(position).getItemDesc());
                    a.putExtra("itemname", sections.get(0).getSections().get(0).getItems().get(position).getItemName());
                    a.putExtra("desc_ar", sections.get(0).getSections().get(0).getItems().get(position).getItemDesc_Ar());
                    a.putExtra("itemname_ar", sections.get(0).getSections().get(0).getItems().get(position).getItemName_Ar());
                    a.putExtra("section", sections.get(0).getSections());
                    a.putExtra("value", 0);
                    a.putExtra("pos", position);
                    a.putExtra("cat_name", sections.get(0).getSubCategoryName());
                    a.putExtra("list", sections.get(0).getSections());
                    a.putExtra("catId", sections.get(0).getMainCategoryId());
                    a.putExtra("main_list", itemsArrayList);
                    a.putExtra("cake_type_id", sections.get(0).getSections().get(0).getItems().get(position).getItemTypeIds());
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(a);
                } else if (sections.get(0).getSections().get(0).getSectionId() == 2 || sections.get(0).getSections().get(0).getSectionId() == 3 || sections.get(0).getSections().get(0).getSectionId() == 4) {

                    Intent a = new Intent(context, ItemsBoxActivity.class);
                    a.putExtra("SubItems", sections.get(0).getSections().get(0).getItems().get(position).getSubItems());
                    a.putExtra("pricelist", sections.get(0).getSections().get(0).getItems().get(position).getPrice());
                    if (language.equalsIgnoreCase("En")) {
                        a.putExtra("title_name", sections.get(0).getSections().get(0).getItems().get(position).getItemName());
                    } else {
                        a.putExtra("title_name", sections.get(0).getSections().get(0).getItems().get(position).getItemName_Ar());
                    }
                    a.putExtra("no_of_items", sections.get(0).getSections().get(0).getItems().get(position).getNoOfItems());
                    a.putExtra("section_list", sections.get(0).getSections());
                    a.putExtra("section_pos", 0);
                    a.putExtra("child_pos", position);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(a);

                }

            }
        });

    }

    @Override
    public int getItemCount() {

        return sections.get(0).getSections().get(0).getItems().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, desc, price;
        LinearLayout layout;
        ImageView img;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            desc = (TextView) itemView.findViewById(R.id.item_description);
            price = (TextView) itemView.findViewById(R.id.price);
            img = (ImageView) itemView.findViewById(R.id.img);

        }

    }

}
