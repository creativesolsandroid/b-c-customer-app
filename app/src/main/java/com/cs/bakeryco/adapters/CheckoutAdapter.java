package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CheckoutActivity;
import com.cs.bakeryco.activities.CommentsActivity;
import com.cs.bakeryco.model.Order;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 15-06-2016.
 */
public class CheckoutAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty;
    Float price;
    Float priceTxt = Float.valueOf(0);
    String language;
    float vat = 5;

    public CheckoutAdapter(Context context, ArrayList<Order> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title, price, totalPrice, qty1, itemDesc;
        ImageView itemType, plusBtn, minusBtn, itemComment, itemIcon;
        //ImageButton plus;
//        LinearLayout additionalsLayout;
        TextView itemtype, itemsize;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.checkout_row, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
            }


            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.price = (TextView) convertView.findViewById(R.id.item_price);
//            holder.qty = (TextView) convertView.findViewById(R.id.item_qty);
            holder.qty1 = (TextView) convertView.findViewById(R.id.item_qty1);
            holder.plusBtn = (ImageView) convertView.findViewById(R.id.plus_btn);
            holder.minusBtn = (ImageView) convertView.findViewById(R.id.minus_btn);
            holder.totalPrice = (TextView) convertView.findViewById(R.id.total_price);

            holder.itemComment = (ImageView) convertView.findViewById(R.id.edit_comment);
//            holder.itemType = (ImageView) convertView.findViewById(R.id.item_type);
            holder.itemDesc = (TextView) convertView.findViewById(R.id.item_desc);
//            holder.additionalsLayout = (LinearLayout) convertView.findViewById(R.id.additionals_layout);
            holder.itemIcon = (ImageView) convertView.findViewById(R.id.item_image);
            holder.itemsize = convertView.findViewById(R.id.item_size);
            holder.itemtype = convertView.findViewById(R.id.item_type);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if(orderList.get(position).getAdditionalName().equals("")){
//            holder.additionalsName.setVisibility(View.GONE);
//        }else {
//            holder.additionalsName.setVisibility(View.VISIBLE);
//            holder.additionalsName.setText(orderList.get(position).getAdditionalName());
//        }


//        if(language.equalsIgnoreCase("En")){
//            holder.itemDesc.setText(orderList.get(position).getDescription());

        holder.itemsize.setVisibility(View.GONE);
        holder.itemtype.setVisibility(View.GONE);

        if (orderList.get(position).getCategoryId().equals("2")) {

            if (orderList.get(position).getSizename().equals("7") || orderList.get(position).getSizename().equals("6") || orderList.get(position).getSizename().equals("7") && orderList.get(position).getSizename().equals("6")) {
                holder.itemsize.setVisibility(View.VISIBLE);
                holder.itemtype.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    holder.itemsize.setText(orderList.get(position).getItemTypeId());
                    holder.itemtype.setText(orderList.get(position).getItemType());
                } else if (language.equalsIgnoreCase("Ar")) {
                    holder.itemsize.setText(orderList.get(position).getItemTypeName_ar());
                    if (orderList.get(position).getItemType().contains("Whole")) {
                        holder.itemtype.setText("قالب");
                    } else if (orderList.get(position).getItemType().contains("Sliced")) {
                        holder.itemtype.setText("شرائح");
                    }
                }
            }
        } else {
            holder.itemsize.setVisibility(View.GONE);
            holder.itemtype.setVisibility(View.GONE);
        }

        if (language.equalsIgnoreCase("En")) {
//            if (orderList.get(position).getCategoryId().equals("2")) {
//                if (orderList.get(position).getItemTypeId().equals("1")) {
//                    holder.title.setText(orderList.get(position).getItemName() + "\n 1 piece");
//                } else if (orderList.get(position).getItemTypeId().equals("2")) {
//                    holder.title.setText(orderList.get(position).getItemName() + "\n Sliced Whole Cake - 8 Piece");
//                }
//            } else {
            holder.title.setText(orderList.get(position).getItemName());
//            }
        } else if (language.equalsIgnoreCase("Ar")) {
            Log.e("TAG", "" + orderList.get(position).getItemNameAr());
//            if (orderList.get(position).getCategoryId().equals("2")) {
//                if (orderList.get(position).getItemTypeId().equals("1")) {
//                    holder.title.setText(orderList.get(position).getItemNameAr() + "\n 1 piece");
//                } else if (orderList.get(position).getItemTypeId().equals("2")) {
//                    holder.title.setText(orderList.get(position).getItemNameAr() + "\n كيكة 8 قطع");
//                }
//            } else {
            holder.title.setText(orderList.get(position).getItemNameAr());
        }
//        }


//        }else if(language.equalsIgnoreCase("Ar")){
////            holder.itemDesc.setText(orderList.get(position).getDescriptionAr());
//            holder.title.setText(orderList.get(position).getItemNameAr());
//        }

        Log.e("TAG", "item name " + orderList.get(position).getItemName());
        Log.e("TAG", "qty " + orderList.get(position).getQty());
//        holder.qty.setText(orderList.get(position).getQty());

        Glide.with(context).load("http://csadms.com/bncservices/images/" + orderList.get(position).getImage()).into(holder.itemIcon);
        Log.e("TAG", "" + orderList.get(position).getImage());
        holder.qty1.setText(orderList.get(position).getQty());
//        float price1;
//        price1 = Float.parseFloat(orderList.get(position).getPrice());
//        NumberFormat nf=new DecimalFormat("####.####");
//        String price3;
////        price3= String.valueOf(price1);
//        int price4;
////        price4= price1;
//        float priceTxt = Float.parseFloat(orderList.get(position).getPrice())* Integer.parseInt(orderList.get(position).getQty());
//        holder.price.setText(""+nf.format(price1));
//        holder.totalPrice.setText(""+nf.format(priceTxt));

        double number, number1;
        priceTxt = Float.parseFloat(orderList.get(position).getPrice()) * Integer.parseInt(orderList.get(position).getQty());
        number = priceTxt;
        number1 = Double.parseDouble(orderList.get(position).getPrice());
        DecimalFormat decim = new DecimalFormat("0.00");
        holder.price.setText("" + decim.format(number1));
        holder.totalPrice.setText("" + decim.format(number));
//        Glide.with(context).load("http://www.ircfood.com/images/"+orderList.get(position).getItemImage()).placeholder(R.drawable.app_logo).into(holder.itemIcon);

		/*ArrayList<HashMap<String, String>> List = SchoolAlertMain.DBcontroller.getAllAlerts();
        //System.out.println("adapter"+alertsArrayList.get(position).getTime());
		if(List.contains(alertsArrayList.get(position).getAlertId())){
			holder.alertList.setBackgroundColor(Color.GRAY);
		}*/


        holder.plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (orderList.get(position).getCategoryId().equals("4")) {
//                    qty = Integer.parseInt(orderList.get(position).getQty()) + 5;
//                    double number;
//                    price = (Float.parseFloat(orderList.get(position).getPrice()) * 5) + (Float.parseFloat(orderList.get(position).getTotalAmount()));
//                    number = price;
//                    DecimalFormat decim = new DecimalFormat("0.00");
//                    decim.format(number);
//                } else {
                qty = Integer.parseInt(orderList.get(position).getQty()) + 1;
                price = Float.parseFloat(orderList.get(position).getPrice()) + (Float.parseFloat(orderList.get(position).getTotalAmount()));
//                }
                myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getItemId());
                orderList = myDbHelper.getOrderInfo();
                notifyDataSetChanged();
                try {
                    double number;
                    number = myDbHelper.getTotalOrderPrice();
                    DecimalFormat decim = new DecimalFormat("0.00");
//                    OrderFragment.orderPrice.setText("" + decim.format(number));
//                    OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    double number1;
                    number1 = myDbHelper.getTotalOrderPrice();
                    DecimalFormat decim1 = new DecimalFormat("0.00");
//                    CategoriesListActivity.orderPrice.setText("" + decim1.format(number1));
//                    CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                double number;
                number = myDbHelper.getTotalOrderPrice();
                DecimalFormat decim = new DecimalFormat("0.00");
                CheckoutActivity.amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                CheckoutActivity.vatAmount.setText("" + decim.format(tax));
                CheckoutActivity.netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                Log.i("TAG", "orderprice2 " + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                CheckoutActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

            }
        });

        holder.minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (orderList.get(position).getCategoryId().equals("4")) {
//                    qty = Integer.parseInt(orderList.get(position).getQty()) - 5;
//                } else {
                qty = Integer.parseInt(orderList.get(position).getQty()) - 1;
//                }
                if (qty == 0) {
                    myDbHelper.deleteItemFromOrder(orderList.get(position).getItemId());


//                } else {
//                    if (orderList.get(position).getCategoryId().equals("4")) {
//                        price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - (Float.parseFloat(orderList.get(position).getPrice()) * 5);
                } else {
                    price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - Float.parseFloat(orderList.get(position).getPrice());

//                    }
                    myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getItemId());
                }


                orderList = myDbHelper.getOrderInfo();

                try {
                    double number;
                    number = myDbHelper.getTotalOrderPrice();
                    DecimalFormat decim = new DecimalFormat("0.00");
//                    OrderFragment.orderPrice.setText("" + decim.format(number));
//                    OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    double number1;
                    number1 = myDbHelper.getTotalOrderPrice();
                    DecimalFormat decim1 = new DecimalFormat("0.00");
//                    CategoriesListActivity.orderPrice.setText("" + decim1.format(number));
//                    CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                double number;
                number = myDbHelper.getTotalOrderPrice();
                DecimalFormat decim = new DecimalFormat("0.00");
                CheckoutActivity.amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                CheckoutActivity.vatAmount.setText("" + decim.format(tax));
                CheckoutActivity.netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                CheckoutActivity.orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                Log.i("TAG", "orderprice3 " + decim.format(myDbHelper.getTotalOrderPrice() + tax));
                CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                CheckoutActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                notifyDataSetChanged();

                if (orderList.size() == 0) {
//                    Intent a = new Intent(context, Menu.class);
//                    context.startActivity(a);
                    ((Activity) context).finish();
                }
                notifyDataSetChanged();
            }
        });

        holder.itemComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderList = myDbHelper.getOrderInfo();
                Intent intent = new Intent(context, CommentsActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    intent.putExtra("title", orderList.get(position).getItemName());
                } else if (language.equalsIgnoreCase("Ar")) {
                    intent.putExtra("title", orderList.get(position).getItemNameAr());
                }

                intent.putExtra("itemImage", orderList.get(position).getImage());
                intent.putExtra("itemId", orderList.get(position).getItemId());
                intent.putExtra("orderId", orderList.get(position).getOrderId());
                intent.putExtra("comment", orderList.get(position).getComment());
                intent.putExtra("size", orderList.get(position).getSizename());
                intent.putExtra("screen", "food");
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}