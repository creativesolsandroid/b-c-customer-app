package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.InfoPopup;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Section;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.bakeryco.activities.PastriesSelection.catId;
import static com.cs.bakeryco.activities.PastriesSelection.counts;
import static com.cs.bakeryco.activities.PastriesSelection.finalcount;
import static com.cs.bakeryco.activities.PastriesSelection.itemQty;
import static com.cs.bakeryco.activities.PastriesSelection.itemfinalQty;
import static com.cs.bakeryco.activities.PastriesSelection.itemfinalprice;
import static com.cs.bakeryco.activities.PastriesSelection.main_list;
import static com.cs.bakeryco.activities.PastriesSelection.position1;
import static com.cs.bakeryco.activities.PastriesSelection.positions;
import static com.cs.bakeryco.activities.PastriesSelection.sections;
import static com.cs.bakeryco.activities.PastriesSelection.selectedCountIndividual;

public class SectionRecyclerViewAdapter extends RecyclerView.Adapter<SectionRecyclerViewAdapter.ViewHolder> {

    public static Context context;
    public LayoutInflater inflater;
    public static ArrayList<Section> orderList = new ArrayList<>();
    ArrayList<Order> orderDB = new ArrayList<>();
    String language;
    public static Activity parentActivity;
    private static DataBaseHelper myDbHelper;
    public static int value, value1 = 0;
    public static int Qty = 0;
    int qty = 0;
    int POP_UP_INFO = 1;
    public static float orderPrice = 0;
    String itemid, title;
    public static int pos;
    public static int count;
    public static String itemname;
    private LayoutInflater mInflater;
    String enteredString;

    public SectionRecyclerViewAdapter(Context context, String title, ArrayList<Section> groups, int value, String language,
                                      Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.parentActivity = parentActivity;
        this.mInflater = LayoutInflater.from(context);
        this.language = language;
        this.value = value;
        this.title = title;
        myDbHelper = new DataBaseHelper(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (language.equalsIgnoreCase("En")) {
            view = mInflater.inflate(R.layout.pastries_selection_list, parent, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            view = mInflater.inflate(R.layout.pastries_selection_list_arabic, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.price.setVisibility(View.VISIBLE);

        if (finalcount){
            holder.mitemQty.setText("" + PastriesSelection.counts.get(position));
            qty = PastriesSelection.counts.get(position);
            holder.mitemQty.setSelection(holder.mitemQty.length());
        }else {
            holder.mitemQty.setText("" + PastriesSelection.selectedCountIndividual.get(position));
            qty = PastriesSelection.selectedCountIndividual.get(position);
            holder.mitemQty.setSelection(holder.mitemQty.length());
        }

        PastriesSelection.section = true;

        Log.i("TAG", "onBindViewHolder: " + qty);

        if (language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(value).getChildItems().get(position).getItemName());
            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(position).getImage()).into(holder.itemImage);
            holder.price.setText(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice() + " SR");
        } else if (language.equalsIgnoreCase("Ar")) {
            holder.title.setText(orderList.get(value).getChildItems().get(position).getItemNameAr());
            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(position).getImage()).into(holder.itemImage);
            holder.price.setText(" ريال" + orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());
        }


        holder.info_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(context, InfoPopup.class);
                a.putExtra("desc", orderList.get(value).getChildItems().get(position).getDescription());
                a.putExtra("itemname", orderList.get(value).getChildItems().get(position).getItemName());
                a.putExtra("desc_ar", orderList.get(value).getChildItems().get(position).getDescriptionAr());
                a.putExtra("itemname_ar", orderList.get(value).getChildItems().get(position).getItemNameAr());
                a.putExtra("section", orderList);
                a.putExtra("value", value);
                a.putExtra("value1", value1);
                a.putExtra("pos", position);
                a.putExtra("cat_name", title);
                a.putExtra("info", "indivial");
                if (finalcount){
                    a.putExtra("qty", counts.get(position));
                }else {
                    a.putExtra("qty", selectedCountIndividual.get(position));
                }
                a.putExtra("list", sections);
                a.putExtra("postion1", position1);
                a.putExtra("catId", catId);
                a.putExtra("main_list", main_list);
                Log.i("TAG", "list " + sections);
                Log.i("TAG", "postion1 " + position1);
                Log.i("TAG", "catId " + catId);
                Log.i("TAG", "main_list " + main_list);
                Log.i("TAG", "cat_name " + title);

                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(a);
            }
        });
//        notifyDataSetChanged();

        holder.mitemQty.setCursorVisible(false);
        holder.mitemQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mitemQty.setCursorVisible(true);
            }
        });

//        holder.plus.setClickable(true);
//        holder.plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                holder.plus.setClickable(true);
//                int qty = Integer.parseInt(mitemQty.getText().toString());
//                if (qty == 9999){
//
//                }
//
//            }
//        });
//        holder.plus.setClickable(true);
    }

//    @Override
//    public boolean onActivityResult(int requestCode,
//                                    int resultCode, Intent data) {
//
//    if (requestCode == POP_UP_INFO
//                && resultCode == Activity.RESULT_OK) {
//       notifyDataSetChanged();
//        Log.i("TAG1", "onActivityResult: ");
//    }
//
//        return true;
//    }

    @Override
    public int getItemCount() {
//        Log.i("TAG", "size " + orderList.get(value).getChildItems().size());
        return orderList.get(value).getChildItems().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextWatcher, View.OnClickListener {
        TextView title, price;
        ImageView itemImage, minus, info_image;
        LinearLayout grid_layout;
        EditText mitemQty;
        ImageView plus;

        ViewHolder(View convertView) {
            super(convertView);
            title = (TextView) convertView.findViewById(R.id.item_name);
            mitemQty = (EditText) convertView.findViewById(R.id.donut_qty);
            itemImage = (ImageView) convertView.findViewById(R.id.donut_image);
            minus = (ImageView) convertView.findViewById(R.id.donut_minus);
            plus = (ImageView) convertView.findViewById(R.id.donut_plus);
            info_image = (ImageView) convertView.findViewById(R.id.item_image);
            grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);
            price = (TextView) convertView.findViewById(R.id.item_price);

            int maxLength = 4;
            mitemQty.setFilters(new InputFilter[]{
                    new InputFilter.LengthFilter(maxLength)
            });

            mitemQty.addTextChangedListener(this);
//            orderPrice = 0;
//            Qty = 0;
            plus.setOnClickListener(this);
            minus.setOnClickListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            Log.i("TAG", "afterTextChanged: ");
            if (mitemQty.getText().toString().length() > 0) {

                enteredString = editable.toString();
                if (enteredString.startsWith("0")) {
                    if (enteredString.length() > 1) {
                        mitemQty.setText(enteredString.substring(1));
                        mitemQty.setSelection(mitemQty.length());
                    }
                }

                qty = Integer.parseInt(mitemQty.getText().toString());
                PastriesSelection.selectedCountIndividual.set(this.getAdapterPosition(), Integer.parseInt(mitemQty.getText().toString()));
                PastriesSelection.counts.set(this.getAdapterPosition(), Integer.parseInt(mitemQty.getText().toString()));
                PastriesSelection.indivialcount.set(this.getAdapterPosition(), Integer.parseInt(mitemQty.getText().toString()));
                AddItems(this.getAdapterPosition(), Integer.parseInt(mitemQty.getText().toString()));
            } else {
//                    mmitemQty.setText("0");
                PastriesSelection.selectedCountIndividual.set(this.getAdapterPosition(), 0);
                PastriesSelection.counts.set(this.getAdapterPosition(), 0);
                PastriesSelection.indivialcount.set(this.getAdapterPosition(), 0 );
                AddItems(this.getAdapterPosition(), 0);
            }
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.donut_plus:
//                    plus.setClickable(true);
//                    int qty = Integer.parseInt(mitemQty.getText().toString());
//                    if (qty == 9999) {
//                        plus.setClickable(false);
//                    }

                    Log.i("TAG", "onClick: " + PastriesSelection.selectedCountIndividual.get(this.getAdapterPosition()));

                    int localCount;
                    if (finalcount){
                        localCount = PastriesSelection.counts.get(this.getAdapterPosition());
                    }else {
                        localCount = PastriesSelection.selectedCountIndividual.get(this.getAdapterPosition());
                    }
                    if (localCount < 9999) {
                        if (finalcount){
                            PastriesSelection.counts.set(this.getAdapterPosition(), (localCount + 1));
                        }else {
                            PastriesSelection.selectedCountIndividual.set(this.getAdapterPosition(), (localCount + 1));
                            PastriesSelection.indivialcount.set(this.getAdapterPosition(), (localCount + 1));
                        }
                        mitemQty.setText("" + (localCount + 1));
                        mitemQty.setSelection(mitemQty.getText().toString().length());
                        AddItems(this.getAdapterPosition(), (localCount + 1));
                        qty = localCount + 1;
                        Log.i("TAG", "onClick: " + PastriesSelection.selectedCountIndividual);
                        if (!PastriesSelection.positions.contains(this.getAdapterPosition())) {

                            positions.add(this.getAdapterPosition());

                        }


                        Log.i("TAG", "postions: " + positions);
                        Log.i("TAG", "Counts: " + counts);
                    }
                    try {
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        if (Qty == 0) {
                            PastriesSelection.orderPrice.setText("-");
                            PastriesSelection.orderQuantity.setText("-");
                            PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        } else {
                            PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice));
                            PastriesSelection.orderQuantity.setText("" + Qty);
                            PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        }

                        Log.i("TAG", "order price7 " + orderPrice);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.donut_minus:
                    if (mitemQty.getText().toString().length() > 0) {
                        if (Integer.parseInt(mitemQty.getText().toString()) > 0) {
                            int localCount1;
                            if (finalcount) {
                                localCount1 = PastriesSelection.counts.get(this.getAdapterPosition());
                                PastriesSelection.counts.set(this.getAdapterPosition(), (localCount1 - 1));
                            }else {
                                localCount1 = PastriesSelection.selectedCountIndividual.get(this.getAdapterPosition());
                                PastriesSelection.selectedCountIndividual.set(this.getAdapterPosition(), (localCount1 - 1));
                                PastriesSelection.indivialcount.set(this.getAdapterPosition(), (localCount1 - 1));
                            }
                            mitemQty.setText("" + (localCount1 - 1));
                            mitemQty.setSelection(mitemQty.getText().toString().length());
                            AddItems(this.getAdapterPosition(), (localCount1 - 1));
                            qty = localCount1 - 1;

                            try {
                                double number;
                                number = myDbHelper.getTotalOrderPrice();
                                DecimalFormat decim = new DecimalFormat("0.00");
//                                OrderFragment.orderPrice.setText("" + decim.format(number));
//                                OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                                double number1;
                                number1 = myDbHelper.getTotalOrderPrice();
                                DecimalFormat decim1 = new DecimalFormat("0.00");
//                                CategoriesListActivity.orderPrice.setText("" + decim1.format(number));
//                                CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                            try {
                                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                                if (Qty == 0) {
                                    PastriesSelection.orderPrice.setText("-");
                                    PastriesSelection.orderQuantity.setText("-");
                                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                                } else {
                                    PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice));
                                    PastriesSelection.orderQuantity.setText("" + Qty);
                                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                                }

                                Log.i("TAG", "order price6 " + orderPrice);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }
                    } else {

                    }
                    break;
            }
        }
    }

    private void AddItems(int position, int quantity) {
        int localSize = itemQty.size();
        ArrayList<String> localArray = new ArrayList<>();
        localArray.addAll(itemQty);
        Log.i("TAG", "AddItems: " + itemQty.size());
        Log.i("TAG", "AddItems1: " + itemQty);
        for (int i = 0; i < localSize; i++) {
            if (localArray.get(i).equals(orderList.get(value).getChildItems().get(position).getItemId())) {
                Qty = Qty - 1;
                orderPrice = orderPrice - Float.parseFloat(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());
                itemQty.remove(orderList.get(value).getChildItems().get(position).getItemId());
            } else {
//            Do nothing
            }

            Log.i("TAG", "AddItems2: " + itemQty);
        }

        for (int j = 0; j < quantity; j++) {
            Qty = Qty + 1;
            orderPrice = orderPrice + Float.parseFloat(orderList.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());
            itemQty.add(orderList.get(value).getChildItems().get(position).getItemId());

//            plus.setClickable(true);
//            int qty = Integer.parseInt(mitemQty.getText().toString());
//            if (qty == 9999) {
//                plus.setClickable(false);
//            }
        }

        itemfinalQty.add(String.valueOf(Qty));
        itemfinalprice.add(String.valueOf(orderPrice));

        if (itemfinalQty.size() == 0) {

            orderPrice = 0;
            Qty = 0;
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                if (Qty == 0) {
                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } else {
                    PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                    PastriesSelection.orderQuantity.setText("" + Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                Log.i("TAG", "AddItems: " + Qty);
                if (Qty == 0) {
                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } else {
                    PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                    PastriesSelection.orderQuantity.setText("" + Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

//    public void showErrorDialog(String message){
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));
//
//        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//        // set dialog message
//        alertDialogBuilder
//                .setMessage(message)
//                .setCancelable(false)
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
//
//        // create alert dialog
//        AlertDialog alertDialog = alertDialogBuilder.create();
//
//        alertDialog.show();
//    }
}
