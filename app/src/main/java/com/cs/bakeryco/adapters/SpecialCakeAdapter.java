package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsBoxActivity;
import com.cs.bakeryco.activities.SpecialCakeItemsActivity;
import com.cs.bakeryco.model.ItemsList;

import java.util.ArrayList;

public class SpecialCakeAdapter extends RecyclerView.Adapter<SpecialCakeAdapter.MyViewHolder> {

    Context context;
//    ArrayList<ItemsList.SubItems> orderLists = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String userid;
    String enteredString;


    public SpecialCakeAdapter(Context context) {
        this.context = context;
//        this.orderLists = ordersCounts;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.special_cake_list, parent, false);

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {




    }

    @Override
    public int getItemCount() {

        return 6;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
//        TextView box_item_name, box_item_desc;
//        EditText mqty;
//        ImageView box_item_img, plus, mins;

        public MyViewHolder(final View convertView) {
            super(convertView);

//            box_item_name = (TextView) convertView.findViewById(R.id.box_item_name);
//            box_item_desc = (TextView) convertView.findViewById(R.id.box_item_desc);
//            mqty = (EditText) convertView.findViewById(R.id.qty);
//
//            box_item_img = (ImageView) convertView.findViewById(R.id.box_img);
//            plus = (ImageView) convertView.findViewById(R.id.plus);
//            mins = (ImageView) convertView.findViewById(R.id.mins);
//
//
//            mqty.addTextChangedListener(this);
//            plus.setOnClickListener(this);
//            mins.setOnClickListener(this);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, SpecialCakeItemsActivity.class);
                    context.startActivity(intent);

                }
            });

        }


    }

//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//
//            if (mqty.getText().toString().length() > 0) {
//
//                enteredString = editable.toString();
//                if (enteredString.startsWith("0")) {
//                    if (enteredString.length() > 1) {
//                        mqty.setText(enteredString.substring(1));
//                        mqty.setSelection(mqty.length());
//                    }
//                }
//                qty = 0;
//                for (Integer i : selectedCount) {
//                    qty = qty + i;
//                }
//                Log.i("TAG", "qty: " + qty);
//
////                if (orderLists.get(getAdapterPosition()).getSubitems_qty() > Integer.parseInt(mqty.getText().toString())) {
////                    int localCount = orderLists.get(getAdapterPosition()).getSubitems_qty() - Integer.parseInt(mqty.getText().toString());
////                    qty = qty - localCount;
////                    Log.i("TAG", "subitemqty > edittxtcount " + qty);
////                } else if (orderLists.get(getAdapterPosition()).getSubitems_qty() < Integer.parseInt(mqty.getText().toString())) {
////                    int localCount = Integer.parseInt(mqty.getText().toString()) - orderLists.get(getAdapterPosition()).getSubitems_qty();
////                    qty = qty + localCount;
////                    Log.i("TAG", "subitemqty < edittxtcount " + qty);
////                    Log.i("TAG", "localcount " + localCount);
////                } else {
////                    qty = qty;
////                    Log.i("TAG", "subitemqty = edittxtcount " + qty);
////                }
//
//
//                Log.i("TAG", "afterTextChanged: " + no_of_items);
//                Log.i("TAG", "afterTextChanged qty: " + qty);
////                if (qty < Integer.parseInt(no_of_items)) {
////                    orderLists.get(getAdapterPosition()).setSubitems_qty(Integer.parseInt(mqty.getText().toString()));
//////                        AddItems(getAdapterPosition(), orderLists.get(getAdapterPosition()).getSubitems_qty());
////                } else {
//////                    mqty.setText("" + orderLists.get(getAdapterPosition()).getSubitems_qty());
//////                    mqty.setSelection(mqty.length());
//////                        if (language.equals("En")) {
////                    showErrorDialog("Please enter less than or equals to box size");
//////                        } else {
//////                    showErrorDialog("Please enter less than or equals to box size");
//////                        }
////                }
//
//                if (selectedCount.get(this.getAdapterPosition()) > Integer.parseInt(mqty.getText().toString())) {
//                    int localCount = selectedCount.get(this.getAdapterPosition()) - Integer.parseInt(mqty.getText().toString());
//                    qty = qty - localCount;
//                } else if (selectedCount.get(this.getAdapterPosition()) < Integer.parseInt(mqty.getText().toString())) {
//                    int localCount = Integer.parseInt(mqty.getText().toString()) - selectedCount.get(this.getAdapterPosition());
//                    qty = qty + localCount;
//                } else {
//                    qty = qty;
//                }
//
//                if (qty <= no_of_items) {
//                    selectedCount.set(this.getAdapterPosition(), Integer.parseInt(mqty.getText().toString()));
//                    box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
//                    AddItems(this.getAdapterPosition(), selectedCount.get(this.getAdapterPosition()));
//                } else {
//                    mqty.setText("" + selectedCount.get(this.getAdapterPosition()));
//                    mqty.setSelection(mqty.length());
//                    box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
////                    if (language.equals("En")) {
//                    showErrorDialog("Please enter less than or equals to box size");
////                    } else {
////                        showErrorDialog("Please enter less than or equals to box size");
////                    }
//                }
//            } else {
//                selectedCount.set(this.getAdapterPosition(), 0);
//                AddItems(getAdapterPosition(), 0);
//            }
//
//        }
//
//        @Override
//        public void onClick(View view) {
//            switch (view.getId()) {
//                case R.id.plus:
//
//                    qty = 0;
//                    for (Integer i : selectedCount) {
//                        qty = qty + i;
//                    }
//
//                    qty = qty + 1;
//                    if (qty <= no_of_items) {
//                        int localCount = selectedCount.get(this.getAdapterPosition());
//                        selectedCount.set(this.getAdapterPosition(), (localCount + 1));
//                        mqty.setText("" + (localCount + 1));
//                        mqty.setSelection(mqty.length());
//                        box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
//                        AddItems(this.getAdapterPosition(), (localCount + 1));
//
//                    } else {
////                    if (language.equals("En")) {
//                        showErrorDialog("You are done with maximum " + no_of_items + " " + title_name + " per box, to order more use other box.");
////                    } else {
////                        showErrorDialog("Please enter less than or equals to box size");
////                    }
//                    }
//                    break;
//
//                case R.id.mins:
////            if (visible) {
////
////                showErrorDialog("Please select the size to proceed with item selection");
////            } else {
//                    if (mqty.length() > 0) {
//                        if (Integer.parseInt(mqty.getText().toString()) > 0) {
//                            int localCount = selectedCount.get(this.getAdapterPosition());
//                            selectedCount.set(this.getAdapterPosition(), (localCount - 1));
//
//                            mqty.setText("" + (localCount - 1));
//                            mqty.setSelection(mqty.length());
//                            box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
//                            AddItems(this.getAdapterPosition(), (localCount - 1));
//                            Log.i("TAG", "onClick: " + localCount);
//                        }
//                    }
////            }
//                    break;
//
//            }
//        }
//    }
//
//    private void AddItems(int position, int quantity) {
//
//        int localSize = selectedItems.size();
//        ArrayList<ItemsList.SubItems> localArray = new ArrayList<>();
//        localArray.addAll(selectedItems);
//        for (int i = 0; i < localSize; i++) {
//            if (localArray.get(i).equals(orderLists.get(position))) {
//                selectedItems.remove(orderLists.get(position));
//                subitemQty.remove(orderLists.get(position).getSubItemName());
//                String pos = Integer.toString(position);
//                boxpos.remove(pos);
//                Log.i("TAG", "box pos remove " + boxpos);
//
//            } else {
////            Do nothing
//            }
//        }
//
//        for (int j = 0; j < quantity; j++) {
//            selectedItems.add(orderLists.get(position));
//            subitemQty.add(orderLists.get(position).getSubItemName());
//            String pos = Integer.toString(position);
//            if (boxpos != null && !boxpos.contains(pos)) {
//                boxpos.add(pos);
//                Log.i("TAG", "box pos " + boxpos);
//
//            }
////           else {
////                boxpos.remove(pos);
////            }
//        }
////        notifyItemChanged(position);
//    }
//
//    public void showErrorDialog(String message) {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Material_Light_Dialog));
//
//        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//        // set dialog message
//        alertDialogBuilder
//                .setMessage(message)
//                .setCancelable(false)
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });
//
//        // create alert dialog
//        AlertDialog alertDialog = alertDialogBuilder.create();
//
//        alertDialog.show();
//    }

}
