package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.AddressActivity;
import com.cs.bakeryco.model.Address;

import java.util.ArrayList;


/**
 * Created by CS on 20-06-2016.
 */
public class AddressAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Address> addressList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    Activity activity;
    String language;
    //public ImageLoader imageLoader;

    public AddressAdapter(Context context, ArrayList<Address> addressList, String language, Activity activity) {
        this.context = context;
        this.addressList = addressList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.activity = activity;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return addressList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView flatNo, landmark, address, addressName;
        ImageView addressType;
        LinearLayout address_layout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.address_list_item, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
            }


            holder.flatNo = (TextView) convertView
                    .findViewById(R.id.flat_no);
//            holder.storeName = (TextView) convertView
//                    .findViewById(R.id.store_name);
            holder.landmark = (TextView) convertView
                    .findViewById(R.id.landmark);
            holder.address = (TextView) convertView
                    .findViewById(R.id.address);
            holder.addressName = (TextView) convertView.findViewById(R.id.address_name);
            holder.addressType = (ImageView) convertView.findViewById(R.id.address_type);
            holder.address_layout = convertView.findViewById(R.id.address_layout);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            if (addressList.get(position).getHouseNo().equals("null")) {
                holder.flatNo.setText("Flat No : ....");
            } else {
                holder.flatNo.setText("Flat No : " + addressList.get(position).getHouseNo());
            }
            if (addressList.get(position).getLandmark().equals("null")) {
                holder.landmark.setText("Landmark : ....");
            } else {
                holder.landmark.setText("Landmark : " + addressList.get(position).getLandmark());
            }
            holder.address.setText("" + addressList.get(position).getAddress());
            holder.addressName.setText("" + addressList.get(position).getAddressName());
        } else if (language.equalsIgnoreCase("Ar")) {

            if (addressList.get(position).getHouseNo().equals("null")) {
                holder.flatNo.setText("شقة رقم : ...");
            } else {
                holder.flatNo.setText("شقة رقم : " + addressList.get(position).getHouseNo());
            }
            if (addressList.get(position).getLandmark().equals("null")) {
                holder.flatNo.setText("معلم أو مكان معروف : ...");
            } else {
                holder.landmark.setText("معلم أو مكان معروف : " + addressList.get(position).getLandmark());
            }

            holder.address.setText("" + addressList.get(position).getAddress());
            holder.addressName.setText("" + addressList.get(position).getAddressName());
        }
        if (addressList.get(position).getAddressType().equalsIgnoreCase("1")) {
            holder.addressType.setImageResource(R.drawable.address_home_selected);
        } else if (addressList.get(position).getAddressType().equalsIgnoreCase("2")) {
            holder.addressType.setImageResource(R.drawable.address_office_selected);
        } else {
            holder.addressType.setImageResource(R.drawable.address_other_selected);
        }

        holder.address_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (toConfirmOrder) {
//                    mPosition = position;

                    Location me = new Location("");
                    Location dest = new Location("");

                    me.setLatitude(Double.parseDouble(addressList.get(position).getLatitude()));
                    me.setLongitude(Double.parseDouble(addressList.get(position).getLongitude()));

                    dest.setLatitude(Constants.store_lat);
                    dest.setLongitude(Constants.store_longi);

                    float dist = (me.distanceTo(dest)) / 1000;
                    Log.i("TAG", "onClick: " + me);
                    if (dist<=Constants.delivery_dist){

                        Constants.address_id = Integer.parseInt(addressList.get(position).getId());
                        Constants.address = addressList.get(position).getAddress();
                        Constants.user_lat = Double.parseDouble(addressList.get(position).getLatitude());
                        Constants.user_longi = Double.parseDouble(addressList.get(position).getLongitude());
                        Constants.address_name = addressList.get(position).getAddressName();
//                        Intent intent = new Intent();
//                        activity.setResult(RESULT_OK, intent);
                        activity.finish();
                    } else {

                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog("The distance too far from restaurant please select near restaurant", activity);
                        } else {
                            Constants.showOneButtonAlertDialog("المسافة بعيده جداً عن المطعم. الرجاء اختيار مطعم أقرب", activity);
                        }

                    }
//                    new GetCurrentTime().execute();
//                    Intent intent = new Intent(AddressActivity.this, OrderConfirmation.class);
//                    intent.putExtra("your_address", addressList.get(mPosition).getAddress());
//                    intent.putExtra("address_id", addressList.get(mPosition).getId());
//                    intent.putExtra("user_latitude", addressList.get(mPosition).getLatitude());
//                    intent.putExtra("user_longitude", addressList.get(mPosition).getLongitude());
//                    intent.putExtra("landmark", addressList.get(mPosition).getLandmark());
//                    intent.putExtra("order_type", Constants.ORDER_TYPE);
//                    startActivity(intent);
                }

//            }
        });

        return convertView;
    }
}