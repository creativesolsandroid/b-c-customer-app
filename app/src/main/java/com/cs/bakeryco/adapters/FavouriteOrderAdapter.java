package com.cs.bakeryco.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.bakeryco.R;
import com.cs.bakeryco.model.FavouriteOrder;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brahmam on 19/2/16.
 */
public class FavouriteOrderAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<FavouriteOrder> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public FavouriteOrderAdapter(Context context, ArrayList<FavouriteOrder> favOrderList, String language) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView address, order_date, order_type, order_price, name, txt, rating;
//        ImageView rating;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.order_history_listitem, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.order_history_listitem_arabic, null);
            }


            holder.name = (TextView) convertView
                    .findViewById(R.id.name);
            holder.address = (TextView) convertView
                    .findViewById(R.id.address);
            holder.order_date = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.order_price = (TextView) convertView
                    .findViewById(R.id.order_price);
            holder.order_type = (TextView) convertView
                    .findViewById(R.id.order_type);
            holder.txt = (TextView) convertView
                    .findViewById(R.id.txt);
            holder.rating = (TextView) convertView.
                    findViewById(R.id.rating);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.txt.setVisibility(View.GONE);
        holder.rating.setVisibility(View.INVISIBLE);

        holder.name.setText("" + favOrderList.get(position).getFavoriteName());

        if (favOrderList.get(position).getOrdertype().equals("Delivery")) {
            holder.address.setText("" + favOrderList.get(position).getUseraddress());
        } else if (favOrderList.get(position).getOrdertype().equals("")) {
            holder.address.setText("New Industrial Area, Riyadh, Saudi Arabia");
        } else {
            holder.address.setText("New Industrial Area, Riyadh, Saudi Arabia");
        }


        double number;
        number = Double.parseDouble(favOrderList.get(position).getTotalPrice());
        DecimalFormat nf = new DecimalFormat("0.00");
        holder.order_price.setText("SR " + nf.format(number) + "");
//        holder.totalPrice.setText(favOrderList.get(position).getTotalPrice()+" SR");

        Log.i("DATE TAG", "" + favOrderList.get(position).getOrderDate());
        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                dateObj = curFormater1.parse(favOrderList.get(position).getOrderDate());
            } catch (ParseException pe) {

            }

        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        String date = postFormater.format(dateObj);

        holder.order_date.setText(date);


        holder.order_type.setVisibility(View.INVISIBLE);

        return convertView;
    }
}