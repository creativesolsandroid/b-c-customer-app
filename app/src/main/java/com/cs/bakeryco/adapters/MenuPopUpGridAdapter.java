package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.MainCategories;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.CategoriesListActivity.catId;

public class MenuPopUpGridAdapter extends RecyclerView.Adapter<MenuPopUpGridAdapter.MyViewHolder> {

    private Context context;
    //    private int selectedPosition = 0;
    ArrayList<ItemsList.Items> orderList = new ArrayList<>();
    private Activity activity;
    int value;
    String language;

    public MenuPopUpGridAdapter(Context context, ArrayList<ItemsList.Items> groups, int value, String language) {
        this.context = context;
        this.activity = activity;
        this.orderList = groups;
        this.value = value;
        this.language = language;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_pop_up_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.sub_cat_name.setText("" + orderList.get(value).getSubCategoryItems().get(position).getSubCategoryName());
        if (value == 0) {
            holder.sub_cat_img_bg.setBackground(context.getResources().getDrawable(R.drawable.menu_pop_up_cake_bg));
        } else if (value == 1) {
            holder.sub_cat_img_bg.setBackground(context.getResources().getDrawable(R.drawable.menu_pop_up_pastries_bg));
        } else if (value == 2) {
            holder.sub_cat_img_bg.setBackground(context.getResources().getDrawable(R.drawable.menu_pop_up_macarons_bg));
        } else if (value == 3) {
            holder.sub_cat_img_bg.setBackground(context.getResources().getDrawable(R.drawable.menu_pop_up_bread_bg));
        } else if (value == 4) {
            holder.sub_cat_img_bg.setBackground(context.getResources().getDrawable(R.drawable.menu_pop_up_sandwich_bg));
        } else if (value == 5) {
            holder.sub_cat_img_bg.setBackground(context.getResources().getDrawable(R.drawable.menu_pop_up_offers_bg));
        }
        Glide.with(context).load(Constants.IMAGE_URL+orderList.get(value).getSubCategoryItems().get(position).getSubCategoryImage()).into(holder.sub_cat_img);



    }

    @Override
    public int getItemCount() {
        return orderList.get(value).getSubCategoryItems().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sub_cat_name;
        ImageView sub_cat_img, sub_cat_img_bg;

        public MyViewHolder(View itemView) {
            super(itemView);
            sub_cat_name = (TextView) itemView.findViewById(R.id.menu_sub_cat_name);
            sub_cat_img = (ImageView) itemView.findViewById(R.id.menu_img);
            sub_cat_img_bg = (ImageView) itemView.findViewById(R.id.menu_img_bg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(value).getSubCategoryItems().get(getAdapterPosition()).getSections());
                    intent.putExtra("postion", getAdapterPosition());
                    intent.putExtra("catId", orderList.get(value).getMainCategoryId());
                    intent.putExtra("main_list", orderList);
                    Log.i("TAG", "pastires_pos: " + getAdapterPosition());
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(value).getSubCategoryItems().get(getAdapterPosition()).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(value).getSubCategoryItems().get(getAdapterPosition()).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);

                }
            });
        }
    }
}
