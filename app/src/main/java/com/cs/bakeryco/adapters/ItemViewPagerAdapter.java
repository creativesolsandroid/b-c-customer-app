package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.model.SubCategories;

import java.util.ArrayList;

public class ItemViewPagerAdapter extends RecyclerView.Adapter<ItemViewPagerAdapter.MyViewHolder> {

    Context context;
    ArrayList<SubCategories> sections = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String language;
    int value;

    public ItemViewPagerAdapter(Context context, ArrayList<SubCategories> orderList, int position, String language) {
        this.context = context;
        this.sections = orderList;
        this.language = language;
        this.value = position;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_page, parent, false);

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        if (language.equalsIgnoreCase("Ar")) {
            holder.mlayout.setRotationY(180);
        }

        Glide.with(context).load(Constants.IMAGE_URL + sections.get(position).getSections().get(0).getChildItems().get(0).getImage()).into(holder.mitem_image);


        holder.mview.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {

        return sections.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mitem_image;
        LinearLayout mlayout;
        View mview;

        public MyViewHolder(final View convertView) {
            super(convertView);

            mitem_image = convertView.findViewById(R.id.image_popup);
            mlayout = convertView.findViewById(R.id.layout);
            mview = convertView.findViewById(R.id.view);

        }

    }

}
