package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.activities.ItemsBoxActivity;
import com.cs.bakeryco.activities.ItemsIndiviualActivity;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.Section;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.ItemsActivity.indivialcount;

public class ItemsListAdapter extends RecyclerView.Adapter<ItemsListAdapter.MyViewHolder> {

    Context context;
    ArrayList<ItemsList.Sections> orderLists = new ArrayList<>();
    int selectionpos;
    Activity activity;
    LayoutInflater inflater;
    String userid, language, title;

    public ItemsListAdapter(Context context, ArrayList<ItemsList.Sections> ordersCounts, int selectionpos, String language, String title) {
        this.context = context;
        this.orderLists = ordersCounts;
        this.userid = userid;
        this.title = title;
        this.selectionpos = selectionpos;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.items_selection_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.items_selection_list_arabic, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);

        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (position == 0){

            holder.itemView.setPadding(16,16,16,16);

        } else {

            holder.itemView.setPadding(16,0,16,16);

        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (orderLists.get(selectionpos).getSectionId()== 1 || orderLists.get(selectionpos).getSectionId() == 5 || orderLists.get(selectionpos).getSectionId() == 6){
                    Intent a = new Intent(context, ItemsIndiviualActivity.class);
                    a.putExtra("desc", orderLists.get(selectionpos).getItems().get(position).getItemDesc());
                    a.putExtra("itemname", orderLists.get(selectionpos).getItems().get(position).getItemName());
                    a.putExtra("desc_ar", orderLists.get(selectionpos).getItems().get(position).getItemDesc_Ar());
                    a.putExtra("itemname_ar", orderLists.get(selectionpos).getItems().get(position).getItemName_Ar());
                    a.putExtra("section", orderLists);
                    a.putExtra("value", selectionpos);
                    a.putExtra("pos", position);
                    a.putExtra("cat_name", title);
                    a.putExtra("list", orderLists);
                    a.putExtra("catId", ItemsActivity.catId);
                    a.putExtra("main_list", ItemsActivity.main_list);
                    a.putExtra("cake_type_id", orderLists.get(selectionpos).getItems().get(position).getItemTypeIds());
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(a);
                } else if (orderLists.get(selectionpos).getSectionId() == 2 || orderLists.get(selectionpos).getSectionId() == 3 || orderLists.get(selectionpos).getSectionId() == 4){

                    Intent a = new Intent(context, ItemsBoxActivity.class);
                    a.putExtra("SubItems",orderLists.get(selectionpos).getItems().get(position).getSubItems());
                    a.putExtra("pricelist",orderLists.get(selectionpos).getItems().get(position).getPrice());
                    if (language.equalsIgnoreCase("En")) {
                        a.putExtra("title_name", orderLists.get(selectionpos).getItems().get(position).getItemName());
                    } else {
                        a.putExtra("title_name", orderLists.get(selectionpos).getItems().get(position).getItemName_Ar());
                    }
                    a.putExtra("no_of_items",orderLists.get(selectionpos).getItems().get(position).getNoOfItems());
                    a.putExtra("section_list",orderLists);
                    a.putExtra("section_pos",selectionpos);
                    a.putExtra("child_pos",position);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(a);

                }

            }
        });


        Log.i("TAG", "onBindViewHolder: " + indivialcount.size());

        if (language.equalsIgnoreCase("En")) {
            holder.item_name.setText("" + orderLists.get(selectionpos).getItems().get(position).getItemName());
            holder.item_desc.setText("" + orderLists.get(selectionpos).getItems().get(position).getItemDesc());
        } else {
            holder.item_name.setText("" + orderLists.get(selectionpos).getItems().get(position).getItemName_Ar());
            holder.item_desc.setText("" + orderLists.get(selectionpos).getItems().get(position).getItemDesc_Ar());
        }
        double price = Double.parseDouble(orderLists.get(selectionpos).getItems().get(position).getPrice().get(0).getPrice());
        holder.item_price.setText("" + Constants.decimalFormat.format(price) + " SR");
        Glide.with(context).load(Constants.IMAGE_URL + orderLists.get(selectionpos).getItems().get(position).getItemImage()).into(holder.item_img);

    }

    @Override
    public int getItemCount() {

        return orderLists.get(selectionpos).getItems().size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item_name, item_desc, item_price;
        ImageView item_img;
        CardView cardView;

        public MyViewHolder(final View convertView) {
            super(convertView);

            item_name = (TextView) convertView.findViewById(R.id.item_name);
            item_desc = (TextView) convertView.findViewById(R.id.item_desc);
            item_price = (TextView) convertView.findViewById(R.id.item_price);
            item_img = (ImageView) convertView.findViewById(R.id.item_img);
            cardView = (CardView) convertView.findViewById(R.id.card1);

//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }

    }

}