package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.model.Section;

import java.util.ArrayList;

/**
 * Created by CS on 1/22/2018.
 */

public class DropDownListAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Section> orderList = new ArrayList<>();
    String language;
    Activity parentActivity;
    private DataBaseHelper myDbHelper;
    int value;


    public DropDownListAdapter(Context context, ArrayList<Section> groups, int value, String language,
                               Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.value = value;
    }

    @Override
    public int getCount() {
        return orderList.get(value).getChildItems().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView drop_list;
//        ImageView itemImage, minus, plus;
//        LinearLayout grid_layout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.drop_down_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.drop_list = (TextView) convertView.findViewById(R.id.pcs_6_box);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        if (orderList.get(value).getChildItems().get(position).getBuildingType().equals("1")){
            holder.drop_list.setVisibility(View.GONE);
        }else if (orderList.get(value).getChildItems().get(position).getBuildingType().equals("2")){
            holder.drop_list.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")) {
                holder.drop_list.setText("" + orderList.get(value).getChildItems().get(position).getDisplayname());
            }else if (language.equalsIgnoreCase("Ar")) {
                holder.drop_list.setText("" + orderList.get(value).getChildItems().get(position).getDisplayname_ar());
            }
            Log.e("TAG","drop_list "+orderList.get(value).getChildItems().get(position).getDisplayname());
        }

//        holder.drop_list.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                PastriesSelection.value1 = position;
//                ((PastriesSelection)context).refreshData();
//            }
//        });



        return convertView;
    }
}
