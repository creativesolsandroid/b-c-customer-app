package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CakeInnerActivity1;
import com.cs.bakeryco.model.Order;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 4/13/2018.
 */

public class CakeInsidelistAdapter extends BaseAdapter {

    Context context;
    public LayoutInflater inflater;
    ArrayList<Order> orderlist = new ArrayList<>();
    String language;
    public static Activity parentActivity;
    private static DataBaseHelper myDbHelper;
    int Qty;
    float price;

    public CakeInsidelistAdapter(Context context, ArrayList<Order> orderlist, String language,
                                 Activity parentActivity) {
        this.context = context;
        this.orderlist = orderlist;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        myDbHelper = new DataBaseHelper(context);
    }


    @Override
    public int getCount() {
        return orderlist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static class ViewHolder {
        TextView mitemName, mitemsize, mqty;
        ImageView itemImage, minus, plus;
        LinearLayout grid_layout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.cake_inner_list, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.cake_inner_list_arabic, null);
            }

            holder.mitemName = (TextView) convertView.findViewById(R.id.item_name);
            holder.mitemsize = (TextView) convertView.findViewById(R.id.item_size);
//            holder.mitemprice = (TextView) convertView.findViewById(R.id.item_price);
            holder.mqty = (TextView) convertView.findViewById(R.id.qty);

            holder.itemImage = (ImageView) convertView.findViewById(R.id.img);
            holder.minus = (ImageView) convertView.findViewById(R.id.mins);
            holder.plus = (ImageView) convertView.findViewById(R.id.plus);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        CakeInnerActivity1.orderlist = myDbHelper.getItemOrderInfo(CakeInnerActivity1.itemid);
        Log.i("TAG", "order list size " + orderlist.size());

//        if (CakeInnerActivity1.itemid.equals(orderlist.get(position).getItemId()) && CakeInnerActivity1.asize.equals(orderlist.get(position).getItemTypeId()) && CakeInnerActivity1.itemtype.equals(orderlist.get(position).getItemType())) {
//
//            Log.i("TAG","name1 "+orderlist.get(position).getItemName());
//
//                holder.mitemName.setText(orderlist.get(position).getItemType()+" "+orderlist.get(position).getItemName());
//
//            if (orderlist.get(position).getItemType().contains("Whole")) {
//                holder.itemImage.setBackgroundResource(R.drawable.whole_cake);
//            } else if (orderlist.get(position).getItemType().contains("Sliced")) {
//                holder.itemImage.setBackgroundResource(R.drawable.sliced_cake);
//            }
//
//            holder.mitemsize.setText(orderlist.get(position).getItemTypeId());
//            holder.mqty.setText(orderlist.get(position).getQty());
//            holder.mitemprice.setText(orderlist.get(position).getTotalAmount());
//
//        }else if (!CakeInnerActivity1.itemid.equals(orderlist.get(position).getItemId()) && !CakeInnerActivity1.asize.equals(orderlist.get(position).getItemTypeId()) && !CakeInnerActivity1.itemtype.equals(orderlist.get(position).getItemType())) {
//
////            holder.mitemName.setText(orderlist.get(position).getItemType()+" "+orderlist.get(position).getItemName());
////
////            if (orderlist.get(position).getItemType().contains("Whole")) {
////                holder.itemImage.setBackgroundResource(R.drawable.whole_cake);
////            } else if (orderlist.get(position).getItemType().contains("Sliced")) {
////                holder.itemImage.setBackgroundResource(R.drawable.sliced_cake);
////            }
////
////            holder.mitemsize.setText(orderlist.get(position).getItemTypeId());
//            holder.mqty.setText(orderlist.get(position).getQty());
//            holder.mitemprice.setText(orderlist.get(position).getTotalAmount());
//
//            Log.i("TAG","name3 "+orderlist.get(position).getItemName());
//
//        }

//        Log.i("TAG","name4 "+orderlist.get(position).getItemName());
        if (language.equalsIgnoreCase("En")) {
            if (orderlist.get(position).getItemType().contains("Whole")) {
                holder.mitemName.setText(orderlist.get(position).getItemName());
            } else if (orderlist.get(position).getItemType().contains("Sliced")) {
                holder.mitemName.setText(orderlist.get(position).getItemName());
            }
            Log.i("TAG", "item name " + orderlist.get(position).getItemType() + " " + orderlist.get(position).getItemName());
        } else if (language.equalsIgnoreCase("Ar")) {
            Log.i("TAG", "item name1 " + orderlist.get(position).getItemType() + " " + orderlist.get(position).getItemName());
            if (orderlist.get(position).getItemType().contains("Whole")) {
                holder.mitemName.setText(orderlist.get(position).getItemNameAr());
            } else if (orderlist.get(position).getItemType().contains("Sliced")) {
                holder.mitemName.setText(orderlist.get(position).getItemNameAr());
            }
        }

        if (language.equalsIgnoreCase("En")) {
            if (orderlist.get(position).getItemType().contains("Whole")) {
                holder.itemImage.setBackgroundResource(R.drawable.whole_cake);
            } else if (orderlist.get(position).getItemType().contains("Sliced")) {
                holder.itemImage.setBackgroundResource(R.drawable.sliced_cake);
            }
        } else if (language.equalsIgnoreCase("Ar")) {
            if (orderlist.get(position).getItemType().contains("Whole")) {
                holder.itemImage.setBackgroundResource(R.drawable.whole_cake);
            } else if (orderlist.get(position).getItemType().contains("Sliced")) {
                holder.itemImage.setBackgroundResource(R.drawable.sliced_cake);
            }
        }


        if (language.equalsIgnoreCase("En")) {
            holder.mitemsize.setText(orderlist.get(position).getItemTypeId());
        } else if (language.equalsIgnoreCase("Ar")) {
            holder.mitemsize.setText(orderlist.get(position).getItemTypeName_ar());
        }

        holder.mqty.setText(orderlist.get(position).getQty());
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
//        holder.mitemprice.setText(decimalFormat.format(Float.parseFloat(orderlist.get(position).getTotalAmount())));

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Qty = Integer.parseInt(orderlist.get(position).getQty());
                price = Float.parseFloat(orderlist.get(position).getTotalAmount());

                Qty = Qty + 1;
                price = price + Float.parseFloat(orderlist.get(position).getPrice());

                holder.mqty.setText(Integer.toString(Qty));
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                holder.mitemprice.setText("" + decimalFormat.format(price));

                myDbHelper.updateOrderwithOrderId(String.valueOf(Qty), String.valueOf(price), orderlist.get(position).getOrderId());
                orderlist = myDbHelper.getItemOrderInfo(CakeInnerActivity1.itemid);
                notifyDataSetChanged();

                try {
                    DecimalFormat decimalFormat1 = new DecimalFormat("0.00");
                    CakeInnerActivity1.orderPrice.setText("" + decimalFormat1.format(myDbHelper.getItemOrderPrice1(orderlist.get(position).getItemId())) + " SR");
                    CakeInnerActivity1.orderQuantity.setText("" + myDbHelper.getItemOrderCount(orderlist.get(position).getItemId()));
                    CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Qty = Integer.parseInt(orderlist.get(position).getQty());
                price = Float.parseFloat(orderlist.get(position).getTotalAmount());

                if (Qty == 1) {
                    myDbHelper.deleteItemFromOrderlist(orderlist.get(position).getOrderId());
                    CakeInnerActivity1.orderlist.clear();
                    CakeInnerActivity1.orderlist = myDbHelper.getItemOrderInfo(orderlist.get(position).getItemId());
                    notifyDataSetChanged();
                    try {
                        DecimalFormat decimalFormat1 = new DecimalFormat("0.00");
                        CakeInnerActivity1.orderPrice.setText("" + decimalFormat1.format(myDbHelper.getItemOrderPrice1(orderlist.get(position).getItemId())) + " SR");
                        CakeInnerActivity1.orderQuantity.setText("" + myDbHelper.getItemOrderCount(orderlist.get(position).getItemId()));
                        CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                        CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getItemOrderCount(orderlist.get(position).getItemId()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (Qty >= 1) {
                    Qty = Qty - 1;
                    price = price - Float.parseFloat(orderlist.get(position).getPrice());
                    holder.mqty.setText(Integer.toString(Qty));
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                    holder.mitemprice.setText("" + (decimalFormat.format(price)));

                    myDbHelper.updateOrderwithOrderId(String.valueOf(Qty), String.valueOf(price), orderlist.get(position).getOrderId());
                    orderlist = myDbHelper.getItemOrderInfo(orderlist.get(position).getItemId());
                    notifyDataSetChanged();
                    try {
                        DecimalFormat decimalFormat1 = new DecimalFormat("0.00");
                        CakeInnerActivity1.orderPrice.setText("" + decimalFormat1.format(myDbHelper.getItemOrderPrice1(orderlist.get(position).getItemId())) + " SR");
                        CakeInnerActivity1.orderQuantity.setText("" + myDbHelper.getItemOrderCount(orderlist.get(position).getItemId()));
                        CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });

//        Log.i("TAG","name4 "+orderlist.get(position).getItemName());
//        holder.mitemName.setText(orderlist.get(position).getItemType()+" "+orderlist.get(position).getItemName());
//
//        if (orderlist.get(position).getItemType().contains("Whole")) {
//            holder.itemImage.setBackgroundResource(R.drawable.whole_cake);
//        } else if (orderlist.get(position).getItemType().contains("Sliced")) {
//            holder.itemImage.setBackgroundResource(R.drawable.sliced_cake);
//        }
//
//        holder.mitemsize.setText(orderlist.get(position).getItemTypeId());
//        holder.mqty.setText(orderlist.get(position).getQty());
//        holder.mitemprice.setText(orderlist.get(position).getTotalAmount());
        return convertView;
    }
}
