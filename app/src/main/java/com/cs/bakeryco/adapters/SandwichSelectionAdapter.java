package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.SandwichSelection;
import com.cs.bakeryco.model.Items;

import java.util.ArrayList;

/**
 * Created by CS on 28-02-2017.
 */

public class SandwichSelectionAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Items> orderList = new ArrayList<>();
    String language;
    Activity parentActivity;
    private DataBaseHelper myDbHelper;

    public SandwichSelectionAdapter(Context context, ArrayList<Items> groups, String language,
                                    Activity parentActivity) {
        this.context = context;
        this.orderList = groups;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {
        TextView title,itemQty;
        ImageView itemImage, minus, plus;
        LinearLayout grid_layout;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.pastries_selection_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.title = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemQty = (TextView) convertView.findViewById(R.id.donut_qty);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.donut_image);
            holder.minus = (ImageView) convertView.findViewById(R.id.donut_minus);
            holder.plus = (ImageView) convertView.findViewById(R.id.donut_plus);
            holder.grid_layout = (LinearLayout) convertView.findViewById(R.id.grid_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(position).getItemName());
        }else if(language.equalsIgnoreCase("Ar")){
            holder.title.setText(orderList.get(position).getItemNameAr());
        }

        Glide.with(context).load(Constants.IMAGE_URL+orderList.get(position).getImages()).into(holder.itemImage);

//        holder.itemImage.setOnDragListener(this);
        holder.itemImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                SandwichSelection.pos = position;
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 1);
                view.setVisibility(View.VISIBLE);
                return true;
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SandwichSelection.Boxitems.contains(orderList.get(position))){
                    SandwichSelection.totalCount = SandwichSelection.totalCount - 1;
                    SandwichSelection.mNumBox.setText(SandwichSelection.totalCount+"/"+ SandwichSelection.boxSize);
                    SandwichSelection.Boxitems.remove(orderList.get(position));
                    SandwichSelection.mBoxAdapter = new SandwichBoxItemsAdapter(parentActivity, SandwichSelection.Boxitems, SandwichSelection.boxSize, language, parentActivity);
                    SandwichSelection.mBoxGridView.setAdapter(SandwichSelection.mBoxAdapter);
                    SandwichSelection.mBoxAdapter.notifyDataSetChanged();
                }
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SandwichSelection.boxSize>SandwichSelection.totalCount) {
                    SandwichSelection.totalCount = SandwichSelection.totalCount + 1;
                    SandwichSelection.mNumBox.setText(SandwichSelection.totalCount+"/"+ SandwichSelection.boxSize);
                    SandwichSelection.Boxitems.add(orderList.get(position));
                    SandwichSelection.mBoxAdapter = new SandwichBoxItemsAdapter(parentActivity, SandwichSelection.Boxitems, SandwichSelection.boxSize, language, parentActivity);
                    SandwichSelection.mBoxGridView.setAdapter(SandwichSelection.mBoxAdapter);
                    SandwichSelection.mBoxAdapter.notifyDataSetChanged();
                }
                else{
                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "You are done with Maximum "+SandwichSelection.boxSize+" " + SandwichSelection.title + " per Box, to order more use other box.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });
        return convertView;
    }

}
