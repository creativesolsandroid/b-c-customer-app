package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.ImageLayout;
import com.cs.bakeryco.activities.OrderConfirmationNew;
import com.cs.bakeryco.activities.SpecialCakeActivity;
import com.cs.bakeryco.model.ItemsList;

import java.util.ArrayList;

public class CouponListAdapter extends RecyclerView.Adapter<CouponListAdapter.MyViewHolder> {

    Context context;
//    ArrayList<String> main_name = new ArrayList<>();
//    ArrayList<Integer> main_img = new ArrayList<>();
//    ArrayList<ItemsList.Items> itemsArrayList = new ArrayList<>();

    Activity activity;
    LayoutInflater inflater;
    String userid, language, title;
    boolean select = false;

    public CouponListAdapter(Context context, String language, Activity activity) {
        this.context = context;
//        this.main_name = main_name;
//        this.main_img = main_img;
//        this.itemsArrayList = itemsArrayList;
        this.activity = activity;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;

        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.coupon_vouchers_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.coupon_vouchers_list_arabic, parent, false);
        }

        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {



    }

    @Override
    public int getItemCount() {

        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
//        TextView text, img;
//        LinearLayout mlayout;

        public MyViewHolder(final View itemView) {
            super(itemView);

//            text = itemView.findViewById(R.id.txt);
//            img = itemView.findViewById(R.id.img);
//            mlayout = itemView.findViewById(R.id.layout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Constants.coupon = true;
                    Constants.coupon_name = "Today 478";
                    Intent a = new Intent(context, OrderConfirmationNew.class);
                    context.startActivity(a);
                    activity.finish();
                }
            });

        }

    }

}
