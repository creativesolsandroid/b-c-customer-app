package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CakeInnerActivity1;
import com.cs.bakeryco.model.CakeInnerlist;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Section;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 3/14/2018.
 */

public class CakeInnerlistAdapter extends BaseAdapter {

    Context context;
    public LayoutInflater inflater;
    public static ArrayList<Section> orderList = new ArrayList<>();
    ArrayList<Order> orderDB = new ArrayList<>();
    String language;
    public static Activity parentActivity;
    private static DataBaseHelper myDbHelper;
    //    public static int value2, value1;
    int Qty = 0;
    float orderPrice = 0;
    String itemid;
    public static int pos;
    public static int count;
    //    public static String itemname;
    String size, itemname, finalname;
    int qty, itemprice, finalprice;


    ArrayList<CakeInnerlist> value = new ArrayList<>();

    public CakeInnerlistAdapter(Context context, ArrayList<CakeInnerlist> value, String language,
                                Activity parentActivity) {
        this.context = context;
        this.value = value;
        this.parentActivity = parentActivity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        myDbHelper = new DataBaseHelper(context);
    }

    @Override
    public int getCount() {
        return value.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static class ViewHolder {
        TextView mitemName, mitemsize, mitemprice, mqty;
        ImageView itemImage, minus, plus;
        LinearLayout grid_layout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.cake_inner_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.checkout_row_arabic, null);
//            }

            holder.mitemName = (TextView) convertView.findViewById(R.id.item_name);
            holder.mitemsize = (TextView) convertView.findViewById(R.id.item_size);
            holder.mitemprice = (TextView) convertView.findViewById(R.id.item_price);
            holder.mqty = (TextView) convertView.findViewById(R.id.qty);

            holder.itemImage = (ImageView) convertView.findViewById(R.id.img);
            holder.minus = (ImageView) convertView.findViewById(R.id.mins);
            holder.plus = (ImageView) convertView.findViewById(R.id.plus);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (value.get(position).getFinalname().contains("Whole " + value.get(position).getItemName())) {
            holder.mitemName.setText(value.get(position).getFinalname());
        } else if (value.get(position).getFinalname().contains("Sliced " + value.get(position).getItemName())) {
            holder.mitemName.setText(value.get(position).getFinalname());
        }

        if (value.get(position).getFinalname().contains("Whole " + value.get(position).getItemName())) {
            holder.itemImage.setBackgroundResource(R.drawable.whole_cake);
        } else if (value.get(position).getFinalname().contains("Sliced " + value.get(position).getItemName())) {
            holder.itemImage.setBackgroundResource(R.drawable.sliced_cake);
        }

        holder.mitemsize.setText(value.get(position).getSize());
        holder.mqty.setText(Integer.toString(value.get(position).getItemQty()));
        holder.mitemprice.setText(Integer.toString(value.get(position).getTotalprice()));


        if (CakeInnerActivity1.itemcount != null) {

            qty = value.get(position).getItemQty();
            orderPrice = value.get(position).getTotalprice();

            int count = qty;
            for (int i = 0; i < CakeInnerActivity1.itemcount.size(); i++) {
                if (CakeInnerActivity1.itemcount.get(i).equals(value.get(position).getItemid())) {
                    itemname = value.get(position).getItemName();
                    count = count + 1;
                }

            }
//            if (count > 0) {

            holder.mqty.setText(Integer.toString(count));
//            } else {
//                holder.mqty.setText("0");
//            }
//        } else {
//            holder.mqty.setText("0");
        }


//        try {
//            DecimalFormat decimalFormat = new DecimalFormat("0.00");
//            CakeInnerActivity1.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
//            CakeInnerActivity1.orderQuantity.setText("" + Integer.toString(Qty));
//            CakeInnerActivity1.mcount_basket.setText("" + Integer.toString(Qty));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Qty = value.get(position).getItemQty();
                orderPrice = value.get(position).getTotalprice();

                Qty = Qty + 1;
                orderPrice = orderPrice + value.get(position).getItemprice();

                CakeInnerActivity1.itemcount.add(value.get(position).getItemid());
                notifyDataSetChanged();

                CakeInnerActivity1.itemfinalcount.add(Qty);
                CakeInnerActivity1.itemfinalprice.add((int) orderPrice);

                Log.i("TAG", "qty " + CakeInnerActivity1.itemfinalcount);
                Log.i("TAG", "price " + CakeInnerActivity1.itemfinalprice);

                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    if (Qty == 0) {
                        CakeInnerActivity1.orderPrice.setText("-");
                        CakeInnerActivity1.orderQuantity.setText("-");
                        CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    } else {
                        CakeInnerActivity1.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                        CakeInnerActivity1.orderQuantity.setText("" + Integer.toString(Qty));
CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Qty = value.get(position).getItemQty();
//                orderPrice = value.get(position).getTotalprice();
//                Qty = Qty + 1;
//                orderPrice = orderPrice + value.get(position).getItemprice();
//
//                value.get(position).setItemQty(Qty);
//                value.get(position).setTotalprice((int) orderPrice);
//
//                Log.i("TAG","qty " + Qty);
//
//                holder.mqty.setText(Integer.toString(Qty));
//                holder.mitemprice.setText(String.valueOf(orderPrice));

                myDbHelper.updateOrder(String.valueOf(Qty), String.valueOf(orderPrice), value.get(position).getItemid());
                orderDB = myDbHelper.getOrderInfo();
                notifyDataSetChanged();

//                try {
//                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                    CakeInnerActivity1.orderPrice.setText("" + decimalFormat.format(myDbHelper.getTotalOrderPrice()) + " SR");
//                    CakeInnerActivity1.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                    CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


//                value.get(position).setItemQty(Qty);
//                value.get(position).setTotalprice((int) orderPrice);
//
//                CakeInnerActivity1.itemfinalcount.add(Qty);
//                CakeInnerActivity1.itemfinalprice.add((int) orderPrice);
//
//                float totalPrice = 0;
//                int totalqty = 0;
//                for(int i = 0; i < CakeInnerActivity1.itemfinalprice.size(); i++){
//                    totalPrice = totalPrice + ((CakeInnerActivity1.itemfinalcount.get(i)) * (CakeInnerActivity1.itemfinalprice.get(i)) );
//                    Log.i("TAG","item Count "+CakeInnerActivity1.itemfinalcount.get(i));
//                    totalqty = totalqty + CakeInnerActivity1.itemfinalcount.get(i);
//
//                }
//
//
//                Log.i("TAG", "qty " + CakeInnerActivity1.itemfinalcount);
//                Log.i("TAG", "price " + CakeInnerActivity1.itemfinalprice);
//
//                holder.mqty.setText(Integer.toString(Qty));
//                holder.mitemprice.setText(String.valueOf(orderPrice));
//
//                try {
//                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                    CakeInnerActivity1.orderPrice.setText("" + decimalFormat.format(totalPrice) + " SR");
//                    CakeInnerActivity1.orderQuantity.setText("" + Integer.toString(totalqty));
//                    CakeInnerActivity1.mcount_basket.setText("" + Integer.toString(totalqty));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


            }
        });


        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Qty = value.get(position).getItemQty();
                orderPrice = value.get(position).getTotalprice();
                if (Qty > 0) {
                    Qty = Qty - 1;
                    orderPrice = orderPrice - value.get(position).getItemprice();
                    value.get(position).setItemQty(Qty);
                    value.get(position).setTotalprice((int) orderPrice);

                    holder.mqty.setText(Integer.toString(Qty));
                    holder.mitemprice.setText(String.valueOf(orderPrice));
                }

                if (Qty == 0) {
                    value.remove(position);
                    notifyDataSetChanged();
                }

                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    if (Qty == 0){
                        CakeInnerActivity1.orderPrice.setText("-");
                        CakeInnerActivity1.orderQuantity.setText("-");
CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    }else {
                        CakeInnerActivity1.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
                        CakeInnerActivity1.orderQuantity.setText("" + Integer.toString(Qty));
CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        return convertView;
    }
}
