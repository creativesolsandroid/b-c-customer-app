package com.cs.bakeryco.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.model.SubCategories;
import com.cs.bakeryco.widgets.CardAdapter;

import java.util.ArrayList;
import java.util.List;

public class CardViewPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private ArrayList<SubCategories> mData = new ArrayList<>();
    private float mBaseElevation;
    int position;
    String mlanguage;
    Context mcontext;

    public CardViewPagerAdapter(Context context, ArrayList<SubCategories> item, int value, String language) {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
        mData = item;
        position = value;
        mlanguage = language;
        mcontext = context;
    }

    public void addCardItemS(Context context, SubCategories item, int value, String language) {
        mViews.add(null);
        mData.add(item);
        position = value;
        mlanguage = language;
        mcontext = context;
    }



    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.view_page, container, false);
        container.addView(view);
        bind(mData.get(position), view);
        CardView cardView = (CardView) view.findViewById(R.id.layout);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(SubCategories item, View view) {
        ImageView titleTextView = (ImageView) view.findViewById(R.id.image_popup);

        Glide.with(mcontext).load(Constants.IMAGE_URL + mData.get(position).getImages()).into(titleTextView);

    }

}
