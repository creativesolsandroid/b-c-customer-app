package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.activities.ItemsIndiviualActivity;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.Section;

import java.util.ArrayList;
import java.util.HashMap;

import static com.cs.bakeryco.activities.ItemsActivity.indivialcount;
import static com.cs.bakeryco.activities.ItemsActivity.itemfinalQty;

public class ViewPagerAdapter1 extends PagerAdapter {

    //    private static ArrayList<Gallery> images;
    ArrayList<String> img = new ArrayList<>();
    ArrayList<String> mediaid = new ArrayList<>();


    private LayoutInflater inflater;
    private static Context context;
    Activity parent;
    int pos, value;
    String language;
    String cat_name, Image, dec, mitemname, dec_ar, mitemname_ar;
    ArrayList<ItemsList.Sections> sections = new ArrayList<>();
    Activity parentActivity;
    TextView mcancel, mcat_name, mview_page_count, mitem_name, mitem_desc;
//    public static ImageView mshare, mitem_image;
    View mview;
    String info;
    private float mBaseElevation;

    LinearLayout cake_layout;
    ImageView plus, mins;
    //    View view1;
//    TextView item_name, item_price, item_desc, total_price, add;
//    EditText mqty;
//    ImageView check1, check2, size_check_box1, size_check_box2;
    int price_pos;
//    TextView cake_name1, cake_name2, cake_desc1, cake_desc2;
//    LinearLayout cake_img_layout1, cake_img_layout2;
//    LinearLayout cake_name_layout1, cake_name_layout2, cake_desc_layout1, cake_desc_layout2, cake_price_layout1, cake_price_layout2, cake_check_layout1, cake_check_layout2;

//    TextView price1, price2;

    String enteredString;
    DataBaseHelper myDbHelper;

    boolean cake_whole = true, cake_sliced = true;

//    View name_view, desc_view, price_view, check_view;

    String itemtype;
    //    int pos, value;
    String desc, desc_ar, cake_type_id;
    int qty, Qty;
    float orderPrice;
    float priceTxt = 0;
//    EditText comment;

    boolean zoom = true;

    private static final String TAG = "Touch";
    @SuppressWarnings("unused")
    private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;

    // These matrices will be used to scale points of the image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    private int selectedCheckBoxPosition = -1;

    private int selectedCheckBoxPosition2 = -1;

    private int selectedCheckBoxPosition3 = -1;

    public int getSelectedCheckBoxPosition() {
        return selectedCheckBoxPosition;
    }

    private int selectedCheckBoxPosition1 = -1;


    public int getSelectedCheckBoxPosition1() {
        return selectedCheckBoxPosition1;
    }

    // these PointF objects are used to record the point(s) the user is touching
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    float dptopixels;

    Activity activity;

    public ViewPagerAdapter1(Context context, ArrayList<ItemsList.Sections> orderList, int value, String language, Activity activity) {
        this.context = context;
        this.parent = parentActivity;
        this.language = language;
        this.value = value;
        this.sections = orderList;
        this.info = info;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
        myDbHelper = new DataBaseHelper(context);

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        if(youtubeFragment != null){
////            youtubeFragment.loadUrl("");
//            youtubeFragment.stopLoading();
////            youtubeFragment.destroy();
//        }
        container.removeView((ViewGroup) object);
    }

//    public void destroyItem(ViewGroup container, int position, Object object) {
//        destroyItem((View) container, position, object);
//    }


    @Override
    public int getCount() {

        int count = 0;
        count = sections.get(value).getItems().size();
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout;
        if (language.equalsIgnoreCase("En")) {
             myImageLayout = inflater.inflate(R.layout.individual_view_pager, view, false);
        } else {
             myImageLayout = inflater.inflate(R.layout.individual_view_pager_arabic, view, false);
        }
        ImageView mitem_image = myImageLayout.findViewById(R.id.image_popup);
//        mlayout = myImageLayout.findViewById(R.id.layout);
//        mview = myImageLayout.findViewById(R.id.view);

//        item_title = myImageLayout.findViewById(R.id.item_title);
        LinearLayout cake_layout = myImageLayout.findViewById(R.id.cake_layout);
//        view1 = myImageLayout.findViewById(R.id.view);
        ImageView plus = myImageLayout.findViewById(R.id.plus);
        ImageView mins = myImageLayout.findViewById(R.id.mins);
        final EditText mqty = myImageLayout.findViewById(R.id.qty);
        TextView item_name = myImageLayout.findViewById(R.id.item_name);
        final TextView item_price = myImageLayout.findViewById(R.id.item_price);
        TextView item_desc = myImageLayout.findViewById(R.id.item_desc);
        final TextView total_price = myImageLayout.findViewById(R.id.total_price);
        TextView add = myImageLayout.findViewById(R.id.add);

        final TextView comment = myImageLayout.findViewById(R.id.comment);

        final ImageView check1 = myImageLayout.findViewById(R.id.check1);
        final ImageView check2 = myImageLayout.findViewById(R.id.check2);
        final ImageView size_check_box1 = myImageLayout.findViewById(R.id.size_check1);
        final ImageView size_check_box2 = myImageLayout.findViewById(R.id.size_check2);

        TextView cake_name1 = myImageLayout.findViewById(R.id.cake_name1);
        TextView cake_name2 = myImageLayout.findViewById(R.id.cake_name2);

        TextView cake_desc1 = myImageLayout.findViewById(R.id.cake_desc1);
        TextView cake_desc2 = myImageLayout.findViewById(R.id.cake_desc2);

        LinearLayout cake_name_layout1 = myImageLayout.findViewById(R.id.cake_name_layout1);
        LinearLayout cake_name_layout2 = myImageLayout.findViewById(R.id.cake_name_layout2);
        LinearLayout cake_img_layout1 = myImageLayout.findViewById(R.id.cake_img1_layout);
        LinearLayout cake_img_layout2 = myImageLayout.findViewById(R.id.cake_img2_layout);
        LinearLayout cake_desc_layout1 = myImageLayout.findViewById(R.id.cake_desc_layout1);
        LinearLayout cake_desc_layout2 = myImageLayout.findViewById(R.id.cake_desc_layout2);
        LinearLayout cake_price_layout1 = myImageLayout.findViewById(R.id.price_layout1);
        LinearLayout cake_price_layout2 = myImageLayout.findViewById(R.id.price_layout2);
        LinearLayout cake_check_layout1 = myImageLayout.findViewById(R.id.check_box_layout1);
        LinearLayout cake_check_layout2 = myImageLayout.findViewById(R.id.check_box_layout2);

        TextView price1 = myImageLayout.findViewById(R.id.price1);
        TextView price2 = myImageLayout.findViewById(R.id.price2);

        ImageView share = myImageLayout.findViewById(R.id.share);

        View name_view = myImageLayout.findViewById(R.id.name_view);
        View desc_view = myImageLayout.findViewById(R.id.desc_view);
        View price_view = myImageLayout.findViewById(R.id.price_view);
        View check_view = myImageLayout.findViewById(R.id.check_view);
        LinearLayout mlayout = myImageLayout.findViewById(R.id.layout);
        final TextView cal = myImageLayout.findViewById(R.id.cal);


        if (language.equalsIgnoreCase("Ar")) {
            mlayout.setRotationY(180);
        }

//        item_title.setText("" + ItemsIndiviualActivity.cat_name);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri imageUri = Uri.parse(Constants.IMAGE_URL + sections.get(value).getItems().get(position).getItemImage());

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "Backery & Company \n " + sections.get(value).getItems().get(position).getItemName() + "\n " + sections.get(value).getItems().get(position).getItemDesc() + "\n Know More : http://onelink.to/5wsjta");

            }
        });

        if (language.equalsIgnoreCase("En")) {
            item_name.setText("" + ItemsIndiviualActivity.mitemname);
            item_desc.setText("" + ItemsIndiviualActivity.desc);
        } else {
            item_name.setText("" + ItemsIndiviualActivity.mitemname_ar);
            item_desc.setText("" + ItemsIndiviualActivity.desc_ar);
        }

        if (sections.get(value).getItems().get(position).getPrice().get(0).getCalories()!= null){
            cal.setVisibility(View.VISIBLE);
            cal.setText("" + Constants.decimalFormat.format(Float.parseFloat(String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getCalories()))) + " Cal");
        } else {
            cal.setVisibility(View.INVISIBLE);
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int view_pager_hieght = width - 10;

        int main_hieght = width - 160;

        int size_with = width - 100;

        Log.i("TAG", "main_hieght: " + main_hieght);
        Log.i("TAG", "view_pager_hieght: " + view_pager_hieght);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
        layoutParams.height = main_hieght;
        mitem_image.setLayoutParams(layoutParams);

        Glide.with(context).load(Constants.IMAGE_URL + sections.get(value).getItems().get(position).getItemImage()).into(mitem_image);

        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
        price_pos = 0;
        qty = 1;

        size_check_box1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                            selectedCheckBoxPosition = position;

                size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                size_check_box2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));

                itemtype = "Whole";
                Log.i("TAG", "whole ");

            }
        });

        size_check_box2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                            selectedCheckBoxPosition2 = position;

                size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));
                size_check_box2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));

                itemtype = "Sliced";
                Log.i("TAG", "sliced ");

            }
        });

        check1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                                selectedCheckBoxPosition1 = position;

//                                itemtype = "Whole";

                check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                check2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));

                double number, number1;
                float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                price_pos = 0;
                number = price;
                item_price.setVisibility(View.GONE);
//                item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                number1 = priceTxt;
                total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");
                if (sections.get(value).getItems().get(position).getPrice().get(0).getCalories()!= null){
                    cal.setVisibility(View.VISIBLE);
                    cal.setText("" + Constants.decimalFormat.format(Float.parseFloat(String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getCalories()))) + " Cal");
                } else {
                    cal.setVisibility(View.INVISIBLE);
                }

            }
        });

        check2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                                selectedCheckBoxPosition3 = position;

//                                itemtype = "Sliced";
                check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));
                check2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));

                double number, number1;
                float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
                price_pos = 1;
                number = price;
                item_price.setVisibility(View.GONE);
//                item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
                number1 = priceTxt;

                total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                if (sections.get(value).getItems().get(position).getPrice().get(1).getCalories()!= null){
                    cal.setVisibility(View.VISIBLE);
                    cal.setText("" + Constants.decimalFormat.format(Float.parseFloat(String.valueOf(sections.get(value).getItems().get(position).getPrice().get(1).getCalories()))) + " Cal");
                } else {
                    cal.setVisibility(View.INVISIBLE);
                }

            }
        });


        if (language.equalsIgnoreCase("En")) {

            item_name.setText(sections.get(value).getItems().get(position).getItemName());
            item_desc.setText(sections.get(value).getItems().get(position).getItemDesc());

            if (ItemsActivity.catId == 2) {

                if (sections.get(value).getSectionId() == 6) {

                    cake_layout.setVisibility(View.GONE);

                    double number, number1;
                    float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                    price_pos = 0;
                    number = price;
                    item_price.setVisibility(View.VISIBLE);
                    item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                    priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                    number1 = priceTxt;
                    total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");
                    if (sections.get(value).getItems().get(position).getPrice().get(0).getCalories()!= null){
                        cal.setVisibility(View.VISIBLE);
                        cal.setText("" + Constants.decimalFormat.format(Float.parseFloat(String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getCalories()))) + " Cal");
                    } else {
                        cal.setVisibility(View.INVISIBLE);
                    }

                } else {

                    cake_layout.setVisibility(View.VISIBLE);
//                    view1.setVisibility(View.VISIBLE);

//                    ItemsIndiviualActivity.cake_type_id = getIntent().getStringExtra("cake_type_id");

//                    if (position == selectedCheckBoxPosition) {
//                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        size_check_box2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        Log.d("TAG", "checkbox CHECKED at pos: " + position);
//                    } else if (position == selectedCheckBoxPosition2) {
//                        size_check_box1.setBackground(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        size_check_box2.setBackground(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        Log.d("TAG", "checkbox UNCHECKED at pos: " + position);
//                    }
//
//                    if (position == selectedCheckBoxPosition1) {
//                        check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        check2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        Log.d("TAG", "checkbox CHECKED at pos: " + position);
//                    } else if (position == selectedCheckBoxPosition3) {
//                        check1.setBackground(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        check2.setBackground(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        Log.d("TAG", "checkbox UNCHECKED at pos: " + position);
//                    }


                    if (ItemsIndiviualActivity.cake_type_id.equals("1")) {

//                        itemtype = "Whole";

                        cake_name_layout1.setVisibility(View.VISIBLE);
//                        cake_img_layout1.setVisibility(View.VISIBLE);
                        cake_desc_layout1.setVisibility(View.VISIBLE);
                        cake_price_layout1.setVisibility(View.VISIBLE);
                        cake_check_layout1.setVisibility(View.VISIBLE);

                        name_view.setVisibility(View.GONE);
                        desc_view.setVisibility(View.GONE);
                        price_view.setVisibility(View.GONE);
                        check_view.setVisibility(View.GONE);

                        cake_name_layout2.setVisibility(View.GONE);
//                        cake_img_layout2.setVisibility(View.GONE);
                        cake_desc_layout2.setVisibility(View.GONE);
                        cake_price_layout2.setVisibility(View.GONE);
                        cake_check_layout2.setVisibility(View.GONE);


                        cake_name1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getSize());
                        cake_desc1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getItemSizeDescription());
                        price1.setText("" + Constants.decimalFormat.format(sections.get(value).getItems().get(pos).getPrice().get(0).getPrice()) + " SR");

                        if (sections.get(value).getItems().get(position).getPrice().get(0).getCalories()!= null){
                            cal.setVisibility(View.VISIBLE);
                            cal.setText("" + Constants.decimalFormat.format(Float.parseFloat(String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getCalories()))) + " Cal");
                        } else {
                            cal.setVisibility(View.INVISIBLE);
                        }

                        double number, number1;
                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        price_pos = 0;
                        number = price;
                        item_price.setVisibility(View.GONE);
//                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        number1 = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                        check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        itemtype = "Whole";


                    } else if (ItemsIndiviualActivity.cake_type_id.equals("2")) {

//                        itemtype = "Sliced";

                        cake_name_layout1.setVisibility(View.GONE);
//                        cake_img_layout1.setVisibility(View.GONE);
                        cake_desc_layout1.setVisibility(View.GONE);
                        cake_price_layout1.setVisibility(View.GONE);
                        cake_check_layout1.setVisibility(View.GONE);

                        name_view.setVisibility(View.GONE);
                        desc_view.setVisibility(View.GONE);
                        price_view.setVisibility(View.GONE);
                        check_view.setVisibility(View.GONE);

                        cake_name_layout2.setVisibility(View.VISIBLE);
//                        cake_img_layout2.setVisibility(View.VISIBLE);
                        cake_desc_layout2.setVisibility(View.VISIBLE);
                        cake_price_layout2.setVisibility(View.VISIBLE);
                        cake_check_layout2.setVisibility(View.VISIBLE);

                        if (sections.get(value).getItems().get(position).getPrice().get(1).getCalories()!= null){
                            cal.setVisibility(View.VISIBLE);
                            cal.setText("" + Constants.decimalFormat.format(Float.parseFloat(String.valueOf(sections.get(value).getItems().get(position).getPrice().get(1).getCalories()))) + " Cal");
                        } else {
                            cal.setVisibility(View.INVISIBLE);
                        }

                        cake_name2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getSize());
                        cake_desc2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getItemSizeDescription());
                        price2.setText("" + Constants.decimalFormat.format(sections.get(value).getItems().get(pos).getPrice().get(1).getPrice()) + " SR");

                        double number, number1;
                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
                        price_pos = 1;
                        number = price;
                        item_price.setVisibility(View.GONE);
//                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
                        number1 = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                        check2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        itemtype = "Whole";


                    } else if (ItemsIndiviualActivity.cake_type_id.equals("1,2")) {

                        cake_name_layout1.setVisibility(View.VISIBLE);
//                        cake_img_layout1.setVisibility(View.VISIBLE);
                        cake_desc_layout1.setVisibility(View.VISIBLE);
                        cake_price_layout1.setVisibility(View.VISIBLE);
                        cake_check_layout1.setVisibility(View.VISIBLE);

                        name_view.setVisibility(View.VISIBLE);
                        desc_view.setVisibility(View.VISIBLE);
                        price_view.setVisibility(View.VISIBLE);
                        check_view.setVisibility(View.VISIBLE);

                        cake_name_layout2.setVisibility(View.VISIBLE);
//                        cake_img_layout2.setVisibility(View.VISIBLE);
                        cake_desc_layout2.setVisibility(View.VISIBLE);
                        cake_price_layout2.setVisibility(View.VISIBLE);
                        cake_check_layout2.setVisibility(View.VISIBLE);

                        double number, number1;
                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        price_pos = 0;
                        number = price;
                        item_price.setVisibility(View.GONE);
//                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        number1 = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                        check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        itemtype = "Whole";


//                        itemtype = "Whole";


                        cake_name1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getSize());
                        cake_desc1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getItemSizeDescription());
                        float mprice1 = Float.parseFloat(sections.get(value).getItems().get(pos).getPrice().get(0).getPrice());
                        double mcake_price1, mcake_price2;
                        mcake_price1 = mprice1;
                        price1.setText("" + Constants.decimalFormat.format(mcake_price1) + " SR");

                        cake_name2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getSize());
                        cake_desc2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getItemSizeDescription());
                        float mprice2 = Float.parseFloat(sections.get(value).getItems().get(pos).getPrice().get(1).getPrice());
                        mcake_price2 = mprice2;
                        price2.setText("" + Constants.decimalFormat.format(mcake_price2) + " SR");

                    }

                }


            } else {

//                view1.setVisibility(View.GONE);
                cake_layout.setVisibility(View.GONE);

                double number, number1;
                float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                price_pos = 0;
                number = price;
                item_price.setVisibility(View.VISIBLE);
                item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                number1 = priceTxt;
                total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                if (sections.get(value).getItems().get(position).getPrice().get(0).getCalories()!= null){
                    cal.setVisibility(View.VISIBLE);
                    cal.setText("" + Constants.decimalFormat.format(Float.parseFloat(String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getCalories()))) + " Cal");
                } else {
                    cal.setVisibility(View.INVISIBLE);
                }

            }

            mqty.setText("" + qty);

            mqty.setCursorVisible(false);
            mqty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mqty.setCursorVisible(true);
                }
            });

            int maxLength = 4;
            mqty.setFilters(new InputFilter[]{
                    new InputFilter.LengthFilter(maxLength)
            });


            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SR", ""));
//                            int localCountplus;

//                            localCountplus = indivialcount.get(position);

//                            if (localCountplus < 9999) {
//                                indivialcount.set(position, (localCountplus + 1));
//                                mqty.setText("" + (localCountplus + 1));
                    if (qty < 9999) {
                        qty = qty + 1;
                        mqty.setText("" + qty);
                        double number;
                        Log.i("TAG", "onClickplus: " + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice()));
                        Log.i("TAG", "piceplus: " + price_txt);
                        price_txt = price_txt + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        Log.i("TAG", "priceTxtplus: " + price_txt);
                        number = price_txt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");
//                                    mqty.setSelection(itemQty.getText().toString().length());
//                                        AddItems(position, (localCountplus + 1));
                    }

                }
            });


            mins.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Integer.parseInt(mqty.getText().toString()) > 1) {
//                                int localCount1;
//                                localCount1 = indivialcount.get(position);

//                                indivialcount.set(position, (localCount1 - 1));
                        float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SR", ""));

                        qty = qty - 1;
                        mqty.setText("" + qty);
                        double number;
                        Log.i("TAG", "onClickmins: " + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice()));
                        price_txt = price_txt - Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        Log.i("TAG", "priceTxtmins: " + price_txt);
                        number = price_txt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");
//                                        AddItems(this.getAdapterPosition(), (localCount1 - 1));
//                                qty = localCount1 - 1;
                    }

                }
            });

            mqty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (mqty.getText().toString().length() > 0) {

                        float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SR", ""));
                        Log.i("TAG", "afterTextChanged: " + price_txt);

                        enteredString = editable.toString();
                        if (enteredString.startsWith("0")) {
                            if (enteredString.length() > 1) {
                                mqty.setText(enteredString.substring(1));
                                mqty.setSelection(mqty.length());
                            }
                        }

                        qty = Integer.parseInt(mqty.getText().toString());
                        price_txt = qty * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        double number;
                        number = price_txt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");

                    } else {
                        mqty.setText("1");
                        double number;
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        number = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                    }
                }
            });


            Log.i("TAG", "onPageScrolled: " + indivialcount);
            Log.i("TAG", "onPageScrolled: " + indivialcount.size());

            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                            for (int i = 0; i < indivialcount.size(); i++) {
//
//                                AddItems(i, indivialcount.get(i));
//
//                            }

                    Qty = qty;
                    if (Qty == 0) {

                        String message;
//                    if(language.equalsIgnoreCase("En")) {
                        message = "Please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.support.v7.view.ContextThemeWrapper(context, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(message)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
//                            else if (itemfinalQty.size() == 0) {
//
//                                String message;
////                    if(language.equalsIgnoreCase("En")) {
//                                message = "Please select atleast one item";
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.support.v7.view.ContextThemeWrapper(ItemsIndiviualActivity.this, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage(message)
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
//                            }
                    else if (sections.get(value).getItems().get(position).getBindingType() == 1) {

//                                for (int i = 0; i < sections.get(value).getItems().size(); i++) {
//                                    int count = 0;
//                                    for (int j = 0; j < ItemsActivity.itemQty.size(); j++) {
//                                        if (sections.get(value).getItems().get(position).getItemId() == ItemsActivity.itemQty.get(j)) {
//                                            count = count + 1;
//                                            orderPrice = count * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
//                                        }
//                                    } //end of selected items for loop

                        Log.i("TAG", "catid" + ItemsActivity.catId);
                        if (qty > 0) {
                            HashMap<String, String> values = new HashMap<>();
                            values.put("itemId", String.valueOf(sections.get(value).getItems().get(position).getItemId()));
                            values.put("itemTypeId", sections.get(value).getItems().get(position).getPrice().get(0).getSize());
                            values.put("subCategoryId", String.valueOf(sections.get(value).getItems().get(position).getSubCategoryId()));
                            values.put("additionals", "");
                            values.put("qty", mqty.getText().toString());
                            values.put("price", sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                            values.put("additionalsPrice", "");
                            values.put("additionalsTypeId", "");
                            values.put("totalAmount", total_price.getText().toString().replace(" SR", ""));
                            if (!comment.getText().toString().equalsIgnoreCase("")) {
                                values.put("comment", comment.getText().toString());
                            } else {
                                values.put("comment", "");
                            }
                            values.put("status", "1");
                            values.put("creationDate", "14/07/2015");
                            values.put("modifiedDate", "14/07/2015");
                            values.put("categoryId", String.valueOf(ItemsActivity.catId));
                            values.put("itemName", sections.get(value).getItems().get(position).getItemName());
                            values.put("itemNameAr", sections.get(value).getItems().get(position).getItemName_Ar());
                            values.put("image", sections.get(value).getItems().get(position).getItemImage());
                            values.put("item_desc", sections.get(value).getItems().get(position).getItemDesc());
                            values.put("item_desc_Ar", sections.get(value).getItems().get(position).getItemDesc_Ar());
                            values.put("sub_itemName", "");
                            values.put("sub_itemName_Ar", "");
                            values.put("sub_itemImage", "");
                            values.put("ItemType", "");
                            values.put("ItemTypeName_ar", "");
                            values.put("sizename", String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getSizeId()));

                            myDbHelper.insertOrder(values);

                            Log.i("TAG", "item name " + sections.get(value).getItems().get(position).getItemName());
//                                        Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
//                                        startActivity(a);
                            activity.finish();
                        }
//                                }// end of master for loop
                    } else if (sections.get(value).getItems().get(position).getBindingType() == 3) {

//                                for (int i = 0; i < sections.get(value).getItems().size(); i++) {
//                                    int count = 0;
//                                    for (int j = 0; j < ItemsActivity.itemQty.size(); j++) {
//                                        if (sections.get(value).getItems().get(position).getItemId() == ItemsActivity.itemQty.get(j)) {
//                                            count = count + 1;
//                                            orderPrice = count * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
//                                        }
//                                    } //end of selected items for loop

                        Log.i("TAG", "catid" + ItemsActivity.catId);
                        if (qty > 0) {
                            HashMap<String, String> values = new HashMap<>();
                            values.put("itemId", String.valueOf(sections.get(value).getItems().get(position).getItemId()));
                            values.put("itemTypeId", sections.get(value).getItems().get(position).getPrice().get(price_pos).getSize());
                            values.put("subCategoryId", String.valueOf(sections.get(value).getItems().get(position).getSubCategoryId()));
                            values.put("additionals", "");
                            values.put("qty", mqty.getText().toString());
                            values.put("price", sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                            values.put("additionalsPrice", "");
                            values.put("additionalsTypeId", "");
                            values.put("totalAmount", total_price.getText().toString().replace(" SR", ""));
                            if (!comment.getText().toString().equalsIgnoreCase("")) {
                                values.put("comment", comment.getText().toString());
                            } else {
                                values.put("comment", "");
                            }
                            values.put("status", "1");
                            values.put("creationDate", "14/07/2015");
                            values.put("modifiedDate", "14/07/2015");
                            values.put("categoryId", String.valueOf(ItemsActivity.catId));
                            values.put("itemName", sections.get(value).getItems().get(position).getItemName());
                            values.put("itemNameAr", sections.get(value).getItems().get(position).getItemName_Ar());
                            values.put("image", sections.get(value).getItems().get(position).getItemImage());
                            values.put("item_desc", sections.get(value).getItems().get(position).getItemDesc());
                            values.put("item_desc_Ar", sections.get(value).getItems().get(position).getItemDesc_Ar());
                            values.put("sub_itemName", "");
                            values.put("sub_itemName_Ar", "");
                            values.put("sub_itemImage", "");
                            values.put("ItemType", itemtype);
                            values.put("sizename", String.valueOf(sections.get(value).getItems().get(position).getPrice().get(price_pos).getSizeId()));
                            values.put("ItemTypeName_ar", sections.get(value).getItems().get(position).getPrice().get(price_pos).getSizeName_Ar());

                            myDbHelper.insertOrder(values);

                            Log.i("TAG", "item name " + sections.get(value).getItems().get(position).getItemName());
//                                        Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
//                                        startActivity(a);
                            activity.finish();
                        }
//                                }// end of master for loop
                    }

                }
            });


        } else if (language.equalsIgnoreCase("Ar")) {

            item_name.setText(sections.get(value).getItems().get(position).getItemName_Ar());
            item_desc.setText(sections.get(value).getItems().get(position).getItemDesc_Ar());

            if (ItemsActivity.catId == 2) {

                if (sections.get(value).getSectionId() == 6) {

                    cake_layout.setVisibility(View.GONE);

                    double number, number1;
                    float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                    price_pos = 0;
                    number = price;
                    item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                    priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                    number1 = priceTxt;
                    total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                } else {

                    cake_layout.setVisibility(View.VISIBLE);
//                    view1.setVisibility(View.VISIBLE);

//                    ItemsIndiviualActivity.cake_type_id = getIntent().getStringExtra("cake_type_id");

//                    if (position == selectedCheckBoxPosition) {
//                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        size_check_box2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        Log.d("TAG", "checkbox CHECKED at pos: " + position);
//                    } else if (position == selectedCheckBoxPosition2) {
//                        size_check_box1.setBackground(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        size_check_box2.setBackground(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        Log.d("TAG", "checkbox UNCHECKED at pos: " + position);
//                    }
//
//                    if (position == selectedCheckBoxPosition1) {
//                        check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        check2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        Log.d("TAG", "checkbox CHECKED at pos: " + position);
//                    } else if (position == selectedCheckBoxPosition3) {
//                        check1.setBackground(context.getResources().getDrawable(R.drawable.item_unselected_icon));
//                        check2.setBackground(context.getResources().getDrawable(R.drawable.item_selected_icon));
//                        Log.d("TAG", "checkbox UNCHECKED at pos: " + position);
//                    }


                    if (ItemsIndiviualActivity.cake_type_id.equals("1")) {

//                        itemtype = "Whole";

                        cake_name_layout1.setVisibility(View.VISIBLE);
//                        cake_img_layout1.setVisibility(View.VISIBLE);
                        cake_desc_layout1.setVisibility(View.VISIBLE);
                        cake_price_layout1.setVisibility(View.VISIBLE);
                        cake_check_layout1.setVisibility(View.VISIBLE);

                        name_view.setVisibility(View.GONE);
                        desc_view.setVisibility(View.GONE);
                        price_view.setVisibility(View.GONE);
                        check_view.setVisibility(View.GONE);

                        cake_name_layout2.setVisibility(View.GONE);
//                        cake_img_layout2.setVisibility(View.GONE);
                        cake_desc_layout2.setVisibility(View.GONE);
                        cake_price_layout2.setVisibility(View.GONE);
                        cake_check_layout2.setVisibility(View.GONE);


                        cake_name1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getSizeName_Ar());
                        cake_desc1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getItemSizeDescription_Ar());
                        price1.setText("" + Constants.decimalFormat.format(sections.get(value).getItems().get(pos).getPrice().get(0).getPrice()) + " SR");

                        double number, number1;
                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        price_pos = 0;
                        number = price;
                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        number1 = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                        check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        itemtype = "Whole";


                    } else if (ItemsIndiviualActivity.cake_type_id.equals("2")) {

//                        itemtype = "Sliced";

                        cake_name_layout1.setVisibility(View.GONE);
//                        cake_img_layout1.setVisibility(View.GONE);
                        cake_desc_layout1.setVisibility(View.GONE);
                        cake_price_layout1.setVisibility(View.GONE);
                        cake_check_layout1.setVisibility(View.GONE);

                        name_view.setVisibility(View.GONE);
                        desc_view.setVisibility(View.GONE);
                        price_view.setVisibility(View.GONE);
                        check_view.setVisibility(View.GONE);

                        cake_name_layout2.setVisibility(View.VISIBLE);
//                        cake_img_layout2.setVisibility(View.VISIBLE);
                        cake_desc_layout2.setVisibility(View.VISIBLE);
                        cake_price_layout2.setVisibility(View.VISIBLE);
                        cake_check_layout2.setVisibility(View.VISIBLE);

                        cake_name2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getSizeName_Ar());
                        cake_desc2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getItemSizeDescription_Ar());
                        price2.setText("" + Constants.decimalFormat.format(sections.get(value).getItems().get(pos).getPrice().get(1).getPrice()) + " SR");

                        double number, number1;
                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
                        price_pos = 1;
                        number = price;
                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
                        number1 = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                        check2.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        itemtype = "Whole";


                    } else if (ItemsIndiviualActivity.cake_type_id.equals("1,2")) {

                        cake_name_layout1.setVisibility(View.VISIBLE);
//                        cake_img_layout1.setVisibility(View.VISIBLE);
                        cake_desc_layout1.setVisibility(View.VISIBLE);
                        cake_price_layout1.setVisibility(View.VISIBLE);
                        cake_check_layout1.setVisibility(View.VISIBLE);

                        name_view.setVisibility(View.VISIBLE);
                        desc_view.setVisibility(View.VISIBLE);
                        price_view.setVisibility(View.VISIBLE);
                        check_view.setVisibility(View.VISIBLE);

                        cake_name_layout2.setVisibility(View.VISIBLE);
//                        cake_img_layout2.setVisibility(View.VISIBLE);
                        cake_desc_layout2.setVisibility(View.VISIBLE);
                        cake_price_layout2.setVisibility(View.VISIBLE);
                        cake_check_layout2.setVisibility(View.VISIBLE);

                        double number, number1;
                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        price_pos = 0;
                        number = price;
                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                        number1 = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

                        check1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        size_check_box1.setImageDrawable(context.getResources().getDrawable(R.drawable.item_selected_icon));
                        itemtype = "Whole";


//                        itemtype = "Whole";


                        cake_name1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getSizeName_Ar());
                        cake_desc1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getItemSizeDescription_Ar());
                        float mprice1 = Float.parseFloat(sections.get(value).getItems().get(pos).getPrice().get(0).getPrice());
                        double mcake_price1, mcake_price2;
                        mcake_price1 = mprice1;
                        price1.setText("" + Constants.decimalFormat.format(mcake_price1) + " SR");

                        cake_name2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getSizeName_Ar());
                        cake_desc2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getItemSizeDescription_Ar());
                        float mprice2 = Float.parseFloat(sections.get(value).getItems().get(pos).getPrice().get(1).getPrice());
                        mcake_price2 = mprice2;
                        price2.setText("" + Constants.decimalFormat.format(mcake_price2) + " SR");

                    }

                }


            } else {

//                view1.setVisibility(View.GONE);
                cake_layout.setVisibility(View.GONE);

                double number, number1;
                float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                price_pos = 0;
                number = price;
                item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                number1 = priceTxt;
                total_price.setText("" + Constants.decimalFormat.format(number1) + " SR");

            }

            mqty.setText("" + qty);

            mqty.setCursorVisible(false);
            mqty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mqty.setCursorVisible(true);
                }
            });

            int maxLength = 4;
            mqty.setFilters(new InputFilter[]{
                    new InputFilter.LengthFilter(maxLength)
            });


            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SR", ""));
//                            int localCountplus;

//                            localCountplus = indivialcount.get(position);

//                            if (localCountplus < 9999) {
//                                indivialcount.set(position, (localCountplus + 1));
//                                mqty.setText("" + (localCountplus + 1));
                    if (qty < 9999) {
                        qty = qty + 1;
                        mqty.setText("" + qty);
                        double number;
                        Log.i("TAG", "onClickplus: " + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice()));
                        Log.i("TAG", "piceplus: " + price_txt);
                        price_txt = price_txt + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        Log.i("TAG", "priceTxtplus: " + price_txt);
                        number = price_txt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");
//                                    mqty.setSelection(itemQty.getText().toString().length());
//                                        AddItems(position, (localCountplus + 1));
                    }

                }
            });


            mins.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Integer.parseInt(mqty.getText().toString()) > 1) {
//                                int localCount1;
//                                localCount1 = indivialcount.get(position);

//                                indivialcount.set(position, (localCount1 - 1));
                        float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SR", ""));

                        qty = qty - 1;
                        mqty.setText("" + qty);
                        double number;
                        Log.i("TAG", "onClickmins: " + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice()));
                        price_txt = price_txt - Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        Log.i("TAG", "priceTxtmins: " + price_txt);
                        number = price_txt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");
//                                        AddItems(this.getAdapterPosition(), (localCount1 - 1));
//                                qty = localCount1 - 1;
                    }

                }
            });

            mqty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (mqty.getText().toString().length() > 0) {

                        float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SR", ""));
                        Log.i("TAG", "afterTextChanged: " + price_txt);

                        enteredString = editable.toString();
                        if (enteredString.startsWith("0")) {
                            if (enteredString.length() > 1) {
                                mqty.setText(enteredString.substring(1));
                                mqty.setSelection(mqty.length());
                            }
                        }

                        qty = Integer.parseInt(mqty.getText().toString());
                        price_txt = qty * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        double number;
                        number = price_txt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");

                    } else {
                        mqty.setText("1");
                        double number;
                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                        number = priceTxt;
                        total_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                        item_price.setText("" + Constants.decimalFormat.format(number) + " SR");
                    }
                }
            });


            Log.i("TAG", "onPageScrolled: " + indivialcount);
            Log.i("TAG", "onPageScrolled: " + indivialcount.size());

            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                            for (int i = 0; i < indivialcount.size(); i++) {
//
//                                AddItems(i, indivialcount.get(i));
//
//                            }

                    Qty = qty;
                    if (Qty == 0) {

                        String message;
                    if(language.equalsIgnoreCase("En")) {
                        message = "Please select atleast one item";
                    }
                    else{
                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
                    }
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.support.v7.view.ContextThemeWrapper(context, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                        // set title
                        if (language.equalsIgnoreCase("En")) {
                            alertDialogBuilder.setTitle("BAKERY & Co.");
                        } else {
                            alertDialogBuilder.setTitle(context.getResources().getString(R.string.app_name_ar));
                        }

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(message)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }
//                            else if (itemfinalQty.size() == 0) {
//
//                                String message;
////                    if(language.equalsIgnoreCase("En")) {
//                                message = "Please select atleast one item";
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.support.v7.view.ContextThemeWrapper(ItemsIndiviualActivity.this, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage(message)
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
//                            }
                    else if (sections.get(value).getItems().get(position).getBindingType() == 1) {

//                                for (int i = 0; i < sections.get(value).getItems().size(); i++) {
//                                    int count = 0;
//                                    for (int j = 0; j < ItemsActivity.itemQty.size(); j++) {
//                                        if (sections.get(value).getItems().get(position).getItemId() == ItemsActivity.itemQty.get(j)) {
//                                            count = count + 1;
//                                            orderPrice = count * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
//                                        }
//                                    } //end of selected items for loop

                        Log.i("TAG", "catid" + ItemsActivity.catId);
                        if (qty > 0) {
                            HashMap<String, String> values = new HashMap<>();
                            values.put("itemId", String.valueOf(sections.get(value).getItems().get(position).getItemId()));
                            values.put("itemTypeId", sections.get(value).getItems().get(position).getPrice().get(0).getSize());
                            values.put("subCategoryId", String.valueOf(sections.get(value).getItems().get(position).getSubCategoryId()));
                            values.put("additionals", "");
                            values.put("qty", mqty.getText().toString());
                            values.put("price", sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
                            values.put("additionalsPrice", "");
                            values.put("additionalsTypeId", "");
                            values.put("totalAmount", total_price.getText().toString().replace(" SR", ""));
                            if (!comment.getText().toString().equalsIgnoreCase("")) {
                                values.put("comment", comment.getText().toString());
                            } else {
                                values.put("comment", "");
                            }
                            values.put("status", "1");
                            values.put("creationDate", "14/07/2015");
                            values.put("modifiedDate", "14/07/2015");
                            values.put("categoryId", String.valueOf(ItemsActivity.catId));
                            values.put("itemName", sections.get(value).getItems().get(position).getItemName());
                            values.put("itemNameAr", sections.get(value).getItems().get(position).getItemName_Ar());
                            values.put("image", sections.get(value).getItems().get(position).getItemImage());
                            values.put("item_desc", sections.get(value).getItems().get(position).getItemDesc());
                            values.put("item_desc_Ar", sections.get(value).getItems().get(position).getItemDesc_Ar());
                            values.put("sub_itemName", "");
                            values.put("sub_itemName_Ar", "");
                            values.put("sub_itemImage", "");
                            values.put("ItemType", "");
                            values.put("ItemTypeName_ar", "");
                            values.put("sizename", String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getSizeId()));

                            myDbHelper.insertOrder(values);

                            Log.i("TAG", "item name " + sections.get(value).getItems().get(position).getItemName());
//                                        Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
//                                        startActivity(a);
                            activity.finish();
                        }
//                                }// end of master for loop
                    } else if (sections.get(value).getItems().get(position).getBindingType() == 3) {

//                                for (int i = 0; i < sections.get(value).getItems().size(); i++) {
//                                    int count = 0;
//                                    for (int j = 0; j < ItemsActivity.itemQty.size(); j++) {
//                                        if (sections.get(value).getItems().get(position).getItemId() == ItemsActivity.itemQty.get(j)) {
//                                            count = count + 1;
//                                            orderPrice = count * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
//                                        }
//                                    } //end of selected items for loop

                        Log.i("TAG", "catid" + ItemsActivity.catId);
                        if (qty > 0) {
                            HashMap<String, String> values = new HashMap<>();
                            values.put("itemId", String.valueOf(sections.get(value).getItems().get(position).getItemId()));
                            values.put("itemTypeId", sections.get(value).getItems().get(position).getPrice().get(price_pos).getSize());
                            values.put("subCategoryId", String.valueOf(sections.get(value).getItems().get(position).getSubCategoryId()));
                            values.put("additionals", "");
                            values.put("qty", mqty.getText().toString());
                            values.put("price", sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                            values.put("additionalsPrice", "");
                            values.put("additionalsTypeId", "");
                            values.put("totalAmount", total_price.getText().toString().replace(" SR", ""));
                            if (!comment.getText().toString().equalsIgnoreCase("")) {
                                values.put("comment", comment.getText().toString());
                            } else {
                                values.put("comment", "");
                            }
                            values.put("status", "1");
                            values.put("creationDate", "14/07/2015");
                            values.put("modifiedDate", "14/07/2015");
                            values.put("categoryId", String.valueOf(ItemsActivity.catId));
                            values.put("itemName", sections.get(value).getItems().get(position).getItemName());
                            values.put("itemNameAr", sections.get(value).getItems().get(position).getItemName_Ar());
                            values.put("image", sections.get(value).getItems().get(position).getItemImage());
                            values.put("item_desc", sections.get(value).getItems().get(position).getItemDesc());
                            values.put("item_desc_Ar", sections.get(value).getItems().get(position).getItemDesc_Ar());
                            values.put("sub_itemName", "");
                            values.put("sub_itemName_Ar", "");
                            values.put("sub_itemImage", "");
                            values.put("ItemType", itemtype);
                            values.put("sizename", String.valueOf(sections.get(value).getItems().get(position).getPrice().get(price_pos).getSizeId()));
                            values.put("ItemTypeName_ar", sections.get(value).getItems().get(position).getPrice().get(price_pos).getSizeName_Ar());

                            myDbHelper.insertOrder(values);

                            Log.i("TAG", "item name " + sections.get(value).getItems().get(position).getItemName());
//                                        Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
//                                        startActivity(a);
                            activity.finish();
                        }
//                                }// end of master for loop
                    }

                }
            });

        }


//        mview.setVisibility(View.GONE);

        if (zoom) {

//            PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(mitem_image);

        }


        view.addView(myImageLayout);
        return myImageLayout;
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public static void showViewContent(int position) {


//        position = pos

    }

    private void AddItems(final int position, final int quantity) {


        int localSize = ItemsActivity.itemQty.size();
        ArrayList<String> localArray = new ArrayList<>();
        localArray.addAll(ItemsActivity.itemQty);
        for (int i = 0; i < localSize; i++) {
            if (localArray.get(i).equals(sections.get(value).getItems().get(position).getItemId())) {
                Qty = Qty - 1;
                orderPrice = orderPrice - Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                ItemsActivity.itemQty.remove(sections.get(value).getItems().get(position).getItemId());
            } else {
//            Do nothing
            }
        }

        for (int j = 0; j < quantity; j++) {
            Qty = Qty + 1;
            orderPrice = orderPrice + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
            ItemsActivity.itemQty.add(String.valueOf(sections.get(value).getItems().get(position).getItemId()));

            Log.i("TAG", "AddItems: " + Qty);
//            plus.setClickable(true);
//            int qty = Integer.parseInt(mitemQty.getText().toString());
//            if (qty == 9999) {
//                plus.setClickable(false);
//            }
        }


        itemfinalQty.add(String.valueOf(Qty));
        ItemsActivity.itemfinalprice.add(String.valueOf(orderPrice));

//        Log.i("TAG1", "AddItems: " + PastriesSelection.itemfinalQty.size());
        Log.i("TAG1", "onResume: " + ItemsActivity.itemQty.size());
        if (itemfinalQty.size() == 0) {

            orderPrice = 0;
            Qty = 0;
//                try {
//                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                    if (Qty == 0) {
//                        orderPrice.setText("-");
//                        orderQuantity.setText("-");
//                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } else {
//                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
//                        PastriesSelection.orderQuantity.setText("" + Qty);
//                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
        } else {
//                try {
//                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                    if (.Qty == 0) {
//                        PastriesSelection.orderPrice.setText("-");
//                        PastriesSelection.orderQuantity.setText("-");
//                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } else {
//                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(.orderPrice) + " SR");
//                        PastriesSelection.orderQuantity.setText("" + .Qty);
//                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
        }


    }

}
