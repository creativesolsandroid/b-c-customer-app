package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.SubCategories;

import java.util.ArrayList;


public class ViewPagerAdapter extends PagerAdapter {

    //    private static ArrayList<Gallery> images;
    ArrayList<String> img = new ArrayList<>();
    ArrayList<String> mediaid = new ArrayList<>();


    private LayoutInflater inflater;
    private static Context context;
    Activity parent;
    int pos, value;
    String language;
    String cat_name, Image, dec, mitemname, dec_ar, mitemname_ar;
    ArrayList<ItemsList.SubCategoryItems> sections = new ArrayList<>();
    Activity parentActivity;
    TextView mcancel, mcat_name, mview_page_count, mitem_name, mitem_desc;
    public static ImageView mshare, mitem_image;
    RelativeLayout mlayout;
    public static View mview;
    String info;
    private float mBaseElevation;

    boolean zoom = true;

    private static final String TAG = "Touch";
    @SuppressWarnings("unused")
    private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;

    // These matrices will be used to scale points of the image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // these PointF objects are used to record the point(s) the user is touching
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    float dptopixels;

    public ViewPagerAdapter(Context context, ArrayList<ItemsList.SubCategoryItems> orderList, int value, String language) {
        this.context = context;
        this.parent = parentActivity;
        this.language = language;
        this.value = value;
        this.sections = orderList;
        this.info = info;
//        this.dptopixels = dpToPixels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        if(youtubeFragment != null){
////            youtubeFragment.loadUrl("");
//            youtubeFragment.stopLoading();
////            youtubeFragment.destroy();
//        }
        container.removeView((ViewGroup) object);
    }

//    public void destroyItem(ViewGroup container, int position, Object object) {
//        destroyItem((View) container, position, object);
//    }

    public void destroyItem(View container, int position, Object object) {
        try {
            throw new UnsupportedOperationException("Required method destroyItem was not overridden");
        } catch (Exception e) {

        }
        destroyItem((ViewGroup) container, position, object);
    }


    @Override
    public int getCount() {

        int count = 0;
        count = sections.size();
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        View myImageLayout = inflater.inflate(R.layout.view_page, view, false);

        mitem_image = myImageLayout.findViewById(R.id.image_popup);
        mlayout = myImageLayout.findViewById(R.id.layout);
        mview = myImageLayout.findViewById(R.id.view);

//        Log.i("TAG", "pagechangingadapter: " + position_changing);

        if (language.equalsIgnoreCase("Ar")) {
            mlayout.setRotationY(180);
        }

        Glide.with(context).load(Constants.IMAGE_URL + sections.get(position).getSubCategoryBackgroundImage()).into(mitem_image);

//        if (position == ItemsActivity.scrollView.getCurrentItem()){
//
//            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
//
//            // Set TextView layout margin 25 pixels to all side
//            // Left Top Right Bottom Margin
//            lp.setMargins(0,25,0,0);
//
//            // Apply the updated layout parameters to TextView
//            mitem_image.setLayoutParams(lp);
//
//            Log.i("TAG", "instantiateItem: " + ItemsActivity.scrollView.getCurrentItem() + " " + position);
//
//
//        }


        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public static void showViewContent(int position) {


//        position = pos

    }
}
