package com.cs.bakeryco.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.MainCategories;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.bakeryco.activities.CategoriesListActivity.catId;

/**
 * Created by CS on 28-02-2017.
 */

public class SubMenuAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ItemsList.Items> orderList = new ArrayList<>();
    private static DataBaseHelper myDbHelper;
    String language;
    int value;


    public SubMenuAdapter(Context context, ArrayList<ItemsList.Items> groups, int value, String language) {
        this.context = context;
        this.orderList = groups;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.value = value;
        myDbHelper = new DataBaseHelper(context);
    }


    public int getCount() {
        return orderList.get(value).getSubCategoryItems().size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title, subcat_dec;
        ImageView itemImage;
        RelativeLayout child_layout;
//        ArrayList<String> list = new ArrayList<>();
//        ArrayList<String> left_list = new ArrayList<>();
//        ArrayList<String> right_list = new ArrayList<>();
        //ImageButton plus;
//        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.categories_recycler_list, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.categories_recycler_list_arabic, null);
            }

            holder.title = (TextView) convertView.findViewById(R.id.sub_cat_name);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.sub_cat_img);
            holder.child_layout = (RelativeLayout) convertView.findViewById(R.id.child_layout);
            holder.subcat_dec = convertView.findViewById(R.id.sub_cat_desc);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position == 0) {

            convertView.setPadding(16, 16, 16, 16);

        } else {

            convertView.setPadding(16, 0, 16, 16);

        }

        if (language.equalsIgnoreCase("Ar")) {
//            holder.child_layout.setRotationY(180);
        }
        if (language.equalsIgnoreCase("En")) {

            holder.title.setText(orderList.get(value).getSubCategoryItems().get(position).getSubCategoryName());
            Log.e("TAG", "title" + orderList.get(value).getSubCategoryItems().get(position).getSubCategoryName());
            holder.subcat_dec.setText("" + orderList.get(value).getSubCategoryItems().get(position).getSubCategoryDesc());

        } else if (language.equalsIgnoreCase("Ar")) {

            holder.title.setText(orderList.get(value).getSubCategoryItems().get(position).getSubCategoryName_Ar());
            holder.subcat_dec.setText("" + orderList.get(value).getSubCategoryItems().get(position).getSubCategoryDesc_Ar());

        }

        Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getSubCategoryItems().get(position).getSubCategoryBackgroundImage()).into(holder.itemImage);

        try {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            PastriesSelection.orderPrice.setText("-");
            PastriesSelection.orderQuantity.setText("-");
            PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.i("TAG", "subitemid " + orderList.get(value).getSubCategoryItems().get(position).getMainCategoryId());
        Log.i("TAG", "subitemqty " + myDbHelper.getSubcatOrderCount(String.valueOf(orderList.get(value).getSubCategoryItems().get(position).getSubCategoryId())));

        holder.child_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (catId == 1) {
                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(1).getSubCategoryItems().get(position).getSections());
                    intent.putExtra("postion", position);
                    intent.putExtra("catId", 1);
                    intent.putExtra("main_list", orderList);
                    Log.i("TAG", "onClick: " + position);
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(1).getSubCategoryItems().get(position).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(1).getSubCategoryItems().get(position).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);
                } else if (catId == 2) {
                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(0).getSubCategoryItems().get(position).getSections());
                    intent.putExtra("postion", position);
                    intent.putExtra("catId", 2);
                    intent.putExtra("main_list", orderList);
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(0).getSubCategoryItems().get(position).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(0).getSubCategoryItems().get(position).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);
                } else if (catId == 3) {
                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(4).getSubCategoryItems().get(position).getSections());
                    intent.putExtra("postion", position);
                    intent.putExtra("catId", 3);
                    intent.putExtra("main_list", orderList);
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(4).getSubCategoryItems().get(position).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(4).getSubCategoryItems().get(position).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);
                } else if (catId == 4) {
                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(2).getSubCategoryItems().get(position).getSections());
                    intent.putExtra("postion", position);
                    intent.putExtra("catId", 4);
                    intent.putExtra("main_list", orderList);
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(2).getSubCategoryItems().get(position).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(2).getSubCategoryItems().get(position).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);
                } else if (catId == 6) {

                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(3).getSubCategoryItems().get(position).getSections());
                    intent.putExtra("postion", position);
                    intent.putExtra("catId", 6);
                    intent.putExtra("main_list", orderList);
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(3).getSubCategoryItems().get(position).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(3).getSubCategoryItems().get(position).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);
                } else if (catId == 13) {
                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(5).getSubCategoryItems().get(position).getSections());
                    intent.putExtra("postion", position);
                    intent.putExtra("catId", 13);
                    intent.putExtra("main_list", orderList);
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(5).getSubCategoryItems().get(position).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(5).getSubCategoryItems().get(position).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);
                } else if (catId == 14) {
                    Intent intent = new Intent(context, ItemsActivity.class);
                    intent.putExtra("list", orderList.get(6).getSubCategoryItems().get(position).getSections());
                    intent.putExtra("postion", position);
                    intent.putExtra("catId", 14);
                    intent.putExtra("main_list", orderList);
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("title", orderList.get(6).getSubCategoryItems().get(position).getSubCategoryName());
                    } else {
                        intent.putExtra("title", orderList.get(6).getSubCategoryItems().get(position).getSubCategoryName_Ar());
                    }
                    context.startActivity(intent);
                }

            }
        });

        return convertView;
    }

}
