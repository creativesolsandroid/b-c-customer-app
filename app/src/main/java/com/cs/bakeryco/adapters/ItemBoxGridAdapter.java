package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.support.v4.view.PagerAdapter;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsBoxActivity;
import com.cs.bakeryco.model.ItemsList;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.ItemsBoxActivity.box_count;
import static com.cs.bakeryco.activities.ItemsBoxActivity.boxpos;
import static com.cs.bakeryco.activities.ItemsBoxActivity.no_of_items;
import static com.cs.bakeryco.activities.ItemsBoxActivity.qty;
import static com.cs.bakeryco.activities.ItemsBoxActivity.selectedCount;
import static com.cs.bakeryco.activities.ItemsBoxActivity.selectedItems;
import static com.cs.bakeryco.activities.ItemsBoxActivity.subitemQty;
import static com.cs.bakeryco.activities.ItemsBoxActivity.title_name;

public class ItemBoxGridAdapter extends PagerAdapter {

    //    private static ArrayList<Gallery> images;
    ArrayList<String> img = new ArrayList<>();
    ArrayList<String> mediaid = new ArrayList<>();


    private LayoutInflater inflater;
    private static Context context;
    Activity parent;
    int pos, value;
    String language;
    String cat_name, Image, dec, mitemname, dec_ar, mitemname_ar;
    ArrayList<ItemsList.SubItems> orderLists = new ArrayList<>();
    String enteredString;

    Activity parentActivity;
    TextView mcancel, mcat_name, mview_page_count, mitem_name, mitem_desc;
    public static ImageView mshare, mitem_image;
    CardView mlayout;
    public static View mview;
    String info;
    private float mBaseElevation;
    Activity activity;

    boolean zoom = true;

    private static final String TAG = "Touch";
    @SuppressWarnings("unused")
    private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;

    // These matrices will be used to scale points of the image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // these PointF objects are used to record the point(s) the user is touching
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    float dptopixels;

    public ItemBoxGridAdapter(Context context, ArrayList<ItemsList.SubItems> orderList, Activity activity, String language) {
        this.context = context;
        this.parent = parentActivity;
        this.language = language;
        this.value = value;
        this.orderLists = orderList;
        this.activity = activity;
        this.info = info;
//        this.dptopixels = dpToPixels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        if(youtubeFragment != null){
////            youtubeFragment.loadUrl("");
//            youtubeFragment.stopLoading();
////            youtubeFragment.destroy();
//        }
        container.removeView((ViewGroup) object);
    }

//    public void destroyItem(ViewGroup container, int position, Object object) {
//        destroyItem((View) container, position, object);
//    }

    public void destroyItem(View container, int position, Object object) {
        try {
            throw new UnsupportedOperationException("Required method destroyItem was not overridden");
        } catch (Exception e) {

        }
        destroyItem((ViewGroup) container, position, object);
    }


    @Override
    public int getCount() {

        int count = 0;
        count = orderLists.size();
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View myImageLayout = null;

        if (language.equalsIgnoreCase("En")) {
            myImageLayout = inflater.inflate(R.layout.box_grid_list, view, false);
        } else {
            myImageLayout = inflater.inflate(R.layout.box_grid_list_arabic, view, false);
        }

        TextView box_item_name = (TextView) myImageLayout.findViewById(R.id.box_item_name);
        TextView box_item_desc = (TextView) myImageLayout.findViewById(R.id.box_item_desc);
        final EditText mqty = (EditText) myImageLayout.findViewById(R.id.qty);

        ImageView box_item_img = (ImageView) myImageLayout.findViewById(R.id.box_img);
        ImageView plus = (ImageView) myImageLayout.findViewById(R.id.plus);
        ImageView mins = (ImageView) myImageLayout.findViewById(R.id.mins);

        if (language.equalsIgnoreCase("En")) {
            box_item_name.setText("" + orderLists.get(position).getSubItemName());
            box_item_desc.setText("" + orderLists.get(position).getSubItemDesc());
        } else {
            box_item_name.setText("" + orderLists.get(position).getSubItemName_Ar());
            box_item_desc.setText("" + orderLists.get(position).getSubItemDesc_Ar());
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int view_pager_hieght = width - 10;

        int main_hieght = width - 120;

        int size_with = width - 100;

        Log.i("TAG", "main_hieght: " + main_hieght);
        Log.i("TAG", "view_pager_hieght: " + view_pager_hieght);

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) box_item_img.getLayoutParams();
        layoutParams.height = main_hieght;
        box_item_img.setLayoutParams(layoutParams);

        Glide.with(context).load(Constants.IMAGE_URL + orderLists.get(position).getSubItemImage()).into(box_item_img);
        mqty.setText("" + selectedCount.get(position));

        mqty.setCursorVisible(false);
        mqty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mqty.setCursorVisible(true);
            }
        });

        mqty.setText("" + selectedCount.get(position));

        mqty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (mqty.getText().toString().length() > 0) {

                    enteredString = editable.toString();
                    if (enteredString.startsWith("0")) {
                        if (enteredString.length() > 1) {
                            mqty.setText(enteredString.substring(1));
                            mqty.setSelection(mqty.length());
                        }
                    }
                    qty = 0;
                    for (Integer i : selectedCount) {
                        qty = qty + i;
                    }
                    Log.i("TAG", "qty: " + qty);

//                if (orderLists.get(getAdapterPosition()).getSubitems_qty() > Integer.parseInt(mqty.getText().toString())) {
//                    int localCount = orderLists.get(getAdapterPosition()).getSubitems_qty() - Integer.parseInt(mqty.getText().toString());
//                    qty = qty - localCount;
//                    Log.i("TAG", "subitemqty > edittxtcount " + qty);
//                } else if (orderLists.get(getAdapterPosition()).getSubitems_qty() < Integer.parseInt(mqty.getText().toString())) {
//                    int localCount = Integer.parseInt(mqty.getText().toString()) - orderLists.get(getAdapterPosition()).getSubitems_qty();
//                    qty = qty + localCount;
//                    Log.i("TAG", "subitemqty < edittxtcount " + qty);
//                    Log.i("TAG", "localcount " + localCount);
//                } else {
//                    qty = qty;
//                    Log.i("TAG", "subitemqty = edittxtcount " + qty);
//                }


                    Log.i("TAG", "afterTextChanged: " + no_of_items);
                    Log.i("TAG", "afterTextChanged qty: " + qty);
//                if (qty < Integer.parseInt(no_of_items)) {
//                    orderLists.get(getAdapterPosition()).setSubitems_qty(Integer.parseInt(mqty.getText().toString()));
////                        AddItems(getAdapterPosition(), orderLists.get(getAdapterPosition()).getSubitems_qty());
//                } else {
////                    mqty.setText("" + orderLists.get(getAdapterPosition()).getSubitems_qty());
////                    mqty.setSelection(mqty.length());
////                        if (language.equals("En")) {
//                    showErrorDialog("Please enter less than or equals to box size");
////                        } else {
////                    showErrorDialog("Please enter less than or equals to box size");
////                        }
//                }

                    if (selectedCount.get(position) > Integer.parseInt(mqty.getText().toString())) {
                        int localCount = selectedCount.get(position) - Integer.parseInt(mqty.getText().toString());
                        qty = qty - localCount;
                    } else if (selectedCount.get(position) < Integer.parseInt(mqty.getText().toString())) {
                        int localCount = Integer.parseInt(mqty.getText().toString()) - selectedCount.get(position);
                        qty = qty + localCount;
                    } else {
                        qty = qty;
                    }

                    if (qty <= no_of_items) {
                        selectedCount.set(position, Integer.parseInt(mqty.getText().toString()));
                        if (language.equalsIgnoreCase("En")) {
                            box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
                        } else {
                            box_count.setText("الكمية : " + ItemsBoxActivity.qty + " / " + no_of_items);
                        }
                        AddItems(position, selectedCount.get(position));
                    } else {
                        mqty.setText("" + selectedCount.get(position));
                        mqty.setSelection(mqty.length());
                        if (language.equalsIgnoreCase("En")) {
                            box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
                        } else {
                            box_count.setText("الكمية : " + ItemsBoxActivity.qty + " / " + no_of_items);
                        }
                    if (language.equals("En")) {
                        showErrorDialog("Please enter less than or equals to box size");
                    } else {
                        showErrorDialog("Please enter less than or equals to box size");
                    }
                    }
                } else {
                    selectedCount.set(position, 0);
                    AddItems(position, 0);
                }

            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                qty = 0;
                for (Integer i : selectedCount) {
                    qty = qty + i;
                }

                qty = qty + 1;
                if (qty <= no_of_items) {
                    int localCount = selectedCount.get(position);
                    selectedCount.set(position, (localCount + 1));
                    mqty.setText("" + (localCount + 1));
                    mqty.setSelection(mqty.length());
                    if (language.equalsIgnoreCase("En")) {
                        box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
                    } else {
                        box_count.setText("الكمية : " + ItemsBoxActivity.qty + " / " + no_of_items);
                    }
                    AddItems(position, (localCount + 1));

                } else {
                    if (language.equals("En")) {
                    showErrorDialog("You are done with maximum " + no_of_items + " " + title_name + " per box, to order more use other box.");
                    } else {
                        showErrorDialog("Please enter less than or equals to box size");
                    }
                }

            }
        });

        mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mqty.length() > 0) {
                    if (Integer.parseInt(mqty.getText().toString()) > 0) {
                        int localCount = selectedCount.get(position);
                        selectedCount.set(position, (localCount - 1));

                        mqty.setText("" + (localCount - 1));
                        mqty.setSelection(mqty.length());
                        if (language.equalsIgnoreCase("En")) {
                            box_count.setText("Quantity : " + ItemsBoxActivity.qty + " / " + no_of_items);
                        } else {
                            box_count.setText("الكمية : " + ItemsBoxActivity.qty + " / " + no_of_items);
                        }
                        AddItems(position, (localCount - 1));
                        Log.i("TAG", "onClick: " + localCount);
                    }
                }

            }
        });

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public static void showViewContent(int position) {


//        position = pos

    }

    private void AddItems(int position, int quantity) {

        int localSize = selectedItems.size();
        ArrayList<ItemsList.SubItems> localArray = new ArrayList<>();
        localArray.addAll(selectedItems);
        for (int i = 0; i < localSize; i++) {
            if (localArray.get(i).equals(orderLists.get(position))) {
                selectedItems.remove(orderLists.get(position));
                subitemQty.remove(orderLists.get(position).getSubItemName());
                String pos = Integer.toString(position);
                boxpos.remove(pos);
                Log.i("TAG", "box pos remove " + boxpos);

            } else {
//            Do nothing
            }
        }

        for (int j = 0; j < quantity; j++) {
            selectedItems.add(orderLists.get(position));
            subitemQty.add(orderLists.get(position).getSubItemName());
            String pos = Integer.toString(position);
            if (boxpos != null && !boxpos.contains(pos)) {
                boxpos.add(pos);
                Log.i("TAG", "box pos " + boxpos);

            }
//           else {
//                boxpos.remove(pos);
//            }
        }
//        notifyItemChanged(position);
    }

    public void showErrorDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Material_Light_Dialog));

        alertDialogBuilder.setTitle("BAKERY & Co.");

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
