package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.model.Address;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.bakeryco.activities.MyAddressActivity.mobile;
import static com.cs.bakeryco.activities.MyAddressActivity.toConfirmOrder;

public class MyAddressAdapter extends BaseAdapter {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<Address> addressArrayList = new ArrayList<>();
    private Activity activity;
    public LayoutInflater inflater;
    String language;
    String userId;
    SharedPreferences userPrefs;


    public MyAddressAdapter(Context context, ArrayList<Address> addressArrayList, Activity activity, String language){
        this.context = context;
        this.activity = activity;
        this.addressArrayList = addressArrayList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return addressArrayList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {

        TextView addressName, addressBody, phone_no, flatNo, landmark;
//        Button editButton, deleteButton;
        LinearLayout address_layout;
    }

    public View getView(final int position, View itemView, ViewGroup parent) {
        final ViewHolder holder;
        if (itemView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                itemView = inflater.inflate(R.layout.list_my_address, null);
            } else {
                itemView = inflater.inflate(R.layout.list_my_address_arabic, null);
            }

            holder.addressName = (TextView) itemView.findViewById(R.id.address_name);
            holder.addressBody = (TextView) itemView.findViewById(R.id.address_body);
//            holder.deleteButton = (Button) itemView.findViewById(R.id.address_delete_button);
//            holder.editButton = (Button) itemView.findViewById(R.id.address_edit_button);
            holder.flatNo = (TextView) itemView.findViewById(R.id.flat_no);
            holder.landmark = (TextView) itemView.findViewById(R.id.landmark);
            holder.address_layout = (LinearLayout) itemView.findViewById(R.id.address_layout);

            holder.addressName.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.addressBody.setBackgroundColor(context.getResources().getColor(R.color.white));

            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        Address myAddress = addressArrayList.get(position);
        holder.addressName.setText(myAddress.getAddressName());
        String address = myAddress.getAddress();
        holder.addressBody.setText(address);

        if (language.equalsIgnoreCase("En")) {
            if (myAddress.getHouseNo().equals("null") || myAddress.getHouseNo().equals("")) {
                holder.flatNo.setText("Flat No : ....");
            } else {
                holder.flatNo.setText("Flat No : " + myAddress.getHouseNo());
            }
            if (myAddress.getLandmark().equals("null") || myAddress.getLandmark().equals("")) {
                holder.landmark.setText("Landmark : ....");
            } else {
                holder.landmark.setText("Landmark : " + myAddress.getLandmark());
            }
        } else if (language.equalsIgnoreCase("Ar")) {

            if (myAddress.getHouseNo().equals("null") || myAddress.getHouseNo().equals("")) {
                holder.flatNo.setText("شقة رقم : ...");
            } else {
                holder.flatNo.setText("شقة رقم : " + myAddress.getHouseNo());
            }
            if (myAddress.getLandmark().equals("null") || myAddress.getLandmark().equals("")) {
                holder.flatNo.setText("معلم أو مكان معروف : ...");
            } else {
                holder.landmark.setText("معلم أو مكان معروف : " + myAddress.getLandmark());
            }
        }

        holder.address_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (toConfirmOrder) {
//                    mPosition = position;

                    Location me = new Location("");
                    Location dest = new Location("");

                    me.setLatitude(Double.parseDouble(addressArrayList.get(position).getLatitude()));
                    me.setLongitude(Double.parseDouble(addressArrayList.get(position).getLongitude()));

                    if (Constants.store_lat == null){

                        Constants.store_lat = 24.533966;
                        Constants.store_longi = 46.899342;

                    }

                    dest.setLatitude(Constants.store_lat);
                    dest.setLongitude(Constants.store_longi);

                    float dist = (me.distanceTo(dest)) / 1000;
                    Log.i("TAG", "onClick: " + me);
                    if (dist<=Constants.delivery_dist){

                        Constants.address_id = Integer.parseInt(addressArrayList.get(position).getId());
                        Constants.address = addressArrayList.get(position).getAddress();
                        Constants.user_lat = Double.parseDouble(addressArrayList.get(position).getLatitude());
                        Constants.user_longi = Double.parseDouble(addressArrayList.get(position).getLongitude());
                        Constants.address_name = addressArrayList.get(position).getAddressName();
//                        Intent intent = new Intent();
//                        activity.setResult(RESULT_OK, intent);
                        activity.finish();
                    } else {

                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog("The distance too far from restaurant please select near restaurant", activity);
                        } else {
                            Constants.showOneButtonAlertDialog("المسافة بعيده جداً عن المطعم. الرجاء اختيار مطعم أقرب", activity);
                        }

                    }
//                    new GetCurrentTime().execute();
//                    Intent intent = new Intent(AddressActivity.this, OrderConfirmation.class);
//                    intent.putExtra("your_address", addressList.get(mPosition).getAddress());
//                    intent.putExtra("address_id", addressList.get(mPosition).getId());
//                    intent.putExtra("user_latitude", addressList.get(mPosition).getLatitude());
//                    intent.putExtra("user_longitude", addressList.get(mPosition).getLongitude());
//                    intent.putExtra("landmark", addressList.get(mPosition).getLandmark());
//                    intent.putExtra("order_type", Constants.ORDER_TYPE);
//                    startActivity(intent);
                }

            }
        });

//        holder.editButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectedPosition = position;
//                startAddressActivity();
//            }
//        });
//
//        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                selectedPosition = position;
//                showTwoButtonAlertDialog(context.getResources().getString(R.string.delete_alert),
//                        context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.yes),
//                        context.getResources().getString(R.string.no), activity);
//            }
//        });

//        holder.phone_no.setText("+" + mobile);
        return itemView;
    }

//    private class DeleteAddressApi extends AsyncTask<String, Integer, String> {
//
//        ACProgressFlower loaderDialog;
////        AlertDialog  = null;
//        String inputStr;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareDeleteAddressJson();
//            loaderDialog = new ACProgressFlower.Builder(context)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            loaderDialog.show();
//
////            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
////            // ...Irrelevant code for customizing the buttons and title
////            LayoutInflater inflater = activity.getLayoutInflater();
////            int layout = R.layout.loder_dialog;
////            View dialogView = inflater.inflate(layout, null);
////            dialogBuilder.setView(dialogView);
////            dialogBuilder.setCancelable(true);
////
////            loaderDialog = dialogBuilder.create();
////            loaderDialog.show();
////            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
////            Window window = loaderDialog.getWindow();
////            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////            lp.copyFrom(window.getAttributes());
////            //This makes the dialog take up the full width
////            Display display = activity.getWindowManager().getDefaultDisplay();
////            Point size = new Point();
////            display.getSize(size);
////            int screenWidth = size.x;
////
////            double d = screenWidth*0.85;
////            lp.width = (int) d;
////            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
////            window.setAttributes(lp);
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<AddAddress> call = apiService.addAddress(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<AddAddress>() {
//                @Override
//                public void onResponse(Call<AddAddress> call, Response<AddAddress> response) {
//                    if(response.isSuccessful()){
//                        AddAddress addressResponse = response.body();
//                        try {
//                            if(addressResponse.getStatus()){
//                                addressArrayList.remove(selectedPosition);
//                                notifyDataSetChanged();
//                            }
//                            else {
//                                if (language.equalsIgnoreCase("En")) {
//                                    String failureResponse = addressResponse.getMessage();
//                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
//                                            context.getResources().getString(R.string.ok), activity);
//                                } else {
//                                    String failureResponse = addressResponse.getMessageAr();
//                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name_ar),
//                                            context.getResources().getString(R.string.ok_ar), activity);
//
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            if (language.equalsIgnoreCase("En")) {
//                                Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//                    else{
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<AddAddress> call, Throwable t) {
//                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if(loaderDialog != null){
//                loaderDialog.dismiss();
//            }
//        }
//    }
//
//    private String prepareDeleteAddressJson(){
//        JSONObject addressObj = new JSONObject();
//
//        try {
//            addressObj.put("AddressId", addressArrayList.get(selectedPosition).getAddressid());
//            addressObj.put("UserId", userId);
////            addressObj.put("HouseNo", addressArrayList.get(selectedPosition).getHouseno());
////            addressObj.put("HouseName", addressArrayList.get(selectedPosition).getAddressname());
////            addressObj.put("LandMark", addressArrayList.get(selectedPosition).getLandmark());
////            addressObj.put("AddressType", "Home");
////            addressObj.put("Address", strAddress);
////            addressObj.put("Latitude", lat);
////            addressObj.put("Longitude", longi);
//            addressObj.put("IsActive", false);
//
//            Log.d(TAG, "prepareDeleteAddressJson: "+addressObj.toString());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return addressObj.toString();
//    }
//
//    private void startAddressActivity(){
//        Intent intent = new Intent(activity, AddAddressActivity.class);
//        intent.putExtra("edit", true);
//        intent.putExtra("AddressId",addressArrayList.get(selectedPosition).getAddressid());
//        intent.putExtra("HouseNo", addressArrayList.get(selectedPosition).getHouseno());
//        intent.putExtra("HouseName", addressArrayList.get(selectedPosition).getAddressname());
//        intent.putExtra("LandMark", addressArrayList.get(selectedPosition).getLandmark());
//        intent.putExtra("AddressType", "Home");
//        intent.putExtra("Address", addressArrayList.get(selectedPosition).getAddress());
//        intent.putExtra("Latitude",addressArrayList.get(selectedPosition).getLatitude());
//        intent.putExtra("Longitude",addressArrayList.get(selectedPosition).getLongitude());
//        ((Activity) context).startActivityForResult(intent, ADD_ADDRESS_INTENT);
//    }
//
//    public void showTwoButtonAlertDialog(String descriptionStr, String titleStr, String positiveStr, String negativeStr, final Activity context){
//        AlertDialog customDialog = null;
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = context.getLayoutInflater();
//        int layout = R.layout.alert_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(false);
//
//        TextView title = (TextView) dialogView.findViewById(R.id.title);
//        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//
//        title.setText(titleStr);
//        yes.setText(positiveStr);
//        no.setText(negativeStr);
//        desc.setText(descriptionStr);
//
//        customDialog = dialogBuilder.create();
//        customDialog.show();
//
//        final AlertDialog finalCustomDialog = customDialog;
//        yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finalCustomDialog.dismiss();
//                String networkStatus = NetworkUtil.getConnectivityStatusString(context);
//                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    new DeleteAddressApi().execute();
//                }
//                else{
//                    if(language.equalsIgnoreCase("En")) {
//                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        });
//
//        no.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finalCustomDialog.dismiss();
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = customDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = context.getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }
}
