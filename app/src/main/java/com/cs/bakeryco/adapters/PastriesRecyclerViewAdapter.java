package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.InfoPopup;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubItems;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.PastriesSelection.boxSize;
import static com.cs.bakeryco.activities.PastriesSelection.boxpos;
import static com.cs.bakeryco.activities.PastriesSelection.catId;
import static com.cs.bakeryco.activities.PastriesSelection.main_list;
import static com.cs.bakeryco.activities.PastriesSelection.maincatid;
import static com.cs.bakeryco.activities.PastriesSelection.mbox_list;
import static com.cs.bakeryco.activities.PastriesSelection.position1;
import static com.cs.bakeryco.activities.PastriesSelection.sectionname;
import static com.cs.bakeryco.activities.PastriesSelection.sections;
import static com.cs.bakeryco.activities.PastriesSelection.selectedCount;
import static com.cs.bakeryco.activities.PastriesSelection.subitemQty;
import static com.cs.bakeryco.activities.PastriesSelection.totalCount;
import static com.cs.bakeryco.activities.PastriesSelection.visible;

public class PastriesRecyclerViewAdapter extends RecyclerView.Adapter<PastriesRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Section> orderList = new ArrayList<>();
    private LayoutInflater mInflater;
    int value;
    int value1;
    String language;
    String enteredString ;
    Activity parentActivity;
    public static Context context;
    int editTextQty;
    String subitemid, subitemcount;
    int focusPos = -1;
    public static LinearLayout mlayout;
    String title;
    ViewPagerAdapter mAdapter;
    int qty;

    public PastriesRecyclerViewAdapter(Context context, String title, ArrayList<Section> orderList, int value, int value1, String language,
                                       Activity parentActivity) {
        this.mInflater = LayoutInflater.from(context);
        this.title = title;
        this.orderList = orderList;
        this.value = value;
        this.value1 = value1;
        this.parentActivity = parentActivity;
        this.language = language;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (language.equalsIgnoreCase("En")) {
            view = mInflater.inflate(R.layout.pastries_selection_list, parent, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            view = mInflater.inflate(R.layout.pastries_selection_list_arabic, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.price.setVisibility(View.GONE);
        Log.i("TAG", "pastries ");
        Log.i("TAG", "pastries size ");

        Log.i("TAG", "pastries " + mbox_list.getVisibility());

        PastriesSelection.pastries = true;


        if (visible) {

            showErrorDialog("Please select the size to proceed with item selection");

        } else {
            if (language.equalsIgnoreCase("En")) {
                int pos = 0;
                if (orderList.get(value).getChildItems().get(value1).getSubItems().size() == 0) {
                } else {
                    holder.title.setText(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
//                notifyDataSetChanged();
                }
            } else if (language.equalsIgnoreCase("Ar")) {

                if (orderList.get(value).getChildItems().get(value1).getSubItems().size() == 0) {
                } else {
                    holder.title.setText(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemName_Ar());
//                notifyDataSetChanged();
                }
            }

            if (orderList.get(value).getChildItems().get(value1).getSubItems().size() == 0) {
            } else {
                Glide.with(context).load(Constants.IMAGE_URL + orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage()).into(holder.itemImage);
//            notifyDataSetChanged();
            }

            holder.info_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//                // ...Irrelevant code for customizing the buttons and title
//                LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();
//
//                View dialogView = inflater.inflate(R.layout.pop_info, null);
//                dialogBuilder.setView(dialogView);

//                ImageView mitem_image = dialogView.findViewById(R.id.item_image);
//                TextView mitem_desc = dialogView.findViewById(R.id.item_desc);
//                TextView mclose_popup = dialogView.findViewById(R.id.close_popup);

                    Intent a = new Intent(context, InfoPopup.class);
//                    a.putExtra("Image", orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage());
                    a.putExtra("desc", orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemDesc());
                    a.putExtra("itemname", orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                    a.putExtra("desc_ar", orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemDesc_Ar());
                    a.putExtra("itemname_ar", orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemName_Ar());
                    a.putExtra("section",orderList);
                    a.putExtra("value",value);
                    a.putExtra("value1",value1);
                    a.putExtra("pos",position);
                    a.putExtra("cat_name",title);
                    a.putExtra("info","box");
                    a.putExtra("qty", selectedCount.get(position));
                    a.putExtra("list", sections);
                    a.putExtra("postion1", position1);
                    a.putExtra("catId", catId);
                    a.putExtra("main_list", main_list);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(a);

//                mitem_desc.setText(orderList.get(value).getChildItems().get(position).getDescription());
//
//                mclose_popup.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        holder.alertDialog.dismiss();
//
//                    }
//                });
//
//                holder.alertDialog = dialogBuilder.create();
//                holder.alertDialog.show();
//
//                //Grab the window of the dialog, and change the width
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                Window window = holder.alertDialog.getWindow();
//                lp.copyFrom(window.getAttributes());
//                //This makes the dialog take up the full width
//                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                window.setAttributes(lp);

                }
            });

//        holder.itemQty.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                gettxt = Integer.parseInt(holder.itemQty.getText().toString());
//
//            }
//        });
//
//
//        if (PastriesSelection.selectedItems != null) {
//            int count = 0;
//            for (int i = 0; i < PastriesSelection.selectedItems.size(); i++) {
//                if (PastriesSelection.selectedItems.get(i).getSubitemName().equals(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName())) {
//                    count = count + 1 ;
//                    count = count + gettxt;
//                }
//            }
//            if (count > 0) {
//                String pos = Integer.toString(position);
//                if (!boxpos.contains(pos)) {
//                    boxpos.add(pos);
//                }
//                holder.itemQty.setText("" + count + gettxt );
//            } else {
//                String pos = Integer.toString(position);
//                if (boxpos.contains(pos)) {
//                    boxpos.remove(pos);
//                }
//                holder.itemQty.setText(gettxt+"");
////                holder.qty.setAlpha(0.5f);
//            }
//        } else {
//            holder.itemQty.setText(gettxt+"");
//        }
//
//        if (PastriesSelection.selectedItems.size() == 0) {
//            totalCount = 0;
////            PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
//        } else {
////            PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
//        }
//
//        holder.itemImage.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                PastriesSelection.pos = position;
//                ClipData data = ClipData.newPlainText("", "");
//                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
//                view.startDrag(data, shadowBuilder, view, 1);
//                view.setVisibility(View.VISIBLE);
//                return true;
//            }
//        });
//
//        holder.plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (subitemQty.size() == 0) {
//                        if (totalCount < boxSize || gettxt < boxSize) {
//                            totalCount = totalCount + 1;
//                            PastriesSelection.selectedItems.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
//                            subitemQty.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
////                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
//                            PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
//                            PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
//                            PastriesSelection.mBoxAdapter.notifyDataSetChanged();
//                            notifyDataSetChanged();
//
//                    } else {
//                        String message;
////                    if(language.equalsIgnoreCase("En")) {
//                        message = "You are done with Maximum " + PastriesSelection.boxSize + " " + PastriesSelection.title + " per Box, to order more use other box.";
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage(message)
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
//                    }
//                } else {
//                    if (totalCount < boxSize) {
//                        totalCount = totalCount + 1;
//                        PastriesSelection.selectedItems.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
//                        subitemQty.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
////                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
//                        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
//                        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
//                        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
//                        notifyDataSetChanged();
//
//                    } else {
//                        String message;
////                    if(language.equalsIgnoreCase("En")) {
//                        message = "You are done with Maximum " + PastriesSelection.boxSize + " " + PastriesSelection.title + " per Box, to order more use other box.";
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage(message)
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
//                    }
//                }
//            }
//        });
//
//        holder.minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (subitemQty.size() == 0) {
//
//                    if (Integer.parseInt(holder.itemQty.getText().toString()) > 0) {
//                        totalCount = totalCount - 1;
//                        PastriesSelection.selectedItems.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
////                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
//                        subitemQty.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
//                        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
//                        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
//                        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
//                        notifyDataSetChanged();
//                    }
//                } else {
//
//                    if (Integer.parseInt(holder.itemQty.getText().toString()) > 0) {
//                        totalCount = totalCount - 1;
//                        PastriesSelection.selectedItems.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
////                        PastriesSelection.mNumBox.setText(totalCount + "/" + boxSize);
//                        subitemQty.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
//                        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
//                        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
//                        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
//                        notifyDataSetChanged();
//                    }
//                }
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return orderList.get(value).getChildItems().get(value1).getSubItems().size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        TextView title, price;
//        EditText itemQty;
//        ImageView itemImage, minus, plus, info_image;
//        LinearLayout grid_layout;
//
//        ViewHolder(View itemView) {
//            super(itemView);
//            title = (TextView) itemView.findViewById(R.id.item_name);
//            itemQty = (EditText) itemView.findViewById(R.id.donut_qty);
//            itemImage = (ImageView) itemView.findViewById(R.id.donut_image);
//            info_image = (ImageView) itemView.findViewById(R.id.item_image);
//            minus = (ImageView) itemView.findViewById(R.id.donut_minus);
//            plus = (ImageView) itemView.findViewById(R.id.donut_plus);
//            grid_layout = (LinearLayout) itemView.findViewById(R.id.grid_layout);
//            price = (TextView) itemView.findViewById(R.id.item_price);
//        }
//    }
//}
            holder.itemQty.setText("" + PastriesSelection.selectedCount.get(position));

//            holder.itemQty.setImeOptions(EditorInfo.IME_ACTION_NEXT);
//            holder.itemQty.setImeActionLabel("Done", EditorInfo.IME_ACTION_NEXT);
//
//            holder.itemQty.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                @Override
//                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                    boolean handled = false;
//
//                    if (actionId == EditorInfo.IME_ACTION_NEXT) {
//                        // do what action you want
//
//                        handled = true;
//
//                        if (subitemQty.size() == 0) {
//
//
//                            showErrorDialog("please select atleast one item");
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//
//                        } else if (totalCount < boxSize) {
//
//
////                    if(language.equalsIgnoreCase("En")) {
//                            showErrorDialog("You are select " + PastriesSelection.totalCount + " of " + PastriesSelection.boxSize + " " + PastriesSelection.title + ", complete the box to proceed.");
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//
//                        } else if (PastriesSelection.Qty == 0) {
//
////                    if(language.equalsIgnoreCase("En")) {
//                            showErrorDialog("please select atleast one item");
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//
//                        } else {
//                            PastriesSelection pastriesSelection = new PastriesSelection();
//                            pastriesSelection.add();
//                        }
//                    } else if (actionId == EditorInfo.IME_ACTION_DONE) {
//                        PastriesSelection pastriesSelection = new PastriesSelection();
//                        pastriesSelection.add();
//                    }
//                    return handled;
//                }
//            });

//        PastriesRecyclerViewAdapter.mlayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String message;
////                    if(language.equalsIgnoreCase("En")) {
//                message = "Please Select the size to proceed with item selection";
////                    }
////                    else{
////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////                    }
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                // set title
//                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage(message)
//                        .setCancelable(false)
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//
//                // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                // show it
//                alertDialog.show();
//
//
//            }
//        });

            holder.itemQty.setCursorVisible(false);
            holder.itemQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.itemQty.setCursorVisible(true);
                }
            });

            holder.itemImage.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    PastriesSelection.pos = position;
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                    view.startDrag(data, shadowBuilder, view, 1);
                    view.setVisibility(View.VISIBLE);
                    return true;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        Log.i("TAG", "pastries size " + orderList.get(value).getChildItems().get(value1).getSubItems().size());
        return orderList.get(value).getChildItems().get(value1).getSubItems().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextWatcher, View.OnClickListener {
        TextView title, price;
        ImageView itemImage, minus, plus, info_image;
        LinearLayout grid_layout;
        EditText itemQty;

        ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.item_name);
            itemQty = (EditText) itemView.findViewById(R.id.donut_qty);
            itemImage = (ImageView) itemView.findViewById(R.id.donut_image);
            info_image = (ImageView) itemView.findViewById(R.id.item_image);
            minus = (ImageView) itemView.findViewById(R.id.donut_minus);
            plus = (ImageView) itemView.findViewById(R.id.donut_plus);
            grid_layout = (LinearLayout) itemView.findViewById(R.id.grid_layout);
            price = (TextView) itemView.findViewById(R.id.item_price);
            mlayout = itemView.findViewById(R.id.layout);

            itemQty.addTextChangedListener(this);
            plus.setOnClickListener(this);
            minus.setOnClickListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (visible) {

                showErrorDialog("Please select the size to proceed with item selection");
            } else {
//                if(itemQty.getText().toString().equals("0")){
//                    itemQty.setText("");
//                }
                if (itemQty.getText().toString().length() > 0) {

                    enteredString = editable.toString();
                    if (enteredString.startsWith("0")) {
                        if (enteredString.length() > 1) {
                            itemQty.setText(enteredString.substring(1));
                            itemQty.setSelection(itemQty.length());
                        }
                    }
                    totalCount = 0;
                    for (Integer i : PastriesSelection.selectedCount) {
                        totalCount = totalCount + i;
                    }

                    if (PastriesSelection.selectedCount.get(this.getAdapterPosition()) > Integer.parseInt(itemQty.getText().toString())) {
                        int localCount = PastriesSelection.selectedCount.get(this.getAdapterPosition()) - Integer.parseInt(itemQty.getText().toString());
                        totalCount = totalCount - localCount;
                    } else if (PastriesSelection.selectedCount.get(this.getAdapterPosition()) < Integer.parseInt(itemQty.getText().toString())) {
                        int localCount = Integer.parseInt(itemQty.getText().toString()) - PastriesSelection.selectedCount.get(this.getAdapterPosition());
                        totalCount = totalCount + localCount;
                    } else {
                        totalCount = totalCount;
                    }

                    if (totalCount <= boxSize) {
                        PastriesSelection.selectedCount.set(this.getAdapterPosition(), Integer.parseInt(itemQty.getText().toString()));
                        PastriesSelection.boxcount.set(this.getAdapterPosition(), Integer.parseInt(itemQty.getText().toString()));
                        AddItems(this.getAdapterPosition(), PastriesSelection.selectedCount.get(this.getAdapterPosition()));
                    } else {
                        itemQty.setText("" + PastriesSelection.selectedCount.get(this.getAdapterPosition()));
                        itemQty.setSelection(itemQty.length());
                        if (language.equals("En")) {
                            showErrorDialog("Please enter less than or equals to box size");
                        } else {
                            showErrorDialog("Please enter less than or equals to box size");
                        }
                    }
                } else {
                    PastriesSelection.selectedCount.set(this.getAdapterPosition(), 0);
                    PastriesSelection.boxcount.set(this.getAdapterPosition(), 0);
                    AddItems(this.getAdapterPosition(), 0);
                }
            }
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.donut_plus:
                    if (visible) {

                        showErrorDialog("Please select the size to proceed with item selection");
                    } else {
                        totalCount = 0;
                        for (Integer i : PastriesSelection.selectedCount) {
                            totalCount = totalCount + i;
                        }

                        totalCount = totalCount + 1;
                        if (totalCount <= boxSize) {
                            int localCount = PastriesSelection.selectedCount.get(this.getAdapterPosition());
                            PastriesSelection.selectedCount.set(this.getAdapterPosition(), (localCount + 1));
                            PastriesSelection.boxcount.set(this.getAdapterPosition(), (localCount + 1));
                            itemQty.setText("" + (localCount + 1));
                            itemQty.setSelection(itemQty.length());
                            AddItems(this.getAdapterPosition(), (localCount + 1));

                        } else {
                            if (language.equals("En")) {
                                showErrorDialog("You are done with maximum " + PastriesSelection.boxSize + " " + PastriesSelection.title + " per box, to order more use other box.");
                            } else {
                                showErrorDialog("Please enter less than or equals to box size");
                            }
                        }
                    }
                    break;

                case R.id.donut_minus:
                    if (visible) {

                        showErrorDialog("Please select the size to proceed with item selection");
                    } else {
                        if (itemQty.length() > 0) {
                            if (Integer.parseInt(itemQty.getText().toString()) > 0) {
                                int localCount = PastriesSelection.selectedCount.get(this.getAdapterPosition());
                                PastriesSelection.selectedCount.set(this.getAdapterPosition(), (localCount - 1));
                                PastriesSelection.boxcount.set(this.getAdapterPosition(), (localCount - 1));
                                itemQty.setText("" + (localCount - 1));
                                itemQty.setSelection(itemQty.length());
                                AddItems(this.getAdapterPosition(), (localCount - 1));
                                Log.i("TAG", "onClick: " + localCount);
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void AddItems(int position, int quantity) {

        int localSize = PastriesSelection.selectedItems.size();
        ArrayList<SubItems> localArray = new ArrayList<>();
        localArray.addAll(PastriesSelection.selectedItems);
        for (int i = 0; i < localSize; i++) {
            if (localArray.get(i).equals(orderList.get(value).getChildItems().get(value1).getSubItems().get(position))) {
                PastriesSelection.selectedItems.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
                subitemQty.remove(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                String pos = Integer.toString(position);
                boxpos.remove(pos);
                Log.i("TAG", "box pos remove " + boxpos);
                Log.i("TAG", "subitemqty remove " + subitemQty);
            } else {
//            Do nothing
            }
        }

        for (int j = 0; j < quantity; j++) {
            PastriesSelection.selectedItems.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position));
            subitemQty.add(orderList.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
            String pos = Integer.toString(position);
            if (boxpos != null && !boxpos.contains(pos)) {
                boxpos.add(pos);
                Log.i("TAG", "box pos " + boxpos);
                Log.i("TAG", "subitemqty " + subitemQty);
            }
//           else {
//                boxpos.remove(pos);
//            }
        }

        PastriesSelection.mBoxAdapter = new PastriesBoxItemsAdapter(parentActivity, maincatid, sectionname, PastriesSelection.selectedItems, value, value1, boxSize, language, parentActivity);
        PastriesSelection.mBoxGridView.setAdapter(PastriesSelection.mBoxAdapter);
        PastriesSelection.mBoxAdapter.notifyDataSetChanged();
//        notifyItemChanged(position);
    }

    public void showErrorDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));

        alertDialogBuilder.setTitle("BAKERY & Co.");

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
