package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.R;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.Section;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.ItemsActivity.indivialcount;
import static com.cs.bakeryco.activities.ItemsActivity.itemsListAdapter;
import static com.cs.bakeryco.activities.ItemsActivity.items_list;
import static com.cs.bakeryco.activities.ItemsActivity.selection_pos;

public class ItemSizeAdapter extends RecyclerView.Adapter<ItemSizeAdapter.MyViewHolder> {

    Context context;
    ArrayList<ItemsList.Sections> sections = new ArrayList<>();

    Activity activity;
    LayoutInflater inflater;
    String userid, language, title;
    boolean select = false;

    public ItemSizeAdapter(Context context, ArrayList<ItemsList.Sections> sections, String language, String title, Activity activity) {
        this.context = context;
        this.sections = sections;
        this.language = language;
        this.title = title;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_size_selector, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_size_selector_arabic, parent, false);
        }

        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
//
//        int view_pager_hieght = width;
//
//        int main_hieght = width - 10;

//        if (position == 4){
//
//            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();
//            layoutParams.width = main_hieght;
//            holder.itemView.setLayoutParams(layoutParams);
//
//        } else {
//
//            RecyclerView.LayoutParams layoutParams1 = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();
//            layoutParams1.width = view_pager_hieght;
//            holder.itemView.setLayoutParams(layoutParams1);
//
//        }

        if (language.equalsIgnoreCase("Ar")){
            holder.layout.setRotationY(180);
        }

        if (selection_pos == position) {
//            holder.size_layout.setBackgroundColor(Color.parseColor("#F5D5DE"));
            holder.size_layout.setBackgroundColor(Color.parseColor("#D5EEF5"));
            Log.i("TAG", "select: " + select);
        } else {
            holder.size_layout.setBackgroundColor(Color.parseColor("#E1D0C4"));
        }


        if (language.equalsIgnoreCase("En")) {
            holder.size_name.setText("" + sections.get(position).getSectionName());
        } else {
            holder.size_name.setText("" + sections.get(position).getSectionName_Ar());
        }
//        holder.size_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                selection_pos = position;
//
//            }
//        });

        Log.i("TAG", "size: " + (sections.size() - 1));

        if (position == (sections.size() - 1)) {

            holder.line.setVisibility(View.GONE);

        } else {

            holder.line.setVisibility(View.VISIBLE);

        }

        if (sections.size() == 4) {

            if (position == 0) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.individual_img));

            } else if (position == 1) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.box_img));

            } else if (position == 2) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.min_img));

            } else if (position == 3) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.box_img));

            }
        } else if (sections.size() == 3) {

            if (position == 0) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.individual_img));

            } else if (position == 1) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.box_img));

            } else if (position == 2) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.min_img));

            }
        } else if (sections.size() == 2) {

            if (position == 0) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.individual_img));

            } else if (position == 1) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.box_img));

            }
        } else if (sections.size() == 1) {

            if (position == 0) {

                holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.individual_img));

            }
        }


        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selection_pos = position;
//
                Log.i("TAG", "selection_name: " + sections.get(selection_pos).getSectionName());

                notifyDataSetChanged();
                itemsListAdapter = new ItemsListAdapter(context, sections, selection_pos, language, title);
                items_list.setAdapter(itemsListAdapter);

            }
        });

        indivialcount.clear();
        for (int k = 0; k < sections.get(selection_pos).getItems().size(); k++) {
            indivialcount.add(0);
        }


    }

    @Override
    public int getItemCount() {

        return sections.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView size_name;
        LinearLayout layout, size_layout;
        ImageView imageView;
        View line;

        public MyViewHolder(final View itemView) {
            super(itemView);

            size_name = (TextView) itemView.findViewById(R.id.standard);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            size_layout = itemView.findViewById(R.id.size_layout);
            line = itemView.findViewById(R.id.view);


        }

    }

}
