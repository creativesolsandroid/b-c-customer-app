package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.OrderConfirmationNew;
import com.cs.bakeryco.model.Order;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.OrderConfirmationNew.coupon_discount;
import static com.cs.bakeryco.activities.OrderConfirmationNew.item_total;
import static com.cs.bakeryco.activities.OrderConfirmationNew.mvat;
import static com.cs.bakeryco.activities.OrderConfirmationNew.service_charges;
import static com.cs.bakeryco.activities.OrderConfirmationNew.subtotal;
import static com.cs.bakeryco.activities.OrderConfirmationNew.total_price1;
import static com.cs.bakeryco.activities.OrderConfirmationNew.total_price2;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyViewHolder> {

    Context context;
    ArrayList<Order> orderLists = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String userid;
    DataBaseHelper myDbHelper;
    String language;
    int qty;
    Float price;
    float vat = 5;
    float tax;

    public MyOrderAdapter(Context context, ArrayList<Order> ordersCounts, String language) {
        this.context = context;
        this.orderLists = ordersCounts;
        this.userid = userid;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        myDbHelper = new DataBaseHelper(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;

        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_order_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_order_list_arabic, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


//        holder.cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//               myDbHelper.deleteItemFromOrder(orderLists.get(position).getOrderId());
//
//            }
//        });



        double number;
        float pirce = Float.parseFloat(orderLists.get(position).getTotalAmount());

        number = pirce;

        holder.item_price.setText("" + Constants.decimalFormat.format(number) + " SR");

        if (language.equalsIgnoreCase("En")) {

            holder.item_name_qty.setText("" + orderLists.get(position).getItemName());

        } else {

            holder.item_name_qty.setText("" + orderLists.get(position).getItemNameAr());

        }

        Glide.with(context).load(Constants.IMAGE_URL + orderLists.get(position).getImage()).into(holder.item_img);

        holder.qty.setText("" + orderLists.get(position).getQty());

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qty = Integer.parseInt(orderLists.get(position).getQty()) + 1;
                price = Float.parseFloat(orderLists.get(position).getPrice()) + (Float.parseFloat(orderLists.get(position).getTotalAmount()));
//                }
                myDbHelper.updateOrderwithOrderId(String.valueOf(qty), String.valueOf(price), orderLists.get(position).getOrderId());
                orderLists = myDbHelper.getOrderInfo();
                notifyDataSetChanged();

                item_total.setText("SR " + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
                service_charges.setText("+ 0");
                tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                mvat.setText("+ " + Constants.decimalFormat.format(tax));
                subtotal.setText("SR " + Constants.decimalFormat.format(tax + myDbHelper.getTotalOrderPrice()));
                coupon_discount.setText("- 0.00");
                total_price1.setText("" + Constants.decimalFormat.format(tax + myDbHelper.getTotalOrderPrice()));
                total_price2.setText("" + Constants.decimalFormat.format(tax + myDbHelper.getTotalOrderPrice()));

            }
        });

        holder.mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qty = Integer.parseInt(orderLists.get(position).getQty()) - 1;

                if (qty == 0) {
                    myDbHelper.deleteItemFromOrderlist(orderLists.get(position).getOrderId());


                } else {
                    price = (Float.parseFloat(orderLists.get(position).getTotalAmount())) - Float.parseFloat(orderLists.get(position).getPrice());

                    myDbHelper.updateOrderwithOrderId(String.valueOf(qty), String.valueOf(price), orderLists.get(position).getOrderId());
                }


                orderLists = myDbHelper.getOrderInfo();
                notifyDataSetChanged();

                if (orderLists.size() == 0) {

                    ((Activity) context).finish();
                }
                notifyDataSetChanged();

                item_total.setText("SR " + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
                service_charges.setText("+ 0.00");
                tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
                mvat.setText("+ " + Constants.decimalFormat.format(tax));
                subtotal.setText("SR " + Constants.decimalFormat.format(tax + myDbHelper.getTotalOrderPrice()));
                coupon_discount.setText("- 0.00");
                total_price1.setText("" + Constants.decimalFormat.format(tax + myDbHelper.getTotalOrderPrice()));
                total_price2.setText("" + Constants.decimalFormat.format(tax + myDbHelper.getTotalOrderPrice()));

            }
        });

        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myDbHelper.deleteItemFromOrderlist(orderLists.get(position).getOrderId());

                orderLists = myDbHelper.getOrderInfo();
                notifyDataSetChanged();

                if (orderLists.size() == 0) {

                    ((Activity) context).finish();
                }
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {

        return orderLists.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView  item_name_qty, comment, item_price, qty;
        ImageView item_img, plus, mins, cancel;

        public MyViewHolder(final View convertView) {
            super(convertView);

//            cancel = (TextView) convertView.findViewById(R.id.cancel);
            item_name_qty = (TextView) convertView.findViewById(R.id.item_name);
            comment = (TextView) convertView.findViewById(R.id.comment);
            item_price = (TextView) convertView.findViewById(R.id.item_price);
            qty = (TextView) convertView.findViewById(R.id.qty);
            plus = (ImageView) convertView.findViewById(R.id.plus);
            mins = (ImageView) convertView.findViewById(R.id.mins);
            cancel = (ImageView) convertView.findViewById(R.id.cancel);

            item_img = (ImageView) convertView.findViewById(R.id.item_img);


        }

    }
}

