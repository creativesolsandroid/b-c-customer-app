package com.cs.bakeryco.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.ItemsActivity;
import com.cs.bakeryco.activities.ItemsBoxActivity;
import com.cs.bakeryco.activities.ItemsIndiviualActivity;
import com.cs.bakeryco.model.ItemsList;

import java.util.ArrayList;

//import static com.cs.chef.Activities.ForgotPasswordActivity.TAG;

public class BannersAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<ItemsList.Banners> bannerList = new ArrayList<>();
    ArrayList<ItemsList.Items> itemsArrayList = new ArrayList<>();

    Context context;
    String language;

    public BannersAdapter(Context context, ArrayList<ItemsList.Items> itemsArrayList, ArrayList<ItemsList.Banners> bannerList, String language) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannerList = bannerList;
        this.itemsArrayList = itemsArrayList;
        this.language = language;
        notifyDataSetChanged();
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1.0f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = mLayoutInflater.inflate(R.layout.banners_stores, container, false);
        } else {
            itemView = mLayoutInflater.inflate(R.layout.banners_stores, container, false);
        }

        final ImageView banner_image = (ImageView) itemView.findViewById(R.id.banners_img);
//        final TextView order_now = (TextView) itemView.findViewById(R.id.order_now);

//        order_now.setVisibility(View.VISIBLE);

//        if (bannerList.get(position).getCategoryId() == 0) {
//
//            order_now.setVisibility(View.GONE);
//
//        } else {
//
//            order_now.setVisibility(View.VISIBLE);
//
//        }

        if (language.equalsIgnoreCase("Ar")){

            banner_image.setRotation(180);

        }

        banner_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (bannerList.get(position).getCategoryId() == 0) {

//                        order_now.setVisibility(View.GONE);
//                        Log.i("TAG", "onClick: " + order_now.getVisibility());

                    } else if (bannerList.get(position).getCategoryId() != 0 && bannerList.get(position).getItemId() == 0) {

//                        order_now.setVisibility(View.VISIBLE);

                        for (int i = 0; i < itemsArrayList.size(); i++) {

                            for (int j = 0; j < itemsArrayList.get(i).getSubCategoryItems().size(); j++) {

                                if (itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryId() == bannerList.get(position).getCategoryId()) {

                                    Intent intent = new Intent(context, ItemsActivity.class);
                                    intent.putExtra("list", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections());
                                    intent.putExtra("postion", j);
                                    intent.putExtra("catId", itemsArrayList.get(i).getSubCategoryItems().get(j).getMainCategoryId());
                                    intent.putExtra("main_list", itemsArrayList);
                                    Log.i("TAG", "onClick: " + itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryName_Ar());
                                    if (language.equalsIgnoreCase("En")) {
                                        intent.putExtra("title", itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryName());
                                        Log.i("TAG", "onClick: " + itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryName());
                                    } else {
                                        Log.i("TAG", "onClick: " + itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryName_Ar());
                                        intent.putExtra("title", itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryName_Ar());
                                    }
                                    context.startActivity(intent);

                                }

                            }

                        }


                    } else if (bannerList.get(position).getCategoryId() != 0 && bannerList.get(position).getItemId() != 0) {
//                        order_now.setVisibility(View.VISIBLE);

                        for (int i = 0; i < itemsArrayList.size(); i++) {
                            for (int j = 0; j < itemsArrayList.get(i).getSubCategoryItems().size(); j++) {
                                for (int k = 0; k < itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().size(); k++) {
                                    for (int l = 0; l < itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().size(); l++) {
                                        Log.i("TAG", "itemid1: " + bannerList.size());
                                        if (itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryId() == bannerList.get(position).getCategoryId()) {
                                            Log.i("TAG", "itemid2: " + bannerList.get(position).getCategoryId());
                                            if (itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemId() == bannerList.get(position).getItemId()) {
                                                Log.i("TAG", "itemid3: " + bannerList.get(position).getItemId());
                                                if (itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getSectionId() == 1 || itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getSectionId() == 5 || itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getSectionId() == 6) {
                                                    Log.i("TAG", "individual: ");

                                                    Intent a = new Intent(context, ItemsIndiviualActivity.class);
                                                    a.putExtra("desc", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemDesc());
                                                    a.putExtra("itemname", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemName());
                                                    a.putExtra("desc_ar", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemDesc_Ar());
                                                    a.putExtra("itemname_ar", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemName_Ar());
                                                    a.putExtra("section", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections());
                                                    a.putExtra("value", k);
                                                    a.putExtra("pos", l);
                                                    a.putExtra("cat_name", itemsArrayList.get(i).getSubCategoryItems().get(j).getSubCategoryName());
                                                    a.putExtra("list", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections());
                                                    a.putExtra("catId", itemsArrayList.get(i).getSubCategoryItems().get(j).getMainCategoryId());
                                                    a.putExtra("main_list", itemsArrayList);
                                                    a.putExtra("cake_type_id", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemTypeIds());
                                                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    context.startActivity(a);

                                                } else if (itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getSectionId() == 2 || itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getSectionId() == 3 || itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getSectionId() == 4) {

                                                    Intent a = new Intent(context, ItemsBoxActivity.class);
                                                    a.putExtra("SubItems", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getSubItems());
                                                    a.putExtra("pricelist", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getPrice());
                                                    if (language.equalsIgnoreCase("En")) {
                                                        a.putExtra("title_name", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemName());
                                                    } else {
                                                        a.putExtra("title_name", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getItemName_Ar());
                                                    }
                                                    a.putExtra("no_of_items", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections().get(k).getItems().get(l).getNoOfItems());
                                                    a.putExtra("section_list", itemsArrayList.get(i).getSubCategoryItems().get(j).getSections());
                                                    a.putExtra("section_pos", k);
                                                    a.putExtra("child_pos", l);
                                                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    context.startActivity(a);

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } else {

//                        order_now.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.IMAGE_URL + bannerList.get(position).getBannerImage())
                .into(banner_image);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}