package com.cs.bakeryco.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.CategoriesListActivity;
import com.cs.bakeryco.activities.ImageLayout;
import com.cs.bakeryco.activities.SpecialCakeActivity;
import com.cs.bakeryco.model.ItemsList;

import java.util.ArrayList;

import static com.cs.bakeryco.activities.ItemsActivity.selection_pos;

public class HomeMainCatList extends RecyclerView.Adapter<HomeMainCatList.MyViewHolder> {

    Context context;
    ArrayList<String> main_name = new ArrayList<>();
    ArrayList<Integer> main_img = new ArrayList<>();
    ArrayList<ItemsList.Items> itemsArrayList = new ArrayList<>();

    Activity activity;
    LayoutInflater inflater;
    String userid, language, title;
    boolean select = false;

    public HomeMainCatList(Context context, ArrayList<String> main_name, ArrayList<Integer> main_img, ArrayList<ItemsList.Items> itemsArrayList, String language) {
        this.context = context;
        this.main_name = main_name;
        this.main_img = main_img;
        this.language = language;
        this.itemsArrayList = itemsArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_main_cat_list, parent, false);

        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.text.setText("" + main_name.get(position));
        holder.img.setCompoundDrawablesWithIntrinsicBounds(0, main_img.get(position), 0, 0);

        if (language.equalsIgnoreCase("Ar")){

            holder.mlayout.setRotation(180);

        }

        holder.mlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (position == 0) {

                    Intent breadIntent = new Intent(context, CategoriesListActivity.class);
                    breadIntent.putExtra("catId", 2);
                    breadIntent.putExtra("mainList", itemsArrayList);
                    context.startActivity(breadIntent);

                } else if (position == 1) {

                    Intent breadIntent = new Intent(context, CategoriesListActivity.class);
                    breadIntent.putExtra("catId", 1);
                    breadIntent.putExtra("mainList", itemsArrayList);
                    context.startActivity(breadIntent);

                } else if (position == 2) {

                    Intent breadIntent = new Intent(context, CategoriesListActivity.class);
                    breadIntent.putExtra("catId", 4);
                    breadIntent.putExtra("mainList", itemsArrayList);
                    context.startActivity(breadIntent);

                } else if (position == 3) {

                    Intent breadIntent = new Intent(context, CategoriesListActivity.class);
                    breadIntent.putExtra("catId", 6);
                    breadIntent.putExtra("mainList", itemsArrayList);
                    context.startActivity(breadIntent);

                } else if (position == 4) {

                    Intent breadIntent = new Intent(context, CategoriesListActivity.class);
                    breadIntent.putExtra("catId", 3);
                    breadIntent.putExtra("mainList", itemsArrayList);
                    context.startActivity(breadIntent);

                } else if (position == 5) {

                    Intent breadIntent = new Intent(context, CategoriesListActivity.class);
                    breadIntent.putExtra("catId", 13);
                    breadIntent.putExtra("mainList", itemsArrayList);
                    context.startActivity(breadIntent);

                } else if (position == 6) {

                    Intent breadIntent = new Intent(context, SpecialCakeActivity.class);
                    context.startActivity(breadIntent);

                }
                else if (position == 7) {

                    Intent breadIntent = new Intent(context, CategoriesListActivity.class);
                    breadIntent.putExtra("catId", 14);
                    breadIntent.putExtra("mainList", itemsArrayList);
                    context.startActivity(breadIntent);

                } else if (position == 8) {

                    Intent a = new Intent(context, ImageLayout.class);
                    if (language.equalsIgnoreCase("En")) {
                        a.putExtra("title", "Catering");
                    } else {
                        a.putExtra("title", "التموين وخدمات الطعام");
                    }
                    context.startActivity(a);

                }

            }
        });
    }

    @Override
    public int getItemCount() {

        return main_name.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView text, img;
        LinearLayout mlayout;

        public MyViewHolder(final View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.txt);
            img = itemView.findViewById(R.id.img);
            mlayout = itemView.findViewById(R.id.layout);

        }

    }

}
