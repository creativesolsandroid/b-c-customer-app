package com.cs.bakeryco.Dialogs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.activities.ForgetPassword;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.activities.RegistrationActivity;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.VerifyRandomNumber;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

//import swarajsaaj.smscodereader.interfaces.OTPListener;
//import swarajsaaj.smscodereader.receivers.OtpReader;

//import com.cs.bakeryco.activities.ForgotPasswordActivity;
//import com.cs.bakeryco.activities.SignUpActivity;

public class VerifyOtpDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    TextView textMobileNumber, textOtpTitle, textBody1, textBody2, textBody3;
    String otpEntered = "", strName, strEmail, strMobile, strPassword, serverOtp;
    Button buttonResend, buttonVerify;
    ImageView imgEditMobile;
    String language;
    OtpView otpView;
    View rootView;
    private static String TAG = "TAG";
    CountDownTimer countDownTimer;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefEditor;
    Context context;
    boolean mregister = false, mlogin = false;


    String response;

    SharedPreferences languagePrefs;

    public static VerifyOtpDialog newInstance() {
        return new VerifyOtpDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.dialog_verify_otp, container, false);
        } else {
            rootView = inflater.inflate(R.layout.dialog_verify_otp_arabic, container, false);
        }
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//        OtpReader.bind(this, "cs-test");
        context = getActivity();


        textMobileNumber = (TextView) rootView.findViewById(R.id.otp_mobile_number);
        imgEditMobile = (ImageView) rootView.findViewById(R.id.edit_mobile_number);
        textOtpTitle = (TextView) rootView.findViewById(R.id.otp_title);
        textBody1 = (TextView) rootView.findViewById(R.id.otp_body1);
        textBody2 = (TextView) rootView.findViewById(R.id.otp_body2);
        textBody3 = (TextView) rootView.findViewById(R.id.otp_body3);
        otpView = (OtpView) rootView.findViewById(R.id.otp_view);

        buttonVerify = (Button) rootView.findViewById(R.id.button_verify_otp);
        buttonResend = (Button) rootView.findViewById(R.id.button_resend_otp);

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

        strName = getArguments().getString("name");
        strEmail = getArguments().getString("email");
        strMobile = getArguments().getString("mobile");
        strPassword = getArguments().getString("password");
        serverOtp = getArguments().getString("otp");
        textMobileNumber.setText(Constants.Country_Code + strMobile);

        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);
//        setTypeface();
        setTimerForResend();

        otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                otpEntered = otp;
            }
        });

        buttonVerify.setOnClickListener(this);
        buttonResend.setOnClickListener(this);
        imgEditMobile.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

//    private void setTypeface(){
//        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
//                "helvetica.ttf");
//
//        textBody1.setTypeface(typeface);
//        textBody2.setTypeface(typeface);
//        textBody3.setTypeface(typeface);
//        textOtpTitle.setTypeface(typeface);
//        buttonResend.setTypeface(typeface);
//        buttonVerify.setTypeface(typeface);
//        textMobileNumber.setTypeface(typeface);
//    }

    private void setTimerForResend() {
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
//                Log.i(TAG, "onTick: "+timeRemaining);
                if (getDialog() != null) {
                    if (language.equalsIgnoreCase("En")) {
                        buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);
                    } else {
                        buttonResend.setText(timeRemaining + " في " + getResources().getString(R.string.otp_msg_resend_ar));
                    }
                }
            }

            public void onFinish() {
                if (getDialog() != null) {
                    if (language.equalsIgnoreCase("En")) {
                        buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
                    } else {
                        buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar));
                    }
                    buttonResend.setEnabled(true);
                    buttonResend.setAlpha(1.0f);
                }
            }

        }.start();
    }

//    @Override
//    public void otpReceived(String messageText) {
//        String[] str = null;
//        try {
//            messageText = messageText.replace("'", "");
//            str = messageText.split(":");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        otpView.setOTP("" + str[1]);
//        otpEntered = str[1];
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_verify_otp:

                Fragment forgot = getFragmentManager().findFragmentByTag("forgot");
                if (forgot != null && forgot.isVisible()) {

                    if (otpEntered.length() != 4) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1_ar), getActivity());
                        }
                    } else if (!serverOtp.equals(otpEntered)) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1_ar), getActivity());
                        }
                    } else {

                        Log.i(TAG, "onClick: ");
                        ForgetPassword.isOTPSuccessful = true;
                        getDialog().cancel();
                    }
                }

                Fragment register = getFragmentManager().findFragmentByTag("register");
                if (register != null && register.isVisible()) {

                    if (otpEntered.length() != 4) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1_ar), getActivity());
                        }
                    } else {

                        mregister = true;
                        new GetVerificationDetails().execute(Constants.VERIFY_RANDOM_NUMBER_URL + "966" + strMobile + "&OTP=" + otpEntered);


                    }

                }

                Fragment login = getFragmentManager().findFragmentByTag("login");
                if (login != null && login.isVisible()) {

                    if (otpEntered.length() != 4) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1), getActivity());
                        } else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1_ar), getActivity());
                        }
                    } else {

                        mlogin = true;
                        new GetVerificationDetails().execute(Constants.VERIFY_RANDOM_NUMBER_URL + "966" + strMobile + "&OTP=" + otpEntered);


                    }

                }

                break;

            case R.id.edit_mobile_number:
//                Re.isOTPVerified = false;
                getDialog().cancel();
                break;

            case R.id.button_resend_otp:
                String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    new verifyMobileApi().execute();
                } else {
//                    if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                    }
                }
                break;
        }
    }

//    private String prepareVerifyMobileJson(){
//        JSONObject parentObj = new JSONObject();
//        JSONObject mobileObj = new JSONObject();
//        try {
//            mobileObj.put("Mobile","966"+strMobile);
//            parentObj.put("VerifyMobile",mobileObj);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return parentObj.toString();
//    }
//
//    private String prepareSignUpJson(){
//        JSONObject parentObj = new JSONObject();
//        JSONObject userDetailsObj = new JSONObject();
//        JSONObject userAuthObj = new JSONObject();
//
//        try {
//            userDetailsObj.put("UserId", "0");
//            userDetailsObj.put("FullName", strName);
//            userDetailsObj.put("FamilyName","");
//            userDetailsObj.put("NickName","");
//            userDetailsObj.put("Gender","");
//            userDetailsObj.put("Mobile","966"+strMobile);
//            userDetailsObj.put("Email", strEmail);
//            userDetailsObj.put("Password", strPassword);
//            userDetailsObj.put("Language", language);
//            userDetailsObj.put("DeviceType", Constants.getDeviceType(getContext()));
//            userDetailsObj.put("UserType", Constants.UserType);
//            userAuthObj.put("DeviceToken","-1");
//
//            parentObj.put("UserDetails", userDetailsObj);
//            parentObj.put("UserAuthActivity",userAuthObj);
//            Log.d(TAG, "prepareSignUpJson: "+parentObj.toString());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return parentObj.toString();
//    }

//    private class userRegistrationApi extends AsyncTask<String, String, String> {
//
////        ACProgressFlower dialog;
//        AlertDialog loaderDialog = null;
//        String inputStr;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareSignUpJson();
////            dialog = new ACProgressFlower.Builder(getContext())
////                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
////                    .themeColor(Color.WHITE)
////                    .fadeColor(Color.DKGRAY).build();
////            dialog.show();
//            Constants.showLoadingDialog(getActivity());
////            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
////            // ...Irrelevant code for customizing the buttons and title
////            LayoutInflater inflater = getLayoutInflater();
////            int layout = R.layout.loder_dialog;
////            View dialogView = inflater.inflate(layout, null);
////            dialogBuilder.setView(dialogView);
////            dialogBuilder.setCancelable(true);
////
////            loaderDialog = dialogBuilder.create();
////            loaderDialog.show();
////            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
////            Window window = loaderDialog.getWindow();
////            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////            lp.copyFrom(window.getAttributes());
////            //This makes the dialog take up the full width
////            Display display = getActivity().getWindowManager().getDefaultDisplay();
////            Point size = new Point();
////            display.getSize(size);
////            int screenWidth = size.x;
////
////            double d = screenWidth*0.85;
////            lp.width = (int) d;
////            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
////            window.setAttributes(lp);
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            APIInterface apiService =
//                    ApiClient.getClient().create(APIInterface.class);
//
//            Call<UserRegistrationResponse> call = apiService.userRegistration(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<UserRegistrationResponse>() {
//                @Override
//                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
//                    if(response.isSuccessful()){
//                        UserRegistrationResponse registrationResponse = response.body();
//                        if(registrationResponse.getStatus()){
////                          status true case
//                            String userId = registrationResponse.getData().getUserid();
//                            userPrefsEditor.putString("userId", userId);
//                            userPrefsEditor.putString("name", registrationResponse.getData().getName());
//                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
//                            userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
//                            userPrefsEditor.commit();
//                            SignUpActivity.isOTPVerified = true;
//                            getDialog().dismiss();
//                        }
//                        else {
////                          status false case
//                            if (language.equalsIgnoreCase("En")) {
//                                String failureResponse = registrationResponse.getMessage();
//                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
//                                        getResources().getString(R.string.ok), getActivity());
//                            } else {
//                                String failureResponse = registrationResponse.getMessage();
//                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
//                                        getResources().getString(R.string.ok_ar), getActivity());
//                            }
//                        }
//                    }
//                    else{
//                        if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                            if (language.equalsIgnoreCase("En")) {
//                                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                        else {
//                            if (language.equalsIgnoreCase("En")) {
//                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    Constants.closeLoadingDialog();
//                }
//
//                @Override
//                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
//                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    Constants.closeLoadingDialog();
//                }
//            });
//            return null;
//        }
//    }

    public class GetVerificationDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (result.equals("")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    if (isVerified) {
                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();
//                                        Intent loginIntent = new Intent();
//                                        serverOtp = otpEntered;
//                                        getDialog().dismiss();


                                        if (mregister) {
                                            RegistrationActivity.isOTPVerified = true;
                                            getDialog().dismiss();
                                        } else {
                                            LoginActivity.login_OTP = true;
                                            getDialog().dismiss();
                                        }

//                                        if (forget){
//
//                                            Intent intent = new Intent(VerifyRandomNumber.this,ResetPassword.class);
//                                            intent.putExtra("mobile",phoneNumber);
//                                            startActivity(intent);
//
//                                        }else {
//
//                                            finish();
//
//                                        }

                                    } else {
//                                        setResult(RESULT_CANCELED);
                                        getDialog().dismiss();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("BAKERY & Co.");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Invalid OTP")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بيكري آند كومباني");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
