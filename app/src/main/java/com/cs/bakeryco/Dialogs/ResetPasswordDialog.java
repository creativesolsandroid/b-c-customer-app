package com.cs.bakeryco.Dialogs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.activities.ForgetPassword;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.cs.bakeryco.Activity.ForgotPasswordActivity;

public class ResetPasswordDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    TextInputLayout inputLayoutPassword, inputLayoutConfirmPassword;
    EditText inputPassword, inputConfirmPassword;
    String strPassword, strConfirmPassword;
    Button buttonSubmit, buttonVerify;
    String strMobile;
    View rootView;
    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String language;
    SharedPreferences languagePrefs;
    String response;

    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.dialog_reset_password, container, false);
        } else {
            rootView = inflater.inflate(R.layout.dialog_reset_password_arabic, container, false);
        }
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        strMobile = getArguments().getString("mobile");

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        inputLayoutPassword = (TextInputLayout) rootView.findViewById(R.id.layout_new_password);
        inputLayoutConfirmPassword = (TextInputLayout) rootView.findViewById(R.id.layout_confirm_password);
        inputPassword = (EditText) rootView.findViewById(R.id.reset_input_password);
        inputConfirmPassword = (EditText) rootView.findViewById(R.id.reset_input_retype_password);

        buttonSubmit = (Button) rootView.findViewById(R.id.reset_submit_button);

//        setTypeface();

        buttonSubmit.setOnClickListener(this);

        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

//    private void setTypeface(){
//        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
//                "helvetica.ttf");
//
//        inputPassword.setTypeface(typeface);
//        inputConfirmPassword.setTypeface(typeface);
//        buttonSubmit.setTypeface(typeface);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reset_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new GetForgetpassword().execute(Constants.FORGOT_PASSWORD_URL +"966" + strMobile+"&psw="+strPassword);
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError("Please enter new password");
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, getActivity());
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, getActivity());
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError("Please enter password");
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        else if (!strPassword.equals(strConfirmPassword)){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError("Passwords not match, please retype");
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_confirm_password_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.reset_input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.reset_input_retype_password:
                    clearErrors();
                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    public class GetForgetpassword extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (result.equals("")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        try {

                            JSONObject jo = new JSONObject(result);
                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");
                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
//                                jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    userPrefEditor.putString("login_status", "loggedin");
                                    userPrefEditor.commit();

                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));
//                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("BAKERY & Co.");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Reset password successful")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
//                                } else if (language.equalsIgnoreCase("Ar")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("اوريجانو");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(" إعادة ضبط كلمة السر تمت بنجاح")
//                                            .setCancelable(false)
//                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }

                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();

                                    ForgetPassword.isResetSuccessful = true;
                                    getDialog().dismiss();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je) {
                                je.printStackTrace();
                                dialog.dismiss();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));
//                            if (language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("Bakery & Company");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("Reset password unsuccessful")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                // set title
//                                alertDialogBuilder.setTitle("اوريجانو");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage(" فشل إعادة ضبط كلمة السر ")
//                                        .setCancelable(false)
//                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                            }

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}
