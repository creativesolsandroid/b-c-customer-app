package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.SpecialCakeAdapter;

public class SpecialCakeActivity extends AppCompatActivity {

    SharedPreferences languagePrefs;
    String language;
    RelativeLayout back_btn;
    ImageView bag;
    TextView bag_count, item_title;;
    RecyclerView cake_list;
    DataBaseHelper myDbHelper;
    SpecialCakeAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.special_cake);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.special_cake_arabic);
        }

        myDbHelper = new DataBaseHelper(SpecialCakeActivity.this);

        bag = (ImageView) findViewById(R.id.bag);

        back_btn = (RelativeLayout) findViewById(R.id.back_btn);

        bag_count = (TextView) findViewById(R.id.bag_count);
        item_title = (TextView) findViewById(R.id.cake_title);

        cake_list = (RecyclerView) findViewById(R.id.cake_list);

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(SpecialCakeActivity.this, OrderConfirmationNew.class);
                startActivity(a);

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        item_title.setText("Special Cake");

        cake_list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));

        mAdapter = new SpecialCakeAdapter(SpecialCakeActivity.this);
        cake_list.setAdapter(mAdapter);

    }

}
