package com.cs.bakeryco.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.Firebase.Config;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.Rest.APIInterface;
import com.cs.bakeryco.Rest.ApiClient1;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by CS on 19-09-2016.
 */
public class SplashActivity extends Activity {
    SharedPreferences userPrefs;
    String mLoginStatus;
    String SENDER_ID = "240307285149";

//    GoogleCloudMessaging gcm;
    Context context;
    //String regId;
//    public static final String REG_ID = "regId";
    private static final String APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    public static String regid = "";
    BroadcastReceiver mRegistrationBroadcastReceiver;

    public static ArrayList<ItemsList.Items> itemsArrayList = new ArrayList<>();
    public static ArrayList<ItemsList.Banners> bannersArrayList = new ArrayList<>();
    SharedPreferences ServiceDataPrefs;
    SharedPreferences.Editor ServiceData_Editer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        ServiceDataPrefs = getSharedPreferences("SERVICEDATA_PREFS", Context.MODE_PRIVATE);
        ServiceData_Editer = ServiceDataPrefs.edit();

//        context = getApplicationContext();
//        // Check device for Play Services APK. If check succeeds, proceed with
//        //  GCM registration.
//        if (checkPlayServices()) {
//            gcm = GoogleCloudMessaging.getInstance(this);
//            regid = getRegistrationId(context);
//            Log.e("RegisterActivity check", "registerInBackground - regId: " + regid);
//
//            if (regid.isEmpty() || regid.equalsIgnoreCase("")) {
//                registerInBackground();
//            }
//
//        } else {
//            Log.i("TAG", "No valid Google Play Services APK found.");
//        }

        ItemsApi();

//        new Handler().postDelayed(new Runnable() {
//
//            /*
//             * Showing splash screen with a timer. This will be useful when you
//             * want to show case your app logo / company
//             */
//
//            @Override
//            public void run() {
//                // This method will be executed once the timer is over
//                // Start your app main activity
//                Intent i = new Intent(SplashActivity.this, MainActivity.class);
//                startActivity(i);
//
//                // close this activity
//                finish();
//            }
//        }, 1000);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    regid = pref.getString("regId", null);

                    Log.i("TAG", "Firebase reg id: " + regid);
                }
            }

        };

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regid = pref.getString("regId", null);

        Log.i("TAG", "Firebase reg id: " + regid);

    }

    private void ItemsApi(){
        String networkStatus;
        final ProgressDialog dialog;
        networkStatus = NetworkUtil.getConnectivityStatusString(SplashActivity.this);
//        dialog = ProgressDialog.show(SplashActivity.this, "",
//                "Loading...");
        networkStatus = NetworkUtil.getConnectivityStatusString(SplashActivity.this);

        APIInterface apiService = ApiClient1.getClient().create(APIInterface.class);
        Call<ItemsList> call = apiService.getItemsList();
        final String finalNetworkStatus = networkStatus;
        call.enqueue(new Callback<ItemsList>() {

            public void onResponse(Call<ItemsList> call, Response<ItemsList> response) {
                if(response.isSuccessful()){
                    Log.i("TAG", "onResponse2" +
                            ": ");

                    ItemsList registrationResponse = response.body();
                    try {
                        //status true case
                        Log.i("TAG", "onResponse1: ");

                        itemsArrayList = registrationResponse.getSuccess().getItems();
                        bannersArrayList = registrationResponse.getSuccess().getBanners();

                        Constants.ItemsArrayList = registrationResponse.getSuccess().getItems();
                        Constants.BannersArrayList = registrationResponse.getSuccess().getBanners();

                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();

                    } catch (Exception e) {
                        Constants.showOneButtonAlertDialog("Something went worng please try again later", SplashActivity.this);
                    }

//                    if(dialog != null)
//                        dialog.dismiss();
                } else {
                    try {
                        Log.d("TAG", "onResponse: "+response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    if(dialog != null)
//                        dialog.dismiss();
                    Log.i("TAG", "onResponse: ");
                    ItemsApi();

                    Toast.makeText(SplashActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<ItemsList> call, Throwable t) {
                Log.d("TAG", "onFailure: "+t.getMessage());
                if(finalNetworkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(SplashActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(SplashActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

//                if(dialog != null)
//                    dialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


//    private boolean checkPlayServices() {
//        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
//                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
//            } else {
//                Log.i("TAG", "This device is not supported.");
//                finish();
//            }
//            return false;
//        }
//        return true;
//    }


    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
//    private String getRegistrationId(Context context) {
//        final SharedPreferences prefs = getGCMPreferences(context);
//        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
//        if (registrationId.isEmpty()) {
//            Log.i("TAG", "Registration not found.");
//            return "";
//        }
//        // Check if app was updated; if so, it must clear the registration ID
//        // since the existing registration ID is not guaranteed to work with
//        // the new app version.
//        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
//        int currentVersion = getAppVersion(context);
//        if (registeredVersion != currentVersion) {
//            Log.i("TAG", "App version changed.");
//            return "";
//        }
//        return registrationId;
//    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
//    private SharedPreferences getGCMPreferences(Context context) {
//        // This sample app persists the registration ID in shared preferences, but
//        // how you store the registration ID in your app is up to you.
//        return getSharedPreferences(SplashActivity.class.getSimpleName(),
//                Context.MODE_PRIVATE);
//    }
//
//    /**
//     * @return Application's version code from the {@code PackageManager}.
//     */
//    private static int getAppVersion(Context context) {
//        try {
//            PackageInfo packageInfo = context.getPackageManager()
//                    .getPackageInfo(context.getPackageName(), 0);
////            NA_Constants.APP_VERSION = packageInfo.versionCode;
//            return packageInfo.versionCode;
//        } catch (PackageManager.NameNotFoundException e) {
//            // should never happen
//            throw new RuntimeException("Could not get package name: " + e);
//        }
//    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
//    @SuppressWarnings("unchecked")
//    private void registerInBackground() {
//        new AsyncTask() {
//            @Override
//            protected String doInBackground(Object... params) {
//                String msg = "";
//
//                // Initially this call goes out to the network to retrieve the token, subsequent calls
//                // are local.
//                InstanceID instanceID = InstanceID.getInstance(context);
//                //String token;
//                try {
//                    regid = instanceID.getToken(SENDER_ID,
//                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
//                    Log.i("TAG", "GCM Registration Token: " + regid);
//                    storeRegistrationId(context, regid);
//                } catch (IOException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }


    // TODO: Implement this method to send any registration to your app's servers.
//	         sendRegistrationToServer(token);

    // Subscribe to topic channels
//	         subscribeTopics(token);

    // You should store a boolean that indicates whether the generated token has been
    // sent to your server. If the boolean is false, send the token to your server,
    // otherwise your server should have already received the token.
//	         sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();


//	            try {
//	                if (gcm == null) {
//	                    gcm = GoogleCloudMessaging.getInstance(context);
//	                }
//	                regid = gcm.register(SENDER_ID);
//	                msg = "Device registered, registration ID=" + regid;
//	                Log.d("RegisterActivity", msg);
//	                // You should send the registration ID to your server over HTTP,
//	                // so it can use GCM/HTTP or CCS to send messages to your app.
//	                // The request to your server should be authenticated if your app
//	                // is using accounts.
//	                sendRegistrationIdToBackend();
//
//	                // For this demo: we don't need to send it because the device
//	                // will send upstream messages to a server that echo back the
//	                // message using the 'from' address in the message.
//
//	                // Persist the registration ID - no need to register again.
//	                storeRegistrationId(context, regid);
//	            } catch (IOException ex) {
//	                msg = "Error :" + ex.getMessage();
//	                // If there is an error, don't just keep trying to register.
//	                // Require the user to click a button again, or perform
//	                // exponential back-off.
//	            }
//                return msg;
//            }
//
////	        @Override
////	        protected void onPostExecute(String msg) {
////	            mDisplay.append(msg + "\n");
////	        }
//
////			@Override
////			protected Object doInBackground(Object... params) {
////				// TODO Auto-generated method stub
////				return null;
////			}
//        }.execute(null, null, null);
//
//    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
//    private void sendRegistrationIdToBackend() {
//        // Your implementation here.
//    }
//
//
//    /**
//     * Stores the registration ID and app versionCode in the application's
//     * {@code SharedPreferences}.
//     *
//     * @param context application's context.
//     * @param regId   registration ID
//     */
//    private void storeRegistrationId(Context context, String regId) {
//        final SharedPreferences prefs = getGCMPreferences(context);
//        int appVersion = getAppVersion(context);
//        Log.i("TAG", "Saving regId on app version " + appVersion);
//
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString(PROPERTY_REG_ID, regId);
//        editor.putInt(PROPERTY_APP_VERSION, appVersion);
//        editor.commit();
//    }
}


