package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.MenuPop;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.ItemSizeAdapter;
import com.cs.bakeryco.adapters.ItemsListAdapter;
import com.cs.bakeryco.adapters.ViewPagerAdapter;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.widgets.CardAdapter;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;

public class ItemsActivity extends AppCompatActivity {

    SharedPreferences languagePrefs;
    String language;
    RelativeLayout back_btn, transparent_layout;
    TextView item_title;
    public static ViewPager scrollView;
    //    ViewPager viewPagerBackground, viewPagerFront;
    DotsIndicator dotsIndicator;
    public static RecyclerView item_size_list;
    public static RecyclerView items_list;
    ImageView menu;
    public static ItemSizeAdapter itemSizeAdapter;

    public static ArrayList<ItemsList.Items> main_list = new ArrayList<>();
    public static ArrayList<ItemsList.Sections> sections = new ArrayList<>();
    public static ArrayList<ItemsList.SubCategoryItems> subcat_list = new ArrayList<>();

    int viewpagerposition = 0;
    public static int catId, position1, selection_pos = 0;
    public static String title;
    ViewPagerAdapter mAdapter;
    public static ItemsListAdapter itemsListAdapter;
    CardAdapter cardAdapter;
    ImageView menu_img;
    RelativeLayout mpicker1, size_layout;
    LinearLayout mlayout;

    public static ArrayList<String> subitemQty = new ArrayList<>();
    public static ArrayList<String> subitemfinalQty = new ArrayList<>();
    public static ArrayList<String> subitemfinalprice = new ArrayList<>();

    public static ArrayList<String> itemQty = new ArrayList<>();
    public static ArrayList<String> itemfinalQty = new ArrayList<>();
    public static ArrayList<String> itemfinalprice = new ArrayList<>();

    public static ArrayList<Integer> indivialcount = new ArrayList<>();

    ImageView bag;
    private DataBaseHelper myDbHelper;
    TextView bag_count;
    ScrollView mscroll_view;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.items_selection);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.items_selection_arabic);
        }

        myDbHelper = new DataBaseHelper(ItemsActivity.this);

        itemQty.clear();
        itemfinalQty.clear();
        itemfinalprice.clear();
        indivialcount.clear();

        bag = (ImageView) findViewById(R.id.bag);
        back_btn = (RelativeLayout) findViewById(R.id.back_btn);
        item_title = (TextView) findViewById(R.id.item_title);
        dotsIndicator = (DotsIndicator) findViewById(R.id.dots_indicator);
        item_size_list = (RecyclerView) findViewById(R.id.item_size_list);
        items_list = (RecyclerView) findViewById(R.id.items_list);
        menu = (ImageView) findViewById(R.id.menu_img);
        scrollView = (ViewPager) findViewById(R.id.picker);
        menu_img = (ImageView) findViewById(R.id.menu_img);
        bag_count = (TextView) findViewById(R.id.bag_count);
//        mscroll_view = (ScrollView) findViewById(R.id.scrollView);


        size_layout = (RelativeLayout) findViewById(R.id.size_layout);
        mpicker1 = (RelativeLayout) findViewById(R.id.picker1);

        mlayout = (LinearLayout) findViewById(R.id.layout);


        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(ItemsActivity.this, OrderConfirmationNew.class);
                startActivity(a);

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        main_list = (ArrayList<ItemsList.Items>) getIntent().getSerializableExtra("main_list");
        sections = (ArrayList<ItemsList.Sections>) getIntent().getSerializableExtra("list");
        title = getIntent().getStringExtra("title");
        catId = getIntent().getExtras().getInt("catId");
        position1 = getIntent().getExtras().getInt("postion");

        transparent_layout = findViewById(R.id.transparent_layout);

        menu_img.setVisibility(View.VISIBLE);
        transparent_layout.setVisibility(View.GONE);

        menu_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (menu_img.getVisibility() == View.VISIBLE) {

                    menu_img.setVisibility(View.GONE);
                    transparent_layout.setVisibility(View.VISIBLE);
                    Intent a = new Intent(ItemsActivity.this, MenuPop.class);
                    a.putExtra("itemsArraylist", main_list);
                    startActivity(a);
                }

            }
        });


        final LinearLayoutManager layoutManager = new LinearLayoutManager(ItemsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        item_size_list.setLayoutManager(layoutManager);

        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, sections, language, title, ItemsActivity.this);
        item_size_list.setAdapter(itemSizeAdapter);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(ItemsActivity.this);
        items_list.setLayoutManager(layoutManager1);


        if (catId == 1) {
            mAdapter = new ViewPagerAdapter(ItemsActivity.this, main_list.get(1).getSubCategoryItems(), position1, language);
        } else if (catId == 2) {
            mAdapter = new ViewPagerAdapter(ItemsActivity.this, main_list.get(0).getSubCategoryItems(), position1, language);
        } else if (catId == 3) {
            mAdapter = new ViewPagerAdapter(ItemsActivity.this, main_list.get(4).getSubCategoryItems(), position1, language);
        } else if (catId == 4) {
            mAdapter = new ViewPagerAdapter(ItemsActivity.this, main_list.get(2).getSubCategoryItems(), position1, language);
        } else if (catId == 6) {
            mAdapter = new ViewPagerAdapter(ItemsActivity.this, main_list.get(3).getSubCategoryItems(), position1, language);
        } else if (catId == 13) {
            mAdapter = new ViewPagerAdapter(ItemsActivity.this, main_list.get(5).getSubCategoryItems(), position1, language);
        } else if (catId == 14) {
            mAdapter = new ViewPagerAdapter(ItemsActivity.this, main_list.get(6).getSubCategoryItems(), position1, language);
        }
//        ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(scrollView, cardAdapter);
//        fragmentCardShadowTransformer.enableScaling(true);

        scrollView.setAdapter(mAdapter);
//        scrollView.setAnimationEnabled(true);
        scrollView.setPageMargin(10);
//        scrollView.setFadeEnabled(false);
//        scrollView.setFadeFactor(0.6f);
        scrollView.setCurrentItem(position1);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int view_pager_hieght = width - 10;

        int main_hieght = width - 10;

        int size_with = width - 100;

        Log.i("TAG", "main_hieght: " + main_hieght);
        Log.i("TAG", "view_pager_hieght: " + view_pager_hieght);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mlayout.getLayoutParams();
        layoutParams.height = main_hieght;
        mlayout.setLayoutParams(layoutParams);

        LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) mpicker1.getLayoutParams();
        layoutParams1.height = view_pager_hieght;
        mpicker1.setLayoutParams(layoutParams1);

        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) scrollView.getLayoutParams();
        layoutParams2.height = view_pager_hieght;
        scrollView.setLayoutParams(layoutParams2);

        RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) size_layout.getLayoutParams();
        layoutParams3.width = size_with;
        layoutParams3.addRule(RelativeLayout.CENTER_IN_PARENT);
        size_layout.setLayoutParams(layoutParams3);

        scrollView.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                if (language.equalsIgnoreCase("En")) {
                    int pageWidth = scrollView.getMeasuredWidth() - scrollView.getPaddingLeft() - scrollView.getPaddingRight();
                    int pageHeight = scrollView.getHeight();
                    int paddingLeft = scrollView.getPaddingLeft();
                    float transformPos = (float) (page.getLeft() - (scrollView.getScrollX() + paddingLeft)) / pageWidth;
                    final float normalizedposition = Math.abs(Math.abs(transformPos) - 1);
                    page.setAlpha(normalizedposition + 0.5f);
                    int max = -pageHeight / 10;
                    if (transformPos < -1) { // [-Infinity,-1)
                        // This page is way off-screen to the left.
                        page.setTranslationY(0);
                    } else if (transformPos <= 1) { // [-1,1]
                        page.setTranslationY(max * (1 - Math.abs(transformPos)));
                    } else { // (1,+Infinity]
                        // This page is way off-screen to the right.
                        page.setTranslationY(0);
                    }
                } else {
                    int pageWidth = scrollView.getMeasuredWidth() - scrollView.getPaddingLeft() - scrollView.getPaddingRight();
                    int pageHeight = scrollView.getHeight();
                    int paddingLeft = scrollView.getPaddingLeft();
                    float transformPos = (float) (page.getLeft() - (scrollView.getScrollX() + paddingLeft)) / pageWidth;
                    final float normalizedposition = Math.abs(Math.abs(transformPos) - 1);
                    page.setAlpha(normalizedposition + 0.5f);
                    int max = -pageHeight / 10;
                    if (transformPos < -1) { // [-Infinity,-1)
                        // This page is way off-screen to the left.
                        page.setTranslationY(0);
                    } else if (transformPos <= 1) { // [-1,1]
                        page.setTranslationY(max * (1 - Math.abs(transformPos)));
                    } else { // (1,+Infinity]
                        // This page is way off-screen to the right.
                        page.setTranslationY(0);
                    }
                }
            }
        });

        if (viewpagerposition == 0) {

            indivialcount.clear();
            selection_pos = 0;

            if (catId == 1) {
                if (language.equalsIgnoreCase("En")) {
                    title = main_list.get(1).getSubCategoryItems().get(position1).getSubCategoryName();
                } else {
                    title = main_list.get(1).getSubCategoryItems().get(position1).getSubCategoryName_Ar();
                }
                if (main_list.get(1).getSubCategoryItems().get(position1).getSections().size() == 1){
                    selection_pos = 0;
                    item_size_list.setVisibility(View.GONE);
                } else {
                    item_size_list.setVisibility(View.VISIBLE);
                    itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(1).getSubCategoryItems().get(position1).getSections(), language, title, ItemsActivity.this);
                }
                itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(1).getSubCategoryItems().get(position1).getSections(), selection_pos, language, title);
                for (int k = 0; k < main_list.get(1).getSubCategoryItems().get(position1).getSections().get(selection_pos).getItems().size(); k++) {
                    indivialcount.add(0);
                }
            } else if (catId == 2) {
                if (language.equalsIgnoreCase("En")) {
                    title = main_list.get(0).getSubCategoryItems().get(position1).getSubCategoryName();
                } else {
                    title = main_list.get(0).getSubCategoryItems().get(position1).getSubCategoryName_Ar();
                }
                if (main_list.get(0).getSubCategoryItems().get(position1).getSections().size() == 1){
                    selection_pos = 0;
                    item_size_list.setVisibility(View.GONE);
                } else {
                    item_size_list.setVisibility(View.VISIBLE);
                    itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(0).getSubCategoryItems().get(position1).getSections(), language, title, ItemsActivity.this);
                }
                itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(0).getSubCategoryItems().get(position1).getSections(), selection_pos, language, title);
                for (int k = 0; k < main_list.get(0).getSubCategoryItems().get(position1).getSections().get(selection_pos).getItems().size(); k++) {
                    indivialcount.add(0);
                }
            } else if (catId == 3) {
                if (language.equalsIgnoreCase("En")) {
                    title = main_list.get(4).getSubCategoryItems().get(position1).getSubCategoryName();
                } else {
                    title = main_list.get(4).getSubCategoryItems().get(position1).getSubCategoryName_Ar();
                }
                if (main_list.get(4).getSubCategoryItems().get(position1).getSections().size() == 1){
                    selection_pos = 0;
                    item_size_list.setVisibility(View.GONE);
                } else {
                    item_size_list.setVisibility(View.VISIBLE);
                    itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(4).getSubCategoryItems().get(position1).getSections(), language, title, ItemsActivity.this);
                }
                itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(4).getSubCategoryItems().get(position1).getSections(), selection_pos, language, title);
                for (int k = 0; k < main_list.get(4).getSubCategoryItems().get(position1).getSections().get(selection_pos).getItems().size(); k++) {
                    indivialcount.add(0);
                }
            } else if (catId == 4) {
                if (language.equalsIgnoreCase("En")) {
                    title = main_list.get(2).getSubCategoryItems().get(position1).getSubCategoryName();
                } else {
                    title = main_list.get(2).getSubCategoryItems().get(position1).getSubCategoryName_Ar();
                }
                if (main_list.get(2).getSubCategoryItems().get(position1).getSections().size() == 1){
                    selection_pos = 0;
                    item_size_list.setVisibility(View.GONE);
                } else {
                    item_size_list.setVisibility(View.VISIBLE);
                    itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(2).getSubCategoryItems().get(position1).getSections(), language, title, ItemsActivity.this);
                }
                itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(2).getSubCategoryItems().get(position1).getSections(), selection_pos, language, title);
                for (int k = 0; k < main_list.get(2).getSubCategoryItems().get(position1).getSections().get(selection_pos).getItems().size(); k++) {
                    indivialcount.add(0);
                }
            } else if (catId == 6) {
                if (language.equalsIgnoreCase("En")) {
                    title = main_list.get(3).getSubCategoryItems().get(position1).getSubCategoryName();
                } else {
                    title = main_list.get(3).getSubCategoryItems().get(position1).getSubCategoryName_Ar();
                }
                if (main_list.get(3).getSubCategoryItems().get(position1).getSections().size() == 1){
                    selection_pos = 0;
                    item_size_list.setVisibility(View.GONE);
                } else {
                    item_size_list.setVisibility(View.VISIBLE);
                    itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(3).getSubCategoryItems().get(position1).getSections(), language, title, ItemsActivity.this);
                }
                itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(3).getSubCategoryItems().get(position1).getSections(), selection_pos, language, title);
                for (int k = 0; k < main_list.get(3).getSubCategoryItems().get(position1).getSections().get(selection_pos).getItems().size(); k++) {
                    indivialcount.add(0);
                }
            } else if (catId == 13) {
                if (language.equalsIgnoreCase("En")) {
                    title = main_list.get(5).getSubCategoryItems().get(position1).getSubCategoryName();
                } else {
                    title = main_list.get(5).getSubCategoryItems().get(position1).getSubCategoryName_Ar();
                }
                if (main_list.get(5).getSubCategoryItems().get(position1).getSections().size() == 1){
                    selection_pos = 0;
                    item_size_list.setVisibility(View.GONE);
                } else {
                    item_size_list.setVisibility(View.VISIBLE);
                    itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(5).getSubCategoryItems().get(position1).getSections(), language, title, ItemsActivity.this);
                }
                itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(5).getSubCategoryItems().get(position1).getSections(), selection_pos, language, title);
                for (int k = 0; k < main_list.get(5).getSubCategoryItems().get(position1).getSections().get(selection_pos).getItems().size(); k++) {
                    indivialcount.add(0);
                }
            } else if (catId == 14) {
                if (language.equalsIgnoreCase("En")) {
                    title = main_list.get(6).getSubCategoryItems().get(position1).getSubCategoryName();
                } else {
                    title = main_list.get(6).getSubCategoryItems().get(position1).getSubCategoryName_Ar();
                }
                if (main_list.get(6).getSubCategoryItems().get(position1).getSections().size() == 1){
                    selection_pos = 0;
                    item_size_list.setVisibility(View.GONE);
                } else {
                    item_size_list.setVisibility(View.VISIBLE);
                    itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(6).getSubCategoryItems().get(position1).getSections(), language, title, ItemsActivity.this);
                }
                itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(6).getSubCategoryItems().get(position1).getSections(), selection_pos, language, title);
                for (int k = 0; k < main_list.get(6).getSubCategoryItems().get(position1).getSections().get(selection_pos).getItems().size(); k++) {
                    indivialcount.add(0);
                }
            }

            Log.i("TAG", "indivialcount: " + indivialcount.size());

            item_title.setText("" + title);
            if (item_size_list.getVisibility() == View.GONE){
                selection_pos = 0;
                item_size_list.setVisibility(View.GONE);
            } else {
                item_size_list.setVisibility(View.VISIBLE);
                item_size_list.setAdapter(itemSizeAdapter);
            }
            items_list.setAdapter(itemsListAdapter);
        }



        scrollView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

//                scrollView.getParent().requestDisallowInterceptTouchEvent(true);


//                scrollView.setPageTransformer(false, new ViewPager.PageTransformer() {
//                    @Override public void transformPage(View page, float position) {
//                        if (scrollView.getCurrentItem() == 0) {
//                            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
////
//                            // Set TextView layout margin 25 pixels to all side
//                            // Left Top Right Bottom Margin
//                            lp.setMargins(0,25,0,0);
//
//                            // Apply the updated layout parameters to TextView
//                            mitem_image.setLayoutParams(lp);
////                            page.setTranslationX(120);
//                        } else if (scrollView.getCurrentItem() == mAdapter.getCount() - 1) {
//                            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
////
//                            // Set TextView layout margin 25 pixels to all side
//                            // Left Top Right Bottom Margin
//                            lp.setMargins(0,25,0,0);
//
//                            // Apply the updated layout parameters to TextView
//                            mitem_image.setLayoutParams(lp);
////                            page.setTranslationX(120);
//                        } else {
//                            page.setTranslationX(0);
//                            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
////
//                            // Set TextView layout margin 25 pixels to all side
//                            // Left Top Right Bottom Margin
//                            lp.setMargins(0,0,0,0);
//
//                            // Apply the updated layout parameters to TextView
//                            mitem_image.setLayoutParams(lp);
//                        }
//                    }
//                });

//                mAdapter.notifyDataSetChanged();

//                if (i == i){

//                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
//
//                    // Set TextView layout margin 25 pixels to all side
//                    // Left Top Right Bottom Margin
//                    lp.setMargins(0,25,0,0);
//
//                    // Apply the updated layout parameters to TextView
//                    mitem_image.setLayoutParams(lp);
//
//                    Log.i("TAG", "instantiateItem: " + ItemsActivity.scrollView.getCurrentItem() + " " + i);


//                } else {
//
//                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
//
//                    // Set TextView layout margin 25 pixels to all side
//                    // Left Top Right Bottom Margin
//                    lp.setMargins(0,0,0,0);
//
//                    // Apply the updated layout parameters to TextView
//                    mitem_image.setLayoutParams(lp);
//
//                    Log.i("TAG", "instantiateItem1: " + ItemsActivity.scrollView.getCurrentItem() + " " + i);
//
//                }


//                position_changing = scrollView.getCurrentItem();
//                if (i == position_changing) {
//
//                    mview.setVisibility(View.VISIBLE);
//
//                } else {
//
//                    mview.setVisibility(View.GONE);
//
//                }


            }

            @Override
            public void onPageSelected(int i) {


                viewpagerposition = i;

                indivialcount.clear();
                selection_pos = 0;

                if (catId == 1) {
                    if (language.equalsIgnoreCase("En")) {
                        title = main_list.get(1).getSubCategoryItems().get(i).getSubCategoryName();
                    } else {
                        title = main_list.get(1).getSubCategoryItems().get(i).getSubCategoryName_Ar();
                    }
                    if (main_list.get(1).getSubCategoryItems().get(i).getSections().size() == 1){
                        selection_pos = 0;
                        item_size_list.setVisibility(View.GONE);
                    } else {
                        item_size_list.setVisibility(View.VISIBLE);
                        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(1).getSubCategoryItems().get(i).getSections(), language, title, ItemsActivity.this);
                    }
                    itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(1).getSubCategoryItems().get(i).getSections(), selection_pos, language, title);
                    for (int k = 0; k < main_list.get(1).getSubCategoryItems().get(i).getSections().get(selection_pos).getItems().size(); k++) {
                        indivialcount.add(1);
                    }
                } else if (catId == 2) {
                    if (language.equalsIgnoreCase("En")) {
                        title = main_list.get(0).getSubCategoryItems().get(i).getSubCategoryName();
                    } else {
                        title = main_list.get(0).getSubCategoryItems().get(i).getSubCategoryName_Ar();
                    }
                    if (main_list.get(0).getSubCategoryItems().get(i).getSections().size() == 1){
                        selection_pos = 0;
                        item_size_list.setVisibility(View.GONE);
                    } else {
                        item_size_list.setVisibility(View.VISIBLE);
                        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(0).getSubCategoryItems().get(i).getSections(), language, title, ItemsActivity.this);
                    }
                    itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(0).getSubCategoryItems().get(i).getSections(), selection_pos, language, title);
                    for (int k = 0; k < main_list.get(0).getSubCategoryItems().get(i).getSections().get(selection_pos).getItems().size(); k++) {
                        indivialcount.add(0);
                    }
                } else if (catId == 3) {
                    if (language.equalsIgnoreCase("En")) {
                        title = main_list.get(4).getSubCategoryItems().get(i).getSubCategoryName();
                    } else {
                        title = main_list.get(4).getSubCategoryItems().get(i).getSubCategoryName_Ar();
                    }
                    if (main_list.get(4).getSubCategoryItems().get(i).getSections().size() == 1){
                        selection_pos = 0;
                        item_size_list.setVisibility(View.GONE);
                    } else {
                        item_size_list.setVisibility(View.VISIBLE);
                        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(4).getSubCategoryItems().get(i).getSections(), language, title, ItemsActivity.this);
                    }
                    itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(4).getSubCategoryItems().get(i).getSections(), selection_pos, language, title);
                    for (int k = 0; k < main_list.get(4).getSubCategoryItems().get(i).getSections().get(selection_pos).getItems().size(); k++) {
                        indivialcount.add(0);
                    }
                } else if (catId == 4) {
                    if (language.equalsIgnoreCase("En")) {
                        title = main_list.get(2).getSubCategoryItems().get(i).getSubCategoryName();
                    } else {
                        title = main_list.get(2).getSubCategoryItems().get(i).getSubCategoryName_Ar();
                    }
                    if (main_list.get(2).getSubCategoryItems().get(i).getSections().size() == 1){
                        selection_pos = 0;
                        item_size_list.setVisibility(View.GONE);
                    } else {
                        item_size_list.setVisibility(View.VISIBLE);
                        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(2).getSubCategoryItems().get(i).getSections(), language, title, ItemsActivity.this);
                    }
                    itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(2).getSubCategoryItems().get(i).getSections(), selection_pos, language, title);
                    for (int k = 0; k < main_list.get(2).getSubCategoryItems().get(i).getSections().get(selection_pos).getItems().size(); k++) {
                        indivialcount.add(0);
                    }
                } else if (catId == 6) {
                    if (language.equalsIgnoreCase("En")) {
                        title = main_list.get(3).getSubCategoryItems().get(i).getSubCategoryName();
                    } else {
                        title = main_list.get(3).getSubCategoryItems().get(i).getSubCategoryName_Ar();
                    }
                    if (main_list.get(3).getSubCategoryItems().get(i).getSections().size() == 1){
                        selection_pos = 0;
                        item_size_list.setVisibility(View.GONE);
                    } else {
                        item_size_list.setVisibility(View.VISIBLE);
                        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(3).getSubCategoryItems().get(i).getSections(), language, title, ItemsActivity.this);
                    }
                    itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(3).getSubCategoryItems().get(i).getSections(), selection_pos, language, title);
                    for (int k = 0; k < main_list.get(3).getSubCategoryItems().get(i).getSections().get(selection_pos).getItems().size(); k++) {
                        indivialcount.add(0);
                    }
                } else if (catId == 13) {
                    if (language.equalsIgnoreCase("En")) {
                        title = main_list.get(5).getSubCategoryItems().get(i).getSubCategoryName();
                    } else {
                        title = main_list.get(5).getSubCategoryItems().get(i).getSubCategoryName_Ar();
                    }
                    if (main_list.get(5).getSubCategoryItems().get(i).getSections().size() == 1){
                        selection_pos = 0;
                        item_size_list.setVisibility(View.GONE);
                    } else {
                        item_size_list.setVisibility(View.VISIBLE);
                        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(5).getSubCategoryItems().get(i).getSections(), language, title, ItemsActivity.this);
                    }
                    itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(5).getSubCategoryItems().get(i).getSections(), selection_pos, language, title);
                    for (int k = 0; k < main_list.get(5).getSubCategoryItems().get(i).getSections().get(selection_pos).getItems().size(); k++) {
                        indivialcount.add(0);
                    }
                } else if (catId == 14) {
                    if (language.equalsIgnoreCase("En")) {
                        title = main_list.get(6).getSubCategoryItems().get(i).getSubCategoryName();
                    } else {
                        title = main_list.get(6).getSubCategoryItems().get(i).getSubCategoryName_Ar();
                    }
                    if (main_list.get(6).getSubCategoryItems().get(i).getSections().size() == 1){
                        selection_pos = 0;
                        item_size_list.setVisibility(View.GONE);
                    } else {
                        item_size_list.setVisibility(View.VISIBLE);
                        itemSizeAdapter = new ItemSizeAdapter(ItemsActivity.this, main_list.get(6).getSubCategoryItems().get(i).getSections(), language, title, ItemsActivity.this);
                    }
                    itemsListAdapter = new ItemsListAdapter(ItemsActivity.this, main_list.get(6).getSubCategoryItems().get(i).getSections(), selection_pos, language, title);
                    for (int k = 0; k < main_list.get(6).getSubCategoryItems().get(i).getSections().get(selection_pos).getItems().size(); k++) {
                        indivialcount.add(0);
                    }
                }

                Log.i("TAG", "indivialcount: " + indivialcount.size());

                item_title.setText("" + title);
                if (item_size_list.getVisibility() == View.VISIBLE) {
                    item_size_list.setAdapter(itemSizeAdapter);
                }
                items_list.setAdapter(itemsListAdapter);
            }

            @Override
            public void onPageScrollStateChanged(int i) {


//                i = viewpagerposition;
//
//                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mitem_image.getLayoutParams();
////
//                    // Set TextView layout margin 25 pixels to all side
//                    // Left Top Right Bottom Margin
//                    lp.setMargins(0,25,0,0);
//
//                    // Apply the updated layout parameters to TextView
//                    mitem_image.setLayoutParams(lp);
////
//                    Log.i("TAG", "instantiateItem1: " + ItemsActivity.scrollView.getCurrentItem() + " " + i);


                Log.i("TAG", "onPageScrollStateChanged: " + i);


            }
        });
//        scrollView.setPageTransformer(false, fragmentCardShadowTransformer);
//        scrollView.setOffscreenPageLimit(3);

        if (language.equalsIgnoreCase("Ar")){
            dotsIndicator.setRotation(180);
        }

        dotsIndicator.setViewPager(scrollView);


    }

    @Override
    public void onResume() {
        super.onResume();

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        menu_img.setVisibility(View.VISIBLE);
        transparent_layout.setVisibility(View.GONE);

    }

    /**
     * Change value in dp to pixels
     *
     * @param dp
     * @param context
     */
    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

}
