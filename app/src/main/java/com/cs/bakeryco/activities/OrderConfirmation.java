package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.GPSTracker;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.HorizontalListAdapter;
import com.cs.bakeryco.adapters.PromoAdapter;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Promos;
import com.cs.bakeryco.widgets.CustomListView;
import com.cs.bakeryco.widgets.DateTimePicker;
import com.cs.bakeryco.widgets.HorizontalListView;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobile.connect.PWConnect;
import com.mobile.connect.checkout.dialog.PWConnectCheckoutActivity;
import com.mobile.connect.exception.PWException;
import com.mobile.connect.exception.PWProviderNotInitializedException;
import com.mobile.connect.payment.PWAccount;
import com.mobile.connect.service.PWProviderBinder;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;

import javax.net.ssl.HttpsURLConnection;

import static com.cs.bakeryco.Constants.ORDER_TYPE;

//import android.support.annotation.Nullable;
//import com.cs.bakeryco.widgets.DateTime;

/**
 * Created by CS on 15-06-2016.
 */
//public class OrderConfirmation extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DateTimePicker.DateWatcher {
    public class OrderConfirmation extends AppCompatActivity {
//    Toolbar toolbar;
//    TextView userName, storeNameTextView, storeAddressTextView, totalItems, totalAmount, estDate, estTime;
//    EditText mobileNo;
//    TextView storeAddressTxt;
//    TextView changeEstTime;
//    HorizontalListView horizontalListView;
//    HorizontalListAdapter mAdapter;
    Boolean isDateSet = false;
    Date datetime = null;
    //    private PromoAdapter mPromoAdapter;
//    private ArrayList<Promos> promosList = new ArrayList<>();
//    String promocodeStr = "No", promoIdStr = "", promoTypeStr = "";
    //    EditText promocode;
    int remainingBonusInt;
//    //    TextView promocode;
////    RelativeLayout promoLayout;
////    ImageView promoCancel, promoArrow;
//    LinearLayout onlinePayment, cashPayment;
//    ImageView monline_right, mcash_on_right;
    int totalItemsCount;
    int totalAmountVal;
    int storeDistance;
    float distanceKilometersFloat;
    int distanceKilometers;
    static final int TIME_DIALOG_ID = 1111;
    List<Date> listTimes;
    private int hour;
    private int minute;
    private int hours;
    private int minutes;
    private int seconds;
    private int timepicker = 0;

    Date timeformat = null;
    Date expDateTime = null;

//    ImageView storeaddress;


    Context ctx;
    boolean checkStatus;
    private Handler handler = new Handler();
    boolean killHandler = false;
    Date oldTime = null;
    Calendar c;
    private String deviceToken;
    String userId, mamount, mvatamount;
    ProgressDialog dialog;
//    Button confirmOrder, editOrder;
    int paymentMode = 2;
    //    String promocodeStr = "No";
    String orderType, fullHours;
    String pickerViewOpen, exp_time_to_dumy;
    String endTime, openTimeStr, todayDate, tomorrowDate;
    String changeTimeIsYes = "false", payerTimeIsYes = "false", No_DistanceStr = "true";
    String expTimeTo;
//    Button backBtn;
    boolean flag = true;
    int minuts, check_count, changeMints;
    String freeOrderId = "";

//    TextView totalcount;

//    TextView mcount_basket;

//    RelativeLayout mcount;

    private Timer timer = new Timer();
    private String expChangeStr = "0000";

    GPSTracker gps;
    private double lat, longi;

//    LinearLayout mDateTimePicker;

    Boolean isToday;

//    CustomListView listview;
//    ConfirmationListAdapter mAdapters;

//    TextView vatPercent, receipt_close;
//    ImageView minvoice;
//    TextView netTotal, vatAmount, amount;
    float vat = 5;
//    RelativeLayout recieptLayout;

    DecimalFormat decim = new DecimalFormat("0.00");
    float tax;

    String time, date, time1;

    private int mYear, mMonth, mDay, mHour, mMinute;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    private String setCardResponse = null;

    private String getCardResponse = null;

    private String timeResponse = null;
    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private String distanceResponse = null;
    private int trafficMins;
    String secs;

//    TextView promocode, sr;
//    RelativeLayout promoLayout;
//    ImageView promoCancel, promoArrow;
    private PromoAdapter mPromoAdapter;
    private ArrayList<Promos> promosList = new ArrayList<>();
    String promocodeStr = "No", promoIdStr = "", promoTypeStr = "";
    JSONArray promoArray = new JSONArray();

    String ItemType;

//    TextView mdate_field, mtime_field;
//    TextView mview_order;

    SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm:ss", Locale.US);

    SharedPreferences userPrefs;
    String response;
    private String storeId, storeName, storeAddress, storeName_Ar, storeAddress_Ar;
    private String address, addressLat, addressLong, addressId, landmark, deliveryST, delivertET, perparationtime, bussinessday;
    private Double latitude, longitude;
    private GoogleMap map;
    MarkerOptions markerOptions;
    private DataBaseHelper myDbHelper;

    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;

    ArrayList<Order> orderList = new ArrayList<>();
    final DecimalFormat decim1 = new DecimalFormat("0.00");


    public static final String PREFS_NAME = "MCOMMERCE_SAMPLE";
    public static final String ACCOUNTS = "ACCOUNTS";

    private PWProviderBinder _binder;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _binder = (PWProviderBinder) service;
            try {
                // replace by custom sandbox access
                _binder.initializeProvider(PWConnect.PWProviderMode.TEST, "Hyperpay.Oregano.mcommerce", "a9ac6927646211e69325035d15b6ff20");
            } catch (PWException ee) {
                ee.printStackTrace();
            }
            Log.i("mainactivity", "bound to remote service...!");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            _binder = null;
        }
    };

    /**
     * A list of the stored accounts
     */
    private List<PWAccount> accounts = new ArrayList<>();

    /**
     * Reference to the preferences where the accounts are stored
     */
    private SharedPreferences sharedSettings;

    SharedPreferences languagePrefs;
    String language;
    String[] order_type_txt_count;

    String PreparationTime;
    int TravelTime = 0, PrayingTime = 0;
    Date kitchenStartDate = null;

    Spinner order_type_spinner;
    TextView mname, mobile_no, order_type_txt, maddress, estDate, estTime, item_total, vatAmount, service_change, coupon_discount, total_price1, confirm_order_btn;
//    LinearLayout changeEstTime;
    ImageView mcard;
    TextView mcash;
    RecyclerView order_details_list;
//    private ArrayAdapter<String> SpinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.checkout_layout);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.order_confirmation_arabic);
        }

//        sharedSettings = getSharedPreferences(PREFS_NAME, 0);
//        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
//        userId = userPrefs.getString("userId", null);
//        response = userPrefs.getString("user_profile", null);
//
//        myDbHelper = new DataBaseHelper(OrderConfirmation.this);
//
//
//        // start the PWConnect service
////        startService(new Intent(OrderConfirmation.this, PWConnectService.class));
////        bindService(new Intent(OrderConfirmation.this, PWConnectService.class), serviceConnection, Context.BIND_AUTO_CREATE);
//
//
//        orderList = myDbHelper.getOrderInfo();
//
//        orderType = ORDER_TYPE;
//
//        storeId = "1";
//        storeName = "Bakery & Company";
//        storeName_Ar = "";
//        storeAddress = "New Industrial Area, Riyadh, Saudi Arabia";
//        latitude = 24.533966;
//        longitude = 46.899342;
//        fullHours = "true";
//
//        PreparationTime = getIntent().getExtras().getString("preparationtime");
//
//        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
//        orderPrefsEditor = orderPrefs.edit();
//
//        myDbHelper = new DataBaseHelper(OrderConfirmation.this);
//
//        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//
////        storeId = getIntent().getExtras().getString("storeId");
////        storeName = getIntent().getExtras().getString("storeName");
////        storeAddress = getIntent().getExtras().getString("storeAddress");
////        storeName_Ar = getIntent().getExtras().getString("storeName_ar");
////        storeAddress_Ar = getIntent().getExtras().getString("storeAddress_ar");
////        latitude = getIntent().getExtras().getDouble("latitude");
////        longitude = getIntent().getExtras().getDouble("longitude");
//
////        Log.e("TAG","fullhours "+getIntent().getExtras().getString("full_hours"));
////        fullHours = getIntent().getExtras().getString("full_hours");
//
//        order_type_spinner = findViewById(R.id.order_type_spinner);
//        mname = (TextView) findViewById(R.id.name);
//        mobile_no = (TextView) findViewById(R.id.mobile_no);
//        order_type_txt = (TextView) findViewById(R.id.order_type_txt);
//        maddress = (TextView) findViewById(R.id.address);
////        onlinePayment = (LinearLayout) findViewById(R.id.online_payment);
////        cashPayment = (LinearLayout) findViewById(R.id.cash_on_pickup);
//        item_total = (TextView) findViewById(R.id.item_total);
//        service_change = (TextView) findViewById(R.id.service_charge);
//        estDate = (TextView) findViewById(R.id.exp_date);
//        estTime = (TextView) findViewById(R.id.exp_time);
//        coupon_discount = (TextView) findViewById(R.id.coupon_discount);
//
//        total_price1 = (TextView) findViewById(R.id.total_price1);
//        confirm_order_btn = (TextView) findViewById(R.id.confirm_order_btn);
//
//        mcard = findViewById(R.id.card);
//        mcash = findViewById(R.id.cash);
//
////        order_details_list = findViewById(R.id.order_details_list);(new design)
//
////        storeaddress = (ImageView) findViewById(R.id.store_address);
////        monline_right = (ImageView) findViewById(R.id.online_right);
////        mcash_on_right = (ImageView) findViewById(R.id.cash_on_right);
////        mcash_on_right.setVisibility(View.VISIBLE);
//
////        promocode = (EditText) findViewById(R.id.promo_code);
//
////        changeEstTime = (LinearLayout) findViewById(R.id.expected_layout);(new design)
////        confirmOrder = (Button) findViewById(R.id.confirm_order_btn);
////        editOrder = (Button) findViewById(R.id.edit_order_btn);
//
////        listview = (CustomListView) findViewById(R.id.horizontal_listview);
//
////        mcount = (RelativeLayout) findViewById(R.id.count);
//
////        mcount_basket = (TextView) findViewById(R.id.total_count);
//
////        promoLayout = (RelativeLayout) findViewById(R.id.promo_layout);
////        promoCancel = (ImageView) findViewById(R.id.promo_cancel);
////        promoArrow = (ImageView) findViewById(R.id.promo_arrow);
////        promocode = (TextView) findViewById(R.id.promo_code);
//
////        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
////        receipt_close = (TextView) findViewById(R.id.receipt_close);
////        amount = (TextView) findViewById(R.id.amount);
//        vatAmount = (TextView) findViewById(R.id.vat);
////        vatPercent = (TextView) findViewById(R.id.vatPercent);
////        netTotal = (TextView) findViewById(R.id.net_total);
////        minvoice = (ImageView) findViewById(R.id.invoice);
//
//        mPromoAdapter = new PromoAdapter(OrderConfirmation.this, promosList);
//
////        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//
//        if (language.equalsIgnoreCase("En")){
//            order_type_txt_count = new String[]{"Pick Up", "Delivery"};
//        } else {
//            order_type_txt_count = new String[]{"طريقة التوصيل", "استلام"};
//        }
////
////        complete_bg.setVisibility(View.GONE);
////        morder_type_txt_view.setVisibility(View.GONE);
////
//        if (language.equalsIgnoreCase("En")) {
//            order_type_txt.setText("" + order_type_txt_count[0]);
//        } else {
//            order_type_txt.setText("" + order_type_txt_count[0]);
//        }
//
////        SpinnerAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, order_type_txt_count) {
////                public View getView(int position, View convertView, ViewGroup parent) {
////                    View v = super.getView(position, convertView, parent);
////
////                    ((TextView) v).setTextSize(1);
////                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
////
////                    return v;
////                }
////
////                public View getDropDownView(int position, View convertView, ViewGroup parent) {
////                    View v = super.getDropDownView(position, convertView, parent);
////                    v.setBackgroundResource(R.color.white);
////                    ((TextView) v).setTextSize(15);
////                    ((TextView) v).setGravity(Gravity.LEFT);
////                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
////
////                    return v;
////                }
////            };
////
////            order_type_spinner.setAdapter(SpinnerAdapter);
////
////
////            order_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
////                @Override
////                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
////
////                    order_type_txt.setText(order_type_txt_count[i]);
////
////                    if (order_type_txt.getText().toString().equalsIgnoreCase("Pick Up")){
////
////                        maddress.setText("" + storeAddress);
////
////                    } else {
////
////                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//////                                if (language.equalsIgnoreCase("En")) {
////                        // set title
////                        alertDialogBuilder.setTitle("BAKERY & Co.");
////
////                        // set dialog message
////                        alertDialogBuilder
////                                .setMessage("Please select Youur delivery address")
////                                .setCancelable(false)
////                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
////                                    public void onClick(DialogInterface dialog, int id) {
////
////                                        Intent  a = new Intent(OrderConfirmation.this, AddressActivity.class);
////                                        startActivity(a);
////
////                                        dialog.dismiss();
////                                    }
////                                });
//////                                } else if (language.equalsIgnoreCase("Ar")) {
//////                                    // set title
//////                                    alertDialogBuilder.setTitle("اوريجانو");
//////
//////                                    // set dialog message
//////                                    alertDialogBuilder
//////                                            .setMessage(" إعادة ضبط كلمة السر تمت بنجاح")
//////                                            .setCancelable(false)
//////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//////                                                public void onClick(DialogInterface dialog, int id) {
//////                                                    dialog.dismiss();
//////                                                    Intent loginIntent = new Intent(getActivity(), MainActivity.class);
//////                                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//////                                                    startActivity(loginIntent);
//////                                                    finish();
//////                                                }
//////                                            });
//////                                }
////
////                        // create alert dialog
////                        AlertDialog alertDialog = alertDialogBuilder.create();
////
////                        // show it
////                        alertDialog.show();
////
////                        maddress.setText("");
////
////                    }
////
////                }
////
////                @Override
////                public void onNothingSelected(AdapterView<?> adapterView) {
////
////                }
////            });
//
//        if (orderType.equalsIgnoreCase("Delivery")) {
//            maddress.setVisibility(View.GONE);
////                    addressLat = getIntent().getExtras().getString("user_latitude");
////                    addressLong = getIntent().getExtras().getString("user_longitude");
////
////                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
////                            Uri.parse("http://maps.google.com/maps?saddr=" + lat + "," + longi + "&daddr=" + addressLat + "," + addressLong));
////                    startActivity(intent);
//        } else {
//            storeaddress.setVisibility(View.VISIBLE);
//            storeaddress.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(Intent.ACTION_VIEW,
//                            Uri.parse("http://maps.google.com/maps?saddr=" + lat + "," + longi + "&daddr=" + latitude + "," + longitude));
//                    startActivity(intent);
//                }
//            });
//        }
//
//
//        mAdapters = new ConfirmationListAdapter(OrderConfirmation.this, orderList, language);
//        listview.setAdapter(mAdapters);
//
//        mview_order.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listview.setVisibility(View.VISIBLE);
//            }
//        });
//
//        promoLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i("TAG", "url " + Constants.GET_PROMOS_URL + userId + "&DToken=" + SplashActivity.regid);
//                new GetPromocodeResponse().execute(Constants.GET_PROMOS_URL + userId + "&DToken=" + SplashActivity.regid);
//            }
//        });
////        horizontalListView = (HorizontalListView) findViewById(R.id.horizontal_listview);
////        mAdapter = new HorizontalListAdapter(OrderConfirmation.this, orderList);
////        mPromoAdapter = new PromoAdapter(OrderConfirmation.this ,promosList);
////        horizontalListView.setAdapter(mAdapter);
//
//        receipt_close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                recieptLayout.setVisibility(View.GONE);
//            }
//        });
//
//
//        minvoice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                recieptLayout.setVisibility(View.VISIBLE);
//            }
//        });
//        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//        amount.setText(" " + decim.format(myDbHelper.getTotalOrderPrice()));
//        vatAmount.setText("" + decim.format(tax));
//        netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
//        listTimes = new ArrayList<>();
//
//        if (response != null) {
//            try {
//                JSONObject property = new JSONObject(response);
//                JSONObject userObjuect = property.getJSONObject("profile");
//
//                userName.setText(userObjuect.getString("fullName"));
//                mobileNo.setText("+" + userObjuect.getString("mobile"));
//            } catch (JSONException e) {
//                Log.d("", "Error while parsing the results!");
//                e.printStackTrace();
//            }
//        }
//
//        if (orderType.equalsIgnoreCase("Delivery")) {
//            storeName = getIntent().getExtras().getString("your_address");
//            storeAddress = getIntent().getExtras().getString("landmark");
//            if (language.equalsIgnoreCase("En")) {
//                storeAddressTxt.setText("Your Address");
//            } else if (language.equalsIgnoreCase("Ar")) {
//                storeAddressTxt.setText("عنوانك");
//            }
//            addressId = getIntent().getExtras().getString("address_id");
//            storeNameTextView.setTextSize(12);
//            storeAddressTextView.setTextSize(12);
//        }
//        if (language.equalsIgnoreCase("En")) {
//            totalItems.setText("" + myDbHelper.getTotalOrderQty());
//        } else if (language.equalsIgnoreCase("Ar")) {
////            horizontalListView.setSelection(orderList.size() - 1);
//            totalItems.setText("" + myDbHelper.getTotalOrderQty());
//        }
//        if (language.equalsIgnoreCase("En")) {
//            storeNameTextView.setText(WordUtils.capitalizeFully(storeName));
//            storeAddressTextView.setText(WordUtils.capitalizeFully(storeAddress));
//        } else if (language.equalsIgnoreCase("Ar")) {
//            storeNameTextView.setText(WordUtils.capitalizeFully(storeName_Ar));
//            storeAddressTextView.setText(WordUtils.capitalizeFully(storeAddress_Ar));
//        }
//        totalItemsCount = myDbHelper.getTotalOrderQty();
//
//        final double price;
//        price = myDbHelper.getTotalOrderPrice();
//        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//        totalAmount.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
//        Log.e("TAG", "" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
//        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
////        lat = 24.70321657;
////        longi = 46.68097073;
//        gps = new GPSTracker(OrderConfirmation.this);
//        if (gps.canGetLocation()) {
//
//            lat = gps.getLatitude();
//            longi = gps.getLongitude();
//            // \n is for new line
////            Toast.makeText(getActivity(), "Your Location is - \nLat: " + lat + "\nLong: " + longi, Toast.LENGTH_LONG).show();
//        } else {
//            // can't get location
//            // GPS or Network is not enabled
//            // Ask user to enable GPS/network in settings
//            gps.showSettingsAlert();
//        }
//
//        changeEstTime.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                pickerViewOpen = "open";
//                exp_time_to_dumy = "";
//                timepicker = 0;
//                datepicker();
////                new GetCurrentTime().execute();
////                datepicker();
//                //showDialog(TIME_DIALOG_ID);
////                new TimePickerDialog(ctx, timePickerListener, hour, minute,
////                        false).show();
//
//            }
//
//        });
//
////        onlinePayment.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                paymentMode = 3;
////                monline_right.setVisibility(View.VISIBLE);
////                mcash_on_right.setVisibility(View.INVISIBLE);
////                if (!totalAmount.getText().toString().equalsIgnoreCase("free")) {
//////                    if (isChecked)
//////                        cashPayment.setChecked(false);
//////
////                } else {
//////                    onlinePayment.setChecked(false);
////                }
////            }
////        });
//
//        cashPayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                if (isChecked)
////                    onlinePayment.setChecked(false);
//                paymentMode = 2;
////                monline_right.setVisibility(View.INVISIBLE);
//                mcash_on_right.setVisibility(View.VISIBLE);
//
//            }
//        });
//
////        if (onlinePayment.performClick()) {
////
////
////        } else if (cashPayment.performClick()) {
////        }
//
//        confirmOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                confirmOrder.setClickable(false);
//                if (paymentMode == 3) {
////                    Calendar now = Calendar.getInstance();
////                    Intent i = new Intent(OrderConfirmation.this, PWConnectCheckoutActivity.class);
////                    PWConnectCheckoutSettings settings = null;
////                    PWPaymentParams genericParams = null;
////
////                    try {
////                        double number;
////                        number=myDbHelper.getTotalOrderPrice();
////                        DecimalFormat decim1 = new DecimalFormat("0.00");
////                        // configure amount, currency, and subject of the transaction
////                        genericParams = _binder.getPaymentParamsFactory().createGenericPaymentParams(Double.parseDouble(decim1.format(number)), PWCurrency.SAUDI_ARABIA_RIYAL, "test subject");
////                        // configure payment params with customer data
//////                        genericParams.setCustomerGivenName("Aliza");
//////                        genericParams.setCustomerFamilyName("Foo");
//////                        genericParams.setCustomerAddressCity("Sampletown");
//////                        genericParams.setCustomerAddressCountryCode("SA");
//////                        genericParams.setCustomerAddressState("PA");
//////                        genericParams.setCustomerAddressStreet("123 Grande St");
//////                        genericParams.setCustomerAddressZip("1234");
//////                        genericParams.setCustomerEmail("aliza.foo@foomail.com");
//////                        genericParams.setCustomerIP("255.0.255.0");
////                        genericParams.setCustomIdentifier(storeId + now.get(Calendar.YEAR) + now.get(Calendar.MONTH) + now.get(Calendar.DATE) + estTime.getText().toString().replace(" ", "").replace(":", ""));
////
////
////                        // create the settings for the payment screens
////                        settings = new PWConnectCheckoutSettings();
////                        settings.setHeaderDescription("Food & Beverages");
////                        settings.setHeaderIconResource(R.drawable.ic_launcher);
//////                        settings.setPaymentVATAmount(4.5);
////                        settings.setSupportedDirectDebitCountries(new String[]{"SA"});
////                        settings.setSupportedPaymentMethods(new PWConnectCheckoutPaymentMethod[]{PWConnectCheckoutPaymentMethod.VISA, PWConnectCheckoutPaymentMethod.MASTERCARD,
////                                PWConnectCheckoutPaymentMethod.DIRECT_DEBIT});
////                        // ask the user if she wants to store the account
////                        settings.setCreateToken(PWConnectCheckoutCreateToken.PROMPT);
////
////                        // retrieve the stored accounts from the settings
//////                        accounts = _binder.getAccountFactory().deserializeAccountList(sharedSettings.getString(ACCOUNTS, _binder.getAccountFactory().serializeAccountList(new ArrayList<PWAccount>())));
////                        settings.setStoredAccounts(accounts);
////
////                        i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_SETTINGS, settings);
////                        i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_GENERIC_PAYMENT_PARAMS, genericParams);
////                        startActivityForResult(i, PWConnectCheckoutActivity.CONNECT_CHECKOUT_ACTIVITY);
////                    } catch (PWException e) {
////                        Log.e("connect", "error creating the payment page", e);
////                    }
//                } else {
//                    new InsertOrder().execute();
//                }
//            }
//        });
//
//        editOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(OrderConfirmation.this, OrderFragment.class);
////                intent.putExtra("startWith",1);
////                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                finish();
//            }
//        });
//
////        promoLayout.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                new GetPromocodeResponse().execute(Constants.GET_PROMOS_URL+userId+"&DToken="+ SplashActivity.regid);
////            }
////        });
////
//        promoCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                promoLayout.setClickable(true);
//                promocode.setText("");
//                promoArrow.setVisibility(View.VISIBLE);
//                promoCancel.setVisibility(View.GONE);
//                promocode.setHint("Apply Promotion");
//                tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//                totalAmount.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
//                if (language.equalsIgnoreCase("En")) {
//                    promocode.setHint("Apply Promotion");
//                } else if (language.equalsIgnoreCase("Ar")) {
//                    promocode.setHint("قدم للعرض");
//                }
//            }
//        });
//
//        new GetCurrentTime().execute();
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        mcount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (myDbHelper.getTotalOrderQty() == 0) {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("There is no items in your order? To proceed checkout please add the items")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
////                     set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
//                                .setCancelable(false)
//                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        Intent a = new Intent(OrderConfirmation.this, CheckoutActivity.class);
//                                        startActivity(a);
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        myDbHelper.deleteOrderTable();
//                                        try {
//                                            OrderTypeActivity.mcount_basket.setText("0");
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        mcount_basket.setText("0");
//                                        Intent a = new Intent(OrderConfirmation.this, OrderFragment.class);
//                                        startActivity(a);
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
//                                .setCancelable(false)
//                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        myDbHelper.deleteOrderTable();
//
//                                        try {
//                                            OrderTypeActivity.mcount_basket.setText("0");
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        mcount_basket.setText("0");
//                                        Intent a = new Intent(OrderConfirmation.this, OrderFragment.class);
//                                        startActivity(a);
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        Intent a = new Intent(OrderConfirmation.this, CheckoutActivity.class);
//                                        startActivity(a);
//                                    }
//                                });
//                    }
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }
//
//            }
//        });
//
//        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//    }
//
//    public void datepicker() {
//
//        SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//        SimpleDateFormat timeFormat6 = new SimpleDateFormat("dd-MM-yyyy HH/mm", Locale.US);
//
//        Date currentServerDate = null;
//        try {
//            currentServerDate = timeFormat5.parse(timeResponse);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        String[] perp_time= PreparationTime.split(":");
//        int perp_time1 = Integer.parseInt(perp_time[0])*60;
//        perp_time1 = perp_time1 + Integer.parseInt(perp_time[1]);
//
//        Calendar now = Calendar.getInstance();
//        now.setTime(currentServerDate);
//        now.add(Calendar.MINUTE, perp_time1);
//        if (orderType.equalsIgnoreCase("Delivery")){
//            now.add(Calendar.MINUTE, TravelTime);
//        }
//        currentServerDate = now.getTime();
//
//        try {
//            String finalDateString = timeFormat6.format(currentServerDate);
//            datetime = timeFormat6.parse(finalDateString);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        final Dialog mDateTimeDialog = new Dialog(OrderConfirmation.this);
//        // Inflate the root layout
//        final RelativeLayout mDateTimeDialogView ;
//        if (language.equalsIgnoreCase("En")) {
//             mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.date_time_picker, null);
//        }else {
//             mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.data_time_picker_arabic, null);
//        }
//        // Grab widget instance
//        final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
//        mDateTimePicker.initData(datetime);
//        mDateTimeDialog.setCancelable(false);
//        mDateTimePicker.setDateChangedListener(OrderConfirmation.this);
//
//        ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new View.OnClickListener
//                () {
//            public void onClick(View v) {
//                SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MMM/yyyy/HH/mm", Locale.US);
//                SimpleDateFormat pickerDateFormat = new SimpleDateFormat("dd/MMM/yyyy", Locale.US);
//                SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//                SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
//                SimpleDateFormat timeFormat3 = new SimpleDateFormat("HH:mm", Locale.US);
//                SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//
//                mDateTimePicker.clearFocus();
//                String result_string = mDateTimePicker.getDay() + "/" + String.valueOf(mDateTimePicker.getMonth()) + "/" + String.valueOf(mDateTimePicker.getYear())
//                        + "/" + String.valueOf(mDateTimePicker.getHour()) + "/" + String.valueOf(mDateTimePicker.getMinute());
//                String userSelectedDateStr = mDateTimePicker.getDay() + "/" + String.valueOf(mDateTimePicker.getMonth()) + "/" + String.valueOf(mDateTimePicker.getYear());
//                String userSelectedTimeStr = String.valueOf(mDateTimePicker.getHour()) + ":" + String.valueOf(mDateTimePicker.getMinute());
//
//                Date userSelectedDate = null, pickerDate = null, stDate = null, edDate = null, userSelectedtime = null, finaldate = null;
//                String deliveryST, deliveryET;
//
//                deliveryST = getIntent().getExtras().getString("deliverystarttime");
//                deliveryET = getIntent().getExtras().getString("deliveryendtime");
//
//                try {
//                    userSelectedDate = pickerDateFormat.parse(userSelectedDateStr);
//                    userSelectedtime = timeFormat3.parse(userSelectedTimeStr);
//                    pickerDate = timeFormat.parse(result_string);
//                    userSelectedDateStr = timeFormat1.format(userSelectedDate);
//                    result_string = timeFormat5.format(pickerDate);
//                    userSelectedTimeStr = timeFormat2.format(userSelectedtime);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//                try {
//                    userSelectedDate = timeFormat1.parse(userSelectedDateStr);
//                    pickerDate = timeFormat5.parse(result_string);
//                    userSelectedtime = timeFormat2.parse(userSelectedTimeStr);
//                    edDate = timeFormat2.parse(deliveryET);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                userSelectedDateStr = timeFormat1.format(userSelectedDate);
//
//                mDateTimeDialog.dismiss();
//
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(datetime);
//                calendar.add(Calendar.DATE, 90);
//                Date PeriodEndDate = calendar.getTime();
//
//                String[] perp_time2 = PreparationTime.split(":");
//                int kitchenTime = Integer.parseInt(perp_time2[0])*60;
//                kitchenTime = kitchenTime + Integer.parseInt(perp_time2[1]);
//                calendar.setTime(pickerDate);
//                calendar.add(Calendar.MINUTE, kitchenTime);
//                kitchenStartDate = calendar.getTime();
//
//                if (orderType.equalsIgnoreCase("Delivery")) {
//
//                    if (getIntent().getExtras().getString("bussinessday").equalsIgnoreCase("0")) {
//                        String startDateStr = userSelectedDateStr + " " + deliveryST;
//                        String endDateStr = userSelectedDateStr + " " + deliveryET;
//                        if (pickerDate.before(datetime) || pickerDate.after(PeriodEndDate)) {
//                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//                            alertDialogBuilder.setTitle("BAKERY & Co.");
//                            alertDialogBuilder
//                                    .setMessage("Order can't be processed for selected time")
//                                    .setCancelable(false)
//                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                            datepicker();
//                                        }
//                                    });
//                            AlertDialog alertDialog = alertDialogBuilder.create();
//                            alertDialog.show();
//                        } else {
//                            checkTimeAvailable(pickerDate, startDateStr, endDateStr, deliveryST, deliveryET);
//                        }
//                    } else {
//                        if (userSelectedtime.after(edDate)) {
//                            if (pickerDate.before(datetime) || pickerDate.after(PeriodEndDate)) {
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//                                alertDialogBuilder
//                                        .setMessage("Order can't be processed for selected time")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                                datepicker();
//                                            }
//                                        });
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//                                alertDialog.show();
//                            } else {
//                                //Start date is today, end date is tomorrow
//                                Calendar now = Calendar.getInstance();
//                                now.setTime(userSelectedDate);
//                                now.add(Calendar.DATE, 1);
//                                edDate = now.getTime();
//                                String edDateStr = timeFormat1.format(edDate);
//
//                                String startDateStr = userSelectedDateStr + " " + deliveryST;
//                                String endDateStr = edDateStr + " " + deliveryET;
//                                checkTimeAvailable(pickerDate, startDateStr, endDateStr, deliveryST, deliveryET);
//                            }
//                        } else {
//                            if (pickerDate.before(datetime) || pickerDate.after(PeriodEndDate)) {
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//                                alertDialogBuilder
//                                        .setMessage("Order can't be processed for selected time")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                                datepicker();
//                                            }
//                                        });
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//                                alertDialog.show();
//                            } else {
//                                //Start date is yesterday, end date is today
//                                Calendar now = Calendar.getInstance();
//                                now.setTime(userSelectedDate);
//                                now.add(Calendar.DATE, -1);
//                                stDate = now.getTime();
//                                String sdDateStr = timeFormat1.format(stDate);
//
//                                String startDateStr = sdDateStr + " " + deliveryST;
//                                String endDateStr = userSelectedDateStr + " " + deliveryET;
//
//                                checkTimeAvailable(pickerDate, startDateStr, endDateStr, deliveryST, deliveryET);
//                            }
//                        }
//                    }
//                }
//                else{
//                    if (pickerDate.before(datetime) || pickerDate.after(PeriodEndDate)) {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//                        alertDialogBuilder
//                                .setMessage("Order can't be processed for selected time")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                        datepicker();
//                                    }
//                                });
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//                        alertDialog.show();
//                    } else {
//                        setEstimatedTime(pickerDate);
//                    }
//                }
//            }
//        });
//
//        // Cancel the dialog when the "Cancel" button is clicked
//        ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                mDateTimeDialog.cancel();
//                if(!isDateSet){
//                    finish();
//                }
//            }
//        });
//        // No title on the dialog window
//        mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        // Set the dialog content view
//        mDateTimeDialog.setContentView(mDateTimeDialogView);
//        // Display the dialog
//        mDateTimeDialog.show();
//    }
//
//    public void checkTimeAvailable(Date pickerDate, String startDateStr, String endDateStr,
//                                   String deliveryST, String deliveryET){
//        Date startDate = null, endDate = null;
//        SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//        try {
//            startDate = timeFormat5.parse(startDateStr);
//            endDate = timeFormat5.parse(endDateStr);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        if((pickerDate.after(startDate) || pickerDate.equals(startDate)) &&
//                pickerDate.before(endDate) || pickerDate.equals(endDate)){
//            setEstimatedTime(pickerDate);
//            isDateSet = true;
//        }
//        else{
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//            alertDialogBuilder.setTitle("BAKERY & Co.");
//            alertDialogBuilder
//                    .setMessage("Order can't be processed for selected time, please select delivery time between " + deliveryST + " to " + deliveryET + ".")
//                    .setCancelable(false)
//                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.dismiss();
//                            datepicker();
//                        }
//                    });
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
//        }
//    }
//    public void setEstimatedTime(Date pickerDate){
//        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
//        SimpleDateFormat timeFormat4 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//        SimpleDateFormat timeformat6 = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US);
//        SimpleDateFormat timeFormat7 = new SimpleDateFormat("HH:mm",Locale.US);
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(pickerDate);
//
//        expTimeTo = timeformat6.format(pickerDate);
//        Date finaldate = null;
//        try {
//            finaldate = timeformat6.parse(expTimeTo);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        date = timeFormat4.format(finaldate);
//        time = timeFormat7.format(finaldate);
//        time1 = timeFormat2.format(finaldate);
//
//        Date finaldate1 = null;
//        String finaldate2;
//
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM,yyyy",Locale.US);
//
//        try {
//            finaldate1 = timeFormat4.parse(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        finaldate2 = dateFormat.format(finaldate1);
//
//        estDate.setText(""+finaldate2);
//        estTime.setText(" "+time1);
//    }
//
//    // Used to convert 24hr format to 12hr format with AM/PM values
//
//    private void updateTime(int hours, int mins, int atTime) {
//
//        int finaHours = hours;
//        int finalMins = mins;
//        String timeSet = "";
//        if (hours > 12) {
//            hours -= 12;
//            timeSet = "PM";
//        } else if (hours == 0) {
//            hours += 12;
//            timeSet = "AM";
//        } else if (hours == 12)
//            timeSet = "PM";
//        else
//            timeSet = "AM";
//
//        String minutes = "";
//        if (mins < 10)
//            minutes = "0" + mins;
//        else
//            minutes = String.valueOf(mins);
//
//        // Append in a StringBuilder
//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
//        Date parseD = doParse(aTime);
////        if (hours < 6 && timeSet.equals("AM") || hours > 10 && timeSet.equals("PM")) {
////            Toast.makeText(OrderConfirmation.this, "Please Select Delivery Time Between 6 AM To 10 PM", Toast.LENGTH_SHORT).show();
////            return;
////        } else {
////            if (mMonth < 9) {
////                date = mDay + "-0" + (mMonth + 1) + "-" + mYear;
////            } else {
////                date = mDay + "-" + (mMonth + 1) + "-" + mYear;
////            }
////            estDate.setText(date);
////            try {
////                timeformat = timeFormat1.parse(finaHours + ":" + finalMins + ":" + seconds);
////            } catch (ParseException e) {
////                e.printStackTrace();
////            }
////            Log.i("TAG", "timeformat " + timeformat);
////            Calendar time2 = Calendar.getInstance();
////            time2.setTime(timeformat);
////            time1 = String.valueOf(time2.getTime());
////            time = new SimpleDateFormat("HH:mm:ss", Locale.US).format(timeformat);
////            time1 = new SimpleDateFormat("hh:mm a", Locale.US).format(timeformat);
////            Log.i("TAG", "esttime4 " + time1);
////            estTime.setText(" " + time1);
////            Log.e("TAG", "time1" + String.valueOf(timeformat));
////        }
//        if (atTime == 1)
////            if(oldTime!=null){
//////                int diff = getTimeCaluculation();
////                Log.v("srinu", "old time not null: "+diff);
////                if(diff<0){
////                    Toast.makeText(ctx,"Please select future time",Toast.LENGTH_SHORT).show();
////                    return;
////                }
////            }
//        /*if(atTime == 1||atTime == 2){
//            oldTime = parseD;
//
//
//            Log.v("srinu", "old time: ");
//        }else{
//            Log.v("srinu", "old time nnnn: ");
//        }*/
//            oldTime = parseD;
//        checkStatus = false;
//
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(parseD);
//    }
//
//    public void getTimeCaluculation() {
//        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
//        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
//        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
//        SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
//        Date current24Date = null, currentServerDate = null;
//        try {
//            current24Date = timeFormat.parse(timeResponse);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        String currentTime = timeFormat1.format(current24Date);
//        try {
//            currentServerDate = timeFormat1.parse(currentTime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        if (changeTimeIsYes.equals("false")) {
//            Log.i("CHANGE TIME", "YES");
//            minuts = 0;
//            check_count = 0;
//            changeMints = 0;
//            check_count = myDbHelper.getTotalOrderQty();
//            if (No_DistanceStr.equals("true")) {
//                check_count = (check_count * 300 / 60) + 1;
//
//
//                PreparationTime = getIntent().getExtras().getString("preparationtime");
//                TravelTime = trafficMins;
//                if (orderType.equalsIgnoreCase("Delivery")) {
//                    minuts = trafficMins;
//                    changeMints = check_count;
//                    if (minuts <= 5) {
//                        minuts = 5;
//                    }
//                    minuts = minuts + check_count;
//                }
////                else {
////                    minuts = trafficMins;
////                    changeMints = check_count;
////                    if (minuts <= 5) {
////                        minuts = 5;
////                    }
////                    if (minuts < check_count) {
////                        minuts = check_count;
////                    }
////
////                }
//
//            }
//
//            Calendar now = Calendar.getInstance();
////            kitchenStartDate = now.getTime();
//            now.setTime(currentServerDate);
//            now.add(Calendar.MINUTE, minuts);
//            currentServerDate = now.getTime();
//            for (Date d : listTimes) {
//                Log.i("CHANGE TIME", "FOR");
//                Date prayerDate = d;
//                Date prayerEndTime;
//                Calendar prayerEnd = Calendar.getInstance();
//                prayerEnd.setTime(d);
//                prayerEnd.add(Calendar.MINUTE, 20);
//                prayerEndTime = prayerEnd.getTime();
//                String payerString = timeFormat1.format(prayerDate);
//                String payerEndString = timeFormat1.format(prayerEndTime);
//                String CTimeString = timeFormat1.format(currentServerDate);
//                String[] startParts = payerString.split(":");
//                String[] endParts = payerEndString.split(":");
//                String[] currentParts = CTimeString.split(":");
//                int startHourInteger = Integer.parseInt(startParts[0]);
//                int startMintInteger = Integer.parseInt(startParts[1]);
//                int endHourInteger = Integer.parseInt(endParts[0]);
//                int endMintInteger = Integer.parseInt(endParts[1]);
//                int CTHourInteger = Integer.parseInt(currentParts[0]);
//                int CTMintInteger = Integer.parseInt(currentParts[1]);
//                int c = (int) (CTHourInteger * 60) + (int) CTMintInteger;
//                int p = (int) (startHourInteger * 60) + (int) startMintInteger - (int) 5;
//                int f = (int) (endHourInteger * 60) + (int) endMintInteger;
//                if (c > p && c < f) {
//                    Log.i("CHANGE TIME", "C>P");
//                    expTimeTo = timeFormat2.format(prayerEndTime);
////                    estTime.setText(expTimeTo);
//                    payerTimeIsYes = "true";
//                }
//            }
//            if (payerTimeIsYes.equals("false")) {
//                expTimeTo = timeFormat2.format(currentServerDate);
////                estTime.setText(expTimeTo);
//            }
//        } else {
//            Log.i("CHANGE TIME", "NO");
//            Date prayerDate = null;
//            try {
//                prayerDate = timeFormat2.parse(expTimeTo);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            String payerString = timeFormat1.format(prayerDate);
//            String CTimeString = timeFormat1.format(currentServerDate);
//            String[] prayerParts = payerString.split(":");
//            String[] currentParts = CTimeString.split(":");
//            int startHourInteger = Integer.parseInt(prayerParts[0]);
//            int startMintInteger = Integer.parseInt(prayerParts[1]);
//            int CTHourInteger = Integer.parseInt(currentParts[0]);
//            int CTMintInteger = Integer.parseInt(currentParts[1]);
//            int c = (int) (CTHourInteger * 60) + (int) CTMintInteger + minuts;
//            int p = (int) (startHourInteger * 60) + (int) startMintInteger;
//            if (c >= p) {
//                changeTimeIsYes = "false";
//                getTimeCaluculation();
//            }
//        }
//
//        String startTime = expTimeTo.replace(" ", "");
//        Log.e("TAG", "fullhours " + fullHours);
//        if (fullHours.equalsIgnoreCase("true")) {
//            return;
//        }
//
//        openTimeStr = getIntent().getExtras().getString("start_time");
//        endTime = getIntent().getExtras().getString("end_time");
//        endTime = endTime.replace(" ", "");
//        Log.i("start TIME", "" + openTimeStr);
//        Log.i("end TIME", "" + endTime);
//
//        if (endTime.equals("12:00AM")) {
//            endTime = "11:59PM";
//        }
//        Date date21 = null, date1 = null, date2 = null;
//        try {
////            date21 = timeFormat3.parse(endTime);
//            date1 = timeFormat2.parse(expTimeTo);
//            date2 = timeFormat3.parse(endTime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        String st_time = timeFormat1.format(date1);
//        String ed_time = timeFormat1.format(date2);
//        String currentServerDate1 = timeFormat1.format(current24Date);
//
//        String[] parts = ed_time.split(":");
//        int endHour = Integer.parseInt(parts[0]);
//        int endMinute = Integer.parseInt(parts[1]);
//
//        String[] parts1 = st_time.split(":");
//        int startHour = Integer.parseInt(parts1[0]);
//        int startMinute = Integer.parseInt(parts1[1]);
//
//        String[] parts2 = currentServerDate1.split(":");
//        int currentHour = Integer.parseInt(parts2[0]);
//        int currentMinute = Integer.parseInt(parts2[1]);
//
//        if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
//            if (startHour < 0 || (startHour > 5 || (startHour == 5 && (startMinute > 59 || 0 > 59)))) {
//                st_time = todayDate + " " + st_time;
//                ed_time = todayDate + " " + ed_time;
//            } else {
//                st_time = tomorrowDate + " " + st_time;
//                ed_time = todayDate + " " + ed_time;
//            }
//        } else {
//            if (currentHour < 0 || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
//                st_time = todayDate + " " + st_time;
//                ed_time = tomorrowDate + " " + ed_time;
//            } else {
//                st_time = todayDate + " " + st_time;
//                ed_time = todayDate + " " + ed_time;
//            }
//        }
//
//        Log.i("DATE TAG ddd", st_time + "  " + ed_time);
//
//        Date date3 = null, date4 = null;
//        try {
////            date21 = timeFormat3.parse(endTime);
//            date3 = timeFormat4.parse(st_time);
//            date4 = timeFormat4.parse(ed_time);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        if (date3.before(date4)) {
//            return;
//        } else {
//            onBackPressed();
//
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//
//            if (language.equalsIgnoreCase("En")) {
//                // set title
//                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage("Sorry store is closed! you can't make the order from this store.")
//                        .setCancelable(false)
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//            } else if (language.equalsIgnoreCase("Ar")) {
//                // set title
//                alertDialogBuilder.setTitle("اوريجانو");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage("نأسف الفرع مغلق ، لا يمكنك إتمام الطلب من ذلك الفرع")
//                        .setCancelable(false)
//                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//            }
//            // create alert dialog
//            AlertDialog alertDialog = alertDialogBuilder.create();
//
//            // show it
//            alertDialog.show();
//
//        }
//
////        long difference = d2.getTime() - d1.getTime();
////        int days = (int) (difference / (1000*60*60*24));
////        int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
////        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
//
//    }
//
//
//    public static String getSourceCode(String requestURL) {
//        String response = "";
//        try {
//            URL url = new URL(requestURL);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("POST");
//            int responseCode = conn.getResponseCode();
//            if (responseCode == HttpsURLConnection.HTTP_OK) {
//                String line;
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "UTF-8"));
//                while ((line = br.readLine()) != null) {
//                    response += line;
//                }
//
//            } else {
//                response = "";
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            return response;
//        }
//        return response;
//    }
//
//    @Override
//    public void onDateChanged(Calendar c) {
//
//    }
//
//    @Override
//    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
//
//    }
//
//    public class GetTimes extends AsyncTask<Void, String, String> {
//        ProgressDialog pd;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            pd = new ProgressDialog(OrderConfirmation.this);
//            pd.setMessage("Please wait...");
//            pd.show();
//        }
//
//        @Override
//        protected String doInBackground(Void... voids) {
//            String res = getSourceCode("http://muslimsalat.com/riyadh/daily.json?key=api_key");
//            Log.d("Responce", "" + res);
//            return res;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            if (s == null) {
//                pd.dismiss();
//            } else {
//                pd.dismiss();
//                listTimes.clear();
//                try {
//                    JSONObject resObj = new JSONObject(s);
//                    JSONArray array = resObj.getJSONArray("items");
//                    listTimes.add(doParse(array.getJSONObject(0).getString("shurooq")));
//                    listTimes.add(doParse(array.getJSONObject(0).getString("dhuhr")));
//                    listTimes.add(doParse(array.getJSONObject(0).getString("asr")));
//                    listTimes.add(doParse(array.getJSONObject(0).getString("maghrib")));
//                    listTimes.add(doParse(array.getJSONObject(0).getString("isha")));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                getTimeCaluculation();
//
//                super.onPostExecute(s);
//            }
//        }
//    }
//
//    String getTimeStr(int hours, int mins) {
//        String timeSet = "";
//        if (hours > 12) {
//            hours -= 12;
//            timeSet = "PM";
//        } else if (hours == 0) {
//            hours += 12;
//            timeSet = "AM";
//        } else if (hours == 12)
//            timeSet = "PM";
//        else
//            timeSet = "AM";
//
//
//        String minutes = "";
//        if (mins < 10)
//            minutes = "0" + mins;
//        else
//            minutes = String.valueOf(mins);
//
//        // Append in a StringBuilder
//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
//        return aTime;
//    }
//
//    Date doParse(String timeStr) {
//        try {
//
//            SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.US); //if 24 hour format
//            return format.parse(timeStr);
//        } catch (Exception e) {
//
//            Log.e("Exception is ", e.toString());
//        }
//        return null;
//    }
//
//
//    public class getTrafficTime extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String networkStatus;
//
//        @Override
//        protected void onPreExecute() {
//            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
//            dialog = ProgressDialog.show(OrderConfirmation.this, "",
//                    "Calculating time...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//                //24.70321657, 46.68097073
//                distanceResponse = jParser
//                        .getJSONFromUrl(URL_DISTANCE + "24.70321657,46.68097073&destinations=" + latitude + "," + longitude + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
//
////                distanceResponse = jParser
////                        .getJSONFromUrl(URL_DISTANCE + lat +","+ longi +"&destinations="+ latitude +","+ longitude+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
//                Log.i("TAG", "user response: " + distanceResponse);
//                return distanceResponse;
//            } else {
//                return "no internet";
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    dialog.dismiss();
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    // set title
//                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Communication error! please check the internet connection?")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
//                    try {
//                        JSONObject jo = new JSONObject(result);
//                        JSONArray ja = jo.getJSONArray("rows");
//                        JSONObject jo1 = ja.getJSONObject(0);
//                        JSONArray ja1 = jo1.getJSONArray("elements");
//                        JSONObject jo2 = ja1.getJSONObject(0);
//                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
//                        secs = jo3.getString("value");
//                        trafficMins = Integer.parseInt(secs) / 60;
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//            dialog.dismiss();
//            new GetCurrentTime().execute();
//            super.onPostExecute(result);
//
//        }
//
//    }
//
//    public class GetPromocodeResponse extends AsyncTask<String, String, String> {
//        URL url = null;
//        double lat, longi;
//        String networkStatus;
//
//        @Override
//        protected void onPreExecute() {
//            promosList.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
//            dialog = ProgressDialog.show(OrderConfirmation.this, "",
//                    "Loading. Please Wait....");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
////            promoFlag = false;
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(OrderConfirmation.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    if (result.equals("")) {
//                        Toast.makeText(OrderConfirmation.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        try {
//                            JSONObject jo = new JSONObject(result);
//
//                            try {
//                                JSONArray ja = jo.getJSONArray("Success");
//                                for (int i = 0; i < ja.length(); i++) {
//
//                                    Promos prom = new Promos();
//                                    JSONObject jo1 = ja.getJSONObject(i);
//
//
//                                    String PromoID = jo1.getString("PromoID");
//                                    String PromoCode = jo1.getString("PromoCode");
//                                    String category = jo1.getString("category");
//                                    String itemprice = jo1.getString("itemprice");
//                                    String Percentage = jo1.getString("Percentage");
//                                    String Img = jo1.getString("Img");
//                                    String Desc_En = jo1.getString("Desc_En");
//                                    String Desc_Ar = jo1.getString("Desc_Ar");
//                                    String PromoTitle_Ar = jo1.getString("PromoTitle_Ar");
//                                    String PromoTitle_En = jo1.getString("PromoTitle_En");
//                                    String ItemId = jo1.getString("ItemId");
//                                    String StoreId = jo1.getString("StoreId");
//                                    String Additonals = jo1.getString("Additonals");
//                                    String Size = jo1.getString("Size");
//                                    String PromoType = jo1.getString("PromoType");
//                                    String RemainingBonus = jo1.getString("RemainingBonus");
//
//                                    prom.setPromoID(PromoID);
//                                    prom.setPromoCode(PromoCode);
//                                    prom.setPercentage(Percentage);
//                                    prom.setDescription(Desc_En);
//                                    prom.setDescriptionAr(Desc_Ar);
//                                    prom.setPromoTitle(PromoTitle_En);
//                                    prom.setPromoTitleAr(PromoTitle_Ar);
//                                    prom.setItemId(ItemId);
//                                    prom.setStoreId(StoreId);
//                                    prom.setAdditionals(Additonals);
//                                    prom.setSize(Size);
//                                    prom.setPromoType(PromoType);
//                                    prom.setRemainingBonus(RemainingBonus);
//                                    prom.setItemPrice(itemprice);
//                                    prom.setCategory(category);
//                                    prom.setImage(Img);
//
//                                    promosList.add(prom);
//
//                                }
//                                dialog();
//                            } catch (JSONException je) {
//                                Toast.makeText(OrderConfirmation.this, "Sorry! no active promotions", Toast.LENGTH_SHORT).show();
////                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
////
//////                                if(language.equalsIgnoreCase("En")) {
////                                // set title
////                                alertDialogBuilder.setTitle("Oregano");
////
////                                // set dialog message
////                                alertDialogBuilder
////                                        .setMessage("No orders in your history")
////                                        .setCancelable(false)
////                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
////                                            public void onClick(DialogInterface dialog, int id) {
////                                                dialog.dismiss();
////                                            }
////                                        });
//////                                }else if(language.equalsIgnoreCase("Ar")){
//////                                    // set title
//////                                    alertDialogBuilder.setTitle("د. كيف");
//////
//////                                    // set dialog message
//////                                    alertDialogBuilder
//////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
//////                                            .setCancelable(false)
//////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//////                                                public void onClick(DialogInterface dialog, int id) {
//////                                                    dialog.dismiss();
//////                                                }
//////                                            });
//////                                }
////
////
////                                // create alert dialog
////                                AlertDialog alertDialog = alertDialogBuilder.create();
////
////                                // show it
////                                alertDialog.show();
//
//
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }
//
//            } else {
//                Toast.makeText(OrderConfirmation.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }
//
//            mPromoAdapter.notifyDataSetChanged();
//            super.onPostExecute(result);
//        }
//    }
//
//
//    public class GetCurrentTime extends AsyncTask<String, String, String> {
//        URL url = null;
//        String cardNumber = null, password = null;
//        double lat, longi;
//        String networkStatus;
//        String serverTime;
//        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//
//        @Override
//        protected void onPreExecute() {
//            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
//            if (flag) {
//                dialog = ProgressDialog.show(OrderConfirmation.this, "",
//                        "Loading. Please Wait....");
//            }
//        }
//
//        @Override
//        protected String doInBackground(String... arg0) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                try {
////                    Calendar c = Calendar.getInstance();
////                    System.out.println("Current time => "+c.getTime());
//
////                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////                    timeResponse = timeFormat.format(c.getTime());
//                    JSONParser jParser = new JSONParser();
//                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Log.d("Responce", "" + serverTime);
//            } else {
//                serverTime = "no internet";
//            }
//            return serverTime;
//        }
//
//        @Override
//        protected void onPostExecute(String result1) {
//            if (serverTime == null) {
//                if (flag) {
//                    dialog.dismiss();
//                }
//            } else if (serverTime.equals("no internet")) {
//                if (flag) {
//                    dialog.dismiss();
//                }
//                Toast.makeText(OrderConfirmation.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
//
//            } else {
//
//                Log.i("TIME IF TAG", "" + flag);
//                if (flag) {
//                    dialog.dismiss();
//                    try {
//                        JSONObject jo = new JSONObject(result1);
//                        timeResponse = jo.getString("DateTime");
//                    } catch (JSONException je) {
//                        je.printStackTrace();
//                    }
//                    Log.i("TIME TAG", "" + flag);
//                    Date d1 = null;
//                    Date d2 = null;
//                    try {
//                        d1 = timeFormat.parse(timeResponse);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                    todayDate = timeFormat1.format(d1);
//                    Log.e("TAG", "" + todayDate);
//                    Calendar prayerEnd = Calendar.getInstance();
//                    prayerEnd.setTime(d1);
//                    prayerEnd.add(Calendar.DATE, 1);
//                    d2 = prayerEnd.getTime();
//                    tomorrowDate = timeFormat1.format(d2);
//                    Log.e("DATE TAG TTTT", todayDate + "  " + tomorrowDate);
//                    datepicker();
//
////                    new GetTimes().execute();
//                    flag = false;
//                    Log.i("TIME IFINSIDE TAG", "" + flag);
//                } else {
////                    getTimeCaluculation();
//                }
//            }
//
//
//            super.onPostExecute(result1);
//        }
//    }
//
//
//        public class InsertOrder extends AsyncTask<String, String, String> {
//            URL url = null;
//            String responce = null;
//            double lat, longi;
//            String networkStatus;
//            String currentTime;
//            String kitchenStart;
//            String estdate, esttime;
//            String estimatedTime, total_amt;
//            JSONObject parent = new JSONObject();
//            InputStream inputStream = null;
//            String result = "";
//
//        String itemId = "", qty = "", comments = "", total_price = null, item_price = "", sizes = "", additionals = "", additionalsPices = "";
//
//        @Override
//        protected void onPreExecute() {
//            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
//            dialog = ProgressDialog.show(OrderConfirmation.this, "",
//                    "Please wait...");
//
//            Calendar c = Calendar.getInstance();
//            System.out.println("Current time => " + c.getTime());
//
//            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
//            currentTime = df3.format(c.getTime());
////            estimatedTime = estTime.getText().toString().replace("am", "AM").replace("pm", "PM");
//            total_amt = totalAmount.getText().toString().replace(" SR", "");
//
//            DecimalFormat decim = new DecimalFormat("0.00");
//
//            mamount = decim.format(Float.parseFloat(String.valueOf(myDbHelper.getTotalOrderPrice()))).replace("0,00", "0.00");
//            float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//            mvatamount = decim.format(tax);
//
//            String expDateString = date + " " + time;
//            kitchenStart = df3.format(kitchenStartDate);
//
//            if (total_amt.equalsIgnoreCase("free")) {
//                paymentMode = 4;
//            }
//
//            String[] perp_time= PreparationTime.split(":");
//            int perp_time1 = Integer.parseInt(perp_time[0])*60;
//            perp_time1 = perp_time1 + Integer.parseInt(perp_time[1]);
//
//            try {
//                JSONArray mainItem = new JSONArray();
//                JSONArray subItem = new JSONArray();
//                JSONArray promoItem = new JSONArray();
//
//                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                String version = pInfo.versionName;
//
//                JSONObject mainObj = new JSONObject();
//                mainObj.put("UserId", userId);
//                mainObj.put("StoreId", storeId);
//                mainObj.put("OrderType", ORDER_TYPE);
//                mainObj.put("ExpectedTime", expDateString);
//                mainObj.put("PreparationTime", perp_time1);
//                mainObj.put("TravelTime", TravelTime);
//                mainObj.put("KichenStartTime", kitchenStart);
//                mainObj.put("OrderDate", currentTime);
//                mainObj.put("IsFavorite", false);
//                mainObj.put("FavoriteName", "");
//                mainObj.put("Comments", "Android v" + version);
//                mainObj.put("PaymentMode", "2");
//                mainObj.put("Total_Price", Constants.convertToArabic(total_amt));
//                mainObj.put("OrderStatus", "New");
//                mainObj.put("Device_token", SplashActivity.regid);
//                mainObj.put("VatPercentage", "5");
//                mainObj.put("SubTotal", Constants.convertToArabic(mamount));
//                mainObj.put("VatCharges", Constants.convertToArabic(mvatamount));
//
//                if (orderType.equalsIgnoreCase("Delivery")) {
//                    mainObj.put("AddressID", addressId);
//                }
//                mainItem.put(mainObj);
//
//                JSONObject promoObj = new JSONObject();
//                promoObj.put("DeviceToken", SplashActivity.regid);
//                promoObj.put("promotionCode", promocodeStr);
//                promoObj.put("BonusAmt", remainingBonusInt);
//                promoItem.put(promoObj);
//
//                for (Order order : orderList) {
//                    JSONObject subObj = new JSONObject();
//
////                    JSONArray subItem1 = new JSONArray();
////                    JSONArray subItem2 = new JSONArray();
//                    if (order.getItemType().equals("Whole")) {
//                        ItemType = String.valueOf(2);
//                    } else if (order.getItemType().equals("Sliced")) {
//                        ItemType = String.valueOf(1);
//                    }
//                    subObj.put("ItemPrice", Constants.convertToArabic(decim.format(Float.parseFloat(order.getPrice()))));
//                    subObj.put("Qty", order.getQty());
//                    subObj.put("Comments", order.getComment());
//                    subObj.put("ItemId", order.getItemId());
//                    subObj.put("Size", order.getSizename());
//                    subObj.put("ItemType", ItemType);
//
//                    JSONArray subsubItems = new JSONArray();
//
//                    if (order.getSub_itemId() != null) {
//                        String ids = order.getSub_itemId();
//                        String qty = order.getSub_itemcount();
//                        List<String> idsList = Arrays.asList(ids.split(","));
//                        List<String> qtyList = Arrays.asList(qty.split(","));
//                        for (int i = 0; i < idsList.size(); i++) {
//                            if(!idsList.get(i).equals("")) {
//                                JSONObject subsubObj = new JSONObject();
//                                subsubObj.put("OrderSubItemId", idsList.get(i));
//                                subsubObj.put("Qty", qtyList.get(i));
//                                subsubItems.put(subsubObj);
//                            }
//                        }
//                    }
//
//                    subObj.put("SubSubItems", subsubItems);
//                    subItem.put(subObj);
//                }
//
//                parent.put("MainItem", mainItem);
//                parent.put("SubItem", subItem);
//                if (remainingBonusInt != 0) {
//                    parent.put("Promotion", promoItem);
//                }
//                Log.e("TAG", parent.toString());
//            } catch (Exception je) {
//                je.printStackTrace();
//            }
//        }
//
//        @Override
//        protected String doInBackground(String... arg0) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                try {
//
//                    try {
//                        Log.e("TAG", parent.toString());
//                        // 1. create HttpClient
//                        HttpClient httpclient = new DefaultHttpClient();
//
//                        // 2. make POST request to the given URL
//                        HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);
//
//
//                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
//                        // ObjectMapper mapper = new ObjectMapper();
//                        // json = mapper.writeValueAsString(person);
//
//                        // 5. set json to StringEntity
//                        StringEntity se = new StringEntity(parent.toString());
//
//                        // 6. set httpPost Entity
//                        httpPost.setEntity(se);
//
//                        // 7. Set some headers to inform server about the type of the content
//                        httpPost.setHeader("Accept", "application/json");
//                        httpPost.setHeader("Content-type", "application/json");
//
//                        // 8. Execute POST request to the given URL
//                        HttpResponse httpResponse = httpclient.execute(httpPost);
//
//                        // 9. receive response as inputStream
//                        inputStream = httpResponse.getEntity().getContent();
//
//                        // 10. convert inputstream to string
//                        if (inputStream != null) {
//                            result = convertInputStreamToString(inputStream);
//                            Log.d("Responce", "" + result);
//                            return result;
//                        }
//
//                    } catch (Exception e) {
//                        Log.d("InputStream", e.getLocalizedMessage());
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//                return "no internet";
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String result1) {
//            if (result1 != null) {
//                if (result1.equalsIgnoreCase("no internet")) {
//                    dialog.dismiss();
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmation.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    // set title
//                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Communication error! please check the internet connection?")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
////                    Constants.orderNow = false;
//                    myDbHelper.deleteOrderTable();
//                    try {
//                        double number;
//                        number = myDbHelper.getTotalOrderPrice();
//                        DecimalFormat decim = new DecimalFormat("0.00");
////                        CheckoutActivity.orderPrice.setText("" + decim.format(number) + " SR");
////                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
////                        CheckoutActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
////                        OrderFragment.orderPrice.setText("" + decim.format(number) + " SR");
////                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
////                        OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
////                        CategoriesListActivity.orderPrice.setText("" + decim.format(number) + " SR");
////                        CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
////                        CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    String order_number = "";
//                    try {
//                        JSONObject jo = new JSONObject(result);
//                        order_number = jo.getString("Success");
//                        Intent intent = new Intent(OrderConfirmation.this, OrderDetails.class);
//                        intent.putExtra("storeId", storeId);
//                        if (language.equalsIgnoreCase("En")) {
//                            intent.putExtra("storeName", storeName);
//                            intent.putExtra("storeAddress", storeAddress);
//                        } else if (language.equalsIgnoreCase("Ar")) {
//                            intent.putExtra("storeName_ar", storeName_Ar);
//                            intent.putExtra("storeAddress_ar", storeAddress_Ar);
//                        }
//                        intent.putExtra("latitude", latitude);
//                        intent.putExtra("longitude", longitude);
//                        intent.putExtra("amount", Constants.convertToArabic(amount.getText().toString()));
//                        intent.putExtra("vatamount", Constants.convertToArabic(vatAmount.getText().toString()));
//                        intent.putExtra("netamount", Constants.convertToArabic(netTotal.getText().toString()));
//                        intent.putExtra("total_amt", Constants.convertToArabic(totalAmount.getText().toString()));
//                        intent.putExtra("total_items", totalItems.getText().toString());
//                        intent.putExtra("expected_time", estTime.getText().toString());
//                        intent.putExtra("Date", estDate.getText().toString());
//                        intent.putExtra("payment_mode", Integer.toString(paymentMode));
//                        intent.putExtra("order_type", orderType);
//                        intent.putExtra("order_number", order_number);
//                        startActivity(intent);
//
//                    } catch (JSONException je) {
//                        je.printStackTrace();
//                    }
//
//
//                }
//            }
//            dialog.dismiss();
//            super.onPostExecute(result1);
//        }
//    }
//
//    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//        String line = "";
//        String result = "";
//        while ((line = bufferedReader.readLine()) != null)
//            result += line;
//
//        inputStream.close();
//        return result;
//
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        timer.cancel();
//    }
//
//
//    public class GetStoredCards extends AsyncTask<String, String, String> {
//        URL url = null;
//        String responce = null;
//        double lat, longi;
//        String networkStatus;
//
//        @Override
//        protected void onPreExecute() {
//            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
////            dialog = ProgressDialog.show(getActivity(), "",
////                    "Loading. Please Wait....");
//        }
//
//        @Override
//        protected String doInBackground(String... arg0) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                try {
//
//                    JSONParser jParser = new JSONParser();
//
//                    getCardResponse = jParser
//                            .getJSONFromUrl(arg0[0]);
//                    Log.i("TAG", "user response:" + getCardResponse);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else {
//                getCardResponse = "no internet";
//            }
//            return getCardResponse;
//        }
//
//        @Override
//        protected void onPostExecute(String result1) {
//            if (getCardResponse == null) {
//
//            } else if (getCardResponse.equals("no internet")) {
//                Toast.makeText(OrderConfirmation.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
//            } else {
//
//                try {
//                    JSONObject jo = new JSONObject(getCardResponse);
//                    JSONArray ja = jo.getJSONArray("Success");
//                    for (int i = 0; i < ja.length(); i++) {
//                        JSONObject jo1 = ja.getJSONObject(i);
//                        String sTokenStr = jo1.getString("SToken");
//
//
//                        List<PWAccount> sToken = _binder.getAccountFactory().deserializeAccountList(sTokenStr);
//                        accounts.addAll(sToken);
//                    }
//                } catch (Exception e) {
//                    Log.d("", "Error while parsing the results!");
//                    e.printStackTrace();
//                }
//
//
//                super.onPostExecute(result1);
//            }
//        }
//    }
//
//
//    public class InsertCardDetails extends AsyncTask<String, String, String> {
//        URL url = null;
//        String responce = null;
//        double lat, longi;
//        String networkStatus;
//
//        @Override
//        protected void onPreExecute() {
//            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmation.this);
////            dialog = ProgressDialog.show(getActivity(), "",
////                    "Loading. Please Wait....");
//        }
//
//        @Override
//        protected String doInBackground(String... arg0) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                try {
//
//                    JSONParser jParser = new JSONParser();
//
//                    setCardResponse = jParser
//                            .getJSONFromUrl(arg0[0]);
//                    Log.i("TAG", "user response:" + setCardResponse);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else {
//                setCardResponse = "no internet";
//            }
//            return setCardResponse;
//        }
//
//        @Override
//        protected void onPostExecute(String result1) {
//            if (setCardResponse == null) {
//
//            } else if (getCardResponse.equals("no internet")) {
//                Toast.makeText(OrderConfirmation.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
//            } else {
//
//                super.onPostExecute(result1);
//            }
//        }
//    }
//
//    @SuppressWarnings("unchecked")
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_CANCELED) {
//            Log.i("mobile.connect.checkout", "user canceled the checkout process/error");
////            Toast.makeText(OrderConfirmation.this, "Checkout cancelled or an error occurred.", Toast.LENGTH_SHORT).show();
//            confirmOrder.setClickable(true);
//        } else if (resultCode == RESULT_OK) {
//            Toast.makeText(OrderConfirmation.this, "Thank you for shopping!", Toast.LENGTH_SHORT).show();
//            new InsertOrder().execute();
//            // if the user added a new account, store it
//            if (data.hasExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_RESULT_ACCOUNT)) {
//                Log.i("mainactivity", "checkout went through, callback has an account");
//                accounts.clear();
//                ArrayList<PWAccount> newAccounts = data.getExtras().getParcelableArrayList(PWConnectCheckoutActivity.CONNECT_CHECKOUT_RESULT_ACCOUNT);
//                accounts.addAll(newAccounts);
//
//                try {
//                    Log.i("Card", "  " + _binder.getAccountFactory().serializeAccountList(accounts).toString().replace(" ", ""));
////                    new InsertCardDetails().execute(Constants.SAVE_CARD_DETAILS_URL+userId+"&sToken="+_binder.getAccountFactory().serializeAccountList(newAccounts).replace(" ",""));
////                    sharedSettings.edit().putString(ACCOUNTS, _binder.getAccountFactory().serializeAccountList(accounts)).commit();
//                } catch (PWProviderNotInitializedException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                Log.i("mainactivity", "checkout went through, callback has transaction result");
//            }
//        }
//    }
//
//    public void dialog() {
//
//        final Dialog dialog2 = new Dialog(OrderConfirmation.this);
//
//        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog2.getWindow().setBackgroundDrawable(
//                new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        if (language.equalsIgnoreCase("En")) {
//            dialog2.setContentView(R.layout.promo_list);
//        } else if (language.equalsIgnoreCase("Ar")) {
//            dialog2.setContentView(R.layout.promo_list_arabic);
//        }
//
//        dialog2.setCanceledOnTouchOutside(true);
//        ListView lv = (ListView) dialog2.findViewById(R.id.promos_list);
//        ImageView cancel = (ImageView) dialog2.findViewById(R.id.cancel_button);
//        lv.setAdapter(mPromoAdapter);
//
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog2.dismiss();
//            }
//        });
//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
//                                    long arg3) {
//                dialog2.dismiss();
//                if (promosList.get(position).getStoreId().equals(storeId) || promosList.get(position).getStoreId().equals("0")) {
//                    remainingBonusInt = 0;
//                    int tprice1 = 0;
//                    // full free drink
//                    if (promosList.get(position).getPromoType().equals("1")) {
//                        if (promosList.get(position).getCategory().equals("0") && promosList.get(position).getItemId().equals("0") && promosList.get(position).getSize().equals("0")) {
//                            int highestPrice = 0;
//                            for (Order order : orderList) {
////                                if ((order.getCategoryId().equals("1") && order.getItemId().equals("1"))) {
//                                int qty = Integer.parseInt(order.getQty());
//                                int tprice = (int) Float.parseFloat(order.getTotalAmount());
//                                tprice = tprice / qty;
//                                tprice1 = (int) Float.parseFloat(order.getTotalAmount()) / qty;
//                                if (highestPrice < tprice) {
//                                    highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//
//                                    freeOrderId = order.getOrderId();
//                                }
//                            }
////                            }
//                            if (highestPrice != 0) {
//                                HighestPriceYes(position, highestPrice, tprice1);
//
//                            } else {
//                                HighestPriceNo();
//                            }
//                        } else if (promosList.get(position).getCategory().equals("0") && promosList.get(position).getItemId().equals("0") && !promosList.get(position).getSize().equals("0")) {
//                            int highestPrice = 0;
//                            for (Order order : orderList) {
//                                if (order.getItemTypeId().equals(promosList.get(position).getSize())) {
//                                    int qty = Integer.parseInt(order.getQty());
//                                    int tprice = (int) Float.parseFloat(order.getTotalAmount());
//                                    tprice = tprice / qty;
//                                    tprice1 = (int) Float.parseFloat(order.getTotalAmount()) / qty;
//                                    if (highestPrice < tprice) {
//                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//
//                                        freeOrderId = order.getOrderId();
//                                    }
//                                }
//                            }
//
//                            if (highestPrice != 0) {
//                                HighestPriceYes(position, highestPrice, tprice1);
//
//                            } else {
//                                HighestPriceNo();
//                            }
//                        } else if (!promosList.get(position).getCategory().equals("0") && !promosList.get(position).getItemId().equals("0") && !promosList.get(position).getSize().equals("0")) {
//                            int highestPrice = 0;
//                            for (Order order : orderList) {
//                                Log.i("TAG", "cat " + order.getCategoryId() + "," + promosList.get(position).getCategory());
//                                Log.i("TAG", "item " + order.getItemId() + "," + promosList.get(position).getItemId());
//                                Log.i("TAG", "cat " + order.getItemTypeId() + "," + promosList.get(position).getSize());
//                                if (order.getCategoryId().equals(promosList.get(position).getCategory()) && order.getItemId().equals(promosList.get(position).getItemId()) && order.getItemTypeId().equals(promosList.get(position).getSize())) {
//                                    Log.i("TAG", "Highest Price");
//                                    int qty = Integer.parseInt(order.getQty());
//                                    int tprice = (int) Float.parseFloat(order.getTotalAmount());
//                                    tprice = tprice / qty;
//                                    tprice1 = (int) Float.parseFloat(order.getTotalAmount()) / qty;
//                                    if (highestPrice < tprice) {
//                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//
//                                        freeOrderId = order.getOrderId();
//                                    }
//                                }
//                            }
//
//                            if (highestPrice != 0) {
//                                HighestPriceYes(position, highestPrice, tprice1);
//
//                            } else {
//                                HighestPriceNo();
//                            }
//                        } else if (!promosList.get(position).getCategory().equals("0") && !promosList.get(position).getItemId().equals("0") && promosList.get(position).getSize().equals("0")) {
//                            int highestPrice = 0;
//                            for (Order order : orderList) {
//                                if (order.getCategoryId().equals(promosList.get(position).getCategory()) && order.getItemId().equals(promosList.get(position).getItemId())) {
//                                    int qty = Integer.parseInt(order.getQty());
//                                    int tprice = (int) Float.parseFloat(order.getTotalAmount());
//                                    tprice = tprice / qty;
//                                    tprice1 = (int) Float.parseFloat(order.getTotalAmount()) / qty;
//                                    if (highestPrice < tprice) {
//                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//
//                                        freeOrderId = order.getOrderId();
//                                    }
//                                }
//                            }
//
//                            if (highestPrice != 0) {
//                                HighestPriceYes(position, highestPrice, tprice1);
//
//                            } else {
//                                HighestPriceNo();
//                            }
//                        } else if (!promosList.get(position).getCategory().equals("0") && promosList.get(position).getItemId().equals("0") && !promosList.get(position).getSize().equals("0")) {
//                            int highestPrice = 0;
//                            for (Order order : orderList) {
//                                if (order.getCategoryId().equals(promosList.get(position).getCategory()) && order.getItemTypeId().equals(promosList.get(position).getSize())) {
//                                    int qty = Integer.parseInt(order.getQty());
//                                    int tprice = (int) Float.parseFloat(order.getTotalAmount());
//                                    tprice = tprice / qty;
//                                    tprice1 = (int) Float.parseFloat(order.getTotalAmount()) / qty;
//                                    if (highestPrice < tprice) {
//                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//
//                                        freeOrderId = order.getOrderId();
//                                    }
//                                }
//                            }
//
//                            if (highestPrice != 0) {
//                                HighestPriceYes(position, highestPrice, tprice1);
//
//                            } else {
//                                HighestPriceNo();
//                            }
//                        } else if (!promosList.get(position).getCategory().equals("0") && promosList.get(position).getItemId().equals("0") && promosList.get(position).getSize().equals("0")) {
//                            int highestPrice = 0;
//                            for (Order order : orderList) {
//                                if (order.getCategoryId().equals(promosList.get(position).getCategory())) {
//                                    int qty = Integer.parseInt(order.getQty());
//                                    int tprice = (int) Float.parseFloat(order.getTotalAmount());
//                                    tprice = tprice / qty;
//                                    tprice1 = (int) Float.parseFloat(order.getTotalAmount()) / qty;
//                                    if (highestPrice < tprice) {
//                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;
//
//                                        freeOrderId = order.getOrderId();
//                                    }
//                                }
//                            }
//
//                            if (highestPrice != 0) {
//                                HighestPriceYes(position, highestPrice, tprice1);
//
//                            } else {
//                                HighestPriceNo();
//                            }
//                        }
//                    } else if (promosList.get(position).getPromoType().equals("6")) {
//                        remainingBonusInt = Integer.parseInt(promosList.get(position).getItemPrice());
//                        int totalPriceInt = (int) myDbHelper.getTotalOrderPrice();
//                        if (language.equalsIgnoreCase("En")) {
//                            totalItems.setText(" " + (myDbHelper.getTotalOrderQty() + 1) + " items");
//                        } else if (language.equalsIgnoreCase("Ar")) {
//                            totalItems.setText("" + (myDbHelper.getTotalOrderQty() + 1) + " الأصناف");
//                        }
//                        if (remainingBonusInt <= totalPriceInt) {
////                            remainingBonusInt = 0;
////                           PromoType2 promo = new /PromoType2();
////                           promo.setItemId(promosList.get(position).getItemId());
////                           promo.setItemPrice("0");
////                           promo.setQty("1");
////                           promo.setSize(promosList.get(position).getSize());
////                           promo.setComments("Free");
////                           promo.setAdditionalID(promosList.get(position).getAdditionals());
////                           promo.setAdditionalPrice("0");
//
////                           promoType2ArrayList.add(promo);
//
//                            try {
//                                Log.i("TAG", "promo array lenght " + promoArray.length());
//                                if (promoArray != null && promoArray.length() == 0) {
//
//                                    JSONObject subObj = new JSONObject();
//
//                                    JSONArray subItem2 = new JSONArray();
//
//                                    subObj.put("ItemPrice", "0");
//                                    subObj.put("Qty", "1");
//                                    subObj.put("Comments", "Free");
//                                    subObj.put("ItemId", promosList.get(position).getItemId());
//                                    subObj.put("Size", promosList.get(position).getSize());
//
////                                    JSONObject subObj1 = new JSONObject();
////                                    subObj1.put("AdditionalID", promosList.get(position).getAdditionals());
////                                    subObj1.put("AdditionalPrice", "0");
////                                    subItem2.put(subObj1);
//
//                                    promoArray.put(subObj);
//                                    if (subItem2.length() > 0) {
//                                        promoArray.put(subItem2);
//                                    }
//                                }
//                                Log.i("TAG", "promo array " + promoArray.toString());
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                            promocodeStr = promosList.get(position).getPromoCode();
//                            promoIdStr = promosList.get(position).getPromoID();
//                            promoTypeStr = promosList.get(position).getPromoType();
//
//                            if (language.equalsIgnoreCase("En")) {
//                                promocode.setText(promosList.get(position).getPromoTitle());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                promocode.setText(promosList.get(position).getPromoTitleAr());
//                            }
//
//                            promoLayout.setClickable(false);
//                            promoArrow.setVisibility(View.GONE);
//                            promoCancel.setVisibility(View.VISIBLE);
//                        } else {
//                            promocodeStr = "";
//                            if (language.equalsIgnoreCase("En")) {
//                                promocode.setHint("Apply Promotion");
//                                Toast.makeText(OrderConfirmation.this, "Specified item not available in cart", Toast.LENGTH_SHORT).show();
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                promocode.setHint("قدم للعرض");
//                                Toast.makeText(OrderConfirmation.this, "المنتجات المحددة غير موجودة في سلة الشراء", Toast.LENGTH_SHORT).show();
//                            }
//
//                            promoIdStr = "";
//                            promoTypeStr = "";
//
//                            tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//
//                            float totalPriceInt1 = Float.parseFloat(decim.format((myDbHelper.getTotalOrderPrice()) + tax));
//
//                            totalAmount.setText("" + decim1.format(totalPriceInt1) + " SR");
////                                paymentMode = 3;
//                        }
//                    }
//                }
//
//            }
//        });
//
//
//        dialog2.show();
//
//    }
//
//    public void HighestPriceYes(int position, float highestPrice, int tprice1) {
//        if (tprice1 > Integer.parseInt(promosList.get(position).getItemPrice())) {
//            remainingBonusInt = Integer.parseInt(promosList.get(position).getItemPrice());
//        } else {
//            remainingBonusInt = tprice1;
//        }
//
//        promocodeStr = promosList.get(position).getPromoCode();
//        if (language.equalsIgnoreCase("En")) {
//            promocode.setText(promosList.get(position).getPromoTitle() + " " + remainingBonusInt + " SR Redeem");
//        } else if (language.equalsIgnoreCase("Ar")) {
//            promocode.setText(promosList.get(position).getPromoTitleAr() + " " + remainingBonusInt + " ريال مجانا ");
//        }
//
//        promoIdStr = promosList.get(position).getPromoID();
//        promoTypeStr = promosList.get(position).getPromoType();
//        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//        float totalPriceInt = Float.parseFloat(decim.format((myDbHelper.getTotalOrderPrice()) + tax));
//        highestPrice = totalPriceInt - remainingBonusInt;
////                            total_amt = highestPrice;
//        if (highestPrice == 0) {
//            totalAmount.setText("Free");
//            paymentMode = 4;
//        } else {
//            totalAmount.setText("" + decim1.format(highestPrice) + " SR");
//        }
//        promoLayout.setClickable(false);
//        promoArrow.setVisibility(View.GONE);
//        promoCancel.setVisibility(View.VISIBLE);
//    }
//
//    public void HighestPriceNo() {
//        promocodeStr = "";
//        if (language.equalsIgnoreCase("En")) {
//            promocode.setHint("Apply promotion");
//            Toast.makeText(OrderConfirmation.this, "Specified item not available in cart", Toast.LENGTH_SHORT).show();
//        } else if (language.equalsIgnoreCase("Ar")) {
//            promocode.setHint("قدم للعرض");
//            Toast.makeText(OrderConfirmation.this, "المنتجات المحددة غير موجودة في سلة الشراء", Toast.LENGTH_SHORT).show();
//        }
//
//        promoIdStr = "";
//        promoTypeStr = "";
//
//        int totalPriceInt = (int) myDbHelper.getTotalOrderPrice();
//
//        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
//
//        totalAmount.setText("" + Float.parseFloat(decim.format((myDbHelper.getTotalOrderPrice()) + tax)));
////                                paymentMode = 3;
//    }
//
//    public static int minutesDiff(Date earlierDate, Date laterDate) {
//        if (earlierDate == null || laterDate == null) return 0;
//
//        return (int) ((laterDate.getTime() / 60000) - (earlierDate.getTime() / 60000));
//    }
    }
}
