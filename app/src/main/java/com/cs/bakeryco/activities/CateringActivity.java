package com.cs.bakeryco.activities;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by CS on 27-09-2016.
 */
public class CateringActivity extends AppCompatActivity {
//    private ArrayList<SubCategories> subCatList = new ArrayList<>();
//    String response;
//    private DataBaseHelper myDbHelper;
//
//    private int lastExpandedPosition = 0;
//    public static TextView orderPrice, orderQuantity ;
//
//    public static ItemsListAdapter mAdapter;
//
//    int catId;
//    RelativeLayout checkOut, addMoreLayout;
//    ExpandableListView mExpListView;
//    Toolbar toolbar;
//    String language;
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.catering_layout);
//        myDbHelper = new DataBaseHelper(CateringActivity.this);
//        catId = 8;
//
//        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
//        setSupportActionBar(toolbar);
////        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mExpListView = (ExpandableListView) findViewById(R.id.expandableListView);
//        checkOut = (RelativeLayout) findViewById(R.id.checkout_layout);
//        addMoreLayout = (RelativeLayout) findViewById(R.id.addmore_layout);
//        orderPrice = (TextView) findViewById(R.id.item_price);
//        orderQuantity = (TextView) findViewById(R.id.item_qty);
//
//        mAdapter = new ItemsListAdapter(CateringActivity.this, subCatList, language);
//        mExpListView.setAdapter(mAdapter);
//
//        mExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                if (lastExpandedPosition != -1
//                        && groupPosition != lastExpandedPosition) {
//                    mExpListView.collapseGroup(lastExpandedPosition);
//                }
//                lastExpandedPosition = groupPosition;
////                mExpListView.setSelectionFromTop(groupPosition, 0);
//            }
//        });
//
//
//
//        checkOut.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(myDbHelper.getTotalOrderQty() == 0){
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CateringActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                    if(language.equalsIgnoreCase("En")) {
//                    // set title
//                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("There is no items in your order? To proceed checkout please add the items")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
////                    }else if(language.equalsIgnoreCase("Ar")){
////                        // set title
////                        alertDialogBuilder.setTitle("اوريجانو");
////
////                        // set dialog message
////                        alertDialogBuilder
////                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
////                                .setCancelable(false)
////                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                    public void onClick(DialogInterface dialog, int id) {
////                                        dialog.dismiss();
////                                    }
////                                });
////                    }
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }else {
//                    Intent checkoutIntent = new Intent(CateringActivity.this, CheckoutActivity.class);
//                    startActivity(checkoutIntent);
//                }
//            }
//        });
//
//        new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL+catId);
//
//        addMoreLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//
//        double number;
//        number=myDbHelper.getTotalOrderPrice();
//        NumberFormat nf=new DecimalFormat("####.####");
//        orderPrice.setText("" + nf.format(number) + " SR");
//        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String  networkStatus;
//        ProgressDialog dialog;
//        @Override
//        protected void onPreExecute() {
//            subCatList.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(CateringActivity.this);
//            dialog = ProgressDialog.show(CateringActivity.this, "",
//                    "Loading items...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            }else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//                if(result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(CateringActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                }else{
//                    if(result.equals("")){
//                        Toast.makeText(CateringActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//                    }else {
//
//                        try {
//                            JSONObject jo = new JSONObject(result);
//                            JSONArray ja = jo.getJSONArray("Success");
//
//                            for(int i = 0; i<ja.length(); i++){
//                                SubCategories cat = new SubCategories();
//                                ArrayList<Items> itemList = new ArrayList<>();
//                                JSONArray ja1 = ja.getJSONArray(i);
//                                for(int k = 0; k < ja1.length(); k++){
//                                    if(k == 0){
//                                        JSONObject mainObj = ja1.getJSONObject(0);
//                                        cat.setCatId(Integer.toString(catId));
//                                        cat.setSubCatId(mainObj.getString("subCatId"));
//                                        cat.setSubCat(mainObj.getString("sName"));
//                                        cat.setSubCatAr(mainObj.getString("sName_Ar"));
//                                        cat.setDescription(mainObj.getString("sDesc"));
//                                        cat.setDescriptionAr(mainObj.getString("sDesc_Ar"));
//                                    }else{
//                                        JSONArray itemsArry = ja1.getJSONArray(k);
//                                        ArrayList<Price> priceList = new ArrayList<>();
//                                        Items items = new Items();
//                                        for(int l = 0; l<itemsArry.length(); l++){
//
//                                            if(l == 0){
//                                                JSONObject itemsObj = itemsArry.getJSONObject(0);
//                                                items.setCatId(Integer.toString(catId));
//                                                items.setSubCatId(cat.getSubCatId());
//                                                items.setItemId(itemsObj.getString("ItemId"));
//                                                items.setItemName(itemsObj.getString("iName"));
//                                                items.setItemNameAr(itemsObj.getString("iName_Ar"));
//                                                items.setDescription(itemsObj.getString("iDesc"));
//                                                items.setDescriptionAr(itemsObj.getString("iDesc_Ar"));
//                                            }else {
//                                                JSONArray priceArray = itemsArry.getJSONArray(l);
//                                                Price price = new Price();
//                                                for(int m = 0; m < priceArray.length(); m++){
//                                                    JSONObject priceObj = priceArray.getJSONObject(m);
//                                                    price.setPrice(priceObj.getString("price"));
//                                                    price.setPriceId(priceObj.getString("priceId"));
//                                                    price.setSize(priceObj.getString("size"));
//                                                    priceList.add(price);
//                                                }
//                                                items.setPriceList(priceList);
//                                                itemList.add(items);
//                                            }
//
//                                        }
//                                        cat.setChildItems(itemList);
//                                    }
//
//                                }
//
//
//                                subCatList.add(cat);
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }
//
//            }else {
//                Toast.makeText(CateringActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }
//            mAdapter.notifyDataSetChanged();
//
//            super.onPostExecute(result);
//
//        }
//
//    }
}
