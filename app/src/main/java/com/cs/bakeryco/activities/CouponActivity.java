package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.CouponListAdapter;

public class CouponActivity extends AppCompatActivity {

    SharedPreferences languagePrefs;
    String language;
    RecyclerView mcoupon_list;
    CouponListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.coupon_vouchers);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.coupon_vouchers_arabic);
        }

        mcoupon_list = (RecyclerView) findViewById(R.id.coupon_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(CouponActivity.this);
        mcoupon_list.setLayoutManager(layoutManager);

        mAdapter = new CouponListAdapter(CouponActivity.this, language, CouponActivity.this);
        mcoupon_list.setAdapter(mAdapter);

    }

}
