package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.TextViewEx;

import me.biubiubiu.justifytext.library.JustifyTextView;

public class AboutUsTxt extends AppCompatActivity {
    JustifyTextView aboutusTxt;
    TextViewEx text8;
    SharedPreferences languagePrefs;
    RelativeLayout back_btn;
    String language;
    Typeface boldTypeface;
    String desc_ar = "بدأ مصنع بيكري آند كومباني بتجهيز منتجات الخبز والحلويات بإستخدام أفضل الموارد المتاحة. حيث يتم تجهيز المعجنات الأصلية يدوياً بالإضافة إلى الساندويتشات والكيكات التي تحضر بأجود المكونات؛ جميع المكونات صحية وطبيعية 100% حيث تستخدم أجود أنواع الدقيق والبيض الطازج وكريمة الزبدة ومنتجات الألبان وقصب السكر النقي والشوكولاتة البيضاء والداكنة بالإضافة إلى أشهى الحشوات وخلطات الفواكه الطبيعية وأفضل أنواع الفانيليا والمنكهات.\n" +
            "n\"" +
            "واليوم يعد مصنع بيكري آند كومباني أحد أهم المصانع التي تنتج آلاف المنتجات يومياً. ويتم تقديم منتجات بيكري آند كومباني في أهم وأفخم المطاعم والمنشآت ليستمتع بها محبي الأطعمة ومتذوقيها.\n" +
            "n\"" +
            "لقد قطعنا أشواطاً طويلة، ولكن هنالك العديد من الثوابت التي لا يمكن أن تتغير، سنظل دائماً ملتزمين باستخدام أجود المكونات وحريصين على استمرارية وتميز منتجاتنا، بالإضافة إلى حرصنا وتفانينا في خدمة عملائنا المميزين وتقديم أجود وأفضل المنتجات لهم.";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.about_us_txt);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.about_us_txt_arabic);
        }

        if (language.equalsIgnoreCase("Ar")) {
            boldTypeface = Typeface.createFromAsset(getAssets(),
                    "helveticaneue_regular.ttf");
        }

        String text = "<HTML><BODY><p align=\\\"justify\\\" style=\"text-align: justify !important;> Bakery & Company began baking confectionary products in low volume bakery facilities. The original handmade pastries, cakes and sandwiches have always used the finest quality. 100 percent natural healthy ingredients: Highest quality flour, Fresh whole grade A eggs, butter creams and dairy products, pure cane sugar, dark and white chocolates, fresh fruit purees and fillings, the world's finest vanilla and essences.</p> <p align=\\\"justify\\\">Today, B&C is a high volume bakery that turns out thousands of products daily. At one point or another, B&C food products are served at the best restaurants and establishments, just waiting to be enjoyed by food enthusiasts and connoisseurs.</p><p align=\\\"justify\\\"> We've come a long way, but there are some things that haven't changed. We will always use the finest quality ingredients and maintain consistency, dedication, and passion to our bakery products for all valued clients.</p></BODY></HTML>";



        if (language.equalsIgnoreCase("Ar")) {
            ((TextView) findViewById(R.id.aboutus_txt)).setTypeface(boldTypeface);
        }

        if (language.equalsIgnoreCase("En")) {
            aboutusTxt = (JustifyTextView) findViewById(R.id.aboutus_txt);
        } else if (language.equalsIgnoreCase("Ar")) {
            text8 = findViewById(R.id.aboutus_txt);
        }

        back_btn = findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

//        if (language.equalsIgnoreCase("En")) {
//            aboutusTxt.set(Paint.Align.LEFT);
//        }else if (language.equalsIgnoreCase("Ar")) {
//            aboutusTxt.setAlignment(Paint.Align.RIGHT);
//        }
        if (language.equalsIgnoreCase("Ar")) {
            text8.setText(desc_ar, true);
        }
        if (language.equalsIgnoreCase("En")) {
            aboutusTxt.setTextColor(Color.parseColor("#000000"));
        } else {
            text8.setTextColor(Color.parseColor("#000000"));
        }
//        aboutusTxt.setLineSpacing(4);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
}