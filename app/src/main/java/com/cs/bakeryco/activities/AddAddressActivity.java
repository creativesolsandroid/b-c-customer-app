package com.cs.bakeryco.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.GPSTracker;
import com.cs.bakeryco.R;
import com.cs.bakeryco.Rest.APIInterface;
import com.cs.bakeryco.Rest.ApiClient;
import com.cs.bakeryco.widgets.FetchAddressIntentService;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    GPSTracker gps;
    Context context;
    Double lat, longi;
    private GoogleMap mMap;
    ImageView mSearchAddress;
    AlertDialog customDialog;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    private LatLng mCenterLatLong;
    private TextView mTitle;
    private ImageButton mSkipButton;
    //    private RelativeLayout titleLayout;
    private RelativeLayout backButton1;
    protected String mAddressOutput;
    private Button mConfirmLocation;
    private LinearLayout mExpandLayout;
    private static String TAG = "TAG";
    private boolean isExpanded = false;
    private GoogleApiClient mGoogleApiClient;
    EditText mLocationAddress, inputHouseNumber, inputHouseName, inputLandmark;
    TextInputLayout inputHouseNumberLayout, inputHouseNameLayout, inputLandmarkLayout;
    private AddressResultReceiver mResultReceiver;
    private ImageView homeImage, officeImage, otherImage;
    private RelativeLayout homeLayout, officeLayout, otherLayout;
    private String strAddress, strHouseNumber, strLandmark, strHouseName, strAddressType;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    SupportMapFragment mapFragment;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    Boolean isEdit;

    JSONObject parent = new JSONObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_add_address);
        } else {
            setContentView(R.layout.activity_add_address_arabic);
        }
        context = this;
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mSkipButton = (ImageButton) findViewById(R.id.skip_button);
        mTitle = (TextView) findViewById(R.id.body_add_address);

        inputHouseNumber = (EditText) findViewById(R.id.input_flat);
        mLocationAddress = (EditText) findViewById(R.id.input_location);
        inputHouseName = (EditText) findViewById(R.id.input_save_as);
        inputLandmark = (EditText) findViewById(R.id.input_landmark);

        mConfirmLocation = (Button) findViewById(R.id.confirm_location_button);
        mSearchAddress = (ImageView) findViewById(R.id.search_address);
        mExpandLayout = (LinearLayout) findViewById(R.id.location_expand);
        backButton1 = (RelativeLayout) findViewById(R.id.back_btn1);

        homeLayout = (RelativeLayout) findViewById(R.id.home_layout);
        officeLayout = (RelativeLayout) findViewById(R.id.office_layout);
        otherLayout = (RelativeLayout) findViewById(R.id.other_layout);

        inputHouseNameLayout = (TextInputLayout) findViewById(R.id.input_layout_save_as);
        inputHouseNumberLayout = (TextInputLayout) findViewById(R.id.input_layout_flat);
        inputLandmarkLayout = (TextInputLayout) findViewById(R.id.input_layout_landmark);

        homeImage = (ImageView) findViewById(R.id.home_image);
        officeImage = (ImageView) findViewById(R.id.office_image);
        otherImage = (ImageView) findViewById(R.id.other_image);

//        setTypeface();

        isEdit = getIntent().getBooleanExtra("edit", false);
        if (isEdit) {
            isExpanded = true;
            mSkipButton.setVisibility(View.VISIBLE);
            mExpandLayout.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")) {
                mConfirmLocation.setText(getResources().getString(R.string.add_address));
            } else {
                mConfirmLocation.setText(getResources().getString(R.string.add_address_ar));
            }
            mLocationAddress.setText(getIntent().getStringExtra("Address"));
            inputHouseName.setText(getIntent().getStringExtra("HouseName"));
            inputHouseNumber.setText(getIntent().getStringExtra("HouseNo"));
            inputLandmark.setText(getIntent().getStringExtra("LandMark"));
        }

        backButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setResult(RESULT_CANCELED);
                finish();

            }
        });

        mapFragment.getMapAsync(this);
        mResultReceiver = new AddressResultReceiver(new Handler());

        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!Constants.isLocationEnabled(context)) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddAddressActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = 0;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.alert_dialog;
                } else {
                    layout = R.layout.alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                if (language.equalsIgnoreCase("En")) {
                    yes.setText("Open location settings");
                    no.setText("Cancel");
                    desc.setText("Location not enabled!");
                } else {
                    yes.setText("Open location settings");
                    no.setText("الغاء");
                    desc.setText("Location not enabled!");
                }

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                        customDialog.dismiss();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
            buildGoogleApiClient();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(context, "Location not supported in this device", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Location not supported in this device", Toast.LENGTH_SHORT).show();
            }
        }

        inputHouseNumber.addTextChangedListener(new TextWatcher(inputHouseNumber));
        inputLandmark.addTextChangedListener(new TextWatcher(inputLandmark));
        inputHouseName.addTextChangedListener(new TextWatcher(inputHouseName));
    }

    private void setTypeface() {
//        mSkipButton.setTypeface(Constants.getTypeFace(context));
//        inputHouseNumber.setTypeface(Constants.getTypeFace(context));
//        mLocationAddress.setTypeface(Constants.getTypeFace(context));
//        mConfirmLocation.setTypeface(Constants.getTypeFace(context));
//        mTitle.setTypeface(Constants.getTypeFace(context));
    }

    public void addAddressClickEvents(View v) {
        switch (v.getId()) {
            case R.id.home_layout:
                strAddressType = "home";
                homeImage.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_bg));
                officeImage.setImageDrawable(getResources().getDrawable(R.drawable.address_unselected_bg));
                otherImage.setImageDrawable(getResources().getDrawable(R.drawable.address_unselected_bg));
                if (language.equalsIgnoreCase("En")) {
                    inputHouseNameLayout.setHint(getResources().getString(R.string.hint_home));
                } else {
                    inputHouseNameLayout.setHint(getResources().getString(R.string.hint_home_ar));
                }
                break;

            case R.id.office_layout:
                strAddressType = "office";
                homeImage.setImageDrawable(getResources().getDrawable(R.drawable.address_unselected_bg));
                officeImage.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_bg));
                otherImage.setImageDrawable(getResources().getDrawable(R.drawable.address_unselected_bg));
                if (language.equalsIgnoreCase("En")) {
                    inputHouseNameLayout.setHint(getResources().getString(R.string.hint_office));
                } else {
                    inputHouseNameLayout.setHint(getResources().getString(R.string.hint_office_ar));
                }
                break;

            case R.id.other_layout:
                strAddressType = "other";
                homeImage.setImageDrawable(getResources().getDrawable(R.drawable.address_unselected_bg));
                officeImage.setImageDrawable(getResources().getDrawable(R.drawable.address_unselected_bg));
                otherImage.setImageDrawable(getResources().getDrawable(R.drawable.address_selected_bg));
                if (language.equalsIgnoreCase("En")) {
                    inputHouseNameLayout.setHint(getResources().getString(R.string.hint_other));
                } else {
                    inputHouseNameLayout.setHint(getResources().getString(R.string.hint_other_ar));
                }
                break;

            case R.id.back_btn1:
                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.skip_button:
                if (isExpanded) {
                    isExpanded = false;
                    mSkipButton.setVisibility(View.GONE);
                    mExpandLayout.setVisibility(View.GONE);
                    if (language.equalsIgnoreCase("En")) {
                        mConfirmLocation.setText(getResources().getString(R.string.confirm_location));
                    } else {
                        mConfirmLocation.setText(getResources().getString(R.string.confirm_location_ar));
                    }
                }
                break;

            case R.id.confirm_location_button:
                if (mLocationAddress.length() > 0) {
                    if (!isExpanded) {
                        isExpanded = true;
                        mSkipButton.setVisibility(View.VISIBLE);
                        mExpandLayout.setVisibility(View.VISIBLE);
                        if (language.equalsIgnoreCase("En")) {
                            mConfirmLocation.setText(getResources().getString(R.string.add_address));
                        } else {
                            mConfirmLocation.setText(getResources().getString(R.string.add_address_ar));
                        }
                    } else {
                        if (validation()) {
                            try {

                                JSONObject mainObj = new JSONObject();
                                mainObj.put("Id", 0);
                                mainObj.put("UserId", userId);
                                mainObj.put("HouseNo", strHouseNumber);
                                mainObj.put("LandMark", strLandmark);
                                mainObj.put("Address", strAddress);
                                mainObj.put("AddressType", strAddressType);
                                mainObj.put("Latitude", lat);
                                mainObj.put("Longitude", longi);
                                mainObj.put("HouseName", strHouseName);

                                parent.put("UserAddress", mainObj);
                                Log.i("TAG", parent.toString());
                            } catch (JSONException je) {
                                je.printStackTrace();
                            }
                            new SaveAddressDetails().execute(parent.toString());
                        }
                    }
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(context, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.search_address:
                openAutocompleteActivity();
                break;
        }
    }

    private boolean validation() {
        strLandmark = inputLandmark.getText().toString();
        strAddress = mLocationAddress.getText().toString();
        strHouseName = inputHouseName.getText().toString();
        strHouseNumber = inputHouseNumber.getText().toString();
        if (strHouseNumber.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputHouseNumberLayout.setError(getResources().getString(R.string.error_house_number));
            } else {
                inputHouseNumberLayout.setError(getResources().getString(R.string.error_house_number_ar));
            }
            Constants.requestEditTextFocus(inputHouseNumber, AddAddressActivity.this);
            return false;
        } else if (strLandmark.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLandmarkLayout.setError(getResources().getString(R.string.error_landmark));
            } else {
                inputLandmarkLayout.setError(getResources().getString(R.string.error_landmark_ar));
            }
            Constants.requestEditTextFocus(inputLandmark, AddAddressActivity.this);
            return false;
        } else if (strHouseName.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputHouseNameLayout.setError(getResources().getString(R.string.error_save_as));
            } else {
                inputHouseNameLayout.setError(getResources().getString(R.string.error_save_as_ar));
            }
            Constants.requestEditTextFocus(inputHouseName, AddAddressActivity.this);
            return false;
        }
        return true;
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_flat:
                    clearErrors();
                    break;
                case R.id.input_landmark:
                    clearErrors();
                    break;
                case R.id.input_save_as:
                    clearErrors();
                    break;
            }
        }
    }

    private void clearErrors() {
        inputHouseNumberLayout.setErrorEnabled(false);
        inputHouseNameLayout.setErrorEnabled(false);
        inputLandmarkLayout.setErrorEnabled(false);
    }

//    private class AddAddressApi extends AsyncTask<String, Integer, String> {
//
//        ProgressDialog dialog;
//        String inputStr;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareAddAddressJson();
//            dialog = ProgressDialog.show(AddAddressActivity.this, "",
//                    "Loading address...");
////            Constants.showLoadingDialog(AddAddressActivity.this);
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            final String networkStatus = NetworkUtil.getConnectivityStatusString(AddAddressActivity.this);
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<AddAddress> call = apiService.addAddress(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<AddAddress>() {
//                @Override
//                public void onResponse(Call<AddAddress> call, Response<AddAddress> response) {
//                    Log.d(TAG, "onResponse: " + response);
//                    if (response.isSuccessful()) {
//                        AddAddress addressResponse = response.body();
//                        try {
//                            if (addressResponse.getStatus()) {
//                                setResult(Activity.RESULT_OK);
//                                finish();
//                            } else {
//                                if (language.equalsIgnoreCase("En")) {
//                                    String failureResponse = addressResponse.getMessage();
//                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
//                                            getResources().getString(R.string.ok), AddAddressActivity.this);
//                                } else {
//                                    String failureResponse = addressResponse.getMessageAr();
//                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
//                                            getResources().getString(R.string.ok_ar), AddAddressActivity.this);
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            if (language.equalsIgnoreCase("En")) {
//                                Toast.makeText(AddAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(AddAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    } else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(AddAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(AddAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    Constants.closeLoadingDialog();
//                }
//
//                @Override
//                public void onFailure(Call<AddAddress> call, Throwable t) {
//                    Log.d(TAG, "onFailure: " + t.toString());
//                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(AddAddressActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(AddAddressActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(AddAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(AddAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    Constants.closeLoadingDialog();
//                }
//            });
//            return null;
//        }
//    }
//
//    private String prepareAddAddressJson() {
//        JSONObject addressObj = new JSONObject();
//
//        try {
//            if (!isEdit) {
//                addressObj.put("AddressId", "0");
//            } else {
//                addressObj.put("AddressId", getIntent().getStringExtra("AddressId"));
//            }
//            addressObj.put("UserId", userId);
//            addressObj.put("HouseNo", strHouseNumber);
//            addressObj.put("HouseName", strHouseName);
//            addressObj.put("LandMark", strLandmark);
//            addressObj.put("AddressType", strAddressType);
//            addressObj.put("Address", strAddress);
//            addressObj.put("Latitude", lat);
//            addressObj.put("Longitude", longi);
//            addressObj.put("IsActive", true);
//
//            Log.d(TAG, "prepareAddAddressJson: " + addressObj.toString());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return addressObj.toString();
//    }

    public class SaveAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        InputStream inputStream = null;
        String response1;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(AddAddressActivity.this);
            dialog = ProgressDialog.show(AddAddressActivity.this, "",
                    "Saving address...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.SAVE_ADDRESS_URL);

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");
                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response1 = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response1);
                            return response1;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("TAG", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response1);
                return response1;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(AddAddressActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddAddressActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }

                }else{
                    if(result.equals("")){
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(AddAddressActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AddAddressActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }else {

                        try{
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            if(s.equals("1")){
                                setResult(RESULT_OK);
                                finish();
                            }
                        }catch (JSONException je){
                            setResult(RESULT_CANCELED);
                            finish();
                        }

                    }
                }

            }else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(AddAddressActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddAddressActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                }
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        } else {
            getGPSCoordinates();
        }

        View mapView = mapFragment.getView();
        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent())
                .findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                mCenterLatLong = cameraPosition.target;
                try {
                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);

                    lat = mCenterLatLong.latitude;
                    longi = mCenterLatLong.longitude;

                    startIntentService(mLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(AddAddressActivity.this);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
        } catch (Exception npe) {
            npe.printStackTrace();
        }
        if (gps != null) {
            if (gps.canGetLocation()) {
                if (!isEdit) {
                    lat = gps.getLatitude();
                    longi = gps.getLongitude();
                } else {
                    lat = getIntent().getDoubleExtra("Latitude", 0);
                    longi = getIntent().getDoubleExtra("Longitude", 0);
                }
                LatLng latLng = new LatLng(lat, longi);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(AddAddressActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(AddAddressActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(AddAddressActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null && !isEdit) {
            changeMap(mLastLocation);
            Log.d(TAG, "ON connected");
        } else {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            Log.d(TAG, "onLocationChanged: ");
            if (location != null)
                if (!isExpanded) {
                    changeMap(location);
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            mGoogleApiClient, this);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //finish();
            }
            return false;
        }
        return true;
    }

    private void changeMap(Location location) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(false);
            LatLng latLong;

            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLong).zoom(15f).build();

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            lat = location.getLatitude();
            longi = location.getLongitude();

//            mLocationMarkerText.setText("Lat : " + location.getLatitude() + "," + "Long : " + location.getLongitude());
            startIntentService(location);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
    }


    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(Constants.LocationConstants.RESULT_DATA_KEY);
            mAreaOutput = resultData.getString(Constants.LocationConstants.LOCATION_DATA_AREA);
            mCityOutput = resultData.getString(Constants.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(Constants.LocationConstants.LOCATION_DATA_STREET);

            Log.d(TAG, "onReceiveResult: " + mStateOutput);
            displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == Constants.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));

            }
        }
    }

    /**
     * Updates the address in the UI.
     */
    protected void displayAddressOutput() {
        try {
            if (mStateOutput != null)
                Log.i(TAG, "displayAddressOutput: " + mStateOutput);
            mLocationAddress.setText(mStateOutput);
//            mLocationAddress.setSelection(mLocationAddress.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        Log.d(TAG, "startIntentService: " + mResultReceiver);
        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(Constants.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

    private void openAutocompleteActivity() {
        Log.d(TAG, "openAutocompleteActivity: ");
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            e.printStackTrace();
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(context, data);

                // TODO call location based filter
                LatLng latLong;
                latLong = place.getLatLng();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLong).zoom(15f).build();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(context, data);
        } else if (resultCode == RESULT_CANCELED) {

        }
    }
}
