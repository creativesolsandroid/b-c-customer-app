package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by CS on 09-08-2016.
 */
public class EditProfile extends Fragment {

    TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutMobile;
    EditText inputName, inputEmail, inputMobile;
    private String strName, strEmail, strMobile, gender = "Male";
    TextView mGenderMale, mGenderFemale, logout;
    TextView updateBtn, changeBtn;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;
    String response;
    ImageView menu;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.edit_profile, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.edit_profile_arabic, container,
                    false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        String profile = userPrefs.getString("user_profile", null);
        myDbHelper = new DataBaseHelper(getActivity());


        inputName = (EditText) rootView.findViewById(R.id.name);
        inputEmail = (EditText) rootView.findViewById(R.id.email);
        inputMobile = (EditText) rootView.findViewById(R.id.mobile_number);

        logout = (TextView) rootView.findViewById(R.id.logout);
        mGenderMale = (TextView) rootView.findViewById(R.id.gender_male);
        mGenderFemale = (TextView) rootView.findViewById(R.id.gender_female);

        changeBtn = (TextView) rootView.findViewById(R.id.change_pass_btn);
        updateBtn = (TextView) rootView.findViewById(R.id.update_btn);

        menu = rootView.findViewById(R.id.menu);

//        inputMobile.setText(Constants.Country_Code);

        inputLayoutName = (TextInputLayout) rootView.findViewById(R.id.input_layout_name);
        inputLayoutEmail = (TextInputLayout) rootView.findViewById(R.id.input_layout_email);
        inputLayoutMobile = (TextInputLayout) rootView.findViewById(R.id.input_layout_mobile);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) getActivity()).menuClick();


            }
        });

        changeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChangePassword.class);
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.alert_dialog;
                } else {
                    layout = R.layout.alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);
                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);
//                desc.setText("Do you want to logout from this account?");
                if (language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.app_name));
                    desc.setText("Do you want to logout from this account?");
                } else {
                    title.setText(getResources().getString(R.string.app_name_ar));
                    desc.setText("هل انت متأكد من تسجيل الخروج؟");
                }
                customDialog = dialogBuilder.create();
                customDialog.show();
                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userPrefEditor.clear();
                        userPrefEditor.commit();
                        myDbHelper.deleteOrderTable();
                        if (language.equalsIgnoreCase("En")) {
                            logout.setText("Login");
                        } else if (language.equalsIgnoreCase("Ar")) {
                            logout.setText("دخول");
                        }
                        ((MainActivity) getActivity()).selectItem(0);
                        finalCustomDialog.dismiss();
                    }
                });
                final AlertDialog finalCustomDialog1 = customDialog;
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finalCustomDialog1.dismiss();
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;
                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

            }
        });

        if (profile != null) {
            try {


                JSONObject property = new JSONObject(profile);
                JSONObject userObjuect = property.getJSONObject("profile");

//                String parts[] = userObjuect.getString("phone").split("-");

//                userId = userObjuect.getString("userId");
                inputName.setText("" + userObjuect.getString("fullName"));

                inputMobile.setText("+" + userObjuect.getString("mobile"));
                inputEmail.setText("" + userObjuect.getString("email"));
                gender = userObjuect.getString("gender");


            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }

        }

        if (gender.equalsIgnoreCase("Male")) {
//            mGenderMale.setTextColor(Color.parseColor("#F4F4F4"));
//            mGenderFemale.setTextColor(Color.parseColor("#3B271A"));
            mGenderMale.setBackground(getResources().getDrawable(R.drawable.edit_profile_selected));
            mGenderFemale.setBackground(getResources().getDrawable(R.drawable.edit_profile_unselected));

        } else if (gender.equalsIgnoreCase("Female")) {

            mGenderMale.setBackground(getResources().getDrawable(R.drawable.edit_profile_unselected));
            mGenderFemale.setBackground(getResources().getDrawable(R.drawable.edit_profile_selected));

        } else {

            mGenderMale.setBackground(getResources().getDrawable(R.drawable.edit_profile_unselected));
            mGenderFemale.setBackground(getResources().getDrawable(R.drawable.edit_profile_unselected));

        }

        mGenderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (language.equalsIgnoreCase("En")) {
                    gender = "Male";
                } else if (language.equalsIgnoreCase("Ar")) {
                    gender = "ذكر";
                }
                mGenderMale.setBackground(getResources().getDrawable(R.drawable.edit_profile_selected));
                mGenderFemale.setBackground(getResources().getDrawable(R.drawable.edit_profile_unselected));
            }
        });

        mGenderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (language.equalsIgnoreCase("En")) {
                    gender = "Female";
                } else if (language.equalsIgnoreCase("Ar")) {
                    gender = "أنثى";
                }
                mGenderMale.setBackground(getResources().getDrawable(R.drawable.edit_profile_unselected));
                mGenderFemale.setBackground(getResources().getDrawable(R.drawable.edit_profile_selected));
            }
        });

        inputName.addTextChangedListener(new TextWatcher(inputName));
        inputMobile.addTextChangedListener(new TextWatcher(inputMobile));
        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();
                strName = inputName.getText().toString().trim();
                strEmail = inputEmail.getText().toString().trim().replaceAll(" ", "");
                strMobile = inputMobile.getText().toString();
                strMobile = strMobile.replace("+966 ", "");

                if (strName.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutName.setError("Please enter Full Name");
                    } else {
                        inputLayoutName.setError("من فضلك ارسل الاسم بالكامل");
                    }
                    Constants.requestEditTextFocus(inputName, getActivity());
                } else if (strMobile.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutMobile.setError("Please enter Mobile Number");
                    } else {
                        inputLayoutMobile.setError("من فضلك أدخل رقم الجوال");
                    }
                    Constants.requestEditTextFocus(inputMobile, getActivity());

                } else if (strMobile.length() != 9) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutMobile.setError("Please enter valid mobile number");
                    } else {
                        inputLayoutMobile.setError("الرجاد ادخال رقم هاتف صحيح");
                    }
                    Constants.requestEditTextFocus(inputMobile, getActivity());

                } else if (strEmail.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutEmail.setError("Please enter Email");
                    } else {
                        inputLayoutEmail.setError("من فضلك ادخل البريد الالكتروني");
                    }
                    Constants.requestEditTextFocus(inputEmail, getActivity());

                } else if (!isValidEmail(strEmail)) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutEmail.setError("Please use email format (example - abc@abc.com");
                    } else {
                        inputLayoutEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
                    }
                    Constants.requestEditTextFocus(inputEmail, getActivity());

                } else {
                    try {
                        JSONArray mainItem = new JSONArray();


                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName", strName);
                        mainObj.put("FamilyName", "");
                        mainObj.put("NickName", "");
                        mainObj.put("Gender", gender);
                        mainObj.put("Email", strEmail);
                        mainObj.put("DeviceToken", SplashActivity.regid);
                        mainObj.put("Language", language);
                        mainObj.put("UserId", userId);
                        mainItem.put(mainObj);


                        parent.put("UserProfile", mainItem);
                        Log.e("TAG", parent.toString());
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                    new InsertRegistration().execute(parent.toString());
                }
            }
        });
        return rootView;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void clearErrors() {
        inputLayoutName.setErrorEnabled(false);
        inputLayoutMobile.setErrorEnabled(false);
        inputLayoutEmail.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.name:
                    if (editable.toString().startsWith(" ")) {
                        inputName.setText("");
                    }
                    clearErrors();
                    break;
                case R.id.mobile_number:
                    String enteredMobile = editable.toString();
                    if (!enteredMobile.contains(Constants.Country_Code)) {
                        if (enteredMobile.length() > Constants.Country_Code.length()) {
                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                            inputMobile.setText(Constants.Country_Code + enteredMobile);
                        } else {
                            inputMobile.setText(Constants.Country_Code);
                        }
                        inputMobile.setSelection(inputMobile.length());
                    }
                    clearErrors();
                    break;
                case R.id.email:
                    clearErrors();
                    break;
            }
        }
    }

    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Updating...");


        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.UPDATE_PROFILE_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (result.equals("")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
//                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    Toast.makeText(getActivity(), "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                    ((MainActivity) getActivity()).selectItem(0);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Bakery & Company");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بيكري آند كومباني");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                }

            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }
}
