package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.Dialogs.VerifyOtpDialog;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by CS on 14-09-2016.
 */
public class RegistrationActivity extends AppCompatActivity {
    EditText mFirstName, mCountryCode, mPhoneNumber, mEmail, mPassword, mConfirmPassword;
    //    TextView mGenderMale, mGenderFemale;
    TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutMobile, inputLayoutPassword, inputLayoutConfirmPass;
    private String firstName, countryCode = "+966", phoneNumber, email, password, confirmPassword;
    //    gender = "Male"
    boolean change;
    String userdetials, mpassword;
    TextView signUpBtn;
    CheckBox terms;
    TextView termsText;
    String response;
    //    Toolbar toolbar;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    public static boolean isOTPVerified = false;

    RelativeLayout back_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.registration);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.registration_arabic);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userdetials = userPrefs.getString("user_profile", null);
        mpassword = userPrefs.getString("password", null);
//        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
//        setSupportActionBar(toolbar);
////        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        change = getIntent().getBooleanExtra("change", false);

        mFirstName = (EditText) findViewById(R.id.name);
        mPhoneNumber = (EditText) findViewById(R.id.mobile_number);
        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        mConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        terms = (CheckBox) findViewById(R.id.terms);
        termsText = (TextView) findViewById(R.id.register_terms_text);
        signUpBtn = (TextView) findViewById(R.id.signup_btn);

        String name = getColoredSpanned("Agree to our ", "#AEAEAE");
        String surName = getColoredSpanned("Treams &amp; Conditions", "#000000");

        termsText.setText(Html.fromHtml(name + " " + surName));

        inputLayoutName = findViewById(R.id.input_layout_name);
        inputLayoutEmail = findViewById(R.id.input_layout_email);
        inputLayoutMobile = findViewById(R.id.input_layout_mobile);
        inputLayoutPassword = findViewById(R.id.layout_password);
        inputLayoutConfirmPass = findViewById(R.id.layout_repeat_password);

        mPhoneNumber.setText("" +countryCode);
        mPhoneNumber.setCursorVisible(false);

        back_btn = (RelativeLayout) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

//        terms = (CheckBox) findViewById(R.id.terms);


//        mGenderMale.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gender = "Male";
//                mGenderMale.setTextColor(Color.parseColor("#F4F4F4"));
//                mGenderFemale.setTextColor(Color.parseColor("#426086"));
//                mGenderMale.setBackgroundColor(Color.parseColor("#426086"));
//                mGenderFemale.setBackgroundColor(Color.parseColor("#F4F4F4"));
//            }
//        });
//
//        mGenderFemale.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gender = "Female";
//                mGenderMale.setTextColor(Color.parseColor("#426086"));
//                mGenderFemale.setTextColor(Color.parseColor("#F4F4F4"));
//                mGenderMale.setBackgroundColor(Color.parseColor("#F4F4F4"));
//                mGenderFemale.setBackgroundColor(Color.parseColor("#426086"));
//            }
//        });

        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(RegistrationActivity.this, WebViewActivity.class);
                loginIntent.putExtra("webview_toshow", "register_terms");
                startActivity(loginIntent);
            }
        });

        if (change) {
            mPhoneNumber.requestFocus();

            mPassword.setText(mpassword);
            mConfirmPassword.setText(mpassword);


            if (userdetials != null) {
                try {
                    JSONObject property = new JSONObject(userdetials);
                    JSONObject userObjuect = property.getJSONObject("profile");

                    mFirstName.setText(userObjuect.getString("fullName"));
//                    mFamilyName.setText(userObjuect.getString("family_name"));
//                    mNickName.setText(userObjuect.getString("nick_name"));
                    mPhoneNumber.setText(userObjuect.getString("mobile").replace("966", ""));
                    mEmail.setText(userObjuect.getString("email"));
//                    if (userObjuect.getString("gender").equals("Male")) {
//                        gender = "Male";
//                        mGenderMale.setTextColor(Color.parseColor("#F4F4F4"));
//                        mGenderFemale.setTextColor(Color.parseColor("#426086"));
//                        mGenderMale.setBackgroundColor(Color.parseColor("#426086"));
//                        mGenderFemale.setBackgroundColor(Color.parseColor("#F4F4F4"));
//                    } else if (userObjuect.getString("gender").equals("Female")) {
//                        gender = "Female";
//                        mGenderMale.setTextColor(Color.parseColor("#426086"));
//                        mGenderFemale.setTextColor(Color.parseColor("#F4F4F4"));
//                        mGenderMale.setBackgroundColor(Color.parseColor("#F4F4F4"));
//                        mGenderFemale.setBackgroundColor(Color.parseColor("#426086"));
//                    }


                } catch (JSONException e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }
            }
        }

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final JSONObject parent = new JSONObject();
                firstName = mFirstName.getText().toString();
//                familyName = mFamilyName.getText().toString();
//                nickName = mNickName.getText().toString();
                phoneNumber = mPhoneNumber.getText().toString();
                Log.i("TAG", "phone number " + phoneNumber);
                phoneNumber = phoneNumber.replaceAll("[()-]", "").replace(" ", "").replace("+966", "");
                Log.i("TAG", "phone number1 " + phoneNumber);

                email = mEmail.getText().toString().replaceAll(" ", "");
                password = mPassword.getText().toString();
                confirmPassword = mConfirmPassword.getText().toString();
                if (firstName.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
                    }

                } else if (firstName.isEmpty()) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
                    }
                } else if (firstName.equals(" ")) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
                    }
                } else if (phoneNumber.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
                    }
                } else if (phoneNumber.length() != 9) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
                    }

                } else if (email.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
                    }
                } else if (!email.matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}")) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
                    }

                } else if (!isValidEmail(email)) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
                    }
                } else if (password.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
                    }
                } else if (password.length() < 4 || password.length() > 20) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                    }
                } else if (password.contains(" ")) {
                    inputLayoutPassword.setError("Spaces are not allowed");
                } else if (!confirmPassword.equals(password)) {
                    if (language.equalsIgnoreCase("En")) {
                        inputLayoutConfirmPass.setError(getResources().getString(R.string.signup_msg_confirm_password));
                    } else if (language.equalsIgnoreCase("Ar")) {
                        inputLayoutConfirmPass.setError(getResources().getString(R.string.signup_msg_confirm_password_ar));
                    }

                } else if (confirmPassword.contains(" ")) {
                    inputLayoutConfirmPass.setError("Spaces are not allowed");
                }
//                else if (gender.equals("null")) {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
//
//
//                    // set title
//                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Please select any gender male or female")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }
                else if (!terms.isChecked()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);


                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Please review and accept the terms of service")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("من فضلك راجع وأقبل شروط الخدمة ")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    try {
                        JSONArray mainItem = new JSONArray();


                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName", firstName);
//                        mainObj.put("FamilyName", familyName);
//                        mainObj.put("NickName", nickName);
//                        mainObj.put("Gender", gender);
                        mainObj.put("Email", email);
                        mainObj.put("Mobile", "966" + phoneNumber);
                        mainObj.put("Password", password);
                        mainObj.put("DeviceToken", SplashActivity.regid);
                        mainObj.put("DeviceType", "Android");
                        mainObj.put("Language", language);
                        mainItem.put(mainObj);


                        parent.put("UserDetails", mainItem);
                        Log.i("TAG", parent.toString());
                    } catch (JSONException je) {

                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(RegistrationActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title

                    // set dialog message
                    if (language.equalsIgnoreCase("En")) {
                        alertDialogBuilder.setTitle("BAKERY & Co.");
                        alertDialogBuilder
                                .setMessage("We will be verifying the mobile number:\n\n" + countryCode + " " + phoneNumber + "\n\nIs this OK, or would you like to edit the number?")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
//                                    new GetVerificationCode().execute(Constants.VERIFY_MOBILE + "966" + phoneNumber, parent.toString());
                                        new InsertRegistration().execute(parent.toString());
//                                    Intent loginIntent = new Intent(RegistrationActivity.this, VerifyRandomNumber.class);
//                                    loginIntent.putExtra("phone_number", "966" + phoneNumber);
//                                    loginIntent.putExtra("forgot", false);
//                                    startActivityForResult(loginIntent, 2);
                                    }
                                });
                        alertDialogBuilder.setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mPhoneNumber.requestFocus();
                            }
                        });
                    } else {
                        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name_ar));
                        alertDialogBuilder
                                .setMessage( getResources().getString(R.string.signup_alert_mobile_verify1_ar) + countryCode + " " + phoneNumber + getResources().getString(R.string.signup_alert_mobile_verify2_ar))
                                .setCancelable(false)
                                .setPositiveButton(getResources().getString(R.string.ok_ar), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
//                                    new GetVerificationCode().execute(Constants.VERIFY_MOBILE + "966" + phoneNumber, parent.toString());
                                        new InsertRegistration().execute(parent.toString());
//                                    Intent loginIntent = new Intent(RegistrationActivity.this, VerifyRandomNumber.class);
//                                    loginIntent.putExtra("phone_number", "966" + phoneNumber);
//                                    loginIntent.putExtra("forgot", false);
//                                    startActivityForResult(loginIntent, 2);
                                    }
                                });
                        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.edit_ar), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mPhoneNumber.requestFocus();
                            }
                        });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });

        mFirstName.addTextChangedListener(new TextWatcher(mFirstName));
        mPhoneNumber.addTextChangedListener(new TextWatcher(mPhoneNumber));
        mEmail.addTextChangedListener(new TextWatcher(mEmail));
        mPassword.addTextChangedListener(new TextWatcher(mPassword));
        mConfirmPassword.addTextChangedListener(new TextWatcher(mConfirmPassword));

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void clearErrors() {
        inputLayoutName.setErrorEnabled(false);
        inputLayoutMobile.setErrorEnabled(false);
        inputLayoutEmail.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutConfirmPass.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.name:
                    if (editable.toString().startsWith(" ")) {
                        mFirstName.setText("");
                    }
                    clearErrors();
                    break;
                case R.id.mobile_number:
                    mPhoneNumber.setCursorVisible(true);
                    String enteredMobile = editable.toString();
                    if (!enteredMobile.contains(Constants.Country_Code)) {
                        if (enteredMobile.length() > Constants.Country_Code.length()) {
                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                            mPhoneNumber.setText(Constants.Country_Code + enteredMobile);
                        } else {
                            mPhoneNumber.setText(Constants.Country_Code);
                        }
                        mPhoneNumber.setSelection(mPhoneNumber.length());
                    }
                    clearErrors();
                    break;
                case R.id.email:
                    clearErrors();
                    break;
                case R.id.password:
                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.confirm_password:
                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (resultCode == RESULT_CANCELED) {
            setResult(RESULT_CANCELED);
            finish();
//            Toast.makeText(LoginActivity.this, "Registration unseccessfull", Toast.LENGTH_SHORT).show();
        }
    }


    public class GetVerificationCode extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String userData;
        String response;
        ProgressDialog dialog;


        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(RegistrationActivity.this);
            dialog = ProgressDialog.show(RegistrationActivity.this, "",
                    "Registering...");


        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                Log.i("TAG", "" + params[0]);
                response = jParser
                        .getJSONFromUrl(params[0]);
                userData = params[1];
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(RegistrationActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (result.equals("")) {
                        Toast.makeText(RegistrationActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONObject jo1 = jo.getJSONObject("Success");
                                String otp = jo1.getString("OTP");
                                String phNo = jo1.getString("MobileNo");
//
                                Intent i = new Intent(RegistrationActivity.this, VerifyRandomNumber.class);
                                i.putExtra("OTP", otp);
                                i.putExtra("phone_number", phNo);
                                i.putExtra("user_data", userData);
                                startActivityForResult(i, 2);

                            } catch (JSONException je) {

                                String msg = null;
                                try {
                                    msg = jo.getString("Failure");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                je.printStackTrace();

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(RegistrationActivity.this, android.R.style.Theme_Material_Light_Dialog));
                                // set title
                                alertDialogBuilder.setTitle("BAKERY & Co.");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(msg)
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            } else {
                Toast.makeText(RegistrationActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }

    private void showVerifyDialog() {
        Bundle args = new Bundle();
        args.putString("name", firstName);
        args.putString("email", email);
        args.putString("mobile", phoneNumber);
        args.putString("password", password);
//        args.putString("otp", serverOtp);

        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "register");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if (newFragment != null) {
                    newFragment.dismiss();

                    if (!isOTPVerified) {
                        Constants.requestEditTextFocus(mPhoneNumber, RegistrationActivity.this);
                        mPhoneNumber.setSelection(mPhoneNumber.length());
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RegistrationActivity.this, "Registration successful", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RegistrationActivity.this, "Registration successful", Toast.LENGTH_SHORT).show();
                        }
                        setResult(RESULT_OK);
                        finish();
                    }
                }
            }
        });
    }

    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(RegistrationActivity.this);
            dialog = ProgressDialog.show(RegistrationActivity.this, "",
                    "Registering...");


        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.REGISTRATION_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(RegistrationActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(RegistrationActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");


                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
                                    userPrefEditor.putString("password", password);

                                    userPrefEditor.commit();

                                    if (isVerified) {
                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();
//                                        Intent loginIntent = new Intent();
                                        setResult(RESULT_OK);
                                        finish();
                                    } else {
                                        showVerifyDialog();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(RegistrationActivity.this, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("BAKERY & Co.");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بيكري آند كومباني");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(RegistrationActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws
            IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }
}