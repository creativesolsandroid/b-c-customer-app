package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;

public class LanguageActivity extends Fragment {

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    TextView arabic, english, done;
    ImageView menu;
    boolean marabic = false, menglish = false;


    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.language_activity, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.language_activity_arabic, container,
                    false);
        }

        arabic = rootView.findViewById(R.id.arabic);
        english = rootView.findViewById(R.id.english);
        done = rootView.findViewById(R.id.done);

        menu = rootView.findViewById(R.id.menu);

//        back_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent a = new Intent(LanguageActivity.this, MainActivity.class);
//                startActivity(a);
//
//            }
//        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        if (language.equalsIgnoreCase("En")) {

            english.setBackground(getResources().getDrawable(R.drawable.user_module_btn_bg));
            arabic.setBackground(getResources().getDrawable(R.drawable.user_module_btn_unselected_bg));

        } else {

            english.setBackground(getResources().getDrawable(R.drawable.user_module_btn_unselected_bg));
            arabic.setBackground(getResources().getDrawable(R.drawable.user_module_btn_bg));

        }

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                marabic = true;
                menglish = false;
                english.setBackground(getResources().getDrawable(R.drawable.user_module_btn_unselected_bg));
                arabic.setBackground(getResources().getDrawable(R.drawable.user_module_btn_bg));

            }
        });

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                menglish = true;
                marabic = false;
                english.setBackground(getResources().getDrawable(R.drawable.user_module_btn_bg));
                arabic.setBackground(getResources().getDrawable(R.drawable.user_module_btn_unselected_bg));

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (menglish) {

                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();

                } else {

                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();

                }

//                ((MainActivity) getActivity()).selectItem(0);

                getActivity().finish();
                Intent a = new Intent(getActivity(), MainActivity.class);
                startActivity(a);

            }
        });


        return rootView;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void onBackPressed() {
//
//        Intent a = new Intent(LanguageActivity.this, MainActivity.class);
//        startActivity(a);
//        finish();
//
//    }

}
