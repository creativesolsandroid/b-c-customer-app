package com.cs.bakeryco.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.FavouriteOrderAdapter;
import com.cs.bakeryco.model.FavouriteOrder;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 26-09-2016.
 */
public class FavoriteOrderActivity extends Fragment {
    private DataBaseHelper myDbHelper;
    String response;
    private ArrayList<FavouriteOrder> favoritesList = new ArrayList<>();
    private SwipeMenuListView orderHistoryListView;
    TextView emptyView;
    private FavouriteOrderAdapter mAdapter;
    SharedPreferences userPrefs;
    String userId;
    //    TextView title;
//    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
//    LinearLayout order_status, order_summary;
//    TextView order_status_txt, order_summary_txt, date;
//    View order_status_view, order_status_view1, order_summary_view, order_summary_view1;
    RelativeLayout order_status_data, order_summary_data, back_btn;
    RecyclerView order_fav;
    View rootView;
    TextView title;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.favourite_order_layout, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.favourite_order_layout_arabic, container,
                    false);
        }

        myDbHelper = new DataBaseHelper(getActivity());
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        title = (TextView) rootView.findViewById(R.id.item_title);
        back_btn = rootView.findViewById(R.id.back_btn);
        order_fav = rootView.findViewById(R.id.order_fav);

//        order_status = rootView.findViewById(R.id.order_status);
//        order_summary = rootView.findViewById(R.id.order_summary);
//
//        order_status_txt = rootView.findViewById(R.id.order_status_txt);
//        order_summary_txt = rootView.findViewById(R.id.order_summary_txt);
//
//        order_status_view = rootView.findViewById(R.id.order_status_view);
//        order_status_view1 = rootView.findViewById(R.id.order_status_view1);
//        order_summary_view = rootView.findViewById(R.id.order_summary_view);
//        order_summary_view1 = rootView.findViewById(R.id.order_summary_view1);

        order_status_data = rootView.findViewById(R.id.order_status_data);
        order_summary_data = rootView.findViewById(R.id.order_summary_data);

        orderHistoryListView = (SwipeMenuListView) rootView.findViewById(R.id.fav_order_listview);
        emptyView = (TextView) rootView.findViewById(R.id.empty_view);
        mAdapter = new FavouriteOrderAdapter(getActivity(), favoritesList, language);
        orderHistoryListView.setAdapter(mAdapter);
        new GetFavoriteOrderDetails().execute(Constants.GET_FAVORITE_ORDERS_URL + userId);
        Log.e("TAG", "user ID" + userId);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).menuClick();

            }
        });

//        order_status.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                order_status_data.setVisibility(View.VISIBLE);
//                order_summary_data.setVisibility(View.GONE);
//
//                order_status_txt.setTextColor(getResources().getColor(R.color.black));
//                order_summary_txt.setTextColor(getResources().getColor(R.color.track));
//
//                order_status_view.setVisibility(View.VISIBLE);
//                order_status_view1.setVisibility(View.GONE);
//
//                order_summary_view.setVisibility(View.GONE);
//                order_summary_view1.setVisibility(View.VISIBLE);
//
//            }
//        });

//        order_summary.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                order_status_data.setVisibility(View.GONE);
//                order_summary_data.setVisibility(View.VISIBLE);
//
//                order_status_txt.setTextColor(getResources().getColor(R.color.track));
//                order_summary_txt.setTextColor(getResources().getColor(R.color.black));
//
//                order_status_view.setVisibility(View.GONE);
//                order_status_view1.setVisibility(View.VISIBLE);
//
//                order_summary_view.setVisibility(View.VISIBLE);
//                order_summary_view1.setVisibility(View.GONE);
//
//            }
//        });

        if(language.equalsIgnoreCase("En")){
            title.setText("Favorite Orders");
            emptyView.setText("There is no orders to display");
        }else if(language.equalsIgnoreCase("Ar")){
            title.setText("المفضلة");
            emptyView.setText("لا يوجد منتجات في السلة");
        }
        orderHistoryListView.setEmptyView(emptyView);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                if (language.equalsIgnoreCase("En")) {
                    deleteItem.setTitle("Delete");
                } else if (language.equalsIgnoreCase("Ar")) {
                    deleteItem.setTitle("حذف");
                }                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
//        orderHistoryListView.setMenuCreator(creator);

        // step 2. listener item click event
        orderHistoryListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        new DeleteFavOrder().execute(Constants.DELETE_FAVORITE_ORDERS_URL + favoritesList.get(position).getOrderId());
                        break;
                }
                return false;
            }
        });


        orderHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                myDbHelper.deleteOrderTable();
                String orderId = favoritesList.get(position).getOrderId();
                new GetOrderDetails().execute(Constants.VIEW_ORDERD_DETAILS_URL + userId + "&orderId=" + orderId);

                Log.e("TAG", orderId);
                Log.e("TAG", userId);
            }
        });


        return rootView;
    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    public class GetFavoriteOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading...");
            favoritesList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", "user response:" + response);
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    FavouriteOrder fo = new FavouriteOrder();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String orderId = jo1.getString("odrId");
                                    String favoriteName = jo1.getString("Fname");
                                    String orderDate = jo1.getString("Odate");
                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
                                    String total_Price = jo1.getString("TotPrice");
                                    String orderType = jo1.getString("OrderType");
                                    String userAddress = jo1.getString("UserAddress");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for (int j = 0; j < ja1.length(); j++) {
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if (itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")) {
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        } else {
                                            itemDetails = itemDetails + ", " + jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr + ", " + jo2.getString("ItmName_ar");
                                        }
                                    }


                                    fo.setOrderId(orderId);
                                    fo.setFavoriteName(favoriteName);
                                    fo.setOrderDate(orderDate);
                                    fo.setStoreName(storeName);
                                    fo.setStoreName_ar(storeName_ar);
                                    fo.setTotalPrice(total_Price);
                                    fo.setItemDetails(itemDetails);
                                    fo.setItemDetails_ar(itemDetailsAr);
                                    fo.setOrdertype(orderType);
                                    fo.setUseraddress(userAddress);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    favoritesList.add(fo);

                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
//                                Toast.makeText(FavoriteOrdersActivity.this, "Sorry You Don't Have any Favorite Orders", Toast.LENGTH_SHORT).show();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

//                        try {
//                            JSONObject jo= new JSONObject(result);
//                            String s = jo.getString("Success");
                        new GetFavoriteOrderDetails().execute(Constants.GET_FAVORITE_ORDERS_URL + userId);
//                            Toast.makeText(FavoriteOrdersActivity.this, "Order deleted successfully", Toast.LENGTH_SHORT).show();

//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(FavoriteOrdersActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
//                        }

                    }
                }

            } else {
                Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray ja = jo.getJSONArray("SubItem");
                            myDbHelper.deleteOrderTable();
                            for (int i = 0; i < ja.length(); i++) {
                                String ids = "", additionalsStr = "", additionalsStrAr = "", additionalsPrice = "", additionalqty = "";
                                String categoryId = "", subCatId = "", itemId = "", itemName = "", itemNameAr = "", itemImage = "", itemDesc = "", itemDescAr = "", itemType = "", itemType_id = "", comments = "";
                                String price = "";
                                int additionPrice = 0;
                                int priceAd = 0, quantity = 0;
                                float finalPrice = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                for (int j = 0; j < ja1.length(); j++) {
                                    if (j == 0) {
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        itemId = jo1.getString("ItemId");
                                        itemName = jo1.getString("ItemName");
                                        itemNameAr = jo1.getString("ItemName_Ar");
                                        itemImage = jo1.getString("Images");
                                        itemDesc = jo1.getString("Description");
                                        itemDescAr = jo1.getString("Description_Ar");
                                        itemType = jo1.getString("Size");
                                        itemType_id = jo1.getString("SizeId");
                                        quantity = jo1.getInt("Qty");
                                        price = jo1.getString("ItemPrice");
                                        categoryId = jo1.getString("CategoryId");
                                        subCatId = jo1.getString("SubCatId");
                                        comments = jo1.getString("Comments");

                                        if (jo1.getJSONArray("SubSubItems").length() == 0) {

                                            JSONArray jsonArray = jo1.getJSONArray("SubSubItems");

                                        } else {

                                            JSONArray jsonArray = jo1.getJSONArray("SubSubItems");
                                            try {
                                                for (int l = 0; l < jsonArray.length(); l++) {
                                                    JSONObject jo3 = jsonArray.getJSONObject(l);

                                                    if (l == 0) {
                                                        ids = jo3.getString("SubItemId");
                                                        additionalsStr = jo3.getString("SubItemName");
                                                        additionalsStrAr = jo3.getString("SubItemName_Ar");
                                                        additionalqty = jo3.getString("OSubItemQuantity");
                                                    } else {
                                                        ids = ids + "," + jo3.getString("SubItemId");
                                                        additionalsStr = additionalsStr + "," + jo3.getString("SubItemName");
                                                        additionalsStrAr = additionalsStrAr + "," + jo3.getString("SubItemName_Ar");
                                                        additionalqty = additionalqty + "," + jo3.getString("OSubItemQuantity");
                                                    }
                                                }

                                                Log.i("TAG", "ids " + ids);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }


                                finalPrice = Float.parseFloat(price) * quantity;


                                values.put("itemId", itemId);
                                values.put("itemTypeId", itemType);
                                values.put("subCategoryId", subCatId);
                                values.put("additionals", "");
                                values.put("qty", Integer.toString(quantity));
                                values.put("price", price);
                                values.put("additionalsPrice", "");
                                values.put("additionalsTypeId", "");
                                values.put("totalAmount", Float.toString(finalPrice));
                                values.put("comment", comments);
                                values.put("status", "1");
                                values.put("creationDate", "14/07/2015");
                                values.put("modifiedDate", "14/07/2015");
                                values.put("categoryId", categoryId);
                                values.put("itemName", itemName);
                                values.put("itemNameAr", itemNameAr);
                                values.put("image", itemImage);
                                values.put("item_desc", "");
                                values.put("item_desc_Ar", "");
                                values.put("sub_itemName", additionalsStr);
                                values.put("sub_itemName_Ar", additionalsStrAr);
                                values.put("sub_itemImage", "");
                                values.put("sub_item_id", ids);
                                values.put("sub_item_count", additionalqty);
                                values.put("ItemType", "");
                                values.put("sizename", itemType_id);
                                values.put("ItemTypeName_ar", "");
                                Log.e("TAG", "price fav" + price);
                                Log.e("TAG", "Qty fav" + quantity);
                                myDbHelper.insertOrder(values);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            Intent intent = new Intent(getActivity(), OrderConfirmationNew.class);
            startActivity(intent);

            super.onPostExecute(result);

        }

    }
}
