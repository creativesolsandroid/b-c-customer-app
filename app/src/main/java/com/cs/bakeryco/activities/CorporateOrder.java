package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.JustifiedTextView;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by CS on 28-10-2016.
 */
public class CorporateOrder extends AppCompatActivity {

    EditText firstName, lastName, position, company, contactNumber, email, city, address, message;
    Toolbar toolbar;
    Button submit;
    TextView readmore;
    JustifiedTextView corporateForm;
    LinearLayout formLayout;

    SharedPreferences userPrefs;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    String response;
    String mLoginStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.corporate_order);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.corporate_order_arabic);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        String profile = userPrefs.getString("user_profile", null);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Display deviceDisplay = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        int layoutWidth = deviceDisplay.getWidth(); // 480
        final int layoutHeight = deviceDisplay.getHeight(); // 800
//        w1 = (int) ((layoutWidth * 10) / (35.5555)); // 90
        int height = (int) ((layoutHeight * 10) / (57.14285)); // 140
        final LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final LinearLayout.LayoutParams Params1 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, height);

        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        position = (EditText) findViewById(R.id.position);
        company = (EditText) findViewById(R.id.company_name);
        contactNumber = (EditText) findViewById(R.id.mobile_number);
        email = (EditText) findViewById(R.id.email);
        city = (EditText) findViewById(R.id.city);
        address = (EditText) findViewById(R.id.address);
        message = (EditText) findViewById(R.id.message_box);
        submit = (Button) findViewById(R.id.submit_btn);
        formLayout = (LinearLayout) findViewById(R.id.form_layout);
        readmore = (TextView) findViewById(R.id.read_more);
        corporateForm = (JustifiedTextView) findViewById(R.id.corporate_form);

        if (language.equalsIgnoreCase("En")) {
            corporateForm.setAlignment(Paint.Align.LEFT);
        }else if (language.equalsIgnoreCase("Ar")) {
            corporateForm.setAlignment(Paint.Align.RIGHT);
        }
        corporateForm.setTextColor(Color.parseColor("#A18566"));
        corporateForm.setLineSpacing(4);

        formLayout.setLayoutParams(Params1);

        readmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = readmore.getText().toString();
                if (language.equalsIgnoreCase("En")) {
                    if (s.equalsIgnoreCase("Read more")) {
                        readmore.setText("Show less");
                        formLayout.setLayoutParams(Params);
                    } else if (s.equalsIgnoreCase("Show less")) {
                        readmore.setText("Read more");
                        formLayout.setLayoutParams(Params1);
                    }
                } else if (language.equalsIgnoreCase("Ar")) {
                    if (s.equalsIgnoreCase("لمزيد من المعلومات")) {
                        readmore.setText(" توضيح الأقل");
                        formLayout.setLayoutParams(Params);
                    } else if (s.equalsIgnoreCase(" توضيح الأقل")) {
                        readmore.setText("لمزيد من المعلومات");
                        formLayout.setLayoutParams(Params1);
                    }
                }
            }
        });

        if (mLoginStatus.equalsIgnoreCase("loggedin")) {
            if (profile != null) {
                try {


                    JSONObject property = new JSONObject(profile);
                    JSONObject userObjuect = property.getJSONObject("profile");

//                String parts[] = userObjuect.getString("phone").split("-");

//                userId = userObjuect.getString("userId");
                    firstName.setText(userObjuect.getString("fullName"));
                    contactNumber.setText("+" + userObjuect.getString("mobile"));
                    email.setText(userObjuect.getString("email"));


                } catch (JSONException e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }

            }
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();
                String fName = firstName.getText().toString();
                String lName = lastName.getText().toString();
                String sPosition = position.getText().toString();
                String sCompany = company.getText().toString();
                String phNumber = contactNumber.getText().toString();
                String sEmail = email.getText().toString().replaceAll(" ", "");
                String sCity = city.getText().toString();
                String sAddress = address.getText().toString();
                String msg = message.getText().toString();

                if (fName.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    firstName.setError("Please enter first name");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        firstName.setError("من فضلك ارسل الاسم بالكامل");
//                    }
                } else if (sPosition.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    position.setError("Please enter position / title");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mPhoneNumber.setError("من فضلك أدخل رقم الجوال");
//                    }
                } else if (sCompany.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    company.setError("Please enter company name");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mPhoneNumber.setError("من فضلك ادخل رقم جوال صحيح");
//                    }

                } else if (phNumber.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    contactNumber.setError("Please enter contact number");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mEmail.setError("من فضلك ادخل البريد الالكتروني");
//                    }
                } else if (sEmail.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    email.setError("Please enter Email");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mEmail.setError("من فضلك ادخل البريد الالكتروني");
//                    }
                } else if (!sEmail.matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}")) {
//                    if (language.equalsIgnoreCase("En")) {
                    email.setError("Please use Email format (example - abc@abc.com");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
//                    }

                } else if (!isValidEmail(sEmail)){
                    if(language.equalsIgnoreCase("En")) {
                        email.setError("Please use email format (example - abc@abc.com");
                    }else if(language.equalsIgnoreCase("Ar")){
                        email.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
                    }
                } else if (sCity.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    city.setError("Please enter city");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mPassword.setError("من فضلك ادخل كلمة السر");
//                    }
                } else if (sAddress.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    address.setError("Please enter address");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mPassword.setError("ثمانية حروف على الأقل");
//                    }
                } else if (msg.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                    message.setError("Please enter your message");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mConfirmPassword.setError("كلمة المرور غير صحيحة ، من فضلك أعد الإدخال ");
//                    }

                } else {
                    try {
//                        JSONArray mainItem = new JSONArray();


                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FName", fName);
                        mainObj.put("LName", lName);
                        mainObj.put("Position", sPosition);
                        mainObj.put("Company", sCompany);
                        mainObj.put("Email", sEmail);
                        mainObj.put("City", sCity);
                        mainObj.put("Address", sAddress);
                        mainObj.put("ContactNo", phNumber);
                        mainObj.put("Msg", msg);
//                        mainItem.put(mainObj);


                        parent.put("CorporateRequest", mainObj);
                        Log.i("TAG", parent.toString());
                    } catch (JSONException je) {

                    }
                    new InsertRegistration().execute(parent.toString());
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CorporateOrder.this);
            dialog = ProgressDialog.show(CorporateOrder.this, "",
                    "Registering...");


        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.CORPORATE_ORDER_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(CorporateOrder.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(CorporateOrder.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String reply = jo.getString("Success");
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CorporateOrder.this, android.R.style.Theme_Material_Light_Dialog));

//                                if(language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("BAKERY & Co.");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage(reply)
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        });
//                                }else if(language.equalsIgnoreCase("Ar")){
//                                    // set title
//                                    alertDialogBuilder.setTitle("اوريجانو");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(result)
//                                            .setCancelable(false)
//                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            } catch (JSONException je) {
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(RegistrationActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                                if(language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(result)
//                                            .setCancelable(false)
//                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }else if(language.equalsIgnoreCase("Ar")){
//                                    // set title
//                                    alertDialogBuilder.setTitle("اوريجانو");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(result)
//                                            .setCancelable(false)
//                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(CorporateOrder.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }
}
