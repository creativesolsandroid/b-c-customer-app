package com.cs.bakeryco.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.MenuPop;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.SubMenuAdapter;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.MainCategories;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubCategories;
import com.cs.bakeryco.model.SubItems;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//import com.cs.bakeryco.adapters.ItemsListAdapter;

/**
 * Created by CS on 22-09-2016.
 */
public class CategoriesListActivity extends AppCompatActivity {

    private ArrayList<Price> pricesList = new ArrayList<>();
    private ArrayList<SubCategories> subCatList = new ArrayList<>();
    private ArrayList<Order> orderlist = new ArrayList<>();

    String response;
    private DataBaseHelper myDbHelper;
    TextView bag_count;
    ImageView bag;


    private int lastExpandedPosition = 0;
//    public static TextView orderPrice, orderQuantity, mcount_basket;

//    ImageView mend;

    //    public static ItemsListAdapter mAdapter;
//    HorizontalScrollView hsv;
//    GridView mGridView;
    ListView cat_list;
    TextView sub_cat_title;
    RelativeLayout back_btn, transparent_layout;
    SubMenuAdapter mSubMenuAdapter;

    public static int catId;
    int position;
    ArrayList<ItemsList.Items> mainitem = new ArrayList<>();
    ArrayList<SubCategories> subcatitem = new ArrayList<>();

    //    RelativeLayout checkOut, addMoreLayout;
//    ExpandableListView mExpListView;
//    Toolbar toolbar;
//    TextView pastry, cake, bread, sandwich, salad, macarons, hotmeals, occation;
//    RelativeLayout mcount;
    SharedPreferences languagePrefs;
    String language;
    ImageView menu_img;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.categories_list);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.categories_list_arabic);
        }
        myDbHelper = new DataBaseHelper(CategoriesListActivity.this);
        Log.e("TAG", "" + getIntent().getExtras().getInt("catId"));
        catId = getIntent().getExtras().getInt("catId");
        mainitem = (ArrayList<ItemsList.Items>) getIntent().getSerializableExtra("mainList");

        cat_list = (ListView) findViewById(R.id.cat_list);
        back_btn = (RelativeLayout) findViewById(R.id.back_btn);
        sub_cat_title = findViewById(R.id.sub_cat_title);
        menu_img = findViewById(R.id.menu_img);
        bag = findViewById(R.id.bag);

        transparent_layout = findViewById(R.id.transparent_layout);

        menu_img.setVisibility(View.VISIBLE);
        transparent_layout.setVisibility(View.GONE);

        menu_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (menu_img.getVisibility() == View.VISIBLE) {

                    menu_img.setVisibility(View.GONE);
                    transparent_layout.setVisibility(View.VISIBLE);
                    Intent a = new Intent(CategoriesListActivity.this, MenuPop.class);
                    a.putExtra("itemsArraylist", mainitem);
                    startActivity(a);
                }

            }
        });

        bag_count = findViewById(R.id.bag_count);

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(CategoriesListActivity.this, OrderConfirmationNew.class);
                startActivity(a);

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        if (catId == 1) {
            position = 1;

            if (language.equalsIgnoreCase("En")) {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName());
            } else {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName_Ar());
            }
            try {
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                cat_list.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
//                new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL);
            }
        } else if (catId == 2) {
            position = 0;
            if (language.equalsIgnoreCase("En")) {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName());
            } else {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName_Ar());
            }

            try {
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                cat_list.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();

            }
        } else if (catId == 3) {
            position = 4;
            if (language.equalsIgnoreCase("En")) {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName());
            } else {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName_Ar());
            }

            try {
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                cat_list.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();

            }
        } else if (catId == 4) {
            position = 2;
            if (language.equalsIgnoreCase("En")) {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName());
            } else {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName_Ar());
            }

            try {
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                cat_list.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();

            }
        } else if (catId == 6) {

            position = 3;

            if (language.equalsIgnoreCase("En")) {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName());
            } else {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName_Ar());
            }
            try {
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                cat_list.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();

            }
        } else if (catId == 13) {
            position = 5;

            if (language.equalsIgnoreCase("En")) {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName());
            } else {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName_Ar());
            }

            try {
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                cat_list.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (catId == 14) {
            position = 6;

            if (language.equalsIgnoreCase("En")) {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName());
            } else {
                sub_cat_title.setText("" + mainitem.get(position).getMainCategoryName_Ar());
            }

            try {
                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
                cat_list.setAdapter(mSubMenuAdapter);
                mSubMenuAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

//    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String networkStatus;
//        ProgressDialog dialog;
//
//        @Override
//        protected void onPreExecute() {
//            mainitem.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(CategoriesListActivity.this);
//            dialog = ProgressDialog.show(CategoriesListActivity.this, "",
//                    "Loading items...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(CategoriesListActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    if (result.equals("")) {
//                        Toast.makeText(CategoriesListActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        try {
//                            JSONObject jo = new JSONObject(result);
//                            JSONArray ja = jo.getJSONArray("Success");
//
//                            for (int i = 0; i < ja.length(); i++) {
//
//                                MainCategories maincat = new MainCategories();
//                                ArrayList<SubCategories> subcatList = new ArrayList<>();
//
//                                JSONObject jo1 = ja.getJSONObject(i);
//
//                                maincat.setMaincatid(jo1.getString("MainCategoryId"));
//                                maincat.setMaincatname(jo1.getString("MainCategoryName"));
//                                maincat.setMaincatname_ar(jo1.getString("MainCategoryName_Ar"));
//                                maincat.setMaincatdesc(jo1.getString("MainCategoryDesc"));
//                                maincat.setMaincatdesc_ar(jo1.getString("MainCategoryDesc_Ar"));
//                                maincat.setMainimg(jo1.getString("Simg"));
//
//                                JSONArray ja1 = jo1.getJSONArray("SubCategoryItems");
//
//                                for (int k = 0; k < ja1.length(); k++) {
//
//                                    SubCategories subcat = new SubCategories();
//                                    ArrayList<Section> sectionList = new ArrayList<>();
//
//                                    JSONObject jo2 = ja1.getJSONObject(k);
//                                    subcat.setCatId(jo2.getString("SubCategoryId"));
//                                    subcat.setSubCat(jo2.getString("SubCategoryName"));
//                                    subcat.setSubCatAr(jo2.getString("SubCategoryName_Ar"));
//                                    subcat.setDescription(jo2.getString("SubCategoryDesc"));
//                                    subcat.setDescriptionAr(jo2.getString("SubCategoryDesc_Ar"));
//                                    subcat.setImages(jo2.getString("SubCategoryImage"));
//                                    subcat.setSubCatSeq(jo2.getString("SubCategorySeq"));
//                                    subcat.setMaincatid(jo2.getString("MainCategoryId"));
//
//
//                                    JSONArray ja2 = jo2.getJSONArray("Sections");
//
//                                    for (int l = 0; l < ja2.length(); l++) {
//
//                                        Section section = new Section();
//                                        ArrayList<Items> itemlist = new ArrayList<>();
//
//                                        JSONObject jo3 = ja2.getJSONObject(l);
//                                        section.setSectionid(jo3.getString("SectionId"));
//                                        section.setSectionname(jo3.getString("SectionName"));
//                                        section.setSectionname_ar(jo3.getString("SectionName_Ar"));
//                                        section.setSectionsubcat(jo3.getString("SectionSubCategory"));
//
//                                        JSONArray ja3 = jo3.getJSONArray("Items");
//
//                                        for (int m = 0; m < ja3.length(); m++) {
//
//                                            Items item = new Items();
//                                            ArrayList<Price> priceList = new ArrayList<>();
//                                            ArrayList<SubItems> subitemsList = new ArrayList<>();
//
//                                            JSONObject jo4 = ja3.getJSONObject(m);
//                                            item.setItemId(jo4.getString("ItemId"));
//                                            item.setSubCatId(jo4.getString("SubCategoryId"));
//                                            item.setSectionid(jo4.getString("SectionId"));
//                                            item.setItemName(jo4.getString("ItemName"));
//                                            item.setItemNameAr(jo4.getString("ItemName_Ar"));
//                                            item.setDescription(jo4.getString("ItemDesc"));
//                                            item.setDescriptionAr(jo4.getString("ItemDesc_Ar"));
//                                            item.setImage(jo4.getString("ItemImage"));
//                                            item.setItemtypeIds(jo4.getString("ItemTypeIds"));
//                                            item.setItemseq(jo4.getString("ItemSequence"));
//                                            item.setBuildingType(jo4.getString("BindingType"));
//                                            item.setIsdeliver(jo4.getString("IsDeliver"));
//                                            item.setNo_of_items(jo4.getString("NoOfItems"));
//                                            item.setDisplayname(jo4.getString("DisplayName"));
//                                            item.setDisplayname_ar(jo4.getString("DisplayName_Ar"));
//
//                                            JSONArray ja4 = jo4.getJSONArray("Price");
//
//                                            for (int n = 0; n < ja4.length(); n++) {
//                                                Price price = new Price();
//
//                                                JSONObject jo5 = ja4.getJSONObject(n);
//                                                price.setPrice(jo5.getString("price"));
//                                                price.setSizeid(jo5.getString("sizeId"));
//                                                price.setSize(jo5.getString("size"));
//                                                price.setSizeName_Ar(jo5.getString("SizeName_Ar"));
//                                                price.setItemsizedesc(jo5.getString("ItemSizeDescription"));
//                                                price.setItemSizeDescription_Ar(jo5.getString("ItemSizeDescription_Ar"));
//                                                price.setPriceId(jo5.getString("priceId"));
//
//                                                Log.i("TAG", "desc " + jo5.getString("ItemSizeDescription"));
//
//                                                priceList.add(price);
//                                            }
//
//                                            JSONArray ja5 = jo4.getJSONArray("SubItems");
//
//                                            try {
//                                                for (int o = 0; o < ja5.length(); o++) {
//                                                    SubItems subItems = new SubItems();
//
//                                                    JSONObject jo6 = ja5.getJSONObject(o);
//                                                    subItems.setItemid(jo6.getString("ItemId"));
//                                                    subItems.setSubitemid(jo6.getString("SubItemId"));
//                                                    subItems.setSubitemName(jo6.getString("SubItemName"));
//                                                    subItems.setSubItemName_Ar(jo6.getString("SubItemName_Ar"));
//                                                    subItems.setSubItemDesc(jo6.getString("SubItemDesc"));
//                                                    subItems.setSubItemDesc_Ar(jo6.getString("SubItemDesc_Ar"));
//                                                    subItems.setSubItemimage(jo6.getString("SubItemImage"));
//                                                    subItems.setSubItemseq(jo6.getString("SubItemSequence"));
//                                                    subItems.setSubItemIsdeliver(jo6.getString("SubItemIsDeliver"));
//
//                                                    subitemsList.add(subItems);
//
//                                                    Log.i("TAG", "subitem " + subitemsList.size());
//
//                                                }
//                                            } catch (JSONException e) {
//                                                e.printStackTrace();
//                                            }
//
//                                            item.setSubItems(subitemsList);
//                                            item.setPriceList(priceList);
//                                            itemlist.add(item);
//                                        }
//                                        section.setChildItems(itemlist);
//                                        sectionList.add(section);
//                                    }
//                                    subcat.setSections(sectionList);
//                                    subcatList.add(subcat);
//
//                                }
//                                maincat.setSubCategories(subcatList);
//                                mainitem.add(maincat);
//                                Log.e("TAG", "maincat " + mainitem.size());
//                            }
//
//                            Log.i("TAG", "position " + position);
//                            Log.i("TAG", "maincat " + mainitem);
//                            mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, mainitem, position, language);
//                            cat_list.setAdapter(mSubMenuAdapter);
//                            mSubMenuAdapter.notifyDataSetChanged();
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            } else {
//                Toast.makeText(CategoriesListActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }
////            mAdapter.notifyDataSetChanged();
//
////            if(subCatList.size()>0) {
////                mSubMenuAdapter = new SubMenuAdapter(CategoriesListActivity.this, subCatList, language);
////                mGridView.setAdapter(mSubMenuAdapter);
////                mSubMenuAdapter.notifyDataSetChanged();
////            }
////            int count =  mAdapter.getGroupCount();
////            for (int i = 0; i <count ; i++) {
////                mExpListView.collapseGroup(i);
////            }
//            super.onPostExecute(result);
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
//        mAdapter.notifyDataSetChanged();
        if (CategoriesListActivity.this != null) {
            mSubMenuAdapter.notifyDataSetChanged();
        }

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        menu_img.setVisibility(View.VISIBLE);
        transparent_layout.setVisibility(View.GONE);

//        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//
//        mcount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (myDbHelper.getTotalOrderQty() == 0) {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CategoriesListActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("There is no items in your order? To proceed checkout please add the items")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
////                         set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CategoriesListActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
//                                .setCancelable(false)
//                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        Intent a = new Intent(CategoriesListActivity.this, CheckoutActivity.class);
//                                        startActivity(a);
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        myDbHelper.deleteOrderTable();
////                                        CheckoutActivity.orderQuantity.setText("0");
////                                        CheckoutActivity.orderPrice.setText("0SR");
////                                        CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("-");
//                                        CategoriesListActivity.orderPrice.setText("-");
//                                        CategoriesListActivity.mcount_basket.setText("0");
////                                        OrderFragment.orderQuantity.setText("0");
////                                        OrderFragment.orderPrice.setText("0.00SR");
////                                        OrderFragment.mcount_basket.setText("0");
//                                        Intent a = new Intent(CategoriesListActivity.this, OrderFragment.class);
//                                        startActivity(a);
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
////                         set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
//                                .setCancelable(false)
//                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        myDbHelper.deleteOrderTable();
////                                        CheckoutActivity.orderQuantity.setText("0");
////                                        CheckoutActivity.orderPrice.setText("0SR");
////                                        CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("-");
//                                        CategoriesListActivity.orderPrice.setText("-");
//                                        CategoriesListActivity.mcount_basket.setText("0");
////                                        OrderFragment.orderQuantity.setText("0");
////                                        OrderFragment.orderPrice.setText("0.00SR");
////                                        OrderFragment.mcount_basket.setText("0");
//                                        dialog.dismiss();
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        Intent a = new Intent(CategoriesListActivity.this, CheckoutActivity.class);
//                                        startActivity(a);
//                                    }
//                                });
//                    }
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }
//
//            }
//        });
//
//        double number;
//        number = myDbHelper.getTotalOrderPrice();
//        DecimalFormat decim = new DecimalFormat("0.00");
//        if (myDbHelper.getTotalOrderQty()==0){
//            orderPrice.setText("-");
//            orderQuantity.setText("-");
//        }else {
//            orderPrice.setText("" + decim.format(number));
//            orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//        }
//        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

    }
}
