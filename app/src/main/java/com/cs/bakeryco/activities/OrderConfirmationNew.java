package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.Rest.APIInterface;
import com.cs.bakeryco.Rest.ApiClient;
import com.cs.bakeryco.adapters.MyOrderAdapter;
import com.cs.bakeryco.model.InsertOrderList;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.StoreInfo;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.bakeryco.Constants.ORDER_TYPE;
import static com.cs.bakeryco.Constants.decimalFormat;

public class OrderConfirmationNew extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    SharedPreferences languagePrefs;
    String language;
    DataBaseHelper myDbHelper;

    ArrayList<Order> orderList = new ArrayList<>();
//    public static TextView total_price, check_out;

    MyOrderAdapter mAdapter;
    Double lat, longi;

    RelativeLayout back_btn;
    RelativeLayout item_info_layout, address_layout, exp_date_layout, exp_time_layout, price_details_info_layout, price_details_list;
    ImageView item_info_btn, price_details_btn;
    RecyclerView myorder;
    CheckBox delivery, pickup;
    TextView pick_up_address, address, exp_date, exp_time, apply_coupon, confirm_order_btn, bag_count;
    public static TextView item_total, service_charges, mvat, subtotal, coupon_discount, total_price1, total_price2;
    EditText select_coupon, additional_remark;

    String addressTxt = "";
    private String timeResponse = null;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    String response;
    String serverTime;
    String starttime, endtime, storeName, storeAddress, imagesURL, is24x7, storename_ar, storeaddress_ar, deliverystarttime, deliveryendtime, perparationtime, businessday;
    Double latitude, longitudes;
    float delivery_distance;

    boolean deliveryboolean = false;

    SharedPreferences userPrefs;
    String mLoginStatus;
    String userId, storeId, ItemType = "0";
    String currentTime;
    String expectedtime, sexp_date, sexp_time;
    int addressId = 0;

    float vat = 5;
    //    RelativeLayout recieptLayout;
    float tax;

    private int mYear, mMonth, mDay, mHour, mMinute;
    boolean isToday;
    String selectedDate, selectedTime, time1;
    String format;
    Date cal_date1;
    RelativeLayout main_layout, empty_layout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.checkout_layout);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.checkout_layout_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        mLoginStatus = userPrefs.getString("login_status", "");


        storeId = "1";


        myDbHelper = new DataBaseHelper(OrderConfirmationNew.this);
        orderList = myDbHelper.getOrderInfo();

        back_btn = findViewById(R.id.back_btn);
        item_info_layout = findViewById(R.id.item_info_layout);
        address_layout = findViewById(R.id.address_layout1);
        exp_date_layout = findViewById(R.id.exp_date_layout);
        exp_time_layout = findViewById(R.id.exp_date_layout);
        price_details_info_layout = findViewById(R.id.price_details_info_layout);
        price_details_list = findViewById(R.id.price_details_list);

        item_info_btn = findViewById(R.id.item_info_btn);
        price_details_btn = findViewById(R.id.price_details_btn);

        delivery = findViewById(R.id.delivery);
        pickup = findViewById(R.id.pickup);

        pick_up_address = findViewById(R.id.pick_up_address);
        address = findViewById(R.id.address);
        exp_date = findViewById(R.id.exp_date);
        exp_time = findViewById(R.id.exp_time);
        apply_coupon = findViewById(R.id.apply_coupon);

        item_total = findViewById(R.id.item_total);
        service_charges = findViewById(R.id.service_charge);
        mvat = findViewById(R.id.vat);
        subtotal = findViewById(R.id.sub_total);
        coupon_discount = findViewById(R.id.coupon_discount);
        total_price1 = findViewById(R.id.total_price1);
        total_price2 = findViewById(R.id.total_price2);
        confirm_order_btn = findViewById(R.id.confirm_order_btn);
        bag_count = findViewById(R.id.bag_count);

        select_coupon = findViewById(R.id.select_coupon);
        additional_remark = findViewById(R.id.additional_remarks);

        myorder = findViewById(R.id.items_list);

        main_layout = findViewById(R.id.main_layout);
        empty_layout = findViewById(R.id.empty_card);

        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);

//        total_price = findViewById(R.id.total_price);
//        check_out = findViewById(R.id.check_out);

        if (myDbHelper.getTotalOrderQty() != 0) {

            main_layout.setVisibility(View.VISIBLE);
            empty_layout.setVisibility(View.GONE);

            new GetCurrentTime().execute();

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        } else {

            main_layout.setVisibility(View.GONE);
            empty_layout.setVisibility(View.VISIBLE);

            bag_count.setVisibility(View.GONE);

        }

        if (Constants.coupon) {

            select_coupon.setText("" + Constants.coupon_name);
            apply_coupon.setText("Remove");

        }

        apply_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    if (apply_coupon.getText().toString().equalsIgnoreCase("Coupon Apply")) {

                        Intent intent = new Intent(OrderConfirmationNew.this, CouponActivity.class);
                        startActivity(intent);

                        apply_coupon.setText("Remove");
                        select_coupon.setText("" + Constants.coupon_name);

                    } else {

                        Constants.coupon_name = "";
                        Constants.coupon = false;
                        if (language.equalsIgnoreCase("En")) {
                            apply_coupon.setText("Coupon Apply");
                        } else {
                            apply_coupon.setText("طلب كوبون");
                        }
                        select_coupon.setText("" + Constants.coupon_name);

                    }
                } else {

                    Intent intent = new Intent(OrderConfirmationNew.this, LoginActivity.class);
                    startActivity(intent);

                }

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        pickup.setChecked(true);
        ORDER_TYPE = "Pickup";

        pickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deliveryboolean = false;
                pickup.setChecked(true);
                delivery.setChecked(false);
                ORDER_TYPE = "Pickup";

                if (language.equalsIgnoreCase("En")) {

                    pick_up_address.setText("Pick Up Address");

                } else {

                    pick_up_address.setText("عنوان الاستلام");

                }

                address.setText("New Industrial Area, Riyadh, Saudi Arabia");

            }
        });

        if (Constants.address_id != 0) {

            addressId = Constants.address_id;

        } else {

            addressId = 0;

        }

        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deliveryboolean = true;
                delivery.setChecked(true);
                pickup.setChecked(false);
                ORDER_TYPE = "Delivery";

                pick_up_address.setText("Delivery Address");


                addressTxt = Constants.address;
                address.setText("" + addressTxt);

                if (Constants.address_id != 0) {

                    addressId = Constants.address_id;

                } else {

                    addressId = 0;

                }

            }
        });

        address_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {

                    if (deliveryboolean) {

                        Constants.store_lat = latitude;
                        Constants.store_longi = longitudes;
                        Constants.delivery_dist = delivery_distance;


                        Intent a = new Intent(OrderConfirmationNew.this, MyAddressActivity.class);
                        a.putExtra("confirm_order", true);
                        startActivity(a);

                    }
                } else {

                    Intent intent = new Intent(OrderConfirmationNew.this, LoginActivity.class);
                    startActivity(intent);

                }

            }
        });

        item_info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (myorder.getVisibility() == View.VISIBLE) {
                    bag_count.setVisibility(View.VISIBLE);
                    myorder.setVisibility(View.GONE);
                    item_info_btn.setImageDrawable(getResources().getDrawable(R.drawable.item_speacial_arrow));
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        bag_count.setVisibility(View.GONE);
                        myorder.setVisibility(View.VISIBLE);
                        item_info_btn.setImageDrawable(getResources().getDrawable(R.drawable.home_arrow));
                    } else {
                        bag_count.setVisibility(View.GONE);
                        myorder.setVisibility(View.VISIBLE);
                        item_info_btn.setImageDrawable(getResources().getDrawable(R.drawable.btn_arrow));
                    }
                }

            }
        });

        price_details_info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (price_details_list.getVisibility() == View.VISIBLE) {
                    price_details_list.setVisibility(View.GONE);
                    price_details_btn.setImageDrawable(getResources().getDrawable(R.drawable.item_speacial_arrow));
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        price_details_list.setVisibility(View.VISIBLE);
                        price_details_btn.setImageDrawable(getResources().getDrawable(R.drawable.home_arrow));
                    } else {
                        price_details_list.setVisibility(View.VISIBLE);
                        price_details_btn.setImageDrawable(getResources().getDrawable(R.drawable.btn_arrow));
                    }
                }

            }
        });

//        if (datetime.before(store_start_date_time)){
//
//
//            calendar.setTime(datetime);
//            calendar.add(Calendar.MINUTE, perp_time);
//
//            Hour = hours.format(calendar.getTime());
//            min = mins.format(calendar.getTime());
//            sec = secs.format(calendar.getTime());
//
//            I_hours = Integer.parseInt(Hour);
//            I_mins = Integer.parseInt(min);
//            I_secs = Integer.parseInt(sec);
//
//            Calendar calendar1 = Calendar.getInstance();
//            calendar1.setTime(datetime);
////                    calendar1.add(Calendar.DAY_OF_MONTH, 1);
//            selectedDate = datestart.format(calendar1.getTime());
//            selectedTime = dateFormat1.format(store_start_date_time);
//            exp_date.setText(selectedDate);
//            exp_time.setText("" + selectedTime);
//
//        } else if (datetime.after(store_start_date_time)){
//
//
//            calendar.setTime(datetime);
//            calendar.add(Calendar.MINUTE, perp_time);
//
//            Hour = hours.format(calendar.getTime());
//            min = mins.format(calendar.getTime());
//            sec = secs.format(calendar.getTime());
//
//            I_hours = Integer.parseInt(Hour);
//            I_mins = Integer.parseInt(min);
//            I_secs = Integer.parseInt(sec);
//
//            Calendar calendar1 = Calendar.getInstance();
//            calendar1.setTime(datetime);
////                    calendar1.add(Calendar.DAY_OF_MONTH, 1);
//            selectedDate = datestart.format(calendar1.getTime());
//            selectedTime = dateFormat1.format(store_start_date_time);
//            exp_date.setText(selectedDate);
//            exp_time.setText("" + selectedTime);
//        }

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        currentTime = df3.format(c.getTime());

        Log.i("TAG", "onCreate: " + currentTime);

        exp_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker();

            }
        });

        exp_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                final int hour = c.get(Calendar.HOUR_OF_DAY);
                final int minute = c.get(Calendar.MINUTE);

                com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance
                        (new TimePickerDialog.OnTimeSetListener() {
                             @Override
                             public void onTimeSet(TimePickerDialog view, int selectedHour, int selectedMinute, int second) {

                                 if (selectedHour == 0) {
//
                                     selectedHour += 12;

                                     format = "AM";
                                 } else if (selectedHour == 12) {

                                     format = "PM";

                                 } else if (selectedHour > 12) {

                                     selectedHour -= 12;

                                     format = "PM";

                                 } else {

                                     format = "AM";
                                 }


//                                 if (mMonth < 9) {
//                                     selectedDate = mDay + "-0" + (mMonth + 1) + "-" + mYear;
//                                 } else {
//                                     selectedDate = mDay + "-" + (mMonth + 1) + "-" + mYear;
//                                 }
                                 selectedTime = String.format("%02d:%02d", selectedHour, selectedMinute) + " " + format;
                                 time1 = String.format("%02d:%02d:%02d", selectedHour, selectedMinute, second) + " " + format;

                                 SimpleDateFormat df6 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                 SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);


                                 exp_time.setText(selectedTime);
                             }
                         },
                                hour,
                                minute,
                                false
                        );
//                String[] perp = perparationtime.split(":");
//                String perp_hours, perp_mins;
//                perp_hours = perp[0];
//                perp_mins = perp[1];
//
//                int perp_time;
//                perp_time = (Integer.parseInt(perp_hours) * 60) + Integer.parseInt(perp_mins);
//
//                Date datetime = null, store_end_date_time = null, store_start_date_time = null;
//                String time;
//                String Hour, min, sec;
//                int I_hours, I_mins, I_secs;
//                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//                SimpleDateFormat storeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
//                SimpleDateFormat datestart = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//                SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);
//                SimpleDateFormat hours = new SimpleDateFormat("hh", Locale.US);
//                SimpleDateFormat mins = new SimpleDateFormat("mm", Locale.US);
//                SimpleDateFormat secs = new SimpleDateFormat("ss", Locale.US);
//
//                try {
//                    datetime = dateFormat.parse(serverTime);
//                    store_start_date_time = storeFormat.parse(starttime);
//                    store_end_date_time = storeFormat.parse(endtime);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//
//
//                time = dateFormat1.format(datetime);
//
//                Calendar calendar = Calendar.getInstance();
//
//
//                calendar.setTime(datetime);
//                calendar.add(Calendar.MINUTE, perp_time);
//
//                Hour = hours.format(calendar.getTime());
//                min = mins.format(calendar.getTime());
//                sec = secs.format(calendar.getTime());
//
//                I_hours = Integer.parseInt(Hour);
//                I_mins = Integer.parseInt(min);
//                I_secs = Integer.parseInt(sec);
//
//
//                if (datetime.before(store_start_date_time)){
//
//
//                    calendar.setTime(datetime);
//                    calendar.add(Calendar.MINUTE, perp_time);
//
//                    Hour = hours.format(calendar.getTime());
//                    min = mins.format(calendar.getTime());
//                    sec = secs.format(calendar.getTime());
//
//                    I_hours = Integer.parseInt(Hour);
//                    I_mins = Integer.parseInt(min);
//                    I_secs = Integer.parseInt(sec);
//
//                    Calendar calendar1 = Calendar.getInstance();
//                    calendar1.setTime(datetime);
////                    calendar1.add(Calendar.DAY_OF_MONTH, 1);
//                    selectedDate = datestart.format(calendar1.getTime());
//                    selectedTime = dateFormat1.format(store_start_date_time);
//                    exp_date.setText(selectedDate);
//                    exp_time.setText("" + selectedTime);
//
//                } else if (datetime.after(store_start_date_time)){
//
//
//                    calendar.setTime(datetime);
//                    calendar.add(Calendar.MINUTE, perp_time);
//
//                    Hour = hours.format(calendar.getTime());
//                    min = mins.format(calendar.getTime());
//                    sec = secs.format(calendar.getTime());
//
//                    I_hours = Integer.parseInt(Hour);
//                    I_mins = Integer.parseInt(min);
//                    I_secs = Integer.parseInt(sec);
//
//                    Calendar calendar1 = Calendar.getInstance();
//                    calendar1.setTime(datetime);
////                    calendar1.add(Calendar.DAY_OF_MONTH, 1);
//                    selectedDate = datestart.format(calendar1.getTime());
//                    selectedTime = dateFormat1.format(store_start_date_time);
//                    exp_date.setText(selectedDate);
//                    exp_time.setText("" + selectedTime);
//                }

                Date final_time = null, time1 = null, endtime2 = null, store_end_date_time = null, store_start_date_time = null;

                String Hour, min, sec, mhours, mmins, msecs;
                int I_hours, I_mins, I_secs, m_hours, m_mins, m_secs;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                SimpleDateFormat storeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                SimpleDateFormat datestart = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm", Locale.US);
                SimpleDateFormat hours = new SimpleDateFormat("HH", Locale.US);
                SimpleDateFormat mins = new SimpleDateFormat("mm", Locale.US);
                SimpleDateFormat secs = new SimpleDateFormat("ss", Locale.US);

                String time;

                try {
                    final_time = dateFormat1.parse(selectedTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                time = dateFormat2.format(final_time);

                try {
                    time1 = dateFormat2.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "onClick: " + time);

                Calendar calendar = Calendar.getInstance();


                calendar.setTime(time1);

                Hour = hours.format(calendar.getTime());
                min = mins.format(calendar.getTime());
                sec = secs.format(calendar.getTime());

                I_hours = Integer.parseInt(Hour);
                I_mins = Integer.parseInt(min);
                I_secs = Integer.parseInt(sec);

                Log.i("TAG", "minTime: " + I_hours + " " + I_mins + " " + I_secs);

//              Max days

                try {
                    store_end_date_time = storeFormat.parse(endtime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String endtime;

                endtime = dateFormat2.format(store_end_date_time);

                Log.i("TAG", "endtime: " + endtime);

                try {
                    endtime2 = dateFormat2.parse(endtime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar calendar1 = Calendar.getInstance();


                calendar1.setTime(endtime2);

                mhours = hours.format(calendar1.getTime());
                mmins = mins.format(calendar1.getTime());
                msecs = secs.format(calendar1.getTime());

                m_hours = Integer.parseInt(mhours);
                m_mins = Integer.parseInt(mmins);
                m_secs = Integer.parseInt(msecs);

                Log.i("TAG", "maxTime: " + m_hours + " " + m_mins + " " + m_secs);

                tpd.setMinTime(I_hours, I_mins, I_secs);
                tpd.setMaxTime(23, 99, m_secs);

//                tpd.

                tpd.setThemeDark(true);
                tpd.vibrate(false);
                tpd.setAccentColor(Color.parseColor("#76C8FC"));
                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Log.d("TimePicker", "Dialog was cancelled");
                    }
                });
//                if (isToday) {
//                    if (minute > 30) {
//                        tpd.setMinTime(hour + 1, minute - 30, 00);
//                    } else {
//                        tpd.setMinTime(hour, (minute + 30), 00);
//                        Log.i("TAG", "onDateSet: " + minute);
//                        Log.i("TAG", "onDateSet1: " + (minute + 30));
//                    }
//                } else {
//                    tpd.setMinTime(0, 0, 0);
//                }
                tpd.show(OrderConfirmationNew.this.getFragmentManager(), "Timepickerdialog");

            }
        });

        item_total.setText("SR " + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
        service_charges.setText("+ 0");
        tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
        mvat.setText("+ " + Constants.decimalFormat1.format(tax));
        subtotal.setText("SR " + Constants.decimalFormat1.format(tax + myDbHelper.getTotalOrderPrice()));
        coupon_discount.setText("- 0.00");
        total_price1.setText("" + Constants.decimalFormat1.format(tax + myDbHelper.getTotalOrderPrice()));
        total_price2.setText("" + Constants.decimalFormat1.format(tax + myDbHelper.getTotalOrderPrice()));

        LinearLayoutManager layoutManager = new LinearLayoutManager(OrderConfirmationNew.this);
        myorder.setLayoutManager(layoutManager);

        mAdapter = new MyOrderAdapter(OrderConfirmationNew.this, orderList, language);
        myorder.setAdapter(mAdapter);


        confirm_order_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ORDER_TYPE.equalsIgnoreCase("Delivery") && addressId == 0) {

                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog("Please Select Address", OrderConfirmationNew.this);
                    } else {
                        Constants.showOneButtonAlertDialog("Please Select Address", OrderConfirmationNew.this);
                    }

                } else {

                    new InsertOrderApi().execute();
                }

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if (Constants.address_id != 0) {

            addressId = Constants.address_id;

            addressTxt = Constants.address;
            address.setText("" + addressTxt);

        } else {

            addressId = 0;

        }

        if (Constants.coupon) {

            select_coupon.setText("" + Constants.coupon_name);
            apply_coupon.setText("Remove");

        } else {

            Constants.coupon_name = "";
            Constants.coupon = false;
            if (language.equalsIgnoreCase("En")) {
                apply_coupon.setText("Coupon Apply");
            } else {
                apply_coupon.setText("طلب كوبون");
            }
            select_coupon.setText("" + Constants.coupon_name);

        }

    }

    public void DatePicker() {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(OrderConfirmationNew.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        if (view.isShown()) {

                            mYear = year;
                            mDay = dayOfMonth;
                            mMonth = monthOfYear;


                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                            SimpleDateFormat df1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

                            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                            Date date1 = null;

                            try {
                                date1 = df.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            selectedDate = df.format(date1);
//
                            Log.i("TAG", "onDateSet start: " + selectedDate);
//
//                                    EndDatePicker();
                            String exp_date1, exp_time1;

                            exp_date1 = df1.format(date1);
                            exp_date.setText(exp_date1);
                        }


                    }
                }, mYear, mMonth, mDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//        String date;
//        c.add(Calendar.MONTH, 3);
//        date = dateFormat.format(c.getTimeInMillis());
//        Log.i("TAG", "StartDatePicker: " + date);


        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//        int max_day = 0;
        int max_days = 91;
        long max = TimeUnit.DAYS.toMillis(Long.parseLong(String.valueOf(max_days)));
//        Log.i("TAG", "maxdays+1: " + max_days);
        Calendar calendar = Calendar.getInstance();

        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + max);
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

    }

    private class InsertOrderApi extends AsyncTask<String, String, String> {

        ProgressDialog dialog;
        String networkStatus;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmationNew.this);
            dialog = ProgressDialog.show(OrderConfirmationNew.this, "",
                    "Loading...");
//            Constants.showLoadingDialog(OrderConfirmationNew.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmationNew.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertOrderList> call = apiService.getinsertorder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertOrderList>() {
                @Override
                public void onResponse(Call<InsertOrderList> call, Response<InsertOrderList> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        InsertOrderList orderItems = response.body();
                        try {
                            if (!orderItems.getSuccess().equalsIgnoreCase("")) {

                                String[] order_ids = orderItems.getSuccess().split(",");
                                String orderId = order_ids[0];
                                mLoginStatus = userPrefs.getString("login_status", "");
                                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                                    myDbHelper.deleteOrderTable();
                                    Intent a = new Intent(OrderConfirmationNew.this, TrackOrderActivity.class);
                                    a.putExtra("orderId", orderId);
                                    a.putExtra("confirm", true);
                                    startActivity(a);
                                } else {
                                    Intent intent = new Intent(OrderConfirmationNew.this, LoginActivity.class);
                                    startActivity(intent);
                                }

                                Log.i("TAG", "onResponse: " + orderId);


                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getFailure();
                                    Constants.showOneButtonAlertDialog(failureResponse, OrderConfirmationNew.this);
                                } else {
                                    String failureResponse = orderItems.getFailure();
                                    Constants.showOneButtonAlertDialog(failureResponse, OrderConfirmationNew.this);
                                }
//                            }
//                            Log.i("TAG", "onResponse: " + data.size());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(OrderConfirmationNew.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OrderConfirmationNew.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderConfirmationNew.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderConfirmationNew.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onResponse: ");
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<InsertOrderList> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderConfirmationNew.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderConfirmationNew.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderConfirmationNew.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderConfirmationNew.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            return null;
        }


        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                JSONArray mainItem = new JSONArray();
                JSONArray subItem = new JSONArray();
                JSONArray promoItem = new JSONArray();

                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                String version = pInfo.versionName;

                Calendar c = Calendar.getInstance();
                System.out.println("Current time => " + c.getTime());

                SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                SimpleDateFormat df4 = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                SimpleDateFormat df5 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

                currentTime = df3.format(c.getTime());

                sexp_date = exp_date.getText().toString();
                sexp_time = exp_time.getText().toString();

                Date expect_date = null;

                try {
                    expect_date = df4.parse(sexp_date + " " + sexp_time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                expectedtime = df5.format(expect_date);


                JSONObject mainObj = new JSONObject();
                mainObj.put("UserId", userId);
                mainObj.put("StoreId", storeId);
                mainObj.put("OrderType", ORDER_TYPE);
                mainObj.put("ExpectedTime", expectedtime);
                mainObj.put("PreparationTime", 0);
                mainObj.put("TravelTime", "");
                mainObj.put("KichenStartTime", "");
                mainObj.put("OrderDate", currentTime);
                mainObj.put("IsFavorite", false);
                mainObj.put("FavoriteName", "");
                mainObj.put("Comments", "Android v" + version);
                mainObj.put("PaymentMode", "2");
                mainObj.put("Total_Price", Constants.convertToArabic(total_price2.getText().toString()));
                mainObj.put("OrderStatus", "New");
                mainObj.put("Device_token", SplashActivity.regid);
                mainObj.put("VatPercentage", "5");
                mainObj.put("SubTotal", Constants.convertToArabic(String.valueOf(myDbHelper.getTotalOrderPrice())));
                float vat = Float.parseFloat(mvat.getText().toString().replace("+ ", ""));
                vat = Float.parseFloat(Constants.decimalFormat1.format(vat));
                mainObj.put("VatCharges", Constants.convertToArabic(String.valueOf(vat)));

                if (ORDER_TYPE.equalsIgnoreCase("Delivery")) {
                    mainObj.put("AddressID", addressId);
                }
                mainItem.put(mainObj);

//                JSONObject promoObj = new JSONObject();
//                promoObj.put("DeviceToken", SplashActivity.regid);
//                promoObj.put("promotionCode", promocodeStr);
//                promoObj.put("BonusAmt", remainingBonusInt);
//                promoItem.put(promoObj);

                for (Order order : orderList) {
                    JSONObject subObj = new JSONObject();

//                    JSONArray subItem1 = new JSONArray();
//                    JSONArray subItem2 = new JSONArray();
                    if (order.getItemType().equals("Whole")) {
                        ItemType = String.valueOf(2);
                    } else if (order.getItemType().equals("Sliced")) {
                        ItemType = String.valueOf(1);
                    }
                    subObj.put("ItemPrice", Constants.convertToArabic(decimalFormat.format(Float.parseFloat(order.getPrice()))));
                    subObj.put("Qty", order.getQty());
                    subObj.put("Comments", order.getComment());
                    subObj.put("ItemId", order.getItemId());
                    subObj.put("Size", order.getSizename());
                    subObj.put("ItemType", ItemType);

                    JSONArray subsubItems = new JSONArray();

                    if (order.getSub_itemId() != null) {
                        String ids = order.getSub_itemId();
                        String qty = order.getSub_itemcount();
                        List<String> idsList = Arrays.asList(ids.split(","));
                        List<String> qtyList = Arrays.asList(qty.split(","));
                        for (int i = 0; i < idsList.size(); i++) {
                            if (!idsList.get(i).equals("")) {
                                JSONObject subsubObj = new JSONObject();
                                subsubObj.put("OrderSubItemId", idsList.get(i));
                                subsubObj.put("Qty", qtyList.get(i));
                                subsubItems.put(subsubObj);
                            }
                        }
                    }

                    subObj.put("SubSubItems", subsubItems);
                    subItem.put(subObj);
                }

                parentObj.put("MainItem", mainItem);
                parentObj.put("SubItem", subItem);
//                if (remainingBonusInt != 0) {
//                    parentObj.put("Promotion", promoItem);
//                }
                Log.e("TAG", parentObj.toString());

            } catch (Exception je) {
                je.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmationNew.this);
//            dialog = ProgressDialog.show(OrderConfirmationNew.this, "",
//                    "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            } else {
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (serverTime == null) {
//                dialog.dismiss();
            } else if (serverTime.equals("no internet")) {
//                dialog.dismiss();
                Toast.makeText(OrderConfirmationNew.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
//                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "29/05/2018 05:00 PM";
                } catch (JSONException je) {
                    je.printStackTrace();
                }
                new GetStoresInfo().execute(Constants.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }


            super.onPostExecute(result1);
        }
    }

    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String dayOfWeek;

        @Override
        protected void onPreExecute() {
            storesList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderConfirmationNew.this);
            dialog = ProgressDialog.show(OrderConfirmationNew.this, "",
                    "Fetching near by store...");
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0] + dayOfWeek);
                Log.i("TAG", "user response1:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderConfirmationNew.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderConfirmationNew.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (result.equals("")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderConfirmationNew.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderConfirmationNew.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            JSONArray ja = mainObj.getJSONArray("success");
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 46.68097073;

                                storeId = (jo.getString("storeId"));
                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
                                starttime = (jo.getString("ST"));
                                endtime = (jo.getString("ET"));
                                storeName = (jo.getString("StoreName"));
                                storeAddress = (jo.getString("StoreAddress"));
                                latitude = (jo.getDouble("Latitude"));
                                longitudes = (jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                imagesURL = (jo.getString("imageURL"));
                                si.setDeliverydistance(jo.getInt("DeliveryDistance"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setDeliveryStart(jo.getString("DeliveryStart"));
                                si.setDeliveryEnd(jo.getString("DeliveryEnd"));
                                si.setPreparationHours(jo.getString("PreparationHours"));
                                si.setBusinessday(jo.getString("businessday"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                si.setStoreName(storeName);
                                si.setStoreAddress(storeAddress);
                                si.setStoreId(storeId);
                                si.setStarttime(starttime);
                                si.setEndtime(endtime);
                                si.setLatitude(latitude);
                                si.setLongitude(longitudes);
                                si.setImageURL(imagesURL);
                                deliverystarttime = si.getDeliveryStart();
                                deliveryendtime = si.getDeliveryEnd();
                                perparationtime = si.getPreparationHours();
                                businessday = si.getBusinessday();

                                try {
                                    si.setAirport(jo.getString("Airport"));
                                } catch (Exception e) {
                                    si.setAirport("false");
                                }
                                try {
                                    si.setDineIn(jo.getString("DineIn"));
                                } catch (Exception e) {
                                    si.setDineIn("false");
                                }
                                try {
                                    si.setLadies(jo.getString("Ladies"));
                                } catch (Exception e) {
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setStoreNumber(jo.getString("phone"));
                                is24x7 = (jo.getString("is24x7"));
                                Log.e("TAG", "24X7 " + is24x7);
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                storename_ar = (jo.getString("StoreName_ar"));
                                storeaddress_ar = (jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                si.setStoreName_ar(storename_ar);
                                si.setIs24x7(is24x7);
                                si.setStoreAddress_ar(storeaddress_ar);
                                try {
                                    si.setMessage(jo.getString("Message"));
                                } catch (Exception e) {
                                    si.setMessage("");
                                }

                                try {
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                } catch (Exception e) {
                                    si.setMessage_ar("");
                                }

//                                Location me = new Location("");
//                                Location dest = new Location("");
//
//                                me.setLatitude(lat);
//                                me.setLongitude(longi);
//
//                                dest.setLatitude(jo.getDouble("Latitude"));
//                                dest.setLongitude(jo.getDouble("Longitude"));
//
//                                float dist = (me.distanceTo(dest)) / 1000;
//                                si.setDistance(dist);


                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                Calendar c = Calendar.getInstance();
                                System.out.println("Current time => " + c.getTime());
                                serverTime = timeResponse;
                                delivery_distance = si.getDeliverydistance();
                                String startTime = si.getStarttime();
                                String endTime = si.getEndtime();
//                                String startTime = "03:00AM";
//                                String endTime = "11:00PM";

//                                if (dist <= si.getDeliverydistance() && (jo.getBoolean("OnlineOrderStatus"))) {
////                                    if(dist <= 50000 && (jo.getBoolean("OnlineOrderStatus"))){
//
//                                    if (startTime.equals("null") && endTime.equals("null")) {
////                                        si.setOpenFlag(-1);
////                                        storesList.add(si);
//                                    } else {
//
//                                        if (endTime.equals("00:00AM")) {
//                                            si.setOpenFlag(1);
//                                            storesList.add(si);
//
//                                            continue;
//                                        } else if (endTime.equals("12:00AM")) {
//                                            endTime = "11:59PM";
//                                        }
//
//                                        Calendar now = Calendar.getInstance();
//
//                                        int hour = now.get(Calendar.HOUR_OF_DAY);
//                                        int minute = now.get(Calendar.MINUTE);
//
//
//                                        Date serverDate = null;
//                                        Date end24Date = null;
//                                        Date start24Date = null;
//                                        Date current24Date = null;
//                                        Date dateToday = null;
//                                        Calendar dateStoreClose = Calendar.getInstance();
//                                        try {
//                                            end24Date = dateFormat2.parse(endTime);
//                                            start24Date = dateFormat2.parse(startTime);
//                                            serverDate = dateFormat.parse(serverTime);
//                                            dateToday = dateFormat.parse(serverTime);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//                                        Date startDate = null;
//                                        Date endDate = null;
//                                        try {
//                                            dateStoreClose.setTime(dateToday);
//                                            dateStoreClose.add(Calendar.DATE, 1);
//                                            String current24 = timeFormat1.format(serverDate);
//                                            String end24 = timeFormat1.format(end24Date);
//                                            String start24 = timeFormat1.format(start24Date);
//                                            String startDateString = dateFormat1.format(dateToday);
//                                            String endDateString = dateFormat1.format(dateToday);
//                                            String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
//                                            dateStoreClose.add(Calendar.DATE, -2);
//                                            String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());
//
//                                            try {
//                                                end24Date = timeFormat1.parse(end24);
//                                                start24Date = timeFormat1.parse(start24);
//                                                current24Date = timeFormat1.parse(current24);
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
//
//                                            String[] parts2 = start24.split(":");
//                                            int startHour = Integer.parseInt(parts2[0]);
//                                            int startMinute = Integer.parseInt(parts2[1]);
//
//                                            String[] parts = end24.split(":");
//                                            int endHour = Integer.parseInt(parts[0]);
//                                            int endMinute = Integer.parseInt(parts[1]);
//
//                                            String[] parts1 = current24.split(":");
//                                            int currentHour = Integer.parseInt(parts1[0]);
//                                            int currentMinute = Integer.parseInt(parts1[1]);
//
//
////                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//
//
//                                            if (startTime.contains("AM") && endTime.contains("AM")) {
//                                                if (startHour < endHour) {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateString + "  " + endTime;
//                                                } else if (startHour > endHour) {
//                                                    if (serverTime.contains("AM")) {
//                                                        if (currentHour > endHour) {
//                                                            startDateString = startDateString + " " + startTime;
//                                                            endDateString = endDateTomorrow + "  " + endTime;
//                                                        } else {
//                                                            startDateString = endDateYesterday + " " + startTime;
//                                                            endDateString = endDateString + "  " + endTime;
//                                                        }
//                                                    } else {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                    }
//                                                }
//                                            } else if (startTime.contains("AM") && endTime.contains("PM")) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                            } else if (startTime.contains("PM") && endTime.contains("AM")) {
//                                                if (serverTime.contains("AM")) {
//                                                    if (currentHour <= endHour) {
//                                                        startDateString = endDateYesterday + " " + startTime;
//                                                        endDateString = endDateString + "  " + endTime;
//                                                    } else {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                    }
//                                                } else {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateTomorrow + "  " + endTime;
//                                                }
//
//                                            } else if (startTime.contains("PM") && endTime.contains("PM")) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                            }
//
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        try {
//                                            startDate = dateFormat2.parse(si.getStarttime());
//                                            endDate = dateFormat2.parse(si.getEndtime());
//                                            Log.i("TAG", "start date " + startDate);
//                                            Log.i("TAG", "end date " + endDate);
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        String serverDateString = null;
//                                        try {
//                                            serverDateString = dateFormat.format(serverDate);
//                                            Log.i("TAG", "server date" + serverDateString);
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        try {
//                                            serverDate = dateFormat.parse(serverDateString);
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        Log.i("TAG DATE", "" + startDate);
//                                        Log.i("TAG DATE1", "" + endDate);
//                                        Log.i("TAG DATE2", "" + serverDate);
//
////                                        if (serverDate.after(startDate) && serverDate.before(endDate)) {
//                                        Log.i("TAG Visible", "true");
//                                        si.setOpenFlag(1);
//                                        storesList.add(si);
//                                        carryout();
////                                        } else {
////                                            si.setOpenFlag(0);
////                                            storesList.add(si);
////
////
////                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmationNew.this, android.R.style.Theme_Material_Light_Dialog));
////
////                                            if (language.equalsIgnoreCase("En")) {
////                                                // set title
////                                                alertDialogBuilder.setTitle("BAKERY & Co.");
////
////                                                // set dialog message
////                                                alertDialogBuilder
////                                                        .setMessage("Sorry! We couldn't find any open stores in your location")
////                                                        .setCancelable(false)
////                                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
////                                                            public void onClick(DialogInterface dialog, int id) {
////                                                                dialog.dismiss();
////                                                            }
////                                                        });
////                                            } else if (language.equalsIgnoreCase("Ar")) {
////                                                // set title
////                                                alertDialogBuilder.setTitle("بيكري آند كومباني");
////
////                                                // set dialog message
////                                                alertDialogBuilder
////                                                        .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
////                                                        .setCancelable(false)
////                                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                            public void onClick(DialogInterface dialog, int id) {
////                                                                dialog.dismiss();
////                                                            }
////                                                        });
////                                            }
////
////                                            // create alert dialog
////                                            AlertDialog alertDialog = alertDialogBuilder.create();
////
////                                            // show it
////                                            alertDialog.show();
////                                        }
//                                    }
//                                } else {
//                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderConfirmationNew.this, android.R.style.Theme_Material_Light_Dialog));
//
//                                    if (language.equalsIgnoreCase("En")) {
//                                        // set title
//                                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                        // set dialog message
//                                        alertDialogBuilder
//                                                .setMessage("Sorry! We couldn't find any open stores in your location")
//                                                .setCancelable(false)
//                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                    public void onClick(DialogInterface dialog, int id) {
//                                                        dialog.dismiss();
//                                                    }
//                                                });
//                                    } else if (language.equalsIgnoreCase("Ar")) {
//                                        // set title
//                                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                                        // set dialog message
//                                        alertDialogBuilder
//                                                .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
//                                                .setCancelable(false)
//                                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                    public void onClick(DialogInterface dialog, int id) {
//                                                        dialog.dismiss();
//                                                    }
//                                                });
//                                    }
//
//                                    // create alert dialog
//                                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                    // show it
//                                    alertDialog.show();
//                                }
                                storesList.add(si);
                            }

                            String[] perp = perparationtime.split(":");
                            String perp_hours, perp_mins;
                            perp_hours = perp[0];
                            perp_mins = perp[1];

                            int perp_time;
                            perp_time = (Integer.parseInt(perp_hours) * 60) + Integer.parseInt(perp_mins);

                            Date datetime = null, store_end_date_time = null, store_start_date_time = null, store_start_date = null, store_end_date = null, server_date = null;
                            String time, mstore_start_date, mstore_end_date, mserver_date;
                            String Hour, min, sec;
                            int I_hours, I_mins, I_secs;
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                            SimpleDateFormat storeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                            SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                            SimpleDateFormat datestart = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);
                            SimpleDateFormat hours = new SimpleDateFormat("hh", Locale.US);
                            SimpleDateFormat mins = new SimpleDateFormat("mm", Locale.US);
                            SimpleDateFormat secs = new SimpleDateFormat("ss", Locale.US);
                            SimpleDateFormat df1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//                            SimpleDateFormat df2 = new SimpleDateFormat("hhMMM yyyy", Locale.US);


                            try {
                                datetime = dateFormat.parse(serverTime);
                                store_start_date_time = storeFormat.parse(starttime);
                                store_end_date_time = storeFormat.parse(endtime);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            mstore_start_date = dateFormat2.format(store_start_date_time);
                            mstore_end_date = dateFormat2.format(store_end_date_time);
                            mserver_date = dateFormat2.format(datetime);

                            try {
                                store_start_date = dateFormat2.parse(mstore_start_date);
                                store_end_date = dateFormat2.parse(mstore_end_date);
                                server_date = dateFormat2.parse(mserver_date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            time = dateFormat1.format(datetime);

                            Calendar calendar = Calendar.getInstance();


                            calendar.setTime(datetime);
                            calendar.add(Calendar.MINUTE, perp_time);

                            Hour = hours.format(calendar.getTime());
                            min = mins.format(calendar.getTime());
                            sec = secs.format(calendar.getTime());

                            I_hours = Integer.parseInt(Hour);
                            I_mins = Integer.parseInt(min);
                            I_secs = Integer.parseInt(sec);

                            String date = dateFormat.format(calendar.getTime());

                            try {
                                datetime = dateFormat.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if (datetime.before(store_start_date_time)) {

//                                if (server_date == store_start_date) {
                                Log.i("TAG", "onPostExecute: " + store_start_date);
                                selectedDate = datestart.format(store_start_date);
                                String exp_date1, exp_time1;

                                Log.i("TAG", "onPostExecute1: " + selectedDate);

                                exp_date1 = df1.format(store_start_date);
                                exp_date.setText("" + exp_date1);
//                                }

                            }

                            if (datetime.after(store_start_date_time) && datetime.before(store_end_date_time)) {

                                selectedDate = datestart.format(calendar.getTime());
                                selectedTime = dateFormat1.format(calendar.getTime());
                                String exp_date1, exp_time1;

                                exp_date1 = df1.format(calendar.getTime());

                                exp_date.setText("" + exp_date1);
                                exp_time.setText("" + selectedTime);

                            }

                            SimpleDateFormat df4 = new SimpleDateFormat("dd MMM yyyy hh:mm aa", Locale.US);
                            SimpleDateFormat df5 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

                            sexp_date = exp_date.getText().toString();
                            sexp_time = exp_time.getText().toString();

                            Log.i("TAG", "exp_date: " + sexp_date);
                            Log.i("TAG", "exp_time: " + sexp_time);

                            Date expect_date = null;

                            try {
                                expect_date = df4.parse(sexp_date + " " + sexp_time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            expectedtime = df5.format(expect_date);

                            Log.i("TAG", "expected_time: " + expectedtime);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);

                    }
                }

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(OrderConfirmationNew.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(OrderConfirmationNew.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                }
            }
            try {
                if ((this.dialog != null) && this.dialog.isShowing()) {
                    this.dialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                this.dialog = null;
            }
//            mAdapter.notifyDataSetChanged();

            super.onPostExecute(result);

        }

    }

    public void carryout() {

//        new GetCurrentTime().execute();

        Intent intent = new Intent(OrderConfirmationNew.this, OrderConfirmation.class);
        if (language.equalsIgnoreCase("En")) {
            intent.putExtra("storeName", storeName);
            intent.putExtra("storeAddress", storeAddress);
        } else if (language.equalsIgnoreCase("Ar")) {
            intent.putExtra("storeName_ar", storename_ar);
            intent.putExtra("storeAddress_ar", storeaddress_ar);
        }
        intent.putExtra("storeImage", imagesURL);
        intent.putExtra("storeId", storeId);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitudes);
        intent.putExtra("lat", lat);
        intent.putExtra("longi", longi);
        intent.putExtra("start_time", starttime);
        intent.putExtra("end_time", endtime);
        intent.putExtra("deliverystarttime", deliverystarttime);
        intent.putExtra("deliveryendtime", deliveryendtime);
        intent.putExtra("preparationtime", perparationtime);
        intent.putExtra("bussinessday", businessday);
        Log.e("TAG", "fullhours " + is24x7);
        intent.putExtra("full_hours", is24x7);
        Log.i("TAG", "order type " + Constants.ORDER_TYPE);
        intent.putExtra("order_type", Constants.ORDER_TYPE);
        startActivity(intent);
    }

}
