package com.cs.bakeryco.activities;

/**
 * Created by CS on 4/30/2017.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by CS on 3/28/2017.
 */

public class ResetPassword extends AppCompatActivity {
    private String response12 = null;
    ProgressDialog dialog;

    EditText newPassword, confirmPassword;
    Button submit;
    //    ImageView backBtn;
    String response, userId, phoneNumber;
    Toolbar toolbar;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.reset_password);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.reset_password_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        phoneNumber = getIntent().getExtras().getString("mobile");
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        submit = (Button) findViewById(R.id.change_password_button);
//        backBtn = (ImageView) findViewById(R.id.back_btn1);

//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                if (newPwd.length() == 0) {
                    newPassword.setError("Please enter new password");
                } else if (newPwd.length() < 8) {
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError("Password must be at least 8 characters");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        newPassword.setError("الرقم السري يجب أن يحتوي على الأقل على 8 حروف");
                    }
                } else if (confirmPwd.length() == 0) {
                    confirmPassword.setError("Please retype password");
                } else if (!newPwd.equals(confirmPwd)) {
                    confirmPassword.setError("Passwords not match, please retype");
                } else {
                    new GetForgetpassword().execute(Constants.FORGOT_PASSWORD_URL +phoneNumber+"&psw="+newPwd);

                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetForgetpassword extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(ResetPassword.this);
            dialog = ProgressDialog.show(ResetPassword.this, "",
                    "Sending E-mail...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ResetPassword.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(ResetPassword.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {

                            JSONObject jo = new JSONObject(result);
                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");
                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
//                                jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    userPrefEditor.putString("login_status", "loggedin");
                                    userPrefEditor.commit();

                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ResetPassword.this, android.R.style.Theme_Material_Light_Dialog));
//                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("BAKERY & Co.");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Reset password successful")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                    Intent loginIntent = new Intent(ResetPassword.this, LoginActivity.class);
//                                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(loginIntent);
                                                    finish();
                                                }
                                            });
//                                } else if (language.equalsIgnoreCase("Ar")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("اوريجانو");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage(" إعادة ضبط كلمة السر تمت بنجاح")
//                                            .setCancelable(false)
//                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                    Intent loginIntent = new Intent(ResetPassword.this, MainActivity.class);
//                                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                    startActivity(loginIntent);
//                                                    finish();
//                                                }
//                                            });
//                                }

                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je) {
                                je.printStackTrace();
                                dialog.dismiss();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ResetPassword.this, android.R.style.Theme_Material_Light_Dialog));
//                            if (language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("Bakery & Company");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("Reset password unsuccessful")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                // set title
//                                alertDialogBuilder.setTitle("اوريجانو");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage(" فشل إعادة ضبط كلمة السر ")
//                                        .setCancelable(false)
//                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                            }

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(ResetPassword.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}

//    public class ChangePasswordResponse extends AsyncTask<String, String, String> {
//        java.net.URL url = null;
//        String responce = null;
//        double lat, longi;
//        String networkStatus;
//        InputStream is = null;
//
//        @Override
//        protected void onPreExecute() {
//            networkStatus = NetworkUtil.getConnectivityStatusString(ResetPassword.this);
//            dialog = ProgressDialog.show(ResetPassword.this, "",
//                    "Please wait...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
////
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String result1) {
//            if (result1 != null) {
//                if (result1.equalsIgnoreCase("no internet")) {
//                    dialog.dismiss();
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ResetPassword.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    // set title
//                    alertDialogBuilder.setTitle("Broated Express");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("Communication Error! Please check the internet connection?")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.dismiss();
//                                }
//                            });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
//
//
//
//                    try {
//                        JSONObject jo= new JSONObject(result1);
//
//                        try{
//                            JSONArray ja = jo.getJSONArray("Success");
//                            JSONObject jo1 = ja.getJSONObject(0);
//                            String userId = jo1.getString("UserId");
//                            String email = jo1.getString("Email");
//                            String fullName = jo1.getString("FullName");
//                            String mobile = jo1.getString("Mobile");
//                            String language1 = jo1.getString("Language");
//                            boolean isVerified = jo1.getBoolean("IsVerified");
//                            String familyName = jo1.getString("FamilyName");
//                            String nickName = jo1.getString("NickName");
//                            String gender = jo1.getString("Gender");
//
//                            try {
//                                JSONObject parent = new JSONObject();
//                                JSONObject jsonObject = new JSONObject();
//                                JSONArray jsonArray = new JSONArray();
//                                jsonArray.put("lv1");
//                                jsonArray.put("lv2");
//
//                                jsonObject.put("userId", userId);
//                                jsonObject.put("fullName", fullName);
//                                jsonObject.put("mobile", mobile);
//                                jsonObject.put("email", email);
////                                jsonObject.put("language", language);
//                                jsonObject.put("family_name", familyName);
//                                jsonObject.put("nick_name", nickName);
//                                jsonObject.put("gender", gender);
//                                jsonObject.put("isVerified", isVerified);
//                                jsonObject.put("user_details", jsonArray);
//                                parent.put("profile", jsonObject);
//                                Log.d("output", parent.toString());
//                                userPrefEditor.putString("user_profile", parent.toString());
//                                userPrefEditor.putString("userId", userId);
//
////                                    userPrefEditor.putString("user_email", email);
////                                    userPrefEditor.putString("user_password", password);
//
//                                userPrefEditor.commit();
//
//                                userPrefEditor.putString("login_status", "loggedin");
//                                userPrefEditor.commit();
//
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ResetPassword.this, android.R.style.Theme_Material_Light_Dialog));
////                                if (language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Broasted Express");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("Reset Password Successful")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                                Intent loginIntent = new Intent(ResetPassword.this, Login.class);
////                                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                                startActivity(loginIntent);
//                                                finish();
//                                            }
//                                        });
////                                } else if (language.equalsIgnoreCase("Ar")) {
////                                    // set title
////                                    alertDialogBuilder.setTitle("اوريجانو");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage(" إعادة ضبط كلمة السر تمت بنجاح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                    Intent loginIntent = new Intent(ResetPassword.this, MainActivity.class);
////                                                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////                                                    startActivity(loginIntent);
////                                                    finish();
////                                                }
////                                            });
////                                }
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }catch (JSONException je) {
//                            je.printStackTrace();
//                            dialog.dismiss();
//                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ResetPassword.this, android.R.style.Theme_Material_Light_Dialog));
////                            if (language.equalsIgnoreCase("En")) {
//                            // set title
//                            alertDialogBuilder.setTitle("Broasted Express");
//
//                            // set dialog message
//                            alertDialogBuilder
//                                    .setMessage("Reset Password Unsuccessful")
//                                    .setCancelable(false)
//                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                        }
//                                    });
////                            } else if (language.equalsIgnoreCase("Ar")) {
////                                // set title
////                                alertDialogBuilder.setTitle("اوريجانو");
////
////                                // set dialog message
////                                alertDialogBuilder
////                                        .setMessage(" فشل إعادة ضبط كلمة السر ")
////                                        .setCancelable(false)
////                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                            public void onClick(DialogInterface dialog, int id) {
////                                                dialog.dismiss();
////                                            }
////                                        });
////                            }
//
//                            // create alert dialog
//                            AlertDialog alertDialog = alertDialogBuilder.create();
//
//                            // show it
//                            alertDialog.show();
//
//
//                        }
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            } else {
//                dialog.dismiss();
//            }
//
//            if(dialog != null) {
//                dialog.dismiss();
//            }
//
//            super.onPostExecute(result1);
//        }
//    }


