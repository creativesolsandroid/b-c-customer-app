package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by CS on 12-07-2016.
 */
public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    private String response12 = null;
    ProgressDialog dialog;

    TextInputLayout inputLayoutOldPassword, inputLayoutNewPassword, inputLayoutConfirmPassword;
    EditText inputOldPassword, inputNewPassword, inputConfirmPassword;
    String strOldPassword, strNewPassword, strConfirmPassword;
    TextView submit, backBtn;
    SharedPreferences userPrefs;
    String response, userId;
    SharedPreferences languagePrefs;
    String language;
    RelativeLayout back_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.change_password);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.change_password_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        inputOldPassword = (EditText) findViewById(R.id.old_password);
        inputNewPassword = (EditText) findViewById(R.id.new_password);
        inputConfirmPassword = (EditText) findViewById(R.id.confirm_password);

        inputLayoutOldPassword = (TextInputLayout) findViewById(R.id.layout_old_password);
        inputLayoutNewPassword = (TextInputLayout) findViewById(R.id.layout_new_password);
        inputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.layout_repeat_password);

        submit = (TextView) findViewById(R.id.change_password_button);

        back_btn = (RelativeLayout) findViewById(R.id.back_btn);

        inputOldPassword.addTextChangedListener(new TextWatcher(inputOldPassword));
        inputNewPassword.addTextChangedListener(new TextWatcher(inputNewPassword));
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        submit.setOnClickListener(this);

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String oldPwd = oldPassword.getText().toString();
//                String newPwd = newPassword.getText().toString();
//                String confirmPwd = confirmPassword.getText().toString();
//                if (oldPwd.length() == 0) {
//                    oldPassword.setError("Please enter old password");
//                } else if (oldPwd.contains(" ")){
//                    oldPassword.setError("Spaces are not allowed");
//                } else if (newPwd.length() == 0) {
//                    newPassword.setError("Please enter new password");
//                } else if (newPwd.length() < 8) {
//                    if (language.equalsIgnoreCase("En")) {
//                        newPassword.setError("Password must be at least 8 characters");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//
//                        newPassword.setError("الرقم السري يجب أن يحتوي على الأقل على 8 حروف");
//                    } else if (!newPwd.matches("^(?=.*?\\d)[a-zA-Z0-9]{8,13}$")) {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);
//
//                        if (language.equalsIgnoreCase("En")) {
//                            // set title
//                            alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                            // set dialog message
//                            alertDialogBuilder
//                                    .setMessage("*Your password must fulfill the following:\nPassword must contain at least 1 numeric digits\nPassword must be at least 8 characters\nPassword must be at less than 20 characters")
//                                    .setCancelable(false)
//                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.dismiss();
//                                        }
//                                    });
//                        }
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
//                    } else if (newPwd.contains(" ")){
//                        newPassword.setError("Spaces are not allowed");
//                    }
//                } else if (confirmPwd.length() == 0) {
//                    confirmPassword.setError("Please retype password");
//                } else if (!newPwd.equals(confirmPwd)) {
//                    confirmPassword.setError("Passwords not match, please retype");
//                } else if (confirmPwd.contains(" ")){
//                    confirmPassword.setError("Spaces are not allowed");
//                }
//                else {
//                    new ChangePasswordResponse().execute(userId, oldPwd, newPwd);
//                }
//            }
//        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_password_button:
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ChangePasswordResponse().execute(userId, strOldPassword, strNewPassword);
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private boolean validations() {
        strOldPassword = inputOldPassword.getText().toString();
        strNewPassword = inputNewPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strOldPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputOldPassword, ChangePassword.this);
            return false;
        } else if (strOldPassword.length() < 4 || strOldPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputOldPassword, ChangePassword.this);
            return false;
        }
        if (strNewPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutNewPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutNewPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputNewPassword, ChangePassword.this);
            return false;
        } else if (strNewPassword.length() < 4 || strNewPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputOldPassword, ChangePassword.this);
            return false;
        } else if (!strNewPassword.equals(strConfirmPassword)) {

            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
                Constants.requestEditTextFocus(inputConfirmPassword, ChangePassword.this);
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match_ar));
                Constants.requestEditTextFocus(inputConfirmPassword, ChangePassword.this);
            }
            return false;
        } else if (strConfirmPassword.contains(" ")){
            inputLayoutConfirmPassword.setError("Spaces are not allowed");
            Constants.requestEditTextFocus(inputConfirmPassword, ChangePassword.this);
            return false;
        } else if (strNewPassword.contains(" ")){
            inputLayoutNewPassword.setError("Spaces are not allowed");
            Constants.requestEditTextFocus(inputNewPassword, ChangePassword.this);
            return false;
        } else if (strOldPassword.contains(" ")){
            inputLayoutOldPassword.setError("Spaces are not allowed");
            Constants.requestEditTextFocus(inputOldPassword, ChangePassword.this);
            return false;
        }

        return true;
    }

    private void clearErrors() {
        inputLayoutOldPassword.setErrorEnabled(false);
        inputLayoutNewPassword.setErrorEnabled(false);
        inputLayoutConfirmPassword.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.old_password:
                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.new_password:
                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.confirm_password:
                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    public class ChangePasswordResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        InputStream is = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
            dialog = ProgressDialog.show(ChangePassword.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    // 2. make POST request to the given URL
//                        HttpGet httpPost = new HttpGet(Constants.CHANGE_PASSWORD_URL+arg0[0]+"?OldPsw="+arg0[1]+"&NewPsw="+arg0[2]);

                    // Making HTTP request
                    try {
                        // defaultHttpClient
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        HttpGet httpPost = new HttpGet(Constants.CHANGE_PASSWORD_URL + arg0[0] + "?OldPsw=" + arg0[1] + "&NewPsw=" + arg0[2]);

                        httpPost.setHeader("Accept", "application/xml");
                        httpPost.setHeader("Content-type", "application/xml");
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        is = httpEntity.getContent();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "iso-8859-1"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        is.close();
                        response12 = sb.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            } else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);

                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    if (response12.contains("false")) {
                        dialog.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);

                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Invalid Old Password")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else if (response12.contains("true")) {
                        dialog.dismiss();
                        Toast.makeText(ChangePassword.this, "Password changed successfully", Toast.LENGTH_SHORT).show();
                        onBackPressed();

                    }
                }
            } else {
                dialog.dismiss();
            }

            super.onPostExecute(result1);
        }
    }
}
