package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.SandwichBoxItemsAdapter;
import com.cs.bakeryco.adapters.SandwichSelectionAdapter;
import com.cs.bakeryco.model.Items;

import java.util.ArrayList;

import static com.cs.bakeryco.R.id.gridview;

/**
 * Created by CS on 28-08-2017.
 */

public class SandwichSelection extends AppCompatActivity implements View.OnClickListener, View.OnDragListener {

    TextView individual, box, mini, petite, boxName;
    TextView m6PcsBox, m12PcsBox, m24PcsBox;
    TextView Title;
    public static String title;
    public static TextView mNumBox;
    LinearLayout pcs_layout;

    ArrayList<Items> items = new ArrayList<>();
    public static ArrayList<Items> Boxitems = new ArrayList<>();
    GridView mGridView;
    public static GridView mBoxGridView;
    SandwichSelectionAdapter mAdapter;
    public static SandwichBoxItemsAdapter mBoxAdapter;
    ImageView fav;

    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;

    public static int pos = 0, totalCount = 0;
    public static int boxSize = 12;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.sandwich_selection);
        }else if (language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.sandwich_selection);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        items = (ArrayList<Items>)getIntent().getSerializableExtra("list");
        title = getIntent().getStringExtra("title");

        pos = 0;
        totalCount = 0;
        boxSize = 12;
        Boxitems.clear();

        individual = (TextView) findViewById(R.id.individual);
        box = (TextView) findViewById(R.id.box);
        mini = (TextView) findViewById(R.id.mini);
        petite = (TextView) findViewById(R.id.petite);
        m6PcsBox = (TextView) findViewById(R.id.pcs_6_box);
        m12PcsBox = (TextView) findViewById(R.id.pcs_12_box);
        m24PcsBox = (TextView) findViewById(R.id.pcs_24_box);
        mNumBox = (TextView) findViewById(R.id.box_size);
        boxName = (TextView) findViewById(R.id.box_name);
        Title = (TextView) findViewById(R.id.header_title);

        try {
            Title.setText(getIntent().getStringExtra("title"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        fav = (ImageView) findViewById(R.id.fav);

        mGridView = (GridView) findViewById(gridview);
        mBoxGridView = (GridView) findViewById(R.id.grid_layout_box);

        pcs_layout = (LinearLayout) findViewById(R.id.pcs_layout);

        mAdapter = new SandwichSelectionAdapter(getApplicationContext(), items, language, SandwichSelection.this);
        mGridView.setAdapter(mAdapter);

        mBoxAdapter = new SandwichBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, SandwichSelection.this);
        mBoxGridView.setAdapter(mBoxAdapter);

//        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//                ClipData data = ClipData.newPlainText("", "");
//                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
//                view.startDrag(data, shadowBuilder, view, 0);
//                view.setVisibility(View.VISIBLE);
//                return true;
//            }
//        });

        mBoxGridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_MOVE){
                    return true;
                }
                return false;
            }
        });

        mBoxGridView.setOnDragListener(this);

        individual.setOnClickListener(this);
        box.setOnClickListener(this);
        mini.setOnClickListener(this);
        petite.setOnClickListener(this);
        m6PcsBox.setOnClickListener(this);
        m12PcsBox.setOnClickListener(this);
        m24PcsBox.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.individual:
                pcs_layout.setVisibility(View.INVISIBLE);
                individual.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                petite.setBackgroundColor(getResources().getColor(R.color.blue_color));
                break;

            case R.id.box:
                if(pcs_layout.getVisibility() == View.VISIBLE) {
                    pcs_layout.setVisibility(View.GONE);
                }
                else{
                    pcs_layout.setVisibility(View.VISIBLE);
                }
                box.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                petite.setBackgroundColor(getResources().getColor(R.color.blue_color));
                break;

            case R.id.mini:
                pcs_layout.setVisibility(View.INVISIBLE);
                mini.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                petite.setBackgroundColor(getResources().getColor(R.color.blue_color));
                break;

            case R.id.petite:
                pcs_layout.setVisibility(View.INVISIBLE);
                petite.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                break;

            case R.id.pcs_6_box:
                pcs_layout.setVisibility(View.INVISIBLE);
                boxName.setText("6pcs Box");
                boxSize = 6;
                mNumBox.setText("0/"+boxSize);
                totalCount = 0;
                Boxitems.clear();
                mBoxGridView.setNumColumns(3);
                mBoxAdapter = new SandwichBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, SandwichSelection.this);
                mBoxAdapter.notifyDataSetChanged();
                break;

            case R.id.pcs_12_box:
                pcs_layout.setVisibility(View.INVISIBLE);
                boxName.setText("12pcs Box");
                boxSize = 12;
                mNumBox.setText("0/"+boxSize);
                Boxitems.clear();
                totalCount = 0;
                mBoxGridView.setNumColumns(6);
                mBoxAdapter = new SandwichBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, SandwichSelection.this);
                mBoxAdapter.notifyDataSetChanged();
                break;

            case R.id.pcs_24_box:
                pcs_layout.setVisibility(View.INVISIBLE);
                boxName.setText("24pcs Box");
                boxSize = 24;
                mNumBox.setText("0/"+boxSize);
                totalCount = 0;
                Boxitems.clear();
                mBoxGridView.setNumColumns(12);
                mBoxAdapter = new SandwichBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, SandwichSelection.this);
                mBoxAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        final int action = event.getAction();
        // Handles each of the expected events
        switch(action) {

            case DragEvent.ACTION_DRAG_STARTED:

                // Returns false. During the current drag and drop operation, this View will
                // not receive events again until ACTION_DRAG_ENDED is sent.
                return true;

            case DragEvent.ACTION_DRAG_ENTERED:
//                Log.i("TAG","drap entered ACTION_DRAG_ENTERED");

                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
//                Log.i("TAG","drap entered ACTION_DRAG_LOCATION");
                // Ignore the event
                return true;

            case DragEvent.ACTION_DRAG_EXITED:

                return true;

            case DragEvent.ACTION_DROP:
                if(boxSize>totalCount) {
                    Boxitems.add(items.get(pos));
                    totalCount = Boxitems.size();
                    SandwichSelection.mNumBox.setText(SandwichSelection.totalCount + "/" + SandwichSelection.boxSize);
                    mBoxAdapter = new SandwichBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, SandwichSelection.this);
                    mBoxGridView.setAdapter(mBoxAdapter);
                    mBoxAdapter.notifyDataSetChanged();
                }
                else{
                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                        message = "You are done with Maximum "+boxSize + getIntent().getStringExtra("title") + " per Box, to order more use other box.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(SandwichSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
                // Returns true. DragEvent.getResult() will return true.
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                mBoxAdapter.notifyDataSetChanged();
                // returns true; the value is ignored.
                return true;

            // An unknown action type was received.
            default:
                Log.e("DragDrop Example","Unknown action type received by OnDragListener.");
                break;
        }
        return false;
    }
}
