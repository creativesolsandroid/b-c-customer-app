package com.cs.bakeryco.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.CakeInsidelistAdapter;
import com.cs.bakeryco.model.CakeDetailslist;
import com.cs.bakeryco.model.CakeInnerlist;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.SubItems;
import com.cs.bakeryco.widgets.CustomListView;
import com.cs.bakeryco.widgets.JustifiedTextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 3/13/2pos18.
 */

public class CakeInnerActivity1 extends AppCompatActivity {

    Toolbar toolbar;
    private DataBaseHelper myDbHelper;
    TextView itemName, itemdsec, msize, msize1, mitemprice, mitemprice1, mcakedec, mcakedec1;
    ImageView mright, mright1, mright2, mright3;
    JustifiedTextView itemDesc;
    ImageView itemimage, itemComment;
    ImageView pieceOfCake, wholeCake, wholeCake1;
    ImageView plusBtn, minusBtn;
    TextView qty, quantity, price, piecePrice, wholePrice, mtotalprice, mtotalqty, mwhole_txt, msliced_txt, mheadertitle;
    RelativeLayout addLayout, wholeCakeLayout, pieceCakeLayout, wholeCakeLayout1, checkOut, addMoreLayout;
    View line;

    RelativeLayout mcount;

    public static TextView orderPrice, orderQuantity, mcount_basket;

    LinearLayout maddmore, mordernow, mprice_layout, mprice_layout1, mwhole_cake_layout, msliced_cake_layout, mwhole_cake, msliced_cake;

    CustomListView mcakelist;

    SharedPreferences languagePrefs;
    private ArrayList<SubItems> subitemsList = new ArrayList<>();
    private ArrayList<CakeDetailslist> pricelist1 = new ArrayList<>();

    private ArrayList<Price> pricelist = new ArrayList<>();

    ArrayList<Integer> value = new ArrayList<>();
    ArrayList<CakeInnerlist> cakelist = new ArrayList<>();

    public static ArrayList<String> itemcount = new ArrayList<>();
    public static ArrayList<Integer> itemfinalcount = new ArrayList<>();
    public static ArrayList<Integer> itemfinalprice = new ArrayList<>();

    public static ArrayList<Order> orderlist = new ArrayList<>();

    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    int value1;
    String language;

    public static int cakeitemqty;

    int itemQty = 1, pos;
    String itemPrice;
    String finalPrice;
    Float itemPrice1, finalPrice1;

    ScrollView mscrollview;

    AlertDialog alertDialog;

    String sItemName, sItemNameAr, sItemId, sCatId, sItemDesc, sItemDescAr, sSubCatId, sItemImage, size, sprice, aname, subcatid, ItemTypeIds;
    int itemprice = 0, catid;

    boolean sitemprice = false, sitemprice1 = false;

    int aprice;
    public static String itemid, asize, asize_ar, itemtype, asizeid;

    CakeInsidelistAdapter mcakeitemadapter;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.cake_inner_screen);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.cake_inner_screen_arabic);
        }
        myDbHelper = new DataBaseHelper(CakeInnerActivity1.this);

        Constants.COMMENTS = "";

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pricelist = (ArrayList<Price>) getIntent().getSerializableExtra(
                "pricelist");

        sItemImage = getIntent().getStringExtra("Image");
        sItemName = getIntent().getStringExtra("ItemName");
        sItemNameAr = getIntent().getStringExtra("ItemName_Ar");
        sItemDesc = getIntent().getStringExtra("Desc");
        sItemDescAr = getIntent().getStringExtra("Desc_Ar");
//        size = getIntent().getStringExtra("size");
//        sprice = getIntent().getStringExtra("price");
        pos = getIntent().getIntExtra("pos", 0);
        itemid = getIntent().getStringExtra("itemid");
        ItemTypeIds = getIntent().getStringExtra("ItemTypeIds");
        subcatid = getIntent().getStringExtra("subcatid");
        catid = getIntent().getIntExtra("catid", 2);

        orderlist = myDbHelper.getItemOrderInfo(itemid);

        itemdsec = (TextView) findViewById(R.id.item_desc);
        msize = (TextView) findViewById(R.id.size);
        msize1 = (TextView) findViewById(R.id.size1);
        mitemprice = (TextView) findViewById(R.id.itemprice);
        mitemprice1 = (TextView) findViewById(R.id.itemprice1);
        qty = (TextView) findViewById(R.id.qty);
        price = (TextView) findViewById(R.id.price);
//        mtotalprice = (TextView) findViewById(R.id.item_price);
//        mtotalqty = (TextView) findViewById(R.id.item_qty);
        mwhole_txt = (TextView) findViewById(R.id.whole_txt);
        msliced_txt = (TextView) findViewById(R.id.sliced_txt);
        mheadertitle = (TextView) findViewById(R.id.header_title);
        mcakedec = (TextView) findViewById(R.id.cake_desc);
        mcakedec1 = (TextView) findViewById(R.id.cake_desc1);

        orderQuantity = (TextView) findViewById(R.id.item_qty);
        orderPrice = (TextView) findViewById(R.id.item_price);
        mcount_basket = findViewById(R.id.total_count);


        itemimage = (ImageView) findViewById(R.id.item_image);
        mright = (ImageView) findViewById(R.id.right);
        mright1 = (ImageView) findViewById(R.id.right1);
        mright2 = (ImageView) findViewById(R.id.right2);
        mright3 = (ImageView) findViewById(R.id.right3);
        plusBtn = (ImageView) findViewById(R.id.count_plus);
        minusBtn = (ImageView) findViewById(R.id.count_mins);
        itemComment = (ImageView) findViewById(R.id.edit_comment);

        addLayout = (RelativeLayout) findViewById(R.id.addmore_layout1);
        checkOut = (RelativeLayout) findViewById(R.id.checkout_layout);
        addMoreLayout = (RelativeLayout) findViewById(R.id.addmore_layout);
        mcount = findViewById(R.id.count);

        mprice_layout = (LinearLayout) findViewById(R.id.price_layout);
        mprice_layout1 = (LinearLayout) findViewById(R.id.price_layout1);
        mwhole_cake_layout = (LinearLayout) findViewById(R.id.whole_cake_layout);
        msliced_cake_layout = (LinearLayout) findViewById(R.id.sliced_cake_layout);
        mwhole_cake = (LinearLayout) findViewById(R.id.whole_cake);
        msliced_cake = (LinearLayout) findViewById(R.id.sliced_cake);

        mcakelist = (CustomListView) findViewById(R.id.cake_list);

        mscrollview = findViewById(R.id.scrollView);

        itemcount.clear();
        itemfinalcount.clear();
        itemfinalprice.clear();
        itemQty = 1;

        mcakeitemadapter = new CakeInsidelistAdapter(getApplicationContext(), orderlist, language, CakeInnerActivity1.this);
        mcakelist.setAdapter(mcakeitemadapter);
        mscrollview.smoothScrollTo(0, 0);

        try {

            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            if (myDbHelper.getItemOrderPrice1(itemid) == 0){
                CakeInnerActivity1.orderPrice.setText("-");
                CakeInnerActivity1.orderQuantity.setText("-");
                CakeInnerActivity1.mcount_basket.setText("0");
            } else {
                CakeInnerActivity1.orderPrice.setText("" + decimalFormat.format(myDbHelper.getItemOrderPrice1(itemid)) + " SR");
                CakeInnerActivity1.orderQuantity.setText("" + myDbHelper.getItemOrderCount(itemid));
                CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Glide.with(CakeInnerActivity1.this).load(Constants.IMAGE_URL + sItemImage).into(itemimage);

        if (language.equalsIgnoreCase("En")) {
            itemdsec.setText(sItemDesc);
            mheadertitle.setText(sItemName);
        } else if (language.equalsIgnoreCase("Ar")) {
            itemdsec.setText(sItemDescAr);
            mheadertitle.setText(sItemNameAr);
        }


        Log.i("TAG", "price " + pricelist.size());

        Log.i("TAG", "price " + pricelist.get(0).getPriceId());

        Log.i("TAG", "price " + pricelist.get(0).getPriceId());

        Log.i("TAG", "cakedesc " + pricelist.get(0).getItemsizedesc());


        mprice_layout.setVisibility(View.GONE);
        if (pricelist.size() == 2) {
            mprice_layout.setVisibility(View.VISIBLE);


            if (pricelist.get(1).getItemsizedesc().equals("null")) {
                mcakedec.setVisibility(View.GONE);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 0);
                lp.weight = (float) 1.5;
                mwhole_cake.setLayoutParams(lp);
                msliced_cake.setLayoutParams(lp);
            } else {
                mcakedec.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    mcakedec.setText(pricelist.get(1).getItemsizedesc());
                } else if (language.equalsIgnoreCase("Ar")) {
                    mcakedec.setText(pricelist.get(1).getItemSizeDescription_Ar());
                }
            }
            if (language.equalsIgnoreCase("En")) {
                msize.setText(pricelist.get(1).getSize());
            } else if (language.equalsIgnoreCase("Ar")) {
                msize.setText(pricelist.get(1).getSizeName_Ar());
            }
        }

        if (ItemTypeIds.contains("1,2")) {

            mwhole_cake_layout.setVisibility(View.VISIBLE);
            mwhole_txt.setVisibility(View.VISIBLE);
            msliced_cake_layout.setVisibility(View.VISIBLE);
            msliced_txt.setVisibility(View.VISIBLE);


        } else if (ItemTypeIds.contains("2")) {

            itemcount.clear();
            itemfinalcount.clear();
            itemfinalprice.clear();
            msliced_cake_layout.setVisibility(View.GONE);
            msliced_txt.setVisibility(View.GONE);
            mwhole_cake_layout.setVisibility(View.VISIBLE);
            mwhole_txt.setVisibility(View.VISIBLE);
        }
//        } else {
//
//            itemcount.clear();
//            itemfinalcount.clear();
//            itemfinalprice.clear();
//            mprice_layout1.setVisibility(View.VISIBLE);
//            msliced_cake_layout.setVisibility(View.VISIBLE);
//            msliced_txt.setVisibility(View.VISIBLE);
//            if (language.equalsIgnoreCase("En")) {
//                msize1.setText(pricelist.get(1).getSize());
//            } else if (language.equalsIgnoreCase("Ar")) {
//                msize1.setText(pricelist.get(1).getSizeName_Ar());
//            }
//
//            if (language.equalsIgnoreCase("En")) {
//                mitemprice1.setText(decimalFormat.format(Float.parseFloat(pricelist.get(1).getPrice())) + " SR");
//            } else if (language.equalsIgnoreCase("Ar")) {
//                mitemprice1.setText(" ريال" + decimalFormat.format(Float.parseFloat(pricelist.get(1).getPrice())));
//            }
//            if (pricelist.get(1).getItemsizedesc().equals("null")) {
//                mcakedec1.setVisibility(View.GONE);
//                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 0);
//                lp.weight = (float) 1.5;
//                mwhole_cake.setLayoutParams(lp);
//                msliced_cake.setLayoutParams(lp);
//            } else {
//                mcakedec1.setVisibility(View.VISIBLE);
//                if (language.equalsIgnoreCase("En")) {
//                    mcakedec1.setText(pricelist.get(1).getItemsizedesc());
//                } else if (language.equalsIgnoreCase("Ar")) {
//                    mcakedec1.setText(pricelist.get(1).getItemSizeDescription_Ar());
//                }
//            }
//        }
        else if (ItemTypeIds.contains("1")) {

            itemcount.clear();
            itemfinalcount.clear();
            itemfinalprice.clear();
            msliced_cake_layout.setVisibility(View.VISIBLE);
            msliced_txt.setVisibility(View.VISIBLE);
            mwhole_cake_layout.setVisibility(View.GONE);
            mwhole_txt.setVisibility(View.GONE);
        }
//        else {
//
//            itemcount.clear();
//            itemfinalcount.clear();
//            itemfinalprice.clear();
//            mprice_layout.setVisibility(View.VISIBLE);
//            mwhole_cake_layout.setVisibility(View.VISIBLE);
//            mwhole_txt.setVisibility(View.VISIBLE);
//            if (language.equalsIgnoreCase("En")) {
//                msize.setText(pricelist.get(0).getSize());
//            } else if (language.equalsIgnoreCase("Ar")) {
//                msize.setText(pricelist.get(0).getSizeName_Ar());
//            }
//
//            if (language.equalsIgnoreCase("En")) {
//                mitemprice.setText(decimalFormat.format(Float.parseFloat(pricelist.get(0).getPrice())) + " SR");
//            } else if (language.equalsIgnoreCase("Ar")) {
//                mitemprice.setText(" ريال" + decimalFormat.format(Float.parseFloat(pricelist.get(0).getPrice())));
//            }
//            if (pricelist.get(0).getItemsizedesc().equals("null")) {
//                mcakedec.setVisibility(View.GONE);
//                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 0);
//                lp.weight = (float) 1.5;
//                mwhole_cake.setLayoutParams(lp);
//                msliced_cake.setLayoutParams(lp);
//            } else {
//                mcakedec.setVisibility(View.VISIBLE);
//                if (language.equalsIgnoreCase("En")) {
//                    mcakedec.setText(pricelist.get(0).getItemsizedesc());
//                } else if (language.equalsIgnoreCase("Ar")) {
//                    mcakedec.setText(pricelist.get(0).getItemSizeDescription_Ar());
//                }
//            }
//        }

//        mprice_layout1.setVisibility(View.GONE);
        if (pricelist.size() == 2) {
//            mprice_layout1.setVisibility(View.VISIBLE);
        }

        if (pricelist.size() == 1) {
            mright1.setBackgroundResource(R.drawable.selected_right);
            mright2.setBackgroundResource(R.drawable.selected_right);
        }
        if (language.equalsIgnoreCase("En")) {
            msize1.setText(pricelist.get(0).getSize());
        } else if (language.equalsIgnoreCase("Ar")) {
            msize1.setText(pricelist.get(0).getSizeName_Ar());
        }

        if (language.equalsIgnoreCase("En")) {
            mitemprice1.setText(decimalFormat.format(Float.parseFloat(pricelist.get(0).getPrice())) + " SR");
        } else if (language.equalsIgnoreCase("Ar")) {
            mitemprice1.setText(" ريال" + decimalFormat.format(Float.parseFloat(pricelist.get(0).getPrice())));
        }
        if (pricelist.get(0).getItemsizedesc().equals("null")) {
            mcakedec1.setVisibility(View.GONE);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 0);
            lp.weight = (float) 1.5;
            mwhole_cake.setLayoutParams(lp);
            msliced_cake.setLayoutParams(lp);
        } else {
            mcakedec1.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")) {
                mcakedec1.setText(pricelist.get(0).getItemsizedesc());
            } else if (language.equalsIgnoreCase("Ar")) {
                mcakedec1.setText(pricelist.get(0).getItemSizeDescription_Ar());
            }
        }

        price.setText(decimalFormat.format(Float.parseFloat(pricelist.get(0).getPrice())));

        mprice_layout.setVisibility(View.GONE);
        if (pricelist.size() == 2) {
            mprice_layout.setVisibility(View.VISIBLE);

            if (language.equalsIgnoreCase("En")) {
                mitemprice.setText(decimalFormat.format(Float.parseFloat(pricelist.get(1).getPrice())) + " SR");
            } else if (language.equalsIgnoreCase("Ar")) {
                mitemprice.setText(" ريال" + decimalFormat.format(Float.parseFloat(pricelist.get(1).getPrice())));
            }


//        itemprice = Integer.parseInt(pricelist.get(0).getPrice());.


            mright.setBackgroundResource(R.drawable.selected_right);
            mright2.setBackgroundResource(R.drawable.selected_right);

            qty.setText("1");
            price.setText(decimalFormat.format(Float.parseFloat(pricelist.get(1).getPrice())));
            itemprice = Integer.parseInt(pricelist.get(1).getPrice());
            aprice = Integer.parseInt(pricelist.get(1).getPrice());
            asize = pricelist.get(1).getSize();
//        if (language.equalsIgnoreCase("Ar")){
            asize_ar = pricelist.get(1).getSizeName_Ar();
//        }
            asizeid = pricelist.get(1).getSizeid();
            itemQty = 1;
        }
        if (pricelist.size() == 1) {
            itemprice = Integer.parseInt(pricelist.get(0).getPrice());
            aprice = Integer.parseInt(pricelist.get(0).getPrice());
            asize = pricelist.get(0).getSize();
//        if (language.equalsIgnoreCase("Ar")){
            asize_ar = pricelist.get(0).getSizeName_Ar();
//        }
            asizeid = pricelist.get(0).getSizeid();
            itemQty = 1;
        }

        mprice_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sitemprice = true;
                mright.setBackgroundResource(R.drawable.selected_right);
                mright1.setBackgroundResource(R.drawable.unselected_right);

                itemprice = Integer.parseInt(pricelist.get(1).getPrice());

                mprice_layout.setVisibility(View.GONE);
                if (pricelist.size() == 2) {
                    mprice_layout.setVisibility(View.VISIBLE);
                    if (itemQty > 1) {

                        itemprice = itemQty * itemprice;

                    }
                    price.setText(decimalFormat.format(Float.parseFloat(String.valueOf(itemprice))));
                    qty.setText(Integer.toString(itemQty));
                    if (pricelist.get(1).getItemsizedesc().equals("null")) {
                        mcakedec.setVisibility(View.GONE);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 0);
                        lp.weight = (float) 1.5;
                        mwhole_cake.setLayoutParams(lp);
                        msliced_cake.setLayoutParams(lp);
                    } else {
                        mcakedec.setVisibility(View.VISIBLE);
                        if (language.equalsIgnoreCase("En")) {
                            mcakedec.setText(pricelist.get(1).getItemsizedesc());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            mcakedec.setText(pricelist.get(1).getItemSizeDescription_Ar());
                        }
                    }

                    asize = pricelist.get(1).getSize();

//                if (language.equalsIgnoreCase("Ar")){
                    asize_ar = pricelist.get(1).getSizeName_Ar();
//                }

                    asizeid = pricelist.get(1).getSizeid();
                    aprice = Integer.parseInt(pricelist.get(1).getPrice());

                }

            }
        });


        mprice_layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sitemprice1 = true;

                mright.setBackgroundResource(R.drawable.unselected_right);
                mright1.setBackgroundResource(R.drawable.selected_right);

                itemprice = Integer.parseInt(pricelist.get(0).getPrice());

//                mprice_layout1.setVisibility(View.GONE);
                if (pricelist.size() == 2) {
//                    mprice_layout1.setVisibility(View.VISIBLE);
                    Log.i("TAG", "price " + decimalFormat.format(Float.parseFloat(pricelist.get(1).getPrice())));

                }
                if (itemQty > 1) {

                    itemprice = itemQty * itemprice;

                }
                price.setText(decimalFormat.format(Float.parseFloat(String.valueOf(itemprice))));
                qty.setText(Integer.toString(itemQty));
                if (pricelist.get(0).getItemsizedesc().equals("null")) {
                    mcakedec1.setVisibility(View.GONE);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 0);
                    lp.weight = (float) 1.5;
                    mwhole_cake.setLayoutParams(lp);
                    msliced_cake.setLayoutParams(lp);
                } else {
                    mcakedec1.setVisibility(View.VISIBLE);
                    if (language.equalsIgnoreCase("En")) {
                        mcakedec1.setText(pricelist.get(0).getItemsizedesc());
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mcakedec1.setText(pricelist.get(0).getItemSizeDescription_Ar());
                    }

                }

                asize = pricelist.get(0).getSize();

//                if (language.equalsIgnoreCase("Ar")){
                asize_ar = pricelist.get(0).getSizeName_Ar();
//                }
                asizeid = pricelist.get(0).getSizeid();
                aprice = Integer.parseInt(pricelist.get(0).getPrice());


            }
        });

        itemComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(CakeInnerActivity1.this, CommentsActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    intent.putExtra("title", sItemName);
                } else if (language.equalsIgnoreCase("Ar")) {
                    intent.putExtra("title", sItemNameAr);
                }

                intent.putExtra("itemImage", sItemImage);
                intent.putExtra("itemId", sItemId);
                intent.putExtra("orderId", "");
                intent.putExtra("comment", "");
                intent.putExtra("size", asizeid);
                intent.putExtra("screen", "item");
                startActivity(intent);

            }
        });


        aname = "Whole " + sItemName;
        itemtype = "Whole";


        mwhole_cake_layout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                mright2.setBackgroundResource(R.drawable.selected_right);
                mright3.setBackgroundResource(R.drawable.unselected_right);

                aname = "Whole " + sItemName;
//                if (language.equalsIgnoreCase("En")) {
                itemtype = "Whole";
//                } else if (language.equalsIgnoreCase("Ar")) {
//                    itemtype = "قالب";
//                }

            }
        });

        msliced_cake_layout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                mright2.setBackgroundResource(R.drawable.unselected_right);
                mright3.setBackgroundResource(R.drawable.selected_right);

                aname = "Sliced " + sItemName;
//                if (language.equalsIgnoreCase("En")) {
                itemtype = "Sliced";
//                } else if (language.equalsIgnoreCase("Ar")) {
//                    itemtype = "شرائح";
//                }

            }
        });


        plusBtn.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                itemQty = itemQty + 1;
                itemprice = itemprice + aprice;
//                if (sitemprice){
//                    aprice = ;
//                }
                qty.setText(Integer.toString(itemQty));
                price.setText(decimalFormat.format(Float.parseFloat(String.valueOf(itemprice))));

            }
        });

        minusBtn.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                if (itemQty > 1) {
                    itemQty = itemQty - 1;

                    itemprice = itemprice - aprice;
                    qty.setText(Integer.toString(itemQty));
                    price.setText(decimalFormat.format(Float.parseFloat(String.valueOf(itemprice))));
                }

            }
        });

        value1 = 0;
        addLayout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                Log.i("TAG", "name " + aname);
                value1 = value1 + 1;

                value.add(value1);

                CakeInnerlist cakeInnerlist = new CakeInnerlist();

                cakeInnerlist.setSize(asize);
                cakeInnerlist.setItemprice(aprice);
                cakeInnerlist.setTotalprice(itemprice);
                cakeInnerlist.setItemName(sItemName);
                cakeInnerlist.setFinalname(aname);
                cakeInnerlist.setItemQty(itemQty);
                cakeInnerlist.setItemid(itemid);
                cakeInnerlist.setItemtype(itemtype);

                Log.i("TAG", "qty " + itemQty);

                cakelist.add(cakeInnerlist);

                boolean isOrderMatched = false;
                int position = 0;
//                for (int i = 0; i <= orderlist.size(); i++) {
//
//                    if (orderlist.size() == 0){
//                        HashMap<String, String> values = new HashMap<>();
//                        values.put("itemId", itemid);
//                        values.put("itemTypeId", asize);
//                        values.put("subCategoryId", subcatid);
//                        values.put("additionals", "");
//                        values.put("qty", String.valueOf(itemQty));
//                        values.put("price", String.valueOf(aprice));
//                        values.put("additionalsPrice", "");
//                        values.put("additionalsTypeId", "");
//                        values.put("totalAmount", String.valueOf(itemprice));
//                        values.put("comment", "");
//                        values.put("status", "1");
//                        values.put("creationDate", "14/07/2015");
//                        values.put("modifiedDate", "14/07/2015");
//                        values.put("categoryId", "");
//                        values.put("itemName", sItemName);
//                        values.put("itemNameAr", sItemNameAr);
//                        values.put("image", sItemImage);
//                        values.put("item_desc", sItemDesc);
//                        values.put("item_desc_Ar", sItemDescAr);
//                        values.put("sub_itemName", "");
//                        values.put("sub_itemName_Ar", "");
//                        values.put("sub_itemImage", "");
//                        values.put("ItemType", itemtype);
//
//                        myDbHelper.insertOrder(values);
//
//                    }else if (!itemid.equals(orderlist.get(i).getItemId()) && !itemtype.equals(orderlist.get(i).getItemType()) && !asize.equals(orderlist.get(i).getItemTypeId())) {
//                        isOrderMatched = false;
//                        HashMap<String, String> values = new HashMap<>();
//                        values.put("itemId", itemid);
//                        values.put("itemTypeId", asize);
//                        values.put("subCategoryId", subcatid);
//                        values.put("additionals", "");
//                        values.put("qty", String.valueOf(itemQty));
//                        values.put("price", String.valueOf(aprice));
//                        values.put("additionalsPrice", "");
//                        values.put("additionalsTypeId", "");
//                        values.put("totalAmount", String.valueOf(itemprice));
//                        values.put("comment", "");
//                        values.put("status", "1");
//                        values.put("creationDate", "14/07/2015");
//                        values.put("modifiedDate", "14/07/2015");
//                        values.put("categoryId", "");
//                        values.put("itemName", sItemName);
//                        values.put("itemNameAr", sItemNameAr);
//                        values.put("image", sItemImage);
//                        values.put("item_desc", sItemDesc);
//                        values.put("item_desc_Ar", sItemDescAr);
//                        values.put("sub_itemName", "");
//                        values.put("sub_itemName_Ar", "");
//                        values.put("sub_itemImage", "");
//                        values.put("ItemType", itemtype);
//
//                        myDbHelper.insertOrder(values);
//                     } else if (itemid.equals(orderlist.get(i).getItemId()) && itemtype.equals(orderlist.get(i).getItemType()) && asize.equals(orderlist.get(i).getItemTypeId())) {
//                        isOrderMatched = true;
//                        position = i;
//                        break;
//                    }
//                }

                if (orderlist.size() > 0) {
                    for (int i = 0; i < orderlist.size(); i++) {
                        if (itemid.equals(orderlist.get(i).getItemId()) && itemtype.equals(orderlist.get(i).getItemType()) && asize.equals(orderlist.get(i).getItemTypeId())) {
                            isOrderMatched = true;
                            position = i;
                            break;
                        }
                    }
                }

                cakeitemqty = itemQty;
                Log.i("TAG", "cake item Qty " + cakeitemqty);
                if (isOrderMatched) {
                    int Qty = itemQty + Integer.parseInt(orderlist.get(position).getQty());
                    float Price = itemprice + Float.parseFloat(orderlist.get(position).getTotalAmount());

                    Log.i("TAG", "update Qty " + Qty);
                    Log.i("TAG", "update Price " + Price);
                    myDbHelper.updateOrderwithOrderId(String.valueOf(Qty), String.valueOf(Price), orderlist.get(position).getOrderId());
                } else {
                    Log.i("TAG", "catid" + catid);

                    Log.i("TAG", "inserting new record");
                    HashMap<String, String> values = new HashMap<>();
                    values.put("itemId", itemid);
                    values.put("itemTypeId", asize);
                    values.put("subCategoryId", subcatid);
                    values.put("additionals", "");
                    values.put("qty", String.valueOf(itemQty));
                    values.put("price", String.valueOf(aprice));
                    values.put("additionalsPrice", "");
                    values.put("additionalsTypeId", "");
                    values.put("totalAmount", String.valueOf(itemprice));
                    values.put("comment", Constants.COMMENTS);
                    values.put("status", "1");
                    values.put("creationDate", "14/07/2015");
                    values.put("modifiedDate", "14/07/2015");
                    values.put("categoryId", String.valueOf(catid));
                    values.put("itemName", sItemName);
                    values.put("itemNameAr", sItemNameAr);
                    values.put("image", sItemImage);
                    values.put("item_desc", sItemDesc);
                    values.put("item_desc_Ar", sItemDescAr);
                    values.put("sub_itemName", "");
                    values.put("sub_itemName_Ar", "");
                    values.put("sub_itemImage", "");
                    values.put("ItemType", itemtype);
                    values.put("sizename", asizeid);
                    values.put("ItemTypeName_ar", asize_ar);

                    myDbHelper.insertOrder(values);
                }
                orderlist.clear();
                orderlist = myDbHelper.getItemOrderInfo(itemid);

                Log.i("TAG", "orderlist size " + orderlist.size());

                try {
                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
                    CakeInnerActivity1.orderPrice.setText("" + decimalFormat.format(myDbHelper.getItemOrderPrice1(itemid)) + " SR");
                    CakeInnerActivity1.orderQuantity.setText("" + myDbHelper.getItemOrderCount(itemid));
                    CakeInnerActivity1.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mcakeitemadapter = new CakeInsidelistAdapter(getApplicationContext(), orderlist, language, CakeInnerActivity1.this);
                mcakelist.setAdapter(mcakeitemadapter);
                mcakeitemadapter.notifyDataSetChanged();
                mscrollview.smoothScrollTo(0, 0);

                itemQty = 1;
                itemprice = aprice;
                qty.setText("1");
                price.setText(decimalFormat.format(Float.parseFloat(String.valueOf(itemprice))));

            }
        });


        addMoreLayout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(CakeInnerActivity1.this, CategoriesListActivity.class);
                startActivity(a);
            }
        });

        checkOut.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                if (mcakeitemadapter.isEmpty()) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "Please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CakeInnerActivity1.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else {

                    Intent a = new Intent(CakeInnerActivity1.this, CheckoutActivity.class);
                    startActivity(a);
                }

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CakeInnerActivity1.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
//                     set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

//                     set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                } else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CakeInnerActivity1.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a = new Intent(CakeInnerActivity1.this, CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
                                        PastriesSelection.orderQuantity.setText("-");
                                        PastriesSelection.orderPrice.setText("-");
                                        PastriesSelection.mcount_basket.setText("0");
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
                                        PastriesSelection.orderQuantity.setText("-");
                                        PastriesSelection.orderPrice.setText("-");
                                        PastriesSelection.mcount_basket.setText("0");
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a = new Intent(CakeInnerActivity1.this, CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
