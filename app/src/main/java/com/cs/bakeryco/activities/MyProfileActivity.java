package com.cs.bakeryco.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 14-09-2016.
 */
public class MyProfileActivity extends AppCompatActivity {
    TextView name, email, feedback, editProfile, mgender;
    EditText mobile;
    LinearLayout manageAddress;
    RelativeLayout changePassword;
    Button logout;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String response;
    String mLoginStatus;
    View line;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    String mmobile, mpassword;
    DataBaseHelper myDbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.myprofile_layout);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.myprofile_layout_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");
//        userId = userPrefs.getString("userId", null);
        response = userPrefs.getString("user_profile", null);
        mpassword = userPrefs.getString("password", null);

        myDbHelper = new DataBaseHelper(MyProfileActivity.this);

        manageAddress = (LinearLayout) findViewById(R.id.manage_address);
        name = (TextView) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.phone);
        changePassword = (RelativeLayout) findViewById(R.id.change_password);
        logout = (Button) findViewById(R.id.logout);
        editProfile = (TextView) findViewById(R.id.edit_profile);
        mgender = findViewById(R.id.gender);

        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                mmobile = userObjuect.getString("mobile");
                name.setText(userObjuect.getString("fullName") + " " + userObjuect.getString("family_name") + "\n" + userObjuect.getString("nick_name"));
                mobile.setText("+" + userObjuect.getString("mobile"));
                email.setText(userObjuect.getString("email"));
                mgender.setText(userObjuect.getString("gender"));
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }


        manageAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MyProfileActivity.this, MyAddressActivity.class);
                startActivity(i);

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPrefEditor.clear();
                userPrefEditor.commit();
                myDbHelper.deleteOrderTable();
                if (language.equalsIgnoreCase("En")) {
                    logout.setText("Login");
                } else if (language.equalsIgnoreCase("Ar")) {
                    logout.setText("دخول");
                }
                onBackPressed();

            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, ChangePassword.class);
                startActivity(intent);
            }
        });


        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyProfileActivity.this, EditProfile.class);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MyProfileActivity.this);
            dialog = ProgressDialog.show(MyProfileActivity.this, "",
                    "Update profile...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MyProfileActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(MyProfileActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MyProfileActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                                if(language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("Invalid Mobile / Password")
//                                            .setCancelable(false)
//                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }else if(language.equalsIgnoreCase("Ar")){
//                                    // set title
//                                    alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("Invalid Mobile / Password")
//                                            .setCancelable(false)
//                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(MyProfileActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("TAG", "mobile number " + mmobile);
        Log.i("TAG", "password " + mpassword);
        Log.i("TAG", "token " + SplashActivity.regid);
        Log.i("TAG", "Language " + language);

        new CheckLoginDetails().execute(Constants.LOGIN_URL + mmobile + "?psw=" + mpassword + "&dtoken=" + SplashActivity.regid + "&lan=" + language);

        response = userPrefs.getString("user_profile", null);
        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                name.setText(userObjuect.getString("fullName") + " " + userObjuect.getString("family_name") + "\n" + userObjuect.getString("nick_name"));
                mobile.setText("+" + userObjuect.getString("mobile"));
                email.setText(userObjuect.getString("email"));
                mgender.setText(userObjuect.getString("gender"));
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }
}
