package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.ItemBoxAdapter;
import com.cs.bakeryco.adapters.ItemBoxGridAdapter;
import com.cs.bakeryco.adapters.ViewPagerAdapter1;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubItems;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemsBoxActivity extends AppCompatActivity {

    SharedPreferences languagePrefs;
    String language;
    TextView box_title, box_price, add;
    public static TextView box_count;
    RelativeLayout back_btn;
    RecyclerView box_list;
    ArrayList<ItemsList.Price> priceList = new ArrayList<>();
    ArrayList<ItemsList.SubItems> subItemsList = new ArrayList<>();

    ArrayList<ItemsList.Sections> sections = new ArrayList<>();
    int selectionpos, childpos;
    DataBaseHelper myDbHelper;
    TextView bag_count;
    ViewPager grid_layout;
    ImageView type_of_views;

    public static String title_name;
    ItemBoxAdapter mAdapter;
    ItemBoxGridAdapter itemBoxGridAdapter;
    public static int no_of_items;

    public static int qty = 0, Qty;
    public static float orderPrice;
    public static float priceTxt = 0;

    boolean mtype_of_view = false;

    public static ArrayList<String> boxpos = new ArrayList<>();
    public static ArrayList<Integer> selectedCount = new ArrayList<>();
    public static ArrayList<ItemsList.SubItems> selectedItems = new ArrayList<ItemsList.SubItems>();
    public static ArrayList<String> subitemQty = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.items_box_selector);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.items_box_selector_arabic);
        }

        selectedCount.clear();
        boxpos.clear();
        selectedItems.clear();
        subitemQty.clear();

        subItemsList = (ArrayList<ItemsList.SubItems>) getIntent().getSerializableExtra("SubItems");
        priceList = (ArrayList<ItemsList.Price>) getIntent().getSerializableExtra("pricelist");
        title_name = getIntent().getStringExtra("title_name");
        no_of_items = getIntent().getIntExtra("no_of_items", 0);
        sections = (ArrayList<ItemsList.Sections>) getIntent().getSerializableExtra("section_list");
        selectionpos = getIntent().getIntExtra("section_pos", 0);
        childpos = getIntent().getIntExtra("child_pos", 0);

        myDbHelper = new DataBaseHelper(ItemsBoxActivity.this);

        back_btn = findViewById(R.id.back_btn);
        box_title = findViewById(R.id.box_title);
        box_count = findViewById(R.id.box_count);
        box_price = findViewById(R.id.box_price);
        box_list = findViewById(R.id.box_items);
        add = findViewById(R.id.add);
        bag_count = findViewById(R.id.bag_count);

        type_of_views = findViewById(R.id.type_of_views);

        grid_layout = findViewById(R.id.grid_layout);

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        for (int k = 0; k < subItemsList.size(); k++) {
            selectedCount.add(0);
        }


        box_title.setText("" + title_name);

        if (language.equalsIgnoreCase("En")) {
            box_count.setText("Quantity : 0 / " + no_of_items);
        } else {
            box_count.setText("الكمية : 0 / " + no_of_items);
        }
        double number;
        float price = Float.parseFloat(priceList.get(0).getPrice());
        number = price;
        box_price.setText("" + Constants.decimalFormat.format(number) + " SR");



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ItemsBoxActivity.this);
        box_list.setLayoutManager(linearLayoutManager);

        type_of_views.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mtype_of_view) {

                    mtype_of_view = false;
                    type_of_views.setImageDrawable(getResources().getDrawable(R.drawable.list_view));
                    box_list.setVisibility(View.GONE);
                    grid_layout.setVisibility(View.VISIBLE);
                    itemBoxGridAdapter = new ItemBoxGridAdapter(ItemsBoxActivity.this, subItemsList, ItemsBoxActivity.this, language);
                    grid_layout.setAdapter(itemBoxGridAdapter);

                } else {

                    mtype_of_view = true;
                    type_of_views.setImageDrawable(getResources().getDrawable(R.drawable.grid_view));
                    grid_layout.setVisibility(View.GONE);
                    box_list.setVisibility(View.VISIBLE);
                    mAdapter = new ItemBoxAdapter(ItemsBoxActivity.this, subItemsList, language);
                    box_list.setAdapter(mAdapter);

                }

            }
        });

        mAdapter = new ItemBoxAdapter(ItemsBoxActivity.this, subItemsList, language);
        box_list.setAdapter(mAdapter);

        grid_layout.setPageMargin(10);


        itemBoxGridAdapter = new ItemBoxGridAdapter(ItemsBoxActivity.this, subItemsList, ItemsBoxActivity.this, language);
        grid_layout.setAdapter(itemBoxGridAdapter);

        grid_layout.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = grid_layout.getMeasuredWidth() - grid_layout.getPaddingLeft() - grid_layout.getPaddingRight();
                int pageHeight = grid_layout.getHeight();
                int paddingLeft = grid_layout.getPaddingLeft();
                float transformPos = (float) (page.getLeft() - (grid_layout.getScrollX() + paddingLeft)) / pageWidth;
                final float normalizedposition = Math.abs(Math.abs(transformPos) - 15);
                page.setAlpha(normalizedposition + 0.5f);
                int max = -pageHeight / 100;
                if (transformPos < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    page.setTranslationY(15);
                } else if (transformPos <= 1) { // [-1,1]
                    page.setTranslationY(max * (1 - Math.abs(transformPos)));
                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    page.setTranslationY(15);
                }
            }
        });

//        grid_layout.setCurrentItem(childpos);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (subitemQty.size() == 0) {


                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "Please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ItemsBoxActivity.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else if (qty < no_of_items) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "You have selected " + qty + " of " + no_of_items + " " + title_name + ", complete the box to proceed.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ItemsBoxActivity.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else if (sections.get(selectionpos).getItems().get(childpos).getBindingType() == 2) {

                    String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = null;
                    if (boxpos != null && boxpos.size() > 0) {
                        for (int i = 0; i < boxpos.size(); i++) {
                            if (addl_ids != null && addl_ids.length() > 0) {

                                addl_ids = addl_ids + "," + sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemId();
                                addl_name = addl_name + "," + sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName();
                                addl_name_ar = addl_name_ar + "," + sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName_Ar();
                                addl_image = addl_image + "," + sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemImage();
//                                addl_price = addl_price + "," + sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getsub();
                                int count = 0;
                                for (int j = 0; j < subitemQty.size(); j++) {
                                    if (subitemQty.get(j).equals(sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = addl_qty + "," + Integer.toString(count);

                            } else {
                                addl_ids = String.valueOf(sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemId());
                                addl_name = sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName();
                                addl_name_ar = sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName_Ar();
                                addl_image = sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemImage();
//                                addl_price = SauceList.get(0).getItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                int count = 0;
                                for (int j = 0; j < subitemQty.size(); j++) {
                                    if (subitemQty.get(j).equals(sections.get(selectionpos).getItems().get(childpos).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = Integer.toString(count);
                            }
                        }
                    }

                    Log.i("TAG", "subitemid " + addl_ids);
                    Log.i("TAG", "subitemname " + addl_name);
                    Log.i("TAG", "subitemcount " + addl_qty);

                    Log.i("TAG", "catid" + ItemsActivity.catId);
                    HashMap<String, String> values = new HashMap<>();
                    values.put("itemId", String.valueOf(sections.get(selectionpos).getItems().get(childpos).getItemId()));
                    values.put("itemTypeId", sections.get(selectionpos).getItems().get(childpos).getPrice().get(0).getSize());
                    values.put("subCategoryId", String.valueOf(sections.get(selectionpos).getItems().get(childpos).getSubCategoryId()));
                    values.put("additionals", "");
                    values.put("qty", "1");
                    values.put("price", sections.get(selectionpos).getItems().get(childpos).getPrice().get(0).getPrice());
                    values.put("additionalsPrice", "");
                    values.put("additionalsTypeId", "");
                    values.put("totalAmount", sections.get(selectionpos).getItems().get(childpos).getPrice().get(0).getPrice());
                    values.put("comment", "");
                    values.put("status", "1");
                    values.put("creationDate", "14/07/2015");
                    values.put("modifiedDate", "14/07/2015");
                    values.put("categoryId", String.valueOf(ItemsActivity.catId));
                    values.put("itemName", sections.get(selectionpos).getItems().get(childpos).getItemName());
                    values.put("itemNameAr", sections.get(selectionpos).getItems().get(childpos).getItemName_Ar());
                    values.put("image", sections.get(selectionpos).getItems().get(childpos).getItemImage());
                    values.put("item_desc", sections.get(selectionpos).getItems().get(childpos).getItemDesc());
                    values.put("item_desc_Ar", sections.get(selectionpos).getItems().get(childpos).getItemDesc_Ar());
                    values.put("sub_itemName", addl_name);
                    values.put("sub_itemName_Ar", addl_name_ar);
                    values.put("sub_itemImage", addl_image);
                    values.put("sub_item_id", addl_ids);
                    values.put("sub_item_count", addl_qty);
                    values.put("ItemType", "");
                    values.put("sizename", String.valueOf(sections.get(selectionpos).getItems().get(childpos).getPrice().get(0).getSizeId()));

                    myDbHelper.insertOrder(values);

                    Log.i("TAG", "item name " + sections.get(selectionpos).getItems().get(childpos).getItemName());
//                                        Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
//                                        startActivity(a);
                    finish();
                }

            }
        });

    }

}
