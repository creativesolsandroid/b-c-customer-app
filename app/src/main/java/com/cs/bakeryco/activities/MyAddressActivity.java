package com.cs.bakeryco.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.GPSTracker;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.MyAddressAdapter;
import com.cs.bakeryco.model.Address;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAddressActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    //    private TextView buttonHome, buttonWork, buttonOther;
    private Button buttonAddAddress;
    ArrayList<Address> myAddressArrayList = new ArrayList<>();
    String response;
    //    private ArrayList<MyAddress.Data> myAddressArrayList = new ArrayList<>();
    private MyAddressAdapter mAddressAdapter;
    private SwipeMenuListView addressList;
    private TextView addressAlert;
    private GoogleApiClient mGoogleApiClient;
    RelativeLayout backbtn;
    Toolbar toolbar;
    String userId;
    public static String mobile;
    SharedPreferences userPrefs;
    private static String TAG = "TAG";
    public static int ADD_ADDRESS_INTENT = 1;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 2;
    //    ImageView search_address;
    public static boolean toConfirmOrder;
    Context context;
    private GoogleMap mMap;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    SupportMapFragment mapFragment;
    int selectedPosition = 0;

    GPSTracker gps;
    Double lat, longi;
    private LatLng mCenterLatLong;

    String language;
    SharedPreferences languagePrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_my_address);
        } else {
            setContentView(R.layout.activity_my_address_arabic);
        }

        context = this;


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");
        mobile = userPrefs.getString("mobile", "+966");

        try {
            toConfirmOrder = getIntent().getExtras().getBoolean("confirm_order", false);
        } catch (NullPointerException npe) {
            toConfirmOrder = false;
        }

//        buttonHome = (TextView) findViewById(R.id.button_address_home);
//        buttonWork = (TextView) findViewById(R.id.button_address_work);
//        buttonOther = (TextView) findViewById(R.id.button_address_other);
        buttonAddAddress = (Button) findViewById(R.id.add_address);
        addressAlert = (TextView) findViewById(R.id.no_address_alert);
        addressList = (SwipeMenuListView) findViewById(R.id.address_list);
        backbtn = (RelativeLayout) findViewById(R.id.back_btn);
//        search_address = findViewById(R.id.search);
//        addressList.showShimmerAdapter();

        new GetAddressDetails().execute(Constants.SAVED_ADDRESS_URL + userId);

        buttonAddAddress.setOnClickListener(this);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        search_address.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openAutocompleteActivity();
//            }
//        });

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {


                // create "edit" item
                SwipeMenuItem editItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                editItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                        0xAE, 0x88)));
                // set item width
                editItem.setWidth(dp2px(90));

                if (language.equalsIgnoreCase("En")) {
                    editItem.setTitle("Edit");
                } else {
                    editItem.setTitle("تعديل");
                }
                // set item title fontsize
                editItem.setTitleSize(18);
                // set item title font color
                editItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(editItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                if (language.equalsIgnoreCase("En")) {
                    deleteItem.setTitle("Delete");
                } else if (language.equalsIgnoreCase("Ar")) {
                    deleteItem.setTitle("حذف");
                }
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        addressList.setMenuCreator(creator);

        // step 2. listener item click event
        addressList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        Intent intent = new Intent(MyAddressActivity.this, AddAddressActivity.class);
                        intent.putExtra("edit", true);
                        intent.putExtra("AddressId", myAddressArrayList.get(position).getId());
                        intent.putExtra("HouseNo", myAddressArrayList.get(position).getHouseNo());
                        intent.putExtra("HouseName", myAddressArrayList.get(position).getAddressName());
                        intent.putExtra("LandMark", myAddressArrayList.get(position).getLandmark());
                        intent.putExtra("AddressType", "Home");
                        intent.putExtra("Address", myAddressArrayList.get(position).getAddress());
                        intent.putExtra("Latitude", myAddressArrayList.get(position).getLatitude());
                        intent.putExtra("Longitude", myAddressArrayList.get(position).getLongitude());
                        startActivityForResult(intent, ADD_ADDRESS_INTENT);
                        break;
                    case 1:

                        selectedPosition = position;

                        if (language.equalsIgnoreCase("En")) {
                            showTwoButtonAlertDialog(context.getResources().getString(R.string.delete_alert),
                                    context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.yes),
                                    context.getResources().getString(R.string.no), MyAddressActivity.this);
                        } else {
                            showTwoButtonAlertDialog(context.getResources().getString(R.string.delete_alert_ar),
                                    context.getResources().getString(R.string.app_name_ar), context.getResources().getString(R.string.yes_ar),
                                    context.getResources().getString(R.string.no_ar), MyAddressActivity.this);
                        }
                        break;
                }
                return false;
            }
        });
    }

    public void showTwoButtonAlertDialog(String descriptionStr, String titleStr, String positiveStr, String negativeStr, final Activity context) {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = 0;
        if (language.equalsIgnoreCase("En")) {
            layout = R.layout.alert_dialog;
        } else {
            layout = R.layout.alert_dialog_arabic;
        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        title.setText(titleStr);
        yes.setText(positiveStr);
        no.setText(negativeStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
                new DeleteAddress().execute(Constants.DELETE_ADDRESS_URL + myAddressArrayList.get(selectedPosition).getId());
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    private void openAutocompleteActivity() {
        Log.d(TAG, "openAutocompleteActivity: ");
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            e.printStackTrace();
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_address:
                Intent intent = new Intent(MyAddressActivity.this, AddAddressActivity.class);
                startActivityForResult(intent, ADD_ADDRESS_INTENT);
                break;

//            case R.id.button_address_home:
//                buttonHome.setBackground(getResources().getDrawable(R.drawable.my_address_left_shape_selected));
//                buttonWork.setBackground(getResources().getDrawable(R.drawable.my_address_middle_shape_unselected));
//                buttonOther.setBackground(getResources().getDrawable(R.drawable.my_address_right_shape_unselected));
//                break;
//
//            case R.id.button_address_work:
//                buttonHome.setBackground(getResources().getDrawable(R.drawable.my_address_left_shape_unselected));
//                buttonWork.setBackground(getResources().getDrawable(R.drawable.my_address_middle_shape_selected));
//                buttonOther.setBackground(getResources().getDrawable(R.drawable.my_address_right_shape_unselected));
//                break;
//
//            case R.id.button_address_other:
//                buttonHome.setBackground(getResources().getDrawable(R.drawable.my_address_left_shape_unselected));
//                buttonWork.setBackground(getResources().getDrawable(R.drawable.my_address_middle_shape_unselected));
//                buttonOther.setBackground(getResources().getDrawable(R.drawable.my_address_right_shape_selected));
//                break;
        }
    }


    public class GetAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MyAddressActivity.this);
            dialog = ProgressDialog.show(MyAddressActivity.this, "",
                    "Loading address...");
            myAddressArrayList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(MyAddressActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyAddressActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (result.equals("")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MyAddressActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MyAddressActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    Address oh = new Address();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String Id = jo1.getString("Id");
                                    String Address = jo1.getString("Address");
                                    String AddressType = jo1.getString("AddressType");
                                    String HouseNo = jo1.getString("HouseNo");
                                    String LandMark = jo1.getString("LandMark");
                                    String Longitude = jo1.getString("Longitude");
                                    String Latitude = jo1.getString("Latitude");
                                    String houseName = jo1.getString("HouseName");


                                    oh.setId(Id);
                                    oh.setAddress(Address);
                                    oh.setAddressType(AddressType);
                                    oh.setHouseNo(HouseNo);
                                    oh.setLandmark(LandMark);
                                    oh.setLatitude(Latitude);
                                    oh.setLongitude(Longitude);
                                    oh.setAddressName(houseName);

                                    myAddressArrayList.add(oh);

                                }
                            } catch (JSONException je) {
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }

                            if (myAddressArrayList.size() > 0) {
                                mAddressAdapter = new MyAddressAdapter(MyAddressActivity.this, myAddressArrayList, MyAddressActivity.this, language);
                                addressList.setAdapter(mAddressAdapter);
                                mAddressAdapter.notifyDataSetChanged();
                                addressAlert.setVisibility(View.GONE);
                            } else {
                                addressAlert.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(MyAddressActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyAddressActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            if (myAddressArrayList.size() > 0) {
//                addAddress.setText("Add a new address");
            }
            mAddressAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


    public class DeleteAddress extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MyAddressActivity.this);
            dialog = ProgressDialog.show(MyAddressActivity.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", "user response:" + response);
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(MyAddressActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyAddressActivity.this, getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (result.equals("")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MyAddressActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MyAddressActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            new GetAddressDetails().execute(Constants.SAVED_ADDRESS_URL + userId);
                            Toast.makeText(MyAddressActivity.this, "Address deleted successfully", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(MyAddressActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MyAddressActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(MyAddressActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyAddressActivity.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


//    private class DeleteAddressApi extends AsyncTask<String, Integer, String> {
//
////        ACProgressFlower loaderDialog;
//        //        AlertDialog  = null;
//        String inputStr;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareDeleteAddressJson();
////            loaderDialog = new ACProgressFlower.Builder(context)
////                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
////                    .themeColor(Color.WHITE)
////                    .fadeColor(Color.DKGRAY).build();
////            loaderDialog.show();
////            Constants.showLoadingDialog(MyMyAddressActivity.this);
////            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
////            // ...Irrelevant code for customizing the buttons and title
////            LayoutInflater inflater = activity.getLayoutInflater();
////            int layout = R.layout.loder_dialog;
////            View dialogView = inflater.inflate(layout, null);
////            dialogBuilder.setView(dialogView);
////            dialogBuilder.setCancelable(true);
////
////            loaderDialog = dialogBuilder.create();
////            loaderDialog.show();
////            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
////            Window window = loaderDialog.getWindow();
////            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////            lp.copyFrom(window.getAttributes());
////            //This makes the dialog take up the full width
////            Display display = activity.getWindowManager().getDefaultDisplay();
////            Point size = new Point();
////            display.getSize(size);
////            int screenWidth = size.x;
////
////            double d = screenWidth*0.85;
////            lp.width = (int) d;
////            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
////            window.setAttributes(lp);
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<AddAddress> call = apiService.addAddress(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<AddAddress>() {
//                @Override
//                public void onResponse(Call<AddAddress> call, Response<AddAddress> response) {
//                    if(response.isSuccessful()){
//                        AddAddress addressResponse = response.body();
//                        try {
//                            if(addressResponse.getStatus()){
//                                myAddressArrayList.remove(selectedPosition);
//                                mAddressAdapter.notifyDataSetChanged();
//                            }
//                            else {
//                                if (language.equalsIgnoreCase("En")) {
//                                    String failureResponse = addressResponse.getMessage();
//                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
//                                            context.getResources().getString(R.string.ok), MyMyAddressActivity.this);
//                                } else {
//                                    String failureResponse = addressResponse.getMessageAr();
//                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name_ar),
//                                            context.getResources().getString(R.string.ok_ar), MyMyAddressActivity.this);
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            if (language.equalsIgnoreCase("En")) {
//                                Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//                    else{
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<AddAddress> call, Throwable t) {
//                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//          Constants.closeLoadingDialog();
//        }
//    }
//
//    private String prepareDeleteAddressJson(){
//        JSONObject addressObj = new JSONObject();
//
//        try {
//            addressObj.put("AddressId", myAddressArrayList.get(selectedPosition).getAddressid());
//            addressObj.put("UserId", userId);
////            addressObj.put("HouseNo", addressArrayList.get(selectedPosition).getHouseno());
////            addressObj.put("HouseName", addressArrayList.get(selectedPosition).getAddressname());
////            addressObj.put("LandMark", addressArrayList.get(selectedPosition).getLandmark());
////            addressObj.put("AddressType", "Home");
////            addressObj.put("Address", strAddress);
////            addressObj.put("Latitude", lat);
////            addressObj.put("Longitude", longi);
//            addressObj.put("IsActive", false);
//
//            Log.d(TAG, "prepareDeleteAddressJson: "+addressObj.toString());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return addressObj.toString();
//    }
//
//    private class GetMyAddressApi extends AsyncTask<String, Integer, String> {
//
//        String inputStr;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareDisplayProfileJSON();
////            dialog = new ACProgressFlower.Builder(MyMyAddressActivity.this)
////                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
////                    .themeColor(Color.WHITE)
////                    .fadeColor(Color.DKGRAY).build();
////            dialog.show();
//            Constants.showLoadingDialog(MyMyAddressActivity.this);
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            APIInterface apiService =
//                    ApiClient.getClient().create(APIInterface.class);
//
//            Call<MyAddress> call = apiService.myAddress(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<MyAddress>() {
//                @Override
//                public void onResponse(Call<MyAddress> call, Response<MyAddress> response) {
//                    if(response.isSuccessful()){
//                        MyAddress myAddress = response.body();
//                        if(myAddress.getStatus()){
//                            myAddressArrayList = myAddress.getData();
////                            if(dialog != null){
////                                dialog.hide();
////                            }
//                            if(myAddressArrayList.size() > 0) {
//                                mAddressAdapter = new MyAddressAdapter(MyMyAddressActivity.this, myAddressArrayList, MyMyAddressActivity.this, language);
//                                addressList.setAdapter(mAddressAdapter);
//                                mAddressAdapter.notifyDataSetChanged();
//                                addressAlert.setVisibility(View.GONE);
//                            }
//                            else {
//                                addressAlert.setVisibility(View.VISIBLE);
//                            }
//                        }
//                        else{
//                            addressAlert.setVisibility(View.VISIBLE);
//                            if (language.equalsIgnoreCase("En")) {
//                                String failureResponse = myAddress.getMessage();
//                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
//                                        getResources().getString(R.string.ok), MyMyAddressActivity.this);
//                            } else {
//                                String failureResponse = myAddress.getMessageAr();
//                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
//                                        getResources().getString(R.string.ok_ar), MyMyAddressActivity.this);
//                            }
//                        }
//                    }
//                    else{
//                        addressAlert.setVisibility(View.VISIBLE);
//                        Toast.makeText(MyMyAddressActivity.this, response.message(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    Constants.closeLoadingDialog();
//                }
//
//                @Override
//                public void onFailure(Call<MyAddress> call, Throwable t) {
//                    final String networkStatus = NetworkUtil.getConnectivityStatusString(MyMyAddressActivity.this);
//                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(MyMyAddressActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(MyMyAddressActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(MyMyAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(MyMyAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    addressAlert.setVisibility(View.VISIBLE);
//                    Constants.closeLoadingDialog();
//                }
//            });
//            return "";
//        }
//    }
//
//    private String prepareDisplayProfileJSON(){
//        JSONObject parentObj = new JSONObject();
//
//        try {
//            parentObj.put("UserId",userId);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return parentObj.toString();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_ADDRESS_INTENT && resultCode == RESULT_OK) {
//            Collections.reverse(myAddressArrayList);
//            myAddressArrayList.addAll((ArrayList<MyAddress.Data>)data.getSerializableExtra("newAddress"));
//            Collections.reverse(myAddressArrayList);
//            mAddressAdapter.notifyDataSetChanged();
//            addressList.scrollTo(0,0);
            new GetAddressDetails().execute(Constants.SAVED_ADDRESS_URL + userId);
        }
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(context, data);

                // TODO call location based filter
                LatLng latLong;
                latLong = place.getLatLng();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLong).zoom(15f).build();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(context, data);
        } else if (resultCode == RESULT_CANCELED) {

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        } else {
            getGPSCoordinates();
        }

        View mapView = mapFragment.getView();
        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent())
                .findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(MyAddressActivity.this);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
        } catch (Exception npe) {
            npe.printStackTrace();
        }
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();

                LatLng latLng = new LatLng(lat, longi);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15f));
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(MyAddressActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(MyAddressActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MyAddressActivity.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    @Override
    protected void onDestroy() {
//        Constants.closeLoadingDialog();
        super.onDestroy();
    }
}
