package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.SpecialCakeItemsAdapter;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class SpecialCakeItemsActivity extends AppCompatActivity {

    DataBaseHelper myDbHelper;
    RelativeLayout back_btn;
    ImageView bag;
    TextView bag_count, item_title;;
    SharedPreferences languagePrefs;
    String language;

    RelativeLayout mpicker1;
    SpecialCakeItemsAdapter mAdapter;
    ViewPager scrollView;
    DotsIndicator dotsIndicator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.special_cake_items);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.special_cake_items_arabic);
        }

        myDbHelper = new DataBaseHelper(SpecialCakeItemsActivity.this);

        back_btn = (RelativeLayout) findViewById(R.id.back_btn);
        bag = (ImageView) findViewById(R.id.bag);
        bag_count = (TextView) findViewById(R.id.bag_count);
        mpicker1 = (RelativeLayout) findViewById(R.id.picker1);
        item_title = (TextView) findViewById(R.id.item_title);
        scrollView = (ViewPager) findViewById(R.id.picker);
        dotsIndicator = (DotsIndicator) findViewById(R.id.dots_indicator);


        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(SpecialCakeItemsActivity.this, OrderConfirmationNew.class);
                startActivity(a);

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        mAdapter = new SpecialCakeItemsAdapter(SpecialCakeItemsActivity.this, language);
        scrollView.setAdapter(mAdapter);
//        scrollView.setAnimationEnabled(true);
        scrollView.setPageMargin(10);
//        scrollView.setFadeEnabled(false);
//        scrollView.setFadeFactor(0.6f);
//        scrollView.setCurrentItem(position1);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int view_pager_hieght = width - 10;

        int main_hieght = width - 10;

        int size_with = width - 100;

        Log.i("TAG", "main_hieght: " + main_hieght);
        Log.i("TAG", "view_pager_hieght: " + view_pager_hieght);

//        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mlayout.getLayoutParams();
//        layoutParams.height = main_hieght;
//        mlayout.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) mpicker1.getLayoutParams();
        layoutParams1.height = view_pager_hieght;
        mpicker1.setLayoutParams(layoutParams1);

        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) scrollView.getLayoutParams();
        layoutParams2.height = view_pager_hieght;
        scrollView.setLayoutParams(layoutParams2);

        scrollView.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = scrollView.getMeasuredWidth() - scrollView.getPaddingLeft() - scrollView.getPaddingRight();
                int pageHeight = scrollView.getHeight();
                int paddingLeft = scrollView.getPaddingLeft();
                float transformPos = (float) (page.getLeft() - (scrollView.getScrollX() + paddingLeft)) / pageWidth;
                final float normalizedposition = Math.abs(Math.abs(transformPos) - 1);
                page.setAlpha(normalizedposition + 0.5f);
                int max = -pageHeight / 10;
                if (transformPos < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    page.setTranslationY(0);
                } else if (transformPos <= 1) { // [-1,1]
                    page.setTranslationY(max * (1 - Math.abs(transformPos)));
                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    page.setTranslationY(0);
                }
            }
        });

        dotsIndicator.setViewPager(scrollView);

    }

}
