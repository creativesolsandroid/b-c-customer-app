package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.Dialogs.VerifyOtpDialog;
import com.cs.bakeryco.JSONParser;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 14-09-2016.
 */
public class LoginActivity extends AppCompatActivity {
    private static final int REGISTRATION_REQUEST = 1;
    private static final int VERIFICATION_REQUEST = 2;
    TextView loginBtn;
    TextView forgotPwd;
    //    LinearLayout signUp;
//    Toolbar toolbar;
    String response;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    //    public static String password, moblieNumber;
    TextInputLayout inputLayoutMobile, inputLayoutPassword;
    String strMobile, strPassword;
    EditText inputPassword;
    EditText inputMobile;

    TextView sign_up_now;
    RelativeLayout back_btn;
    public static boolean login_OTP = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.login_screen);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.login_screen_arabic);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

//        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        inputMobile = (com.bachors.prefixinput.EditText) findViewById(R.id.mobile_number);
        inputPassword = (EditText) findViewById(R.id.password);
        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.layout_password);
//        signUp = (LinearLayout) findViewById(R.id.signup_btn);
        forgotPwd = (TextView) findViewById(R.id.forgot_pwd);
        loginBtn = (TextView) findViewById(R.id.login_btn);
        sign_up_now = (TextView) findViewById(R.id.sign_up_now);
        back_btn = (RelativeLayout) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        if (language.equalsIgnoreCase("En")) {

            String name = getColoredSpanned("New user ?", "#000000");
            String surName = getColoredSpanned("Signup now", "#426085");

            sign_up_now.setText(Html.fromHtml(name + " " + surName));

        } else {

            String name = getColoredSpanned("مستخدم جديد؟", "#000000");
            String surName = getColoredSpanned(" سجل الآن", "#426085");

            sign_up_now.setText(Html.fromHtml(name + " " + surName));

        }



        sign_up_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(a);

            }
        });

        inputMobile.setText(Constants.Country_Code);
        inputMobile.setCursorVisible(false);

        inputMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputMobile.setCursorVisible(true);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userSignIn(v);

            }
        });

        inputMobile.addTextChangedListener(new TextWatcher(inputMobile));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));

//        signUp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
//                startActivityForResult(intent, REGISTRATION_REQUEST);
//            }
//        });

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPassword.class);
                intent.putExtra("pass", strPassword);
                intent.putExtra("mobile", strMobile);
                startActivity(intent);
            }
        });
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

//    private void setTypeface() {
//        inputMobile.setTypeface(Constants.getTypeFace(context));
//        inputPassword.setTypeface(Constants.getTypeFace(context));
//    }

    public void userSignIn(View view) {
        if (validations()) {
            String networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new CheckLoginDetails().execute(Constants.LOGIN_URL + "966" + strMobile + "?psw=" + strPassword + "&dtoken=" + SplashActivity.regid + "&lan=" + language);
            } else {
                if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private boolean validations() {
        strMobile = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();
        strMobile = strMobile.replace("+966 ", "");

        if (strMobile.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
            inputLayoutMobile.setError("Please enter mobile number");
            } else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, LoginActivity.this);
            return false;
        } else if (strMobile.length() != 9) {
            if (language.equalsIgnoreCase("En")) {
            inputLayoutMobile.setError("Please enter valid mobile number");
            } else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, LoginActivity.this);
            return false;
        } else if (strPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
            inputLayoutPassword.setError("Please enter password");
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, LoginActivity.this);
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, LoginActivity.this);
            return false;
        } else if (strPassword.contains(" ")) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutMobile.setError("Spaces are not allowed");
            } else {
                inputLayoutMobile.setError("Spaces are not allowed");
            }

            Constants.requestEditTextFocus(inputPassword, LoginActivity.this);
            return false;

        }
        return true;
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.mobile_number:
                    inputMobile.setCursorVisible(true);
//                    if (language.equalsIgnoreCase("En")) {
                        String enteredMobile = editable.toString();
                        if (!enteredMobile.contains(Constants.Country_Code)) {
                            if (enteredMobile.length() > Constants.Country_Code.length()) {
                                enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                                inputMobile.setText(Constants.Country_Code + enteredMobile);
                            } else {
                                inputMobile.setText(Constants.Country_Code);
                            }
                            inputMobile.setSelection(inputMobile.length());
                        }
//                    }
                    clearErrors();
                    break;
                case R.id.password:
                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    private void clearErrors() {
        inputLayoutMobile.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REGISTRATION_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == VERIFICATION_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (resultCode == RESULT_CANCELED) {
//            Toast.makeText(LoginActivity.this, "Registration unseccessfull", Toast.LENGTH_SHORT).show();
        }
    }

    private void showVerifyDialog(){
        Bundle args = new Bundle();
//        args.putString("name", firstName);
//        args.putString("email", email);
        args.putString("mobile", strMobile);
//        args.putString("password", password);
//        args.putString("otp", serverOtp);

        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "login");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if(newFragment!=null){
                    newFragment.dismiss();

                    if(!login_OTP){
                        Constants.requestEditTextFocus(inputMobile, LoginActivity.this);
                        inputMobile.setSelection(inputMobile.length());
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                        }
                        setResult(RESULT_OK);
                        finish();
                    }
                }
            }
        });
    }

    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            dialog = ProgressDialog.show(LoginActivity.this, "",
                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoginActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LoginActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);
                                    userPrefEditor.putString("password", strPassword);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    if (isVerified) {
                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();
//                                        Intent loginIntent = new Intent();
                                        setResult(RESULT_OK);
                                        finish();
                                    } else {
                                        showVerifyDialog();
//                                        finish();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("BAKERY & Co.");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Invalid mobile / password")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بيكري آند كومباني");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Invalid mobile / password")
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LoginActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}
