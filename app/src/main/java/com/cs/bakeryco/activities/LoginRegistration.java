package com.cs.bakeryco.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.cs.bakeryco.R;

public class LoginRegistration extends AppCompatActivity {

    TextView login, registration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_registration);

        login = findViewById(R.id.login);
        registration = findViewById(R.id.registration);

        login.setBackground(getResources().getDrawable(R.drawable.user_module_btn_unselected_bg));
        registration.setBackground(getResources().getDrawable(R.drawable.user_module_btn_unselected_bg));

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login.setBackground(getResources().getDrawable(R.drawable.user_module_btn_bg));

                Intent intent = new Intent(LoginRegistration.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });

        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registration.setBackground(getResources().getDrawable(R.drawable.user_module_btn_bg));

                Intent intent = new Intent(LoginRegistration.this, RegistrationActivity.class);
                startActivity(intent);
                finish();

            }
        });


    }

}
