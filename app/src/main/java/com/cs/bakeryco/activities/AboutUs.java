package com.cs.bakeryco.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.MainActivity;
import com.cs.bakeryco.R;
import com.cs.bakeryco.widgets.TextViewEx;

import me.biubiubiu.justifytext.library.JustifyTextView;


/**
 * Created by CS on 11-07-2016.
 */
public class AboutUs extends Fragment {

    TextView aboutusTxt, aboutuslessTxt;
    TextView view_more;
    RelativeLayout back_btn;
    TextViewEx text8;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences userPrefs;
    String mLoginStatus;
    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    LinearLayout catering_layout, address_layout, phone, phone1, track_order_layout;
    Typeface boldTypeface;
    String desc_ar = "بدأ مصنع بيكري آند كومباني بتجهيز منتجات الخبز والحلويات بإستخدام أفضل الموارد المتاحة. حيث يتم تجهيز المعجنات الأصلية يدوياً بالإضافة إلى الساندويتشات والكيكات التي تحضر بأجود المكونات؛ جميع المكونات صحية وطبيعية 100% حيث تستخدم أجود أنواع الدقيق والبيض الطازج وكريمة الزبدة ومنتجات الألبان وقصب السكر النقي والشوكولاتة البيضاء والداكنة بالإضافة إلى أشهى الحشوات وخلطات الفواكه الطبيعية وأفضل أنواع الفانيليا والمنكهات.\n" +
            "n\"" +
            "واليوم يعد مصنع بيكري آند كومباني أحد أهم المصانع التي تنتج آلاف المنتجات يومياً. ويتم تقديم منتجات بيكري آند كومباني في أهم وأفخم المطاعم والمنشآت ليستمتع بها محبي الأطعمة ومتذوقيها.\n" +
            "n\"" +
            "لقد قطعنا أشواطاً طويلة، ولكن هنالك العديد من الثوابت التي لا يمكن أن تتغير، سنظل دائماً ملتزمين باستخدام أجود المكونات وحريصين على استمرارية وتميز منتجاتنا، بالإضافة إلى حرصنا وتفانينا في خدمة عملائنا المميزين وتقديم أجود وأفضل المنتجات لهم.";
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.about_us, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.about_us_arabic, container,
                    false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        if (language.equalsIgnoreCase("Ar")) {
            boldTypeface = Typeface.createFromAsset(getActivity().getAssets(),
                    "helveticaneue_regular.ttf");
        }

        String text = "<HTML><BODY><p align=\\\"justify\\\" style=\"text-align: justify !important;> Bakery & Company began baking confectionary products in low volume bakery facilities. The original handmade pastries, cakes and sandwiches have always used the finest quality. 100 percent natural healthy ingredients: Highest quality flour, Fresh whole grade A eggs, butter creams and dairy products, pure cane sugar, dark and white chocolates, fresh fruit purees and fillings, the world's finest vanilla and essences.</p> <p align=\\\"justify\\\">Today, B&C is a high volume bakery that turns out thousands of products daily. At one point or another, B&C food products are served at the best restaurants and establishments, just waiting to be enjoyed by food enthusiasts and connoisseurs.</p><p align=\\\"justify\\\"> We've come a long way, but there are some things that haven't changed. We will always use the finest quality ingredients and maintain consistency, dedication, and passion to our bakery products for all valued clients.</p></BODY></HTML>";


        if (language.equalsIgnoreCase("Ar")) {
            ((TextView) rootView.findViewById(R.id.aboutus_txt)).setTypeface(boldTypeface);
        }

        if (language.equalsIgnoreCase("En")) {
            aboutusTxt = (TextView) rootView.findViewById(R.id.aboutus_txt);
            aboutuslessTxt = (TextView) rootView.findViewById(R.id.aboutus_less_txt);
        } else if (language.equalsIgnoreCase("Ar")) {
//            text8 = rootView.findViewById(R.id.aboutus_txt);
        }
        view_more = rootView.findViewById(R.id.view_more);
        back_btn = rootView.findViewById(R.id.back_btn);

        catering_layout = rootView.findViewById(R.id.catering_layout);
        address_layout = rootView.findViewById(R.id.address_layout);
        track_order_layout = rootView.findViewById(R.id.track_order_layout);
        phone = rootView.findViewById(R.id.phone);
        phone1 = rootView.findViewById(R.id.phone1);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) getActivity()).menuClick();

            }
        });

        track_order_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent a = new Intent(getActivity(), TrackOrderActivity.class);
                    a.putExtra("orderId", "-1");
                    startActivity(a);
                    getActivity().finish();
                } else {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                }

            }
        });

        catering_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(getActivity(), ImageLayout.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("title", "Catering");
                } else {
                    a.putExtra("title", "التموين وخدمات الطعام");
                }
                startActivity(a);

            }
        });

        address_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String geoUri = "http://maps.google.com/maps?q=loc:" + 24.533966 + "," + 46.899342 + " (" + "Bakery & Company" + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);

            }
        });

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + "966593133334"));
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + "966593133334"));
                    startActivity(intent);
                }

            }
        });

        phone1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + "966593133334"));
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + "966593133334"));
                    startActivity(intent);
                }

            }
        });

        view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (view_more.getText().toString().equalsIgnoreCase("Show More")) {
//
//                    view_more.setText("Show Less");
//                    aboutusTxt.setVisibility(View.VISIBLE);
//                    aboutuslessTxt.setVisibility(View.GONE);
//
//                } else {
//
//                    view_more.setText("Show More");
//                    aboutusTxt.setVisibility(View.GONE);
//                    aboutuslessTxt.setVisibility(View.VISIBLE);
//
//                }

                Intent a = new Intent(getActivity(), AboutUsTxt.class);
                startActivity(a);

            }
        });

//        if (language.equalsIgnoreCase("En")) {
//            aboutusTxt.set(Paint.Align.LEFT);
//        }else if (language.equalsIgnoreCase("Ar")) {
//            aboutusTxt.setAlignment(Paint.Align.RIGHT);
//        }
//            aboutusTxt.setTextColor(Color.parseColor("#6B727C"));

//      aboutusTxt.setLineSpacing(4);

        return rootView;
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+966593133334"));
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), "Call phone permission denied, unable to make call", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Call phone permission denied, unable to make call", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

}
