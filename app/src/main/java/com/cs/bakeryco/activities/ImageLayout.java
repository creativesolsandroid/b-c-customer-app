package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.R;

public class ImageLayout extends AppCompatActivity {

//    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;
    ImageView image;
    RelativeLayout backbtn;
    TextView text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.image_layout);
        } else {
            setContentView(R.layout.image_layout_arabic);
        }

//        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        image = findViewById(R.id.imageview);
        text = findViewById(R.id.header_title);
        backbtn = findViewById(R.id.back_btn);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        if (language.equalsIgnoreCase("En")) {
            if (getIntent().getExtras().getString("title").equals("Occasions")){
                text.setText(getIntent().getExtras().getString("title"));
                image.setImageResource(R.drawable.baking_soon);
            }
        }else if (language.equalsIgnoreCase("Ar")) {
            if (getIntent().getExtras().getString("title").equals("مناسبات")){
                text.setText(getIntent().getExtras().getString("title"));
                image.setImageResource(R.drawable.baking_soon);
            }
        }

        if (language.equalsIgnoreCase("En")) {
            if (getIntent().getExtras().getString("title").equals("Catering")){
                text.setText(getIntent().getExtras().getString("title"));
                image.setImageResource(R.drawable.catering1);
            }
        }else if (language.equalsIgnoreCase("Ar")) {
            if (getIntent().getExtras().getString("title").equals("التموين وخدمات الطعام")){
                text.setText(getIntent().getExtras().getString("title"));
                image.setImageResource(R.drawable.catering1);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
