package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.CheckoutAdapter;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.Order;
import com.cs.bakeryco.widgets.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by CS on 14-09-2016.
 */
public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;


    public static TextView orderPrice, orderQuantity, mcount_basket, netTotal, vatAmount, amount;
    ListView listView;
    RelativeLayout ordernowLayout, addMoreLayout, recieptLayout;
    TextView vatPercent, receipt_close;
    ImageView minvoice;
    float vat = 5;
    Toolbar toolbar;

    AlertDialog alertDialog;
    String favNameTxt;
    String userId;
    public static int CHECKOUT_REQUEST = 1;

    ProgressDialog dialog;

    String ItemType;

    RelativeLayout mcount;
    CheckoutAdapter mAdapter;
    SharedPreferences languagePrefs;
    SharedPreferences userPrefs;
    String language;

    TextView totalcount;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.checkout_layout);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.checkout_layout_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "-1");

        myDbHelper = new DataBaseHelper(CheckoutActivity.this);

        orderList = myDbHelper.getOrderInfo();

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
//        setSupportActionBar(toolbar);
////        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice = (ImageView) findViewById(R.id.invoice);

        orderPrice = (TextView) findViewById(R.id.item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);
        addMoreLayout = (RelativeLayout) findViewById(R.id.addmore_layout);
//        ordernowLayout = (RelativeLayout) findViewById(R.id.ordernow_layout);
        listView = (ListView) findViewById(R.id.order_info_list);

        mcount = (RelativeLayout) findViewById(R.id.count);
        mcount_basket = (TextView) findViewById(R.id.total_count);

        mAdapter = new CheckoutAdapter(CheckoutActivity.this, orderList, language);
        listView.setAdapter(mAdapter);

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
        double number;
        number = myDbHelper.getTotalOrderPrice();
        DecimalFormat decim = new DecimalFormat("0.00");
        amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
        float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
        vatAmount.setText("" + decim.format(tax));
        netTotal.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
        number = myDbHelper.getTotalOrderPrice();
        orderPrice.setText("" + decim.format(myDbHelper.getTotalOrderPrice() + tax));
        Log.i("TAG", "orderprice1 " + decim.format(myDbHelper.getTotalOrderPrice() + tax));
        orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
        ordernowLayout.setOnClickListener(this);
        addMoreLayout.setOnClickListener(this);
        Log.i("TAG","user id "+userId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addmore_layout:
                Intent intent = new Intent(CheckoutActivity.this, OrderFragment.class);
//                intent.putExtra("startWith",1);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
//            case R.id.clear_order:
//                AlertDialog.Builder alertDialogBuilder1 = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//
//                if(language.equalsIgnoreCase("En")) {
//                    // set title
//                    alertDialogBuilder1.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder1
//                            .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear")
//                            .setCancelable(false)
//                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    myDbHelper.deleteOrderTable();
//                                    try {
//                                        CheckoutActivity.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        OrderFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                    } catch (NullPointerException e) {
//                                        e.printStackTrace();
//                                    }
//                                    dialog.dismiss();
//                                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
//                                    intent.putExtra("startWith",2);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            });
//                }else if(language.equalsIgnoreCase("Ar")){
//                    // set title
//                    alertDialogBuilder1.setTitle("اوريجانو");
//
//                    // set dialog message
//                    alertDialogBuilder1
//                            .setMessage(" منتج في الحقيبة ، من خلال هذا الإجراء ستصبح حقيبتك خالية من المنتجا ت" +myDbHelper.getTotalOrderQty()+"  لديك  ")
//                            .setCancelable(false)
//                            .setNegativeButton("لا", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                            .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    myDbHelper.deleteOrderTable();
//                                    try {
//                                        CheckoutActivity.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CheckoutActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        OrderFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                        CategoryItems.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                                        CategoryItems.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                                    } catch (NullPointerException e) {
//                                        e.printStackTrace();
//                                    }
//                                    dialog.dismiss();
//                                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
//                                    intent.putExtra("startWith",2);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            });
//                }
//
//
//                // create alert dialog
//                AlertDialog alertDialog1 = alertDialogBuilder1.create();
//
//                // show it
//                alertDialog1.show();
//                break;
//            case R.id.ordernow_layout:
//                if (myDbHelper.getTotalOrderQty() == 0) {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("There is no items in your order? To proceed checkout please add the items")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
//
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
//                    Intent menuIntent = new Intent(CheckoutActivity.this, OrderTypeActivity.class);
//                    startActivity(menuIntent);
//                }
//                break;

        }

    }

    public class InsertOrder extends AsyncTask<String, String, String> {
        URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        String currentTime;
        String kitchenStart;
        String estdate, esttime;
        String estimatedTime, total_amt;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";
        String mamount, mvatamount;


        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CheckoutActivity.this);
            dialog = ProgressDialog.show(CheckoutActivity.this, "",
                    "Please wait...");

            Calendar c = Calendar.getInstance();
            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
            dateFormat1.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
            df3.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));

            currentTime = df3.format(c.getTime()).replace("a.m", "AM")
                    .replace("p.m", "PM").replace("am", "AM")
                    .replace("pm", "PM").replace("AM.", "AM")
                    .replace("PM.", "PM");
            estimatedTime = dateFormat1.format(c.getTime()).replace("a.m", "AM")
                    .replace("p.m", "PM").replace("am", "AM")
                    .replace("pm", "PM").replace("AM.", "AM")
                    .replace("PM.", "PM");

            total_amt = netTotal.getText().toString().replace(" SR", "");
            mamount = Constants.decimalFormat.format(Float.parseFloat(String.valueOf(myDbHelper.getTotalOrderPrice()))).replace("0,00", "0.00");

            float tax = myDbHelper.getTotalOrderPrice() * (vat / 100);
            mvatamount = Constants.decimalFormat.format(tax);

            try {
                JSONArray mainItem = new JSONArray();
                JSONArray subItem = new JSONArray();
                JSONArray promoItem = new JSONArray();

                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                String version = pInfo.versionName;

                JSONObject mainObj = new JSONObject();
                mainObj.put("StoreId", "1");
                mainObj.put("IsFavorite", true);
                mainObj.put("FavoriteName", favNameTxt);
                mainObj.put("UserId", userId);
                mainObj.put("OrderDate", currentTime);
                mainObj.put("PaymentMode", "0");
                mainObj.put("ExpectedTime", estimatedTime);
                mainObj.put("Device_token", SplashActivity.regid);
                mainObj.put("OrderStatus", "Close");
                mainObj.put("Total_Price", total_amt);
                mainObj.put("Comments", "Android v" + version);
                mainObj.put("OrderType", Constants.ORDER_TYPE);
                mainObj.put("VatPercentage", "5");
                mainObj.put("SubTotal", Constants.convertToArabic(mamount));
                mainObj.put("VatCharges", Constants.convertToArabic(mvatamount));

                mainItem.put(mainObj);

                for (Order order : orderList) {
                    JSONObject subObj = new JSONObject();

//                    JSONArray subItem1 = new JSONArray();
//                    JSONArray subItem2 = new JSONArray();
                    if (order.getItemType().equals("Whole")) {
                        ItemType = String.valueOf(2);
                    } else if (order.getItemType().equals("Sliced")) {
                        ItemType = String.valueOf(1);
                    }
                    subObj.put("ItemPrice", Constants.convertToArabic(Constants.decimalFormat.format(Float.parseFloat(order.getPrice()))));
                    subObj.put("Qty", order.getQty());
                    subObj.put("Comments", order.getComment());
                    subObj.put("ItemId", order.getItemId());
                    subObj.put("Size", order.getSizename());
                    subObj.put("ItemType", ItemType);

                    JSONArray subsubItems = new JSONArray();

                    if (order.getSub_itemId() != null) {
                        String ids = order.getSub_itemId();
                        String qty = order.getSub_itemcount();
                        List<String> idsList = Arrays.asList(ids.split(","));
                        List<String> qtyList = Arrays.asList(qty.split(","));
                        for (int i = 0; i < idsList.size(); i++) {
                            if (!idsList.get(i).equals("")) {
                                JSONObject subsubObj = new JSONObject();
                                subsubObj.put("OrderSubItemId", idsList.get(i));
                                subsubObj.put("Qty", qtyList.get(i));
                                subsubItems.put(subsubObj);
                            }
                        }
                    }

                    subObj.put("SubSubItems", subsubItems);
                    subItem.put(subObj);
                }

                parent.put("MainItem", mainItem);
                parent.put("SubItem", subItem);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {
                        Log.e("TAG", parent.toString());
                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.INSERT_ORDER_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(parent.toString());

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            Log.d("Responce", "" + result);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {

                    String order_number = "";
                    try {
                        JSONObject jo = new JSONObject(result);
                        myDbHelper.deleteOrderTable();
                        Intent intent = new Intent(CheckoutActivity.this, OrderFragment.class);
                        startActivity(intent);

                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                }
            }
            dialog.dismiss();
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    protected void onResume() {
        super.onResume();
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "-1");
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckoutActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = null;
                if (language.equalsIgnoreCase("En")) {
                    dialogView = inflater.inflate(R.layout.dialog_save_order, null);
                } else if (language.equalsIgnoreCase("Ar")) {
                    dialogView = inflater.inflate(R.layout.dialog_save_order_ar, null);
                }
//                View dialogView = inflater.inflate(R.layout.cancel_order_dialog, null);
                dialogBuilder.setView(dialogView);

                final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                TextView cancelBtn = (TextView) dialogView.findViewById(R.id.cancel);
                final TextView clearOrder = (TextView) dialogView.findViewById(R.id.order_clear);
                final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                TextView cancelAlertText = (TextView) dialogView.findViewById(R.id.cancel_alert_text);
                if (language.equalsIgnoreCase("En")) {
                    cancelAlertText.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                } else if (language.equalsIgnoreCase("Ar")) {
                    cancelAlertText.setText(" منتج في الحقيبة ، من خلال هذا الإجراء ستصبح حقيبتك خالية من المنتجا ت" + myDbHelper.getTotalOrderQty() + "  لديك  ");
                }

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                clearOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDbHelper.deleteOrderTable();
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        Intent intent = new Intent(CheckoutActivity.this, OrderFragment.class);
                        startActivity(intent);
                    }
                });

                Log.i("TAG","user id1 "+userId);
                saveOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        favNameTxt = favName.getText().toString();
                        if (favNameTxt.length() > 0) {
                            alertDialog.dismiss();
                            if (userId.equals("-1")) {
                                Log.i("TAG","user id2 "+userId);
                                Intent intent = new Intent(CheckoutActivity.this, LoginActivity.class);
                                startActivityForResult(intent, CHECKOUT_REQUEST);
                            } else {
                                Log.i("TAG","user id3 "+userId);
                                new InsertOrder().execute();
                            }
                        }
                    }
                });

                favName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        favNameTxt = favName.getText().toString();
                        if (favNameTxt.length() > 0) {
                            saveOrder.setTextColor(Color.parseColor("#2A8AEE"));
                        } else {
                            saveOrder.setTextColor(Color.parseColor("#555555"));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);


//                if (myDbHelper.getTotalOrderQty() == 0) {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("There is no items in your order? To proceed checkout please add the items")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
////                     create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                } else {
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(CheckoutActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
//                                .setCancelable(false)
//                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        myDbHelper.deleteOrderTable();
////                                        CheckoutActivity.orderQuantity.setText("0");
////                                        CheckoutActivity.orderPrice.setText("0.00SR");
////                                        CheckoutActivity.mcount_basket.setText("0");
////                                        CategoriesListActivity.orderQuantity.setText("0");
////                                        CategoriesListActivity.orderPrice.setText("0.00SR");
////                                        CategoriesListActivity.mcount_basket.setText("0");
////                                        OrderFragment.orderQuantity.setText("0");
////                                        OrderFragment.orderPrice.setText("0.00SR");
////                                        OrderFragment.mcount_basket.setText("0");
//                                        Intent a = new Intent(CheckoutActivity.this, OrderFragment.class);
//                                        startActivity(a);
//                                        finish();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
//                        alertDialogBuilder.setTitle("بيكري آند كومباني");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
//                                .setCancelable(false)
//                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        myDbHelper.deleteOrderTable();
////                                        CheckoutActivity.orderQuantity.setText("0");
////                                        CheckoutActivity.orderPrice.setText("0.00SR");
////                                        CheckoutActivity.mcount_basket.setText("0");
////                                        CategoriesListActivity.orderQuantity.setText("0");
////                                        CategoriesListActivity.orderPrice.setText("0.00SR");
////                                        CategoriesListActivity.mcount_basket.setText("0");
////                                        OrderFragment.orderQuantity.setText("0");
////                                        OrderFragment.orderPrice.setText("0.00SR");
////                                        OrderFragment.mcount_basket.setText("0");
//                                        Intent a = new Intent(CheckoutActivity.this, OrderFragment.class);
//                                        startActivity(a);
//                                        finish();
//                                    }
//                                });
//                        alertDialogBuilder
//                                .setNegativeButton("لا", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
////
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }

            }
        });

//        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
    }
}
