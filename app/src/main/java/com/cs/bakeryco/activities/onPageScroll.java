package com.cs.bakeryco.activities;

interface onPageScroll {
    void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);
}
