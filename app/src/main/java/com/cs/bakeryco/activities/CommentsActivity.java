package com.cs.bakeryco.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;

/**
 * Created by CS on 23-06-2016.
 */
public class CommentsActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText commentBox;
    TextView addButton, itemName;
    ImageView itemIcon;
    String itemId, title, size;
    View rootView;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.comments_layout);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.comments_layout_arabic);
        }
        myDbHelper = new DataBaseHelper(CommentsActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        itemId = getIntent().getExtras().getString("itemId");
        title = getIntent().getExtras().getString("title");
        size = getIntent().getExtras().getString("size");
        commentBox = (EditText) findViewById(R.id.comment_box);
        addButton = (TextView) findViewById(R.id.add_comment_button);
        itemName = (TextView) findViewById(R.id.item_name);
        itemIcon = (ImageView) findViewById(R.id.item_icon);

        if(getIntent().getExtras().getString("screen").equals("food")){
            commentBox.setText(getIntent().getExtras().getString("comment"));
        }else {
            if(Constants.COMMENTS.length()>1) {
                commentBox.setText(Constants.COMMENTS);
            }
            else{
                commentBox.setText(getIntent().getExtras().getString("comment"));
            }
        }

//        else {
////            size = "1";
//            commentBox.setText(Constants.COMMENTS);
//        }
        itemName.setText(title);
//        itemIcon.setImageResource(R.drawable.child_image);
//        Glide.with(CommentsActivity.this).load("http://85.194.94.241/OreganoServices/images/"+getIntent().getExtras().getString("itemImage")).into(itemIcon);
        Glide.with(CommentsActivity.this).load(Constants.IMAGE_URL+getIntent().getExtras().getString("itemImage")).into(itemIcon);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = commentBox.getText().toString();
                if(getIntent().getExtras().getString("screen").equals("food")){
                    myDbHelper.updateComment(getIntent().getExtras().getString("orderId"), text, size);
                }else if(getIntent().getExtras().getString("screen").equals("item")){
                    Constants.COMMENTS = text;
                }


                onBackPressed();
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
