package com.cs.bakeryco.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.Constants;
import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.ViewPagerAdapter1;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.Section;

import java.util.ArrayList;
import java.util.HashMap;

import static com.cs.bakeryco.activities.ItemsActivity.indivialcount;
import static com.cs.bakeryco.activities.ItemsActivity.itemfinalQty;

public class ItemsIndiviualActivity extends AppCompatActivity {

    SharedPreferences languagePrefs;
    String language;
    RelativeLayout back_btn;
//    LinearLayout cake_layout;
//    ImageView plus, mins;
//    View view;
    TextView item_title, item_name, item_price, item_desc, total_price, add;
//    EditText mqty;
//    CheckBox check1, check2;
    int price_pos;
//    TextView cake_name1, cake_name2, cake_desc1, cake_desc2;
//    LinearLayout cake_name_layout1, cake_name_layout2, cake_img_layout1, cake_img_layout2, cake_desc_layout1, cake_desc_layout2;
    ViewPager viewPager;
    ViewPagerAdapter1 mAdapter;
    String enteredString;
    DataBaseHelper myDbHelper;

    boolean cake_whole = true, cake_sliced = true;

    String itemtype;

    ArrayList<ItemsList.Sections> sections = new ArrayList<>();
    int pos, value;
    public static String Image, desc, mitemname, desc_ar, mitemname_ar, cat_name, cake_type_id;
    int qty, Qty;
    float orderPrice;
    float priceTxt = 0;
//    EditText comment;
    TextView bag_count;
    ImageView bag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.item_individual_pop_up);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.item_individual_pop_up_arabic);
        }

        myDbHelper = new DataBaseHelper(ItemsIndiviualActivity.this);

        item_title = findViewById(R.id.item_title);
        viewPager = findViewById(R.id.view_pager);
//        cake_layout = findViewById(R.id.cake_layout);
//        view = findViewById(R.id.view);
//        plus = findViewById(R.id.plus);
//        mins = findViewById(R.id.mins);
//        mqty = findViewById(R.id.qty);
//        item_name = findViewById(R.id.item_name);
//        item_price = findViewById(R.id.item_price);
//        item_desc = findViewById(R.id.item_desc);
//        total_price = findViewById(R.id.total_price);
//        add = findViewById(R.id.add);
        back_btn = findViewById(R.id.back_btn);
        bag = findViewById(R.id.bag);

//        comment = findViewById(R.id.comment);
//
//        check1 = findViewById(R.id.check1);
//        check2 = findViewById(R.id.check2);
//
//        cake_name1 = findViewById(R.id.cake_name1);
//        cake_name2 = findViewById(R.id.cake_name2);
//
//        cake_desc1 = findViewById(R.id.cake_desc1);
//        cake_desc2 = findViewById(R.id.cake_desc2);
//
//        cake_name_layout1 = findViewById(R.id.cake_name_layout1);
//        cake_name_layout2 = findViewById(R.id.cake_name_layout2);
//        cake_img_layout1 = findViewById(R.id.cake_img1_layout);
//        cake_img_layout2 = findViewById(R.id.cake_img2_layout);
//        cake_desc_layout1 = findViewById(R.id.cake_desc_layout1);
//        cake_desc_layout2 = findViewById(R.id.cake_desc_layout2);
//
        bag_count = findViewById(R.id.bag_count);

        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
                startActivity(a);

            }
        });

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        sections = (ArrayList<ItemsList.Sections>) getIntent().getSerializableExtra("section");
        pos = getIntent().getIntExtra("pos", 0);
        value = getIntent().getIntExtra("value", 0);
        cat_name = getIntent().getStringExtra("cat_name");
        desc = getIntent().getStringExtra("desc");
        mitemname = getIntent().getStringExtra("itemname");
        desc_ar = getIntent().getStringExtra("desc_ar");
        mitemname_ar = getIntent().getStringExtra("itemname_ar");
        cake_type_id = getIntent().getStringExtra("cake_type_id");

        item_title.setText("" + cat_name);

        viewPager.setPageMargin(10);


//        item_name.setText("" + mitemname);
//        item_desc.setText("" + desc);

        mAdapter = new ViewPagerAdapter1(ItemsIndiviualActivity.this, sections, value, language, ItemsIndiviualActivity.this);
        viewPager.setAdapter(mAdapter);
        
        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = viewPager.getMeasuredWidth() - viewPager.getPaddingLeft() - viewPager.getPaddingRight();
                int pageHeight = viewPager.getHeight();
                int paddingLeft = viewPager.getPaddingLeft();
                float transformPos = (float) (page.getLeft() - (viewPager.getScrollX() + paddingLeft)) / pageWidth;
                final float normalizedposition = Math.abs(Math.abs(transformPos) - 15);
                page.setAlpha(normalizedposition + 0.5f);
                int max = -pageHeight / 100;
                if (transformPos < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    page.setTranslationY(15);
                } else if (transformPos <= 1) { // [-1,1]
                    page.setTranslationY(max * (1 - Math.abs(transformPos)));
                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    page.setTranslationY(15);
                }
            }
        });
//        tab.setupWithViewPager(viewPager,true);
        viewPager.setCurrentItem(pos);
//        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {
//
//                Log.e("TAG", "currentpos: " + position);
//                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                price_pos = 0;
//                qty = 1;
//
//
//                if (language.equalsIgnoreCase("En")) {
//
////                    item_name.setText(sections.get(value).getItems().get(position).getItemName());
////                    item_desc.setText(sections.get(value).getItems().get(position).getItemDesc());
//
//                    if (ItemsActivity.catId == 2) {
//
//                        if (sections.get(value).getSectionId() == 6) {
//
////                            cake_layout.setVisibility(View.GONE);
////                            view.setVisibility(View.GONE);
//
//                            double number, number1;
//                            float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                            price_pos = 0;
//                            number = price;
////                            item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
//                            priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                            number1 = priceTxt;
////                            total_price.setText("" + Constants.decimalFormat.format(number1) + " SAR");
//
//                        } else {
//
////                            cake_layout.setVisibility(View.VISIBLE);
////                            view.setVisibility(View.VISIBLE);
//
//
//                            if (cake_type_id.equals("1")) {
//
//                                itemtype = "Whole";
//
////                                cake_name_layout1.setVisibility(View.VISIBLE);
////                                cake_img_layout1.setVisibility(View.VISIBLE);
////                                cake_desc_layout1.setVisibility(View.VISIBLE);
////
////                                cake_name_layout2.setVisibility(View.GONE);
////                                cake_img_layout2.setVisibility(View.GONE);
////                                cake_desc_layout2.setVisibility(View.GONE);
////
////                                cake_name1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getSize());
////                                cake_desc1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getItemSizeDescription());
//
//                                double number, number1;
//                                float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                                price_pos = 0;
//                                number = price;
////                                item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
//                                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                                number1 = priceTxt;
////                                total_price.setText("" + Constants.decimalFormat.format(number1) + " SAR");
//
////                                check1.setChecked(true);
//
//                            } else if (cake_type_id.equals("2")) {
//
//                                itemtype = "Sliced";
//
////                                cake_name_layout1.setVisibility(View.GONE);
////                                cake_img_layout1.setVisibility(View.GONE);
////                                cake_desc_layout1.setVisibility(View.GONE);
////
////                                cake_name_layout2.setVisibility(View.VISIBLE);
////                                cake_img_layout2.setVisibility(View.VISIBLE);
////                                cake_desc_layout2.setVisibility(View.VISIBLE);
////
////                                cake_name2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getSize());
////                                cake_desc2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getItemSizeDescription());
//
//                                double number, number1;
//                                float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
//                                price_pos = 1;
//                                number = price;
////                                item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
//                                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
//                                number1 = priceTxt;
////                                total_price.setText("" + Constants.decimalFormat.format(number1) + " SAR");
//
////                                check2.setChecked(true);
//
//                            } else if (cake_type_id.equals("1,2")) {
//
////                                cake_name_layout1.setVisibility(View.VISIBLE);
////                                cake_img_layout1.setVisibility(View.VISIBLE);
////                                cake_desc_layout1.setVisibility(View.VISIBLE);
////
////                                cake_name_layout2.setVisibility(View.VISIBLE);
////                                cake_img_layout2.setVisibility(View.VISIBLE);
////                                cake_desc_layout2.setVisibility(View.VISIBLE);
//
//                                double number, number1;
//                                float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                                price_pos = 0;
//                                number = price;
////                                item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
//                                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                                number1 = priceTxt;
////                                total_price.setText("" + Constants.decimalFormat.format(number1) + " SAR");
//
////                                check1.setChecked(true);
//
//                                itemtype = "Whole";
//
////                                check1.setOnClickListener(new View.OnClickListener() {
////                                    @Override
////                                    public void onClick(View view) {
//
////                                        check2.setChecked(false);
////                                        check1.setChecked(true);
////
//                                itemtype = "Whole";
//
////                                        double number, number1;
////                                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                                price_pos = 0;
//                                number = price;
////                                        item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
////                                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                                number1 = priceTxt;
////                                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SAR");
////                            }
////                                });
//
////                                check2.setOnClickListener(new View.OnClickListener() {
////                                    @Override
////                                    public void onClick(View view) {
//
////                                        check1.setChecked(false);
////                                        check2.setChecked(true);
//
//                                        itemtype = "Sliced";
//
////                                        double number, number1;
////                                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
////                                        price_pos = 1;
//                                        number = price;
////                                        item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
////                                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(1).getPrice());
//                                        number1 = priceTxt;
////                                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SAR");
//
////                                    }
////                                });
//
//
////                                cake_name1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getSize());
////                                cake_desc1.setText("" + sections.get(value).getItems().get(pos).getPrice().get(0).getItemSizeDescription());
//
////                                cake_name2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getSize());
////                                cake_desc2.setText("" + sections.get(value).getItems().get(pos).getPrice().get(1).getItemSizeDescription());
//
//                            }
//
//                        }
//
//
//                    } else {
//
////                        view.setVisibility(View.GONE);
////                        cake_layout.setVisibility(View.GONE);
//
//                        double number, number1;
//                        float price = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                        price_pos = 0;
//                        number = price;
////                        item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
//                        priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
//                        number1 = priceTxt;
////                        total_price.setText("" + Constants.decimalFormat.format(number1) + " SAR");
//
//                    }
//
////                    mqty.setText("" + qty);
//
////                    mqty.setCursorVisible(false);
////                    mqty.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View v) {
////                            mqty.setCursorVisible(true);
////                        }
////                    });
////
////                    int maxLength = 4;
////                    mqty.setFilters(new InputFilter[]{
////                            new InputFilter.LengthFilter(maxLength)
////                    });
//
//
////                    plus.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////
////
////                            float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SAR", ""));
//////                            int localCountplus;
////
//////                            localCountplus = indivialcount.get(position);
////
//////                            if (localCountplus < 9999) {
//////                                indivialcount.set(position, (localCountplus + 1));
//////                                mqty.setText("" + (localCountplus + 1));
////                            if (qty < 9999) {
////                                qty = qty + 1;
////                                mqty.setText("" + qty);
////                                double number;
////                                Log.i("TAG", "onClickplus: " + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice()));
////                                Log.i("TAG", "piceplus: " + price_txt);
////                                price_txt = price_txt + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
////                                Log.i("TAG", "priceTxtplus: " + price_txt);
////                                number = price_txt;
////                                total_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
//////                                    mqty.setSelection(itemQty.getText().toString().length());
//////                                        AddItems(position, (localCountplus + 1));
////                            }
////
////                        }
////                    });
//
//
////                    mins.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////
////                            if (Integer.parseInt(mqty.getText().toString()) > 1) {
//////                                int localCount1;
//////                                localCount1 = indivialcount.get(position);
////
//////                                indivialcount.set(position, (localCount1 - 1));
////                                float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SAR", ""));
////
////                                qty = qty - 1;
////                                mqty.setText("" + qty);
////                                double number;
////                                Log.i("TAG", "onClickmins: " + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice()));
////                                price_txt = price_txt - Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
////                                Log.i("TAG", "priceTxtmins: " + price_txt);
////                                number = price_txt;
////                                total_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
//////                                        AddItems(this.getAdapterPosition(), (localCount1 - 1));
//////                                qty = localCount1 - 1;
////                            }
////
////                        }
////                    });
//
////                    mqty.addTextChangedListener(new TextWatcher() {
////                        @Override
////                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
////
////                        }
////
////                        @Override
////                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
////
////                        }
////
////                        @Override
////                        public void afterTextChanged(Editable editable) {
////
////                            if (mqty.getText().toString().length() > 0) {
////
////                                float price_txt = Float.parseFloat(total_price.getText().toString().replace(" SAR", ""));
////                                Log.i("TAG", "afterTextChanged: " + price_txt);
////
////                                enteredString = editable.toString();
////                                if (enteredString.startsWith("0")) {
////                                    if (enteredString.length() > 1) {
////                                        mqty.setText(enteredString.substring(1));
////                                        mqty.setSelection(mqty.length());
////                                    }
////                                }
////
////                                qty = Integer.parseInt(mqty.getText().toString());
////                                price_txt = qty * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
////                                double number;
////                                number = price_txt;
////                                total_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
////
////                            } else {
////                                mqty.setText("1");
////                                double number;
////                                priceTxt = Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
////                                number = priceTxt;
////                                total_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
////                                item_price.setText("" + Constants.decimalFormat.format(number) + " SAR");
////                            }
////                        }
////                    });
////
////
////                    Log.i("TAG", "onPageScrolled: " + indivialcount);
////                    Log.i("TAG", "onPageScrolled: " + indivialcount.size());
////
////                    add.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////
//////                            for (int i = 0; i < indivialcount.size(); i++) {
//////
//////                                AddItems(i, indivialcount.get(i));
//////
//////                            }
////
////                            Qty = qty;
////                            if (Qty == 0) {
////
////                                String message;
//////                    if(language.equalsIgnoreCase("En")) {
////                                message = "Please select atleast one item";
//////                    }
//////                    else{
//////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//////                    }
////                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.support.v7.view.ContextThemeWrapper(ItemsIndiviualActivity.this, android.R.style.Theme_Material_Light_Dialog));
//////        if(language.equalsIgnoreCase("En")) {
////                                // set title
////                                alertDialogBuilder.setTitle("BAKERY & Co.");
////
////                                // set dialog message
////                                alertDialogBuilder
////                                        .setMessage(message)
////                                        .setCancelable(false)
////                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
////                                            public void onClick(DialogInterface dialog, int id) {
////                                                dialog.dismiss();
////                                            }
////                                        });
////
////                                // create alert dialog
////                                AlertDialog alertDialog = alertDialogBuilder.create();
////
////                                // show it
////                                alertDialog.show();
////                            }
//////                            else if (itemfinalQty.size() == 0) {
//////
//////                                String message;
////////                    if(language.equalsIgnoreCase("En")) {
//////                                message = "Please select atleast one item";
////////                    }
////////                    else{
////////                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
////////                    }
//////                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.support.v7.view.ContextThemeWrapper(ItemsIndiviualActivity.this, android.R.style.Theme_Material_Light_Dialog));
////////        if(language.equalsIgnoreCase("En")) {
//////                                // set title
//////                                alertDialogBuilder.setTitle("BAKERY & Co.");
//////
//////                                // set dialog message
//////                                alertDialogBuilder
//////                                        .setMessage(message)
//////                                        .setCancelable(false)
//////                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//////                                            public void onClick(DialogInterface dialog, int id) {
//////                                                dialog.dismiss();
//////                                            }
//////                                        });
//////
//////                                // create alert dialog
//////                                AlertDialog alertDialog = alertDialogBuilder.create();
//////
//////                                // show it
//////                                alertDialog.show();
//////                            }
////                            else if (sections.get(value).getItems().get(position).getBindingType() == 1) {
////
//////                                for (int i = 0; i < sections.get(value).getItems().size(); i++) {
//////                                    int count = 0;
//////                                    for (int j = 0; j < ItemsActivity.itemQty.size(); j++) {
//////                                        if (sections.get(value).getItems().get(position).getItemId() == ItemsActivity.itemQty.get(j)) {
//////                                            count = count + 1;
//////                                            orderPrice = count * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
//////                                        }
//////                                    } //end of selected items for loop
////
////                                Log.i("TAG", "catid" + ItemsActivity.catId);
////                                if (qty > 0) {
////                                    HashMap<String, String> values = new HashMap<>();
////                                    values.put("itemId", String.valueOf(sections.get(value).getItems().get(position).getItemId()));
////                                    values.put("itemTypeId", sections.get(value).getItems().get(position).getPrice().get(0).getSize());
////                                    values.put("subCategoryId", String.valueOf(sections.get(value).getItems().get(position).getSubCategoryId()));
////                                    values.put("additionals", "");
////                                    values.put("qty", mqty.getText().toString());
////                                    values.put("price", sections.get(value).getItems().get(position).getPrice().get(0).getPrice());
////                                    values.put("additionalsPrice", "");
////                                    values.put("additionalsTypeId", "");
////                                    values.put("totalAmount", total_price.getText().toString().replace(" SAR", ""));
////                                    if (!comment.getText().toString().equalsIgnoreCase("")) {
////                                        values.put("comment", comment.getText().toString());
////                                    } else {
////                                        values.put("comment", "");
////                                    }
////                                    values.put("status", "1");
////                                    values.put("creationDate", "14/07/2015");
////                                    values.put("modifiedDate", "14/07/2015");
////                                    values.put("categoryId", String.valueOf(ItemsActivity.catId));
////                                    values.put("itemName", sections.get(value).getItems().get(position).getItemName());
////                                    values.put("itemNameAr", sections.get(value).getItems().get(position).getItemName_Ar());
////                                    values.put("image", sections.get(value).getItems().get(position).getItemImage());
////                                    values.put("item_desc", sections.get(value).getItems().get(position).getItemDesc());
////                                    values.put("item_desc_Ar", sections.get(value).getItems().get(position).getItemDesc_Ar());
////                                    values.put("sub_itemName", "");
////                                    values.put("sub_itemName_Ar", "");
////                                    values.put("sub_itemImage", "");
////                                    values.put("ItemType", "");
////                                    values.put("ItemTypeName_ar", "");
////                                    values.put("sizename", String.valueOf(sections.get(value).getItems().get(position).getPrice().get(0).getSizeId()));
////
////                                    myDbHelper.insertOrder(values);
////
////                                    Log.i("TAG", "item name " + sections.get(value).getItems().get(position).getItemName());
//////                                        Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
//////                                        startActivity(a);
////                                    finish();
////                                }
//////                                }// end of master for loop
////                            } else if (sections.get(value).getItems().get(position).getBindingType() == 3) {
////
//////                                for (int i = 0; i < sections.get(value).getItems().size(); i++) {
//////                                    int count = 0;
//////                                    for (int j = 0; j < ItemsActivity.itemQty.size(); j++) {
//////                                        if (sections.get(value).getItems().get(position).getItemId() == ItemsActivity.itemQty.get(j)) {
//////                                            count = count + 1;
//////                                            orderPrice = count * Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
//////                                        }
//////                                    } //end of selected items for loop
////
////                                Log.i("TAG", "catid" + ItemsActivity.catId);
////                                if (qty > 0) {
////                                    HashMap<String, String> values = new HashMap<>();
////                                    values.put("itemId", String.valueOf(sections.get(value).getItems().get(position).getItemId()));
////                                    values.put("itemTypeId", sections.get(value).getItems().get(position).getPrice().get(price_pos).getSize());
////                                    values.put("subCategoryId", String.valueOf(sections.get(value).getItems().get(position).getSubCategoryId()));
////                                    values.put("additionals", "");
////                                    values.put("qty", mqty.getText().toString());
////                                    values.put("price", sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
////                                    values.put("additionalsPrice", "");
////                                    values.put("additionalsTypeId", "");
////                                    values.put("totalAmount", total_price.getText().toString().replace(" SAR", ""));
////                                    if (!comment.getText().toString().equalsIgnoreCase("")) {
////                                        values.put("comment", comment.getText().toString());
////                                    } else {
////                                        values.put("comment", "");
////                                    }
////                                    values.put("status", "1");
////                                    values.put("creationDate", "14/07/2015");
////                                    values.put("modifiedDate", "14/07/2015");
////                                    values.put("categoryId", String.valueOf(ItemsActivity.catId));
////                                    values.put("itemName", sections.get(value).getItems().get(position).getItemName());
////                                    values.put("itemNameAr", sections.get(value).getItems().get(position).getItemName_Ar());
////                                    values.put("image", sections.get(value).getItems().get(position).getItemImage());
////                                    values.put("item_desc", sections.get(value).getItems().get(position).getItemDesc());
////                                    values.put("item_desc_Ar", sections.get(value).getItems().get(position).getItemDesc_Ar());
////                                    values.put("sub_itemName", "");
////                                    values.put("sub_itemName_Ar", "");
////                                    values.put("sub_itemImage", "");
////                                    values.put("ItemType", itemtype);
////                                    values.put("sizename", String.valueOf(sections.get(value).getItems().get(position).getPrice().get(price_pos).getSizeId()));
////                                    values.put("ItemTypeName_ar", sections.get(value).getItems().get(position).getPrice().get(price_pos).getSizeName_Ar());
////
////                                    myDbHelper.insertOrder(values);
////
////                                    Log.i("TAG", "item name " + sections.get(value).getItems().get(position).getItemName());
//////                                        Intent a = new Intent(ItemsIndiviualActivity.this, OrderConfirmationNew.class);
//////                                        startActivity(a);
////                                    finish();
////                                }
//////                                }// end of master for loop
////                            }
////
////                        }
////                    });
////
////
////                } else if (language.equalsIgnoreCase("Ar")) {
////
////                    item_name.setText(sections.get(value).getItems().get(position).getItemName_Ar());
////                    item_desc.setText(sections.get(value).getItems().get(position).getItemDesc_Ar());
////                    item_price.setText("" + Constants.decimalFormat.format(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice()) + " SAR");
////
////                    mqty.setText("" + indivialcount.get(position));
//////                            }
//////                        }
////
////
////                    plus.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////
////                            int localCountplus;
//////                                    if (finalcount){
//////                                        localCountplus = PastriesSelection.counts.get(position);
//////                                    }else {
////                            localCountplus = indivialcount.get(position);
//////                                    }
////
////                            if (localCountplus < 9999) {
//////                                        if (finalcount) {
//////                                            PastriesSelection.counts.set(position, (localCountplus + 1));
//////                                        }else {
////                                indivialcount.set(position, (localCountplus + 1));
//////                                        }
////                                mqty.setText("" + (localCountplus + 1));
//////                                    mqty.setSelection(itemQty.getText().toString().length());
////                                qty = localCountplus + 1;
//////                                        AddItems(position, (localCountplus + 1));
////                            }
////
////                        }
////                    });
////
////                    mins.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////
////                            if (Integer.parseInt(mqty.getText().toString()) > 0) {
////                                int localCount1;
//////                                        if (finalcount) {
//////                                            localCount1 = PastriesSelection.counts.get(this.getAdapterPosition());
//////                                            PastriesSelection.counts.set(this.getAdapterPosition(), (localCount1 - 1));
//////                                        }else {
////                                localCount1 = indivialcount.get(position);
//////                                            PastriesSelection.selectedCountIndividual.set(this.getAdapterPosition(), (localCount1 - 1));
////                                PastriesSelection.indivialcount.set(position, (localCount1 - 1));
//////                                        }
////                                mqty.setText("" + (localCount1 - 1));
//////                                    itemQty.setSelection(itemQty.getText().toString().length());
//////                                        AddItems(this.getAdapterPosition(), (localCount1 - 1));
////                                qty = localCount1 - 1;
////                            }
////
////                        }
////                    });
////
////
////                    Log.i("TAG", "onPageScrolled: " + indivialcount);
////                    Log.i("TAG", "onPageScrolled: " + indivialcount.size());
////
////                    add.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////
////                            for (int i = 0; i < indivialcount.size(); i++) {
////
////                                PastriesSelection.selectedCountIndividual.set(i, indivialcount.get(i));
////                                AddItems(i, indivialcount.get(i));
////
////                            }
////
////                            finish();
////                        }
////                    });
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//        Log.e("TAG", "currentpos: " + viewPager.getVerticalScrollbarPosition());
    }



    private void AddItems(final int position, final int quantity) {


        int localSize = ItemsActivity.itemQty.size();
        ArrayList<String> localArray = new ArrayList<>();
        localArray.addAll(ItemsActivity.itemQty);
        for (int i = 0; i < localSize; i++) {
            if (localArray.get(i).equals(sections.get(value).getItems().get(position).getItemId())) {
                Qty = Qty - 1;
                orderPrice = orderPrice - Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
                ItemsActivity.itemQty.remove(sections.get(value).getItems().get(position).getItemId());
            } else {
//            Do nothing
            }
        }

        for (int j = 0; j < quantity; j++) {
            Qty = Qty + 1;
            orderPrice = orderPrice + Float.parseFloat(sections.get(value).getItems().get(position).getPrice().get(price_pos).getPrice());
            ItemsActivity.itemQty.add(String.valueOf(sections.get(value).getItems().get(position).getItemId()));

            Log.i("TAG", "AddItems: " + Qty);
//            plus.setClickable(true);
//            int qty = Integer.parseInt(mitemQty.getText().toString());
//            if (qty == 9999) {
//                plus.setClickable(false);
//            }
        }


        itemfinalQty.add(String.valueOf(Qty));
        ItemsActivity.itemfinalprice.add(String.valueOf(orderPrice));

//        Log.i("TAG1", "AddItems: " + PastriesSelection.itemfinalQty.size());
        Log.i("TAG1", "onResume: " + ItemsActivity.itemQty.size());
        if (itemfinalQty.size() == 0) {

            orderPrice = 0;
            Qty = 0;
//                try {
//                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                    if (Qty == 0) {
//                        orderPrice.setText("-");
//                        orderQuantity.setText("-");
//                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } else {
//                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice) + " SR");
//                        PastriesSelection.orderQuantity.setText("" + Qty);
//                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
        } else {
//                try {
//                    DecimalFormat decimalFormat = new DecimalFormat("0.00");
//                    if (.Qty == 0) {
//                        PastriesSelection.orderPrice.setText("-");
//                        PastriesSelection.orderQuantity.setText("-");
//                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    } else {
//                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(.orderPrice) + " SR");
//                        PastriesSelection.orderQuantity.setText("" + .Qty);
//                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
        }


    }

    public void showErrorDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ItemsIndiviualActivity.this, android.R.style.Theme_Material_Light_Dialog));

        alertDialogBuilder.setTitle("BAKERY & Co.");

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
