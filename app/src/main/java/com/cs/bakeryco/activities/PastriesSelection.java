package com.cs.bakeryco.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.bakeryco.DataBaseHelper;
import com.cs.bakeryco.R;
import com.cs.bakeryco.adapters.CakeListAdapter;
import com.cs.bakeryco.adapters.DropDownListAdapter;
import com.cs.bakeryco.adapters.PastriesBoxItemsAdapter;
import com.cs.bakeryco.adapters.PastriesRecyclerViewAdapter;
import com.cs.bakeryco.adapters.SectionRecyclerViewAdapter;
import com.cs.bakeryco.model.CakeDetailslist;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.MainCategories;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubItems;
import com.cs.bakeryco.widgets.CustomGridView1;
import com.cs.bakeryco.widgets.SpacesItemDecoration;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 28-08-2017.
 */

public class PastriesSelection extends AppCompatActivity implements View.OnClickListener, View.OnDragListener {

    TextView individual, box, mini, petite, boxName;
//    TextView m6PcsBox, m12PcsBox, m24PcsBox;

    public static ArrayList<Integer> positions = new ArrayList<>();
    public static ArrayList<Integer> counts = new ArrayList<>();
    public static boolean finalcount = false;

    ListView boxlist, minilist, petitelist;
    TextView Title;

    public static String title;
    //    public static TextView mNumBox;
    LinearLayout pcs_layout, mbottom_bar;

    RelativeLayout maddmore, maddmore1, mgrid_layout;
    RelativeLayout mcount;

    public static int rowSize = 2, catId, position1;
    public static TextView orderPrice, orderQuantity, mcount_basket;

    public static ArrayList<MainCategories> main_list = new ArrayList<>();

    public static ArrayList<Section> sections = new ArrayList<>();
    ArrayList<CakeDetailslist> pricelist = new ArrayList<>();
    public static ArrayList<SubItems> selectedItems = new ArrayList<SubItems>();
    ArrayList<Items> items = new ArrayList<>();

    public static ArrayList<String> boxpos = new ArrayList<>();
    public static ArrayList<Items> Boxitems = new ArrayList<>();
    public static ArrayList<Integer> selectedCount = new ArrayList<>();
    public static ArrayList<Integer> selectedCountIndividual = new ArrayList<>();
    public static ArrayList<Integer> indivialcount = new ArrayList<>();
    public static ArrayList<String> indivialname = new ArrayList<>();
    public static ArrayList<Integer> boxcount = new ArrayList<>();

    public static ArrayList<String> image = new ArrayList<>();

    public static ArrayList<String> subitemQty = new ArrayList<>();
    public static ArrayList<String> subitemfinalQty = new ArrayList<>();
    public static ArrayList<String> subitemfinalprice = new ArrayList<>();

    public static ArrayList<String> itemQty = new ArrayList<>();
    public static ArrayList<String> itemfinalQty = new ArrayList<>();
    public static ArrayList<String> itemfinalprice = new ArrayList<>();
    CustomGridView1 mGridView1;
    RecyclerView mGridView, mGridView2;
    public static GridView mBoxGridView, mGridview2;
    public static PastriesRecyclerViewAdapter mSubItemAdapter;
    DropDownListAdapter mDropDownAdapter;
    public static PastriesBoxItemsAdapter mBoxAdapter;
    ImageView fav;
    int value1 = 0;
    public static boolean isAlertShowing = false;
    public static boolean pastries = false;
    public static boolean pastries_box = false;
    public static boolean section = false;
    private static Context mContext;

    public static SectionRecyclerViewAdapter mItemAdapter;
    public static CakeListAdapter mCakeAdapter;

    LinearLayout mindividual_list_layout, mbox_list_layout, mmini_list_layout, mpetite_list_layout;

    public static boolean visible = false;

    public static ListView mindividual_list, mbox_list, mmini_list, mpetite_list;

    FrameLayout mbox_layout;

    public static ImageView qty_plus, qty_mins;

    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;

    DataBaseHelper myDbHelper;

    public static int Qty = 1;
    float orderprice;

//    String value, value1;

    boolean isordercomplete = false;

    int position, pos1;
    public static int pos = 0, totalCount = 0;
    public static int boxSize;
    public static String maincatid, sectionname;

    String no_of_items;

    public static Context getContext() {
        return mContext;

    }

    public void setContext(Context mContext) {
        PastriesSelection.mContext = mContext;
    }


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.pastries_selection);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.pastries_selection_arabic);
        }

        Log.i("TAG", "onCreate: " + itemQty.size());

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        main_list = (ArrayList<MainCategories>) getIntent().getSerializableExtra("main_list");
        sections = (ArrayList<Section>) getIntent().getSerializableExtra("list");
        title = getIntent().getStringExtra("title");
        catId = getIntent().getExtras().getInt("catId");
        position1 = getIntent().getExtras().getInt("postion");

        Log.i("TAG", "list1 " + sections);
        Log.i("TAG", "postion " + position1);
        Log.i("TAG", "catId1 " + catId);
        Log.i("TAG", "main_list1 " + main_list);
        Log.i("TAG", "cat_name1 " + title);
        Log.i("TAG", "onBindViewHolder1: " + SectionRecyclerViewAdapter.Qty);

        try {
            SectionRecyclerViewAdapter.Qty = 0;
            SectionRecyclerViewAdapter.orderPrice = 0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        pos = 0;
        totalCount = 0;
        boxSize = 0;
        Boxitems.clear();
        subitemQty.clear();
        subitemfinalprice.clear();
        subitemfinalQty.clear();
        itemQty.clear();
        itemfinalQty.clear();
        itemfinalprice.clear();
        selectedItems.clear();
        myDbHelper = new DataBaseHelper(PastriesSelection.this);

        Log.i("TAG", "onCreate1: " + itemQty.size());


        individual = (TextView) findViewById(R.id.individual);
        box = (TextView) findViewById(R.id.box);
        mini = (TextView) findViewById(R.id.mini);
        petite = (TextView) findViewById(R.id.petite);
//        mNumBox = (TextView) findViewById(R.id.box_size);
        boxName = (TextView) findViewById(R.id.box_name);
        Title = (TextView) findViewById(R.id.header_title);

        qty_plus = (ImageView) findViewById(R.id.count_plus);
        qty_mins = (ImageView) findViewById(R.id.count_mins);

        mbox_layout = (FrameLayout) findViewById(R.id.box_layout);

        mindividual_list = (ListView) findViewById(R.id.individual_list);
        mbox_list = (ListView) findViewById(R.id.box_list);
        mmini_list = (ListView) findViewById(R.id.mini_list);
        mpetite_list = (ListView) findViewById(R.id.petite_list);

        mindividual_list_layout = (LinearLayout) findViewById(R.id.individual_list_layout);
        mbox_list_layout = (LinearLayout) findViewById(R.id.box_list_layout);
        mmini_list_layout = (LinearLayout) findViewById(R.id.mini_list_layout);
        mpetite_list_layout = (LinearLayout) findViewById(R.id.petite_list_layout);
        mbottom_bar = (LinearLayout) findViewById(R.id.bottom_bar);

        orderPrice = (TextView) findViewById(R.id.item_price);
        orderQuantity = (TextView) findViewById(R.id.item_qty);
        mcount_basket = (TextView) findViewById(R.id.total_count);

        maddmore = (RelativeLayout) findViewById(R.id.addmore_layout);
        maddmore1 = (RelativeLayout) findViewById(R.id.addmore_layout1);
        mgrid_layout = (RelativeLayout) findViewById(R.id.grid_layout);
        mcount = (RelativeLayout) findViewById(R.id.count);


        try {
            Title.setText(getIntent().getStringExtra("title"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        fav = (ImageView) findViewById(R.id.fav);

        mGridView = (RecyclerView) findViewById(R.id.gridview);
        mGridView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        mGridView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
//        mGridView.addItemDecoration(new EqualSpacingItemDecoration(2)); // 16px. In practice, you'll want to use getDimensionPixelSize

        mGridView1 = (CustomGridView1) findViewById(R.id.gridview1);

        mGridView2 = (RecyclerView) findViewById(R.id.gridview2);
        mGridView2.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        mGridView2.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        mBoxGridView = (GridView) findViewById(R.id.grid_layout_box);

        pcs_layout = (LinearLayout) findViewById(R.id.pcs_layout);

//        mGridView1.setBackgroundResource(R.drawable.white_bg);
//        mgrid_layout.setBackgroundResource(R.drawable.cakebg);
//
//        mGridView1.setHorizontalSpacing(5);
//        mGridView1.setVerticalSpacing(5);

        mbottom_bar.setVisibility(View.VISIBLE);


        for (int j = 0; j < main_list.size(); j++) {
            maincatid = String.valueOf(catId);
        }
//        for (int i = 0; i < sections.size(); i++) {
//            if (sections.get(i).getSectionname().contains("Box")) {
//                sectionname = sections.get(i).getSectionname();
//
//            }
//        }

        if (sections.size() == 1) {
            position = 0;
            int pos = 0;

            if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                if (catId == 2) {
                    mbox_layout.setVisibility(View.GONE);
                    qty_plus.setVisibility(View.GONE);
                    qty_mins.setVisibility(View.GONE);
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.GONE);
                } else {

                    mbox_layout.setVisibility(View.VISIBLE);
                    qty_plus.setVisibility(View.VISIBLE);
                    qty_mins.setVisibility(View.VISIBLE);
                    subitemfinalQty.clear();
                    subitemfinalprice.clear();
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }


            }

            if (language.equalsIgnoreCase("En")) {
                individual.setText(sections.get(0).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                individual.setText(sections.get(0).getSectionname_ar());
            }

            itemQty.clear();
            try {
                if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(0).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            indivialcount.clear();
            boxcount.clear();
            selectedCountIndividual.clear();
            counts.clear();
            indivialcount.clear();
            boxcount.clear();
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.GONE);
            mini.setVisibility(View.GONE);
            petite.setVisibility(View.GONE);
            if (catId == 2) {
                mindividual_list_layout.setVisibility(View.GONE);
            } else {
                mindividual_list_layout.setVisibility(View.VISIBLE);
            }
            mbox_list_layout.setVisibility(View.GONE);
            mmini_list_layout.setVisibility(View.GONE);
            mpetite_list_layout.setVisibility(View.GONE);

        } else if (sections.size() == 2) {

            position = 1;
            pos = 0;

//            for (int i = 0; i < pos; i++) {
            if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            }

            if (language.equalsIgnoreCase("En")) {
                individual.setText(sections.get(0).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                individual.setText(sections.get(0).getSectionname_ar());
            }
            if (language.equalsIgnoreCase("En")) {
                box.setText(sections.get(1).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                box.setText(sections.get(1).getSectionname_ar());
            }

            itemQty.clear();
            try {
                if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(0).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(1).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(1).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(1).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            indivialcount.clear();
            boxcount.clear();
            selectedCountIndividual.clear();
            counts.clear();
            indivialcount.clear();
            boxcount.clear();
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.VISIBLE);
            mini.setVisibility(View.GONE);
            petite.setVisibility(View.GONE);
            mindividual_list_layout.setVisibility(View.VISIBLE);
            mbox_list_layout.setVisibility(View.VISIBLE);
            mmini_list_layout.setVisibility(View.GONE);
            mpetite_list_layout.setVisibility(View.GONE);

        } else if (sections.size() == 3) {

            position = 2;
            int pos = 0;
//            int value = ;
//            for (int i = 0; i < pos; i++) {
//                pos++;
            if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            }
            if (language.equalsIgnoreCase("En")) {
                individual.setText(sections.get(0).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                individual.setText(sections.get(0).getSectionname_ar());
            }
            if (language.equalsIgnoreCase("En")) {
                box.setText(sections.get(1).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                box.setText(sections.get(1).getSectionname_ar());
            }
            if (language.equalsIgnoreCase("En")) {
                mini.setText(sections.get(2).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                mini.setText(sections.get(2).getSectionname_ar());
            }
            itemQty.clear();
            try {
                if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(0).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(1).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(1).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(1).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(2).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(2).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(2).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            indivialcount.clear();
            boxcount.clear();
            selectedCountIndividual.clear();
            counts.clear();
            indivialcount.clear();
            boxcount.clear();
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.VISIBLE);
            mini.setVisibility(View.VISIBLE);
            petite.setVisibility(View.GONE);
            mindividual_list_layout.setVisibility(View.VISIBLE);
            mbox_list_layout.setVisibility(View.VISIBLE);
            mmini_list_layout.setVisibility(View.VISIBLE);
            mpetite_list_layout.setVisibility(View.GONE);

        } else if (sections.size() == 4) {

            position = 3;
            int pos = 0;

//            for (int i = 0; i < pos; i++) {
            if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);


            } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            } else if (sections.get(3).getChildItems().get(0).getBuildingType().equals("1")) {

                mbox_layout.setVisibility(View.GONE);
                qty_plus.setVisibility(View.GONE);
                qty_mins.setVisibility(View.GONE);
                itemfinalQty.clear();
                itemfinalprice.clear();
                maddmore.setVisibility(View.VISIBLE);
                maddmore1.setVisibility(View.GONE);

            } else if (sections.get(3).getChildItems().get(0).getBuildingType().equals("2")) {

                mbox_layout.setVisibility(View.VISIBLE);
                qty_plus.setVisibility(View.VISIBLE);
                qty_mins.setVisibility(View.VISIBLE);
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                maddmore.setVisibility(View.GONE);
                maddmore1.setVisibility(View.VISIBLE);

            }

            if (language.equalsIgnoreCase("En")) {
                individual.setText(sections.get(0).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                individual.setText(sections.get(0).getSectionname_ar());
            }
            if (language.equalsIgnoreCase("En")) {
                box.setText(sections.get(1).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                box.setText(sections.get(1).getSectionname_ar());
            }
            if (language.equalsIgnoreCase("En")) {
                mini.setText(sections.get(2).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                mini.setText(sections.get(2).getSectionname_ar());
            }
            if (language.equalsIgnoreCase("En")) {
                petite.setText(sections.get(3).getSectionname());
            } else if (language.equalsIgnoreCase("Ar")) {
                petite.setText(sections.get(3).getSectionname_ar());
            }
            itemQty.clear();
            try {
                if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(0).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(0).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(1).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(1).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(1).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(2).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(2).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(2).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(3).getChildItems().get(pos).getBuildingType().equals("1")) {

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                } else if (sections.get(3).getChildItems().get(pos).getBuildingType().equals("2")) {
                    PastriesSelection.orderPrice.setText(sections.get(3).getChildItems().get(pos).getPriceList().get(0).getPrice());
                    PastriesSelection.orderQuantity.setText(Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            indivialcount.clear();
            boxcount.clear();
            selectedCountIndividual.clear();
            counts.clear();
            indivialcount.clear();
            boxcount.clear();
            individual.performClick();
            individual.setVisibility(View.VISIBLE);
            box.setVisibility(View.VISIBLE);
            mini.setVisibility(View.VISIBLE);
            petite.setVisibility(View.VISIBLE);
            mindividual_list_layout.setVisibility(View.VISIBLE);
            mbox_list_layout.setVisibility(View.VISIBLE);
            mmini_list_layout.setVisibility(View.VISIBLE);
            mpetite_list_layout.setVisibility(View.VISIBLE);
        }

        mindividual_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                value1 = i;

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                boxpos.clear();

                if (sections.size() >= 1) {

                    position = 0;

                    boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }
                    boxpos.clear();
                    mGridView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
                    Log.i("TAG", "section size " + sections.get(position).getChildItems().get(value1).getSubItems().size());
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        position = 0;

                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();
                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name " + sections.get(0).getSectionname());
                        sectionname = sections.get(0).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);
                    }
                }
                mindividual_list.setVisibility(View.GONE);
                visible = false;


            }
        });

        mbox_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();
                if (sections.size() == 1) {

                    position = 0;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }
                    boxpos.clear();

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    Log.i("TAG", "section size " + sections.size());
                    Log.i("TAG", "section size1 " + sections.get(position).getChildItems().size());
                    Log.i("TAG", "section size2 " + sections.get(position).getChildItems().get(value1).getSubItems().size());
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 0;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name1 " + sections.get(0).getSectionname());
                        sectionname = sections.get(0).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }


                } else if (sections.size() >= 2) {

                    position = 1;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }

                    boxpos.clear();

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    Log.i("TAG", "section size " + sections.size());
                    Log.i("TAG", "section size1 " + sections.get(position).getChildItems().size());
                    Log.i("TAG", "section size2 " + sections.get(position).getChildItems().get(value1).getSubItems().size());
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 1;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(1).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(1).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(1).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(1).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }


                        sectionname = sections.get(1).getSectionname();

                        Log.i("TAG", "section name2 " + sectionname);

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }


                }
                mbox_list.setVisibility(View.GONE);
                visible = false;

            }
        });

        mmini_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();

                if (sections.size() == 1) {

                    position = 0;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }

                    boxpos.clear();

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    Log.i("TAG", "section size " + sections.get(position).getChildItems().get(value1).getSubItems().size());
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 0;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name3 " + sections.get(0).getSectionname());
//                        if (!sections.get(0).getSectionname().equals("Box")) {
                        sectionname = sections.get(0).getSectionname();

//                        }

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);
                    }

                } else if (sections.size() == 2) {

                    position = 1;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }

                    boxpos.clear();
                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    Log.i("TAG", "section size " + sections.get(position).getChildItems().get(value1).getSubItems().size());
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("2")) {
                        position = 1;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(1).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(1).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(1).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(1).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name4 " + sections.get(1).getSectionname());
                        sectionname = sections.get(1).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() >= 3) {

                    position = 2;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);

                    }

                    boxpos.clear();

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    Log.i("TAG", "section size " + sections.get(position).getChildItems().get(value1).getSubItems().size());
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(2).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 2;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(2).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(2).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(2).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(2).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name5 " + sections.get(2).getSectionname());
                        sectionname = sections.get(2).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                }
                mmini_list.setVisibility(View.GONE);
                visible = false;

            }
        });

        mpetite_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                subitemQty.clear();
                subitemfinalQty.clear();
                subitemfinalprice.clear();

                if (sections.size() == 1) {

                    position = 0;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }

                    boxpos.clear();
                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("1")) {

                    } else if (sections.get(0).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 0;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(0).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(0).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(0).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name6 " + sections.get(0).getSectionname());
                        sectionname = sections.get(0).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() == 2) {

                    position = 1;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }
                    boxpos.clear();

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("1")) {

                    } else if (sections.get(1).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 1;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(1).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(1).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(1).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(1).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name7 " + sections.get(1).getSectionname());
                        sectionname = sections.get(1).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() == 3) {

                    position = 2;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }

                    boxpos.clear();

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(2).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 2;

                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(2).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(2).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(2).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(2).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name8 " + sections.get(2).getSectionname());
                        sectionname = sections.get(2).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }

                } else if (sections.size() == 4) {

                    position = 3;

                    value1 = i;

                    for (int k = 0; k < sections.get(position).getChildItems().get(value1).getSubItems().size(); k++) {
                        selectedCount.add(0);
                        boxcount.add(0);
                    }
                    boxpos.clear();

                    mGridView1.setVisibility(View.GONE);
                    mGridView2.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    String title = getIntent().getStringExtra("title");
                    mSubItemAdapter = new PastriesRecyclerViewAdapter(getApplicationContext(), title, sections, position, value1, language, PastriesSelection.this);
                    mGridView.setAdapter(mSubItemAdapter);

                    if (sections.get(3).getChildItems().get(value1).getBuildingType().equals("2")) {

                        position = 3;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice())));
                        Qty = 1;
                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        subitemfinalQty.add(String.valueOf(Qty));
                        if (language.equalsIgnoreCase("En")) {
                            boxName.setText(sections.get(3).getChildItems().get(value1).getDisplayname());
                        } else if (language.equalsIgnoreCase("Ar")) {
                            boxName.setText(sections.get(3).getChildItems().get(value1).getDisplayname_ar());
                        }

                        no_of_items = sections.get(3).getChildItems().get(value1).getNo_of_items();

                        Noofboxes();

                        boxSize = Integer.parseInt(sections.get(3).getChildItems().get(value1).getNo_of_items());
                        selectedItems.clear();

                        for (int j = 0; j < main_list.size(); j++) {
                            maincatid = String.valueOf(catId);
                        }

                        Log.i("TAG", "section name9 " + sections.get(3).getSectionname());
                        sectionname = sections.get(3).getSectionname();

                        mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                        mBoxGridView.setAdapter(mBoxAdapter);

                    }
                }
                mpetite_list.setVisibility(View.GONE);
                visible = false;

            }
        });

        if (sections.get(position).getChildItems().get(0).getBuildingType().equals("2")) {

            if (catId == 2) {

                mGridView1.setVisibility(View.VISIBLE);
                mGridView2.setVisibility(View.VISIBLE);


            }
        }

        individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
                selectedCountIndividual.clear();
                indivialcount.clear();
                boxcount.clear();
                counts.clear();
                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }

            }
        });

        box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
                selectedCountIndividual.clear();
                counts.clear();
                indivialcount.clear();
                boxcount.clear();

                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }

            }
        });

        mini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
                selectedCountIndividual.clear();
                counts.clear();
                indivialcount.clear();
                boxcount.clear();

                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }


            }
        });

        petite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemQty.clear();
                itemfinalQty.clear();
                itemfinalprice.clear();
                selectedCountIndividual.clear();
                counts.clear();
                indivialcount.clear();
                boxcount.clear();

                if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
                    maddmore.setVisibility(View.VISIBLE);
                    maddmore1.setVisibility(View.GONE);
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
                    maddmore.setVisibility(View.GONE);
                    maddmore1.setVisibility(View.VISIBLE);
                }


            }
        });

        mGridView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pos1 = i;


            }
        });

        maddmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isordercomplete = true;

                if (SectionRecyclerViewAdapter.Qty == 0) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "Please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else if (itemfinalQty.size() == 0) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "Please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {

                    for (int i = 0; i < sections.get(position).getChildItems().size(); i++) {
                        int count = 0;
                        for (int j = 0; j < itemQty.size(); j++) {
                            if (sections.get(position).getChildItems().get(i).getItemId() == itemQty.get(j)) {
                                count = count + 1;
                                SectionRecyclerViewAdapter.orderPrice = count * Float.parseFloat(sections.get(position).getChildItems().get(i).getPriceList().get(0).getPrice());
                            }
                        } //end of selected items for loop

                        Log.i("TAG", "catid" + catId);
                        if (count > 0) {
                            HashMap<String, String> values = new HashMap<>();
                            values.put("itemId", sections.get(position).getChildItems().get(i).getItemId());
                            values.put("itemTypeId", sections.get(position).getChildItems().get(i).getPriceList().get(0).getSize());
                            values.put("subCategoryId", sections.get(position).getChildItems().get(i).getSubCatId());
                            values.put("additionals", "");
                            values.put("qty", String.valueOf(count));
                            values.put("price", sections.get(position).getChildItems().get(i).getPriceList().get(0).getPrice());
                            values.put("additionalsPrice", "");
                            values.put("additionalsTypeId", "");
                            values.put("totalAmount", String.valueOf(SectionRecyclerViewAdapter.orderPrice));
                            values.put("comment", "");
                            values.put("status", "1");
                            values.put("creationDate", "14/07/2015");
                            values.put("modifiedDate", "14/07/2015");
                            values.put("categoryId", String.valueOf(catId));
                            values.put("itemName", sections.get(position).getChildItems().get(i).getItemName());
                            values.put("itemNameAr", sections.get(position).getChildItems().get(i).getItemNameAr());
                            values.put("image", sections.get(position).getChildItems().get(i).getImage());
                            values.put("item_desc", sections.get(position).getChildItems().get(i).getDescription());
                            values.put("item_desc_Ar", sections.get(position).getChildItems().get(i).getDescriptionAr());
                            values.put("sub_itemName", "");
                            values.put("sub_itemName_Ar", "");
                            values.put("sub_itemImage", "");
                            values.put("ItemType", "");
                            values.put("sizename", sections.get(position).getChildItems().get(i).getPriceList().get(0).getSizeid());

                            myDbHelper.insertOrder(values);

                            Log.i("TAG", "item name " + sections.get(position).getChildItems().get(i).getItemName());
                            Intent a = new Intent(PastriesSelection.this, CategoriesListActivity.class);
                            startActivity(a);
                            finish();
                        }
                    }// end of master for loop
                }

            }
        });


        maddmore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isordercomplete = true;

                if (subitemQty.size() == 0) {


                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "Please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else if (totalCount < boxSize) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "You have selected " + PastriesSelection.totalCount + " of " + PastriesSelection.boxSize + " " + PastriesSelection.title + ", complete the box to proceed.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else if (Qty == 0) {

                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "Please select atleast one item";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {

                    if (Qty == 1) {
                        orderprice = Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());
                    }
                    Log.i("TAG", "order price " + orderprice);
                    Log.i("TAG", "catid" + catId);

                    String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = null;
                    if (boxpos != null && boxpos.size() > 0) {
                        for (int i = 0; i < boxpos.size(); i++) {
                            if (addl_ids != null && addl_ids.length() > 0) {

                                addl_ids = addl_ids + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemid();
                                addl_name = addl_name + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName();
                                addl_name_ar = addl_name_ar + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName_Ar();
                                addl_image = addl_image + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemimage();
//                                addl_price = addl_price + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getsub();
                                int count = 0;
                                for (int j = 0; j < subitemQty.size(); j++) {
                                    if (subitemQty.get(j).equals(sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = addl_qty + "," + Integer.toString(count);

                            } else {
                                addl_ids = sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemid();
                                addl_name = sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName();
                                addl_name_ar = sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName_Ar();
                                addl_image = sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemimage();
//                                addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                int count = 0;
                                for (int j = 0; j < subitemQty.size(); j++) {
                                    if (subitemQty.get(j).equals(sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName())) {
                                        count = count + 1;
                                    }
                                }
                                addl_qty = Integer.toString(count);
                            }
                        }
                    }

                    Log.i("TAG", "subitemid " + addl_ids);
                    Log.i("TAG", "subitemname " + addl_name);
                    Log.i("TAG", "subitemcount " + addl_qty);

                    HashMap<String, String> values = new HashMap<>();
                    values.put("itemId", sections.get(position).getChildItems().get(value1).getItemId());
                    values.put("itemTypeId", sections.get(position).getChildItems().get(value1).getPriceList().get(0).getSize());
                    values.put("subCategoryId", sections.get(position).getChildItems().get(value1).getSubCatId());
                    values.put("additionals", "");
                    values.put("qty", String.valueOf(Qty));
                    values.put("price", sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());
                    values.put("additionalsPrice", "");
                    values.put("additionalsTypeId", "");
                    values.put("totalAmount", String.valueOf(orderprice));
                    values.put("comment", "");
                    values.put("status", "1");
                    values.put("creationDate", "14/07/2015");
                    values.put("modifiedDate", "14/07/2015");
                    values.put("categoryId", String.valueOf(catId));
                    values.put("itemName", sections.get(position).getChildItems().get(value1).getItemName());
                    values.put("itemNameAr", sections.get(position).getChildItems().get(value1).getItemNameAr());
                    values.put("image", sections.get(position).getChildItems().get(value1).getImage());
                    values.put("item_desc", sections.get(position).getChildItems().get(value1).getDescription());
                    values.put("item_desc_Ar", sections.get(position).getChildItems().get(value1).getDescriptionAr());
                    values.put("sub_itemName", addl_name);
                    values.put("sub_itemName_Ar", addl_name_ar);
                    values.put("sub_itemImage", addl_image);
                    values.put("sub_item_id", addl_ids);
                    values.put("sub_item_count", addl_qty);
                    values.put("ItemType", "");
                    values.put("sizename", sections.get(position).getChildItems().get(value1).getPriceList().get(0).getSizeid());

                    myDbHelper.insertOrder(values);

                    Log.i("TAG", "item name list" + sections.get(position).getChildItems().get(value1).getItemName());
                    Log.i("TAG", "order price " + orderprice);

                    Intent a = new Intent(PastriesSelection.this, CategoriesListActivity.class);
                    startActivity(a);
                    finish();
                }

            }
        });

        qty_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subitemfinalQty.size() == 0) {

                    orderprice = 0;
                    Qty = 1;

                    Qty = Qty + 1;
                    orderprice = Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice()) * Qty;

                    subitemfinalQty.add(String.valueOf(Qty));
                    subitemfinalprice.add(String.valueOf(orderprice));

                    try {
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice));
                        PastriesSelection.orderQuantity.setText("" + Qty);
                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        Log.i("TAG", "order price " + orderprice);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    Qty = Qty + 1;
                    orderprice = Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice()) * Qty;

                    subitemfinalQty.add(String.valueOf(Qty));
                    subitemfinalprice.add(String.valueOf(orderprice));

                    try {
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice));
                        PastriesSelection.orderQuantity.setText("" + Qty);
                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        Log.i("TAG", "order price1 " + orderprice);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        });

        qty_mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subitemfinalQty.size() == 0) {

                    if (Qty > 1) {

                        orderprice = 0;
                        Qty = 1;

                        Qty = Qty - 1;
                        orderprice = orderprice - Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());

                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        try {
                            double number;
                            number = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim = new DecimalFormat("0.00");
//                            OrderFragment.orderPrice.setText("" + decim.format(number));
//                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            double number1;
                            number1 = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim1 = new DecimalFormat("0.00");
//                            CategoriesListActivity.orderPrice.setText("" + decim1.format(number));
//                            CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        try {
                            DecimalFormat decimalFormat = new DecimalFormat("0.00");

                            PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice));
                            PastriesSelection.orderQuantity.setText("" + Qty);
                            PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                            Log.i("TAG", "order price2 " + orderprice);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {

                    if (Qty > 1) {

                        Qty = Qty - 1;
                        orderprice = orderprice - Float.parseFloat(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());

                        orderQuantity.setText("" + Qty);
                        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                        try {
                            double number;
                            number = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim = new DecimalFormat("0.00");
//                            OrderFragment.orderPrice.setText("" + decim.format(number));
//                            OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            OrderFragment.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            double number1;
                            number1 = myDbHelper.getTotalOrderPrice();
                            DecimalFormat decim1 = new DecimalFormat("0.00");
//                            CategoriesListActivity.orderPrice.setText("" + decim1.format(number));
//                            CategoriesListActivity.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
//                            CategoriesListActivity.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        try {
                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                            PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderprice));
                            PastriesSelection.orderQuantity.setText("" + Qty);
                            PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            Log.i("TAG", "order price3 " + orderprice);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            }
        });


//        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
//                ClipData data = ClipData.newPlainText("", "");
//                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
//                view.startDrag(data, shadowBuilder, view, 0);
//                view.setVisibility(View.VISIBLE);
//                return true;
//            }
//        });


        mBoxGridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    return true;
                }
                return false;
            }
        });

        mBoxGridView.setOnDragListener(this);

        individual.setOnClickListener(this);
        box.setOnClickListener(this);
        mini.setOnClickListener(this);
        petite.setOnClickListener(this);
//        m6PcsBox.setOnClickListener(this);
//        m12PcsBox.setOnClickListener(this);
//        m24PcsBox.setOnClickListener(this);

        try {

            PastriesSelection.orderPrice.setText("-");
            PastriesSelection.orderQuantity.setText("-");
            PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

        } catch (Exception e) {
            e.printStackTrace();
        }
        itemQty.clear();
        selectedCountIndividual.clear();
        counts.clear();
        indivialcount.clear();
        boxcount.clear();
        individual.performClick();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.individual:

                int finalItemCOunt = 0, finalitemcount1 = 0;
                if (finalcount) {
                    for (Integer integer : counts) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                } else {
                    for (Integer integer : selectedCountIndividual) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                }
                Log.i("TAG", "itemfinalqty " + itemfinalQty.size());
                Log.i("TAG", "subitemfinalqty " + subitemQty.size());
                Log.i("TAG", "Qty" + SectionRecyclerViewAdapter.Qty);
                Log.i("TAG", "totalCount " + totalCount);
//                Log.i("TAG", "Qty1" + Qty);

                if (finalItemCOunt > 0 || finalitemcount1 > 0) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("You haven't finished your order yet. Do you want to leave without finishing? ")
                            .setCancelable(false)
                            .setPositiveButton("Leave order", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    visible = true;
                                    itemQty.clear();
                                    itemfinalQty.clear();
                                    itemfinalprice.clear();
                                    selectedCountIndividual.clear();
                                    counts.clear();
                                    indivialcount.clear();
                                    boxcount.clear();
                                    subitemQty.clear();
                                    SectionRecyclerViewAdapter.Qty = 0;

//                pcs_layout.setVisibility(View.INVISIBLE);
                                    individual.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                                    box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    petite.setBackgroundColor(getResources().getColor(R.color.blue_color));

                                    PastriesSelection.orderPrice.setText("-");
                                    PastriesSelection.orderQuantity.setText("-");
                                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//
//                if (sections.size() == 1) {
                                    position = 0;


                                    pcs_layout.setVisibility(View.VISIBLE);
                                    mindividual_list.setVisibility(View.VISIBLE);
                                    visible = true;

                                    mbox_list.setVisibility(View.GONE);

                                    mmini_list.setVisibility(View.GONE);

                                    mpetite_list.setVisibility(View.GONE);


                                    Log.i("TAG", "sectionname1 " + sections.get(0).getSectionname());
                                    if (sections.get(0).getSectionname().equals("Box")) {
                                        sectionname = sections.get(0).getSectionname();

                                    }


                                    if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                                        mbox_layout.setVisibility(View.GONE);
                                        qty_plus.setVisibility(View.GONE);
                                        qty_mins.setVisibility(View.GONE);
                                        mGridView1.setVisibility(View.GONE);
                                        mGridView2.setVisibility(View.VISIBLE);
                                        mGridView.setVisibility(View.GONE);
                                        maddmore.setVisibility(View.VISIBLE);
                                        maddmore1.setVisibility(View.GONE);
                                        mbottom_bar.setVisibility(View.VISIBLE);

//                    mGridView1.setBackgroundResource(R.drawable.cake_bg);
//                    mgrid_layout.setBackgroundResource(R.drawable.white_bg);
//                    mGridView1.setHorizontalSpacing(5);
//                    mGridView1.setVerticalSpacing(5);
                                        Log.i("TAG", "onClick: " + sections.get(position).getChildItems().size());

                                        for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                            selectedCountIndividual.add(0);
                                            indivialcount.add(0);
                                            counts.add(0);
                                        }


                                        Log.i("TAG", "itemfinalqty " + itemfinalQty.size());
                                        Log.i("TAG", "subitemfinalqty " + subitemQty.size());
                                        Log.i("TAG", "Qty" + SectionRecyclerViewAdapter.Qty);
                                        Log.i("TAG", "Qty1" + Qty);
                                        Log.i("TAG", "onClick: " + selectedCountIndividual.size());
                                        mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                        mGridView2.setAdapter(mItemAdapter);

                                        Log.e("TAG", "section name " + sections.get(position).getSectionname());

                                    } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("3")) {
                                        if (catId == 2) {


                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.VISIBLE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.GONE);
                                            mbottom_bar.setVisibility(View.GONE);


//                        mGridView1.setBackgroundColor(Color.parseColor("#FFFFFF"));

                                            for (int j = 0; j < sections.get(position).getChildItems().size(); j++) {
                                                for (int k = 0; k < sections.get(position).getChildItems().get(j).getPriceList().size(); k++) {

                                                    Log.i("TAG", "price " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                                    Log.i("TAG", "name " + sections.get(position).getChildItems().get(j).getItemName());

                                                    CakeDetailslist pricearray = new CakeDetailslist();

                                                    pricearray.setItemid(sections.get(position).getChildItems().get(j).getItemId());
                                                    pricearray.setPrice(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                                    pricearray.setSize(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSize());
                                                    pricearray.setSizeId(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSizeid());
                                                    pricearray.setCakedesc(sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());
                                                    pricearray.setPriceid(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPriceId());

                                                    pricelist.add(pricearray);

//                                if (sections.get(position).getChildItems().size() == 1 ||  sections.get(position).getChildItems().size() == 2 || sections.get(position).getChildItems().size() == 3){
//
//                                    mGridView1.setBackgroundResource(R.drawable.cake_screen_bg1);
//                                }else {
//                                    mGridView1.setBackgroundResource(R.drawable.cake_screen_bg);
//                                }
//                                mgrid_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));

                                                    mGridView1.setHorizontalSpacing(2);
                                                    mGridView1.setVerticalSpacing(2);

//                                if (CakeInnerActivity1.cakeitemqty != 0){
//
//                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("You haven't finished your order yet. Do you want to leave without finishing? ")
//                                            .setCancelable(false)
//                                            .setPositiveButton("Leave order", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    pcs_layout.setVisibility(View.VISIBLE);
//                                                    mindividual_list.setVisibility(View.VISIBLE);
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                    alertDialogBuilder
//                                            .setNegativeButton("Continue order", new DialogInterface.OnClickListener() {
//                                                @Override1q1      `
//                                                public void onClick(DialogInterface dialog, int which) {
//                                                    pcs_layout.setVisibility(View.GONE);
//                                                    mindividual_list.setVisibility(View.GONE);
//                                                    dialog.dismiss();
//                                                }
//                                            });
//
//                                    // create alert dialog
//                                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                    // show it
//                                    alertDialog.show();
//
//
//                                }else {

                                                    mCakeAdapter = new CakeListAdapter(getApplicationContext(), sections, position, pricelist, language, PastriesSelection.this);
                                                    mGridView1.setAdapter(mCakeAdapter);
//                                }
                                                }
                                            }
                                        } else {
                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                    }

//                if (catId == 2) {
//                    Log.i("TAG", "sectionname1 " + sections.get(position).getSectionname());
//                } else {
//                    Log.i("TAG", "sectionname " + sections.get(position).getSectionname());
                                    selectedCount.clear();
                                    boxcount.clear();

                                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                    mindividual_list.setAdapter(mDropDownAdapter);

                                    mDropDownAdapter.notifyDataSetChanged();
                                }
                            });
                    alertDialogBuilder
                            .setNegativeButton("Continue order", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();


                } else {
                    visible = true;
                    itemQty.clear();
                    itemfinalQty.clear();
                    itemfinalprice.clear();
                    selectedCountIndividual.clear();
                    counts.clear();
                    indivialcount.clear();
                    boxcount.clear();
                    subitemQty.clear();
                    SectionRecyclerViewAdapter.Qty = 0;

//                pcs_layout.setVisibility(View.INVISIBLE);
                    individual.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                    box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    petite.setBackgroundColor(getResources().getColor(R.color.blue_color));

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//
//                if (sections.size() == 1) {
                    position = 0;


                    pcs_layout.setVisibility(View.VISIBLE);
                    mindividual_list.setVisibility(View.VISIBLE);
                    visible = true;

                    mbox_list.setVisibility(View.GONE);

                    mmini_list.setVisibility(View.GONE);

                    mpetite_list.setVisibility(View.GONE);


                    Log.i("TAG", "sectionname1 " + sections.get(0).getSectionname());
                    if (sections.get(0).getSectionname().equals("Box")) {
                        sectionname = sections.get(0).getSectionname();

                    }


                    if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                        mbox_layout.setVisibility(View.GONE);
                        qty_plus.setVisibility(View.GONE);
                        qty_mins.setVisibility(View.GONE);
                        mGridView1.setVisibility(View.GONE);
                        mGridView2.setVisibility(View.VISIBLE);
                        mGridView.setVisibility(View.GONE);
                        maddmore.setVisibility(View.VISIBLE);
                        maddmore1.setVisibility(View.GONE);
                        mbottom_bar.setVisibility(View.VISIBLE);

//                    mGridView1.setBackgroundResource(R.drawable.cake_bg);
//                    mgrid_layout.setBackgroundResource(R.drawable.white_bg);
//                    mGridView1.setHorizontalSpacing(5);
//                    mGridView1.setVerticalSpacing(5);

                        for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                            counts.add(0);
                            selectedCountIndividual.add(0);
                            indivialcount.add(0);
                        }

                        Log.i("TAG", "itemfinalqty " + itemfinalQty.size());
                        Log.i("TAG", "subitemfinalqty " + subitemQty.size());
                        Log.i("TAG", "Qty" + SectionRecyclerViewAdapter.Qty);
                        Log.i("TAG", "Qty1" + Qty);
                        Log.i("TAG", "onClick: " + selectedCountIndividual.size());


                        mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                        mGridView2.setAdapter(mItemAdapter);

                        Log.e("TAG", "section name " + sections.get(position).getSectionname());

                    } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("3")) {
                        if (catId == 2) {


                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.VISIBLE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.GONE);
                            mbottom_bar.setVisibility(View.GONE);


//                        mGridView1.setBackgroundColor(Color.parseColor("#FFFFFF"));

                            for (int j = 0; j < sections.get(position).getChildItems().size(); j++) {
                                for (int k = 0; k < sections.get(position).getChildItems().get(j).getPriceList().size(); k++) {

                                    Log.i("TAG", "price " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                    Log.i("TAG", "name " + sections.get(position).getChildItems().get(j).getItemName());

                                    CakeDetailslist pricearray = new CakeDetailslist();

                                    pricearray.setItemid(sections.get(position).getChildItems().get(j).getItemId());
                                    pricearray.setPrice(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                    pricearray.setSize(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSize());
                                    pricearray.setSizeId(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSizeid());
                                    pricearray.setCakedesc(sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());
                                    pricearray.setPriceid(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPriceId());

                                    pricelist.add(pricearray);

//                                if (sections.get(position).getChildItems().size() == 1 ||  sections.get(position).getChildItems().size() == 2 || sections.get(position).getChildItems().size() == 3){
//
//                                    mGridView1.setBackgroundResource(R.drawable.cake_screen_bg1);
//                                }else {
//                                    mGridView1.setBackgroundResource(R.drawable.cake_screen_bg);
//                                }
//                                mgrid_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));

                                    mGridView1.setHorizontalSpacing(2);
                                    mGridView1.setVerticalSpacing(2);

//                                if (CakeInnerActivity1.cakeitemqty != 0){
//
//                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("You haven't finished your order yet. Do you want to leave without finishing? ")
//                                            .setCancelable(false)
//                                            .setPositiveButton("Leave order", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    pcs_layout.setVisibility(View.VISIBLE);
//                                                    mindividual_list.setVisibility(View.VISIBLE);
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                    alertDialogBuilder
//                                            .setNegativeButton("Continue order", new DialogInterface.OnClickListener() {
//                                                @Override1q1      `
//                                                public void onClick(DialogInterface dialog, int which) {
//                                                    pcs_layout.setVisibility(View.GONE);
//                                                    mindividual_list.setVisibility(View.GONE);
//                                                    dialog.dismiss();
//                                                }
//                                            });
//
//                                    // create alert dialog
//                                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                    // show it
//                                    alertDialog.show();
//
//
//                                }else {

                                    mCakeAdapter = new CakeListAdapter(getApplicationContext(), sections, position, pricelist, language, PastriesSelection.this);
                                    mGridView1.setAdapter(mCakeAdapter);
//                                }
                                }
                            }
                        } else {
                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                    }

//                if (catId == 2) {
//                    Log.i("TAG", "sectionname1 " + sections.get(position).getSectionname());
//                } else {
//                    Log.i("TAG", "sectionname " + sections.get(position).getSectionname());
                    selectedCount.clear();
                    boxcount.clear();

                    mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                    mindividual_list.setAdapter(mDropDownAdapter);

                    mDropDownAdapter.notifyDataSetChanged();
                }


//                if () {
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
////        if(language.equalsIgnoreCase("En")) {
//                    // set title
//                    alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("You haven't finished your order yet. Do you want to leave without finishing? ")
//                            .setCancelable(false)
//                            .setPositiveButton("Leave order", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    pcs_layout.setVisibility(View.VISIBLE);
//                                    mindividual_list.setVisibility(View.VISIBLE);
//                                    dialog.dismiss();
//                                }
//                            });
//                    alertDialogBuilder
//                            .setNegativeButton("Continue order", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    pcs_layout.setVisibility(View.GONE);
//                                    mindividual_list.setVisibility(View.GONE);
//                                    dialog.dismiss();
//                                }
//                            });
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//
//
//                } else {

//                }
                break;

            case R.id.box:

                finalItemCOunt = 0;
                finalitemcount1 = 0;
                if (finalcount) {
                    for (Integer integer : counts) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                } else {
                    for (Integer integer : selectedCountIndividual) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                }
                if (finalItemCOunt > 0 || finalitemcount1 > 0) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("You haven't finished your order yet. Do you want to leave without finishing? ")
                            .setCancelable(false)
                            .setPositiveButton("Leave order", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    visible = true;
                                    itemQty.clear();
                                    itemfinalQty.clear();
                                    itemfinalprice.clear();
                                    selectedCountIndividual.clear();
                                    counts.clear();
                                    indivialcount.clear();
                                    boxcount.clear();
                                    subitemQty.clear();
                                    SectionRecyclerViewAdapter.Qty = 0;

                                    box.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                                    individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    petite.setBackgroundColor(getResources().getColor(R.color.blue_color));
//                mGridView1.setBackgroundResource(R.drawable.white_bg);
//                mGridView1.setHorizontalSpacing(5);
//                mGridView1.setVerticalSpacing(5);
//                mgrid_layout.setBackgroundResource(R.drawable.cakebg);
                                    mbottom_bar.setVisibility(View.VISIBLE);

                                    PastriesSelection.orderPrice.setText("-");
                                    PastriesSelection.orderQuantity.setText("-");
                                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                                    if (sections.size() == 1) {

                                        position = 0;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);
                                        mbox_list.setVisibility(View.VISIBLE);
                                        mmini_list.setVisibility(View.GONE);
                                        mpetite_list.setVisibility(View.GONE);

//                    mbox_layout.setVisibility(View.VISBLE);
                                        qty_plus.setVisibility(View.VISIBLE);
                                        qty_mins.setVisibility(View.VISIBLE);

                                        Log.i("TAG", "sectionname2 " + sections.get(0).getSectionname());
                                        if (sections.get(0).getSectionname().equals("Box")) {
                                            sectionname = sections.get(0).getSectionname();

                                        }

                                        if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }


                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mbox_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    } else if (sections.size() >= 2) {

                                        position = 1;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);
                                        mbox_list.setVisibility(View.VISIBLE);
                                        mmini_list.setVisibility(View.GONE);
                                        mpetite_list.setVisibility(View.GONE);
                                        visible = true;

                                        Log.i("TAG", "sectionname4 " + sections.get(1).getSectionname());
                                        if (sections.get(1).getSectionname().equals("Box")) {
                                            sectionname = sections.get(1).getSectionname();

                                        }

                                        if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);

                                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("3")) {

                                            if (catId == 2) {

                                                mbox_layout.setVisibility(View.GONE);
                                                qty_plus.setVisibility(View.GONE);
                                                qty_mins.setVisibility(View.GONE);
                                                mGridView1.setVisibility(View.VISIBLE);
                                                mGridView2.setVisibility(View.GONE);
                                                mGridView.setVisibility(View.GONE);
                                                maddmore.setVisibility(View.GONE);
                                                maddmore1.setVisibility(View.GONE);
                                                mbottom_bar.setVisibility(View.GONE);

//                            mGridView1.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                            mGridView1.setBackgroundResource(R.drawable.leftntopbrd);

                                                for (int j = 0; j < sections.get(position).getChildItems().size(); j++) {
                                                    for (int k = 0; k < sections.get(position).getChildItems().get(j).getPriceList().size(); k++) {

                                                        Log.i("TAG", "price " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                                        Log.i("TAG", "name " + sections.get(position).getChildItems().get(j).getItemName());
                                                        Log.i("TAG", "cakedec " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());

                                                        CakeDetailslist pricearray = new CakeDetailslist();

                                                        pricearray.setItemid(sections.get(position).getChildItems().get(j).getItemId());
                                                        pricearray.setPrice(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                                        pricearray.setSize(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSize());
                                                        pricearray.setSizeId(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSizeid());
                                                        pricearray.setCakedesc(sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());
                                                        pricearray.setPriceid(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPriceId());

                                                        pricelist.add(pricearray);
                                                        Log.i("TAG", "cakedec " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());

//                                    mgrid_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));

                                                        mGridView1.setHorizontalSpacing(2);
                                                        mGridView1.setVerticalSpacing(2);

                                                        Log.i("TAG", "cake " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());

                                                        mCakeAdapter = new CakeListAdapter(getApplicationContext(), sections, position, pricelist, language, PastriesSelection.this);
                                                        mGridView1.setAdapter(mCakeAdapter);
                                                    }
                                                }
                                            } else {
                                                mbox_layout.setVisibility(View.VISIBLE);
                                                qty_plus.setVisibility(View.VISIBLE);
                                                qty_mins.setVisibility(View.VISIBLE);
                                                mGridView1.setVisibility(View.GONE);
                                                mGridView2.setVisibility(View.GONE);
                                                mGridView.setVisibility(View.VISIBLE);
                                                maddmore.setVisibility(View.GONE);
                                                maddmore1.setVisibility(View.VISIBLE);
                                            }

                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        Log.i("TAG", "sectionname " + sections.get(position).getSectionname());
                                        Log.i("TAG", "itemfinalqty " + itemfinalQty.size());
                                        Log.i("TAG", "subitemfinalqty " + subitemQty.size());
                                        Log.i("TAG", "Qty" + SectionRecyclerViewAdapter.Qty);
                                        Log.i("TAG", "Qty1" + Qty);

                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mbox_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    }
                                }
                            });
                    alertDialogBuilder
                            .setNegativeButton("Continue order", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();


                } else {
                    visible = true;
                    itemQty.clear();
                    itemfinalQty.clear();
                    itemfinalprice.clear();
                    selectedCountIndividual.clear();
                    counts.clear();
                    indivialcount.clear();
                    boxcount.clear();
                    subitemQty.clear();
                    SectionRecyclerViewAdapter.Qty = 0;

                    box.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                    individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    petite.setBackgroundColor(getResources().getColor(R.color.blue_color));
//                mGridView1.setBackgroundResource(R.drawable.white_bg);
//                mGridView1.setHorizontalSpacing(5);
//                mGridView1.setVerticalSpacing(5);
//                mgrid_layout.setBackgroundResource(R.drawable.cakebg);
                    mbottom_bar.setVisibility(View.VISIBLE);

                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                    if (sections.size() == 1) {

                        position = 0;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);
                        mbox_list.setVisibility(View.VISIBLE);
                        mmini_list.setVisibility(View.GONE);
                        mpetite_list.setVisibility(View.GONE);

//                    mbox_layout.setVisibility(View.VISBLE);
                        qty_plus.setVisibility(View.VISIBLE);
                        qty_mins.setVisibility(View.VISIBLE);

                        Log.i("TAG", "sectionname2 " + sections.get(0).getSectionname());
                        if (sections.get(0).getSectionname().equals("Box")) {
                            sectionname = sections.get(0).getSectionname();

                        }

                        if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }


                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mbox_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    } else if (sections.size() >= 2) {

                        position = 1;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);
                        mbox_list.setVisibility(View.VISIBLE);
                        mmini_list.setVisibility(View.GONE);
                        mpetite_list.setVisibility(View.GONE);
                        visible = true;

                        Log.i("TAG", "sectionname4 " + sections.get(1).getSectionname());
                        if (sections.get(1).getSectionname().equals("Box")) {
                            sectionname = sections.get(1).getSectionname();

                        }

                        if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);

                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);

                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("3")) {

                            if (catId == 2) {

                                mbox_layout.setVisibility(View.GONE);
                                qty_plus.setVisibility(View.GONE);
                                qty_mins.setVisibility(View.GONE);
                                mGridView1.setVisibility(View.VISIBLE);
                                mGridView2.setVisibility(View.GONE);
                                mGridView.setVisibility(View.GONE);
                                maddmore.setVisibility(View.GONE);
                                maddmore1.setVisibility(View.GONE);
                                mbottom_bar.setVisibility(View.GONE);

//                            mGridView1.setBackgroundColor(Color.parseColor("#FFFFFF"));
//                            mGridView1.setBackgroundResource(R.drawable.leftntopbrd);

                                for (int j = 0; j < sections.get(position).getChildItems().size(); j++) {
                                    for (int k = 0; k < sections.get(position).getChildItems().get(j).getPriceList().size(); k++) {

                                        Log.i("TAG", "price " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                        Log.i("TAG", "name " + sections.get(position).getChildItems().get(j).getItemName());
                                        Log.i("TAG", "cakedec " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());

                                        CakeDetailslist pricearray = new CakeDetailslist();

                                        pricearray.setItemid(sections.get(position).getChildItems().get(j).getItemId());
                                        pricearray.setPrice(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPrice());
                                        pricearray.setSize(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSize());
                                        pricearray.setSizeId(sections.get(position).getChildItems().get(j).getPriceList().get(k).getSizeid());
                                        pricearray.setCakedesc(sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());
                                        pricearray.setPriceid(sections.get(position).getChildItems().get(j).getPriceList().get(k).getPriceId());

                                        pricelist.add(pricearray);
                                        Log.i("TAG", "cakedec " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());

//                                    mgrid_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));

                                        mGridView1.setHorizontalSpacing(2);
                                        mGridView1.setVerticalSpacing(2);

                                        Log.i("TAG", "cake " + sections.get(position).getChildItems().get(j).getPriceList().get(k).getItemsizedesc());

                                        mCakeAdapter = new CakeListAdapter(getApplicationContext(), sections, position, pricelist, language, PastriesSelection.this);
                                        mGridView1.setAdapter(mCakeAdapter);
                                    }
                                }
                            } else {
                                mbox_layout.setVisibility(View.VISIBLE);
                                qty_plus.setVisibility(View.VISIBLE);
                                qty_mins.setVisibility(View.VISIBLE);
                                mGridView1.setVisibility(View.GONE);
                                mGridView2.setVisibility(View.GONE);
                                mGridView.setVisibility(View.VISIBLE);
                                maddmore.setVisibility(View.GONE);
                                maddmore1.setVisibility(View.VISIBLE);
                            }

                        }

                        selectedCount.clear();
                        boxcount.clear();
                        Log.i("TAG", "sectionname " + sections.get(position).getSectionname());
                        Log.i("TAG", "itemfinalqty " + itemfinalQty.size());
                        Log.i("TAG", "subitemfinalqty " + subitemQty.size());
                        Log.i("TAG", "Qty" + SectionRecyclerViewAdapter.Qty);
                        Log.i("TAG", "Qty1" + Qty);

                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mbox_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    }
                }
                break;

            case R.id.mini:

                finalItemCOunt = 0;
                finalitemcount1 = 0;
                if (finalcount) {
                    for (Integer integer : counts) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                } else {
                    for (Integer integer : selectedCountIndividual) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                }
                if (finalItemCOunt > 0 || finalitemcount1 > 0) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("You haven't finished your order yet. Do you want to leave without finishing? ")
                            .setCancelable(false)
                            .setPositiveButton("Leave order", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    visible = true;
                                    itemQty.clear();
                                    itemfinalQty.clear();
                                    itemfinalprice.clear();
                                    selectedCountIndividual.clear();
                                    counts.clear();
                                    indivialcount.clear();
                                    boxcount.clear();
                                    subitemQty.clear();
                                    SectionRecyclerViewAdapter.Qty = 0;

//                pcs_layout.setVisibility(View.INVISIBLE);
                                    mini.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                                    individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    petite.setBackgroundColor(getResources().getColor(R.color.blue_color));

                                    try {

                                        PastriesSelection.orderPrice.setText("-");
                                        PastriesSelection.orderQuantity.setText("-");
                                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (sections.size() == 1) {

                                        position = 0;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);
                                        mbox_list.setVisibility(View.GONE);
                                        mmini_list.setVisibility(View.VISIBLE);
                                        mpetite_list.setVisibility(View.GONE);
                                        visible = true;


                                        Log.i("TAG", "sectionname5 " + sections.get(0).getSectionname());
                                        if (sections.get(0).getSectionname().equals("Box")) {
                                            sectionname = sections.get(0).getSectionname();

                                        }

                                        if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mmini_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    } else if (sections.size() == 2) {

                                        position = 1;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);

                                        mbox_list.setVisibility(View.GONE);

                                        mmini_list.setVisibility(View.VISIBLE);

                                        mpetite_list.setVisibility(View.GONE);


                                        Log.i("TAG", "sectionname6 " + sections.get(1).getSectionname());
                                        if (sections.get(1).getSectionname().equals("Box")) {
                                            sectionname = sections.get(1).getSectionname();

                                        }

                                        if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mmini_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    } else if (sections.size() >= 3) {

                                        position = 2;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);
                                        mbox_list.setVisibility(View.GONE);
                                        mmini_list.setVisibility(View.VISIBLE);
                                        mpetite_list.setVisibility(View.GONE);
                                        visible = true;

                                        Log.i("TAG", "sectionname7 " + sections.get(2).getSectionname());
                                        if (sections.get(2).getSectionname().equals("Box")) {
                                            sectionname = sections.get(2).getSectionname();

                                        }

                                        if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mmini_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    }
                                }
                            });
                    alertDialogBuilder
                            .setNegativeButton("Continue order", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();


                } else {
                    visible = true;
                    itemQty.clear();
                    itemfinalQty.clear();
                    itemfinalprice.clear();
                    selectedCountIndividual.clear();
                    counts.clear();
                    indivialcount.clear();
                    boxcount.clear();
                    subitemQty.clear();
                    SectionRecyclerViewAdapter.Qty = 0;

//                pcs_layout.setVisibility(View.INVISIBLE);
                    mini.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                    individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    box.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    petite.setBackgroundColor(getResources().getColor(R.color.blue_color));

                    try {

                        PastriesSelection.orderPrice.setText("-");
                        PastriesSelection.orderQuantity.setText("-");
                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (sections.size() == 1) {

                        position = 0;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);
                        mbox_list.setVisibility(View.GONE);
                        mmini_list.setVisibility(View.VISIBLE);
                        mpetite_list.setVisibility(View.GONE);
                        visible = true;


                        Log.i("TAG", "sectionname5 " + sections.get(0).getSectionname());
                        if (sections.get(0).getSectionname().equals("Box")) {
                            sectionname = sections.get(0).getSectionname();

                        }

                        if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                        selectedCount.clear();
                        boxcount.clear();
                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mmini_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    } else if (sections.size() == 2) {

                        position = 1;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);

                        mbox_list.setVisibility(View.GONE);

                        mmini_list.setVisibility(View.VISIBLE);

                        mpetite_list.setVisibility(View.GONE);


                        Log.i("TAG", "sectionname6 " + sections.get(1).getSectionname());
                        if (sections.get(1).getSectionname().equals("Box")) {
                            sectionname = sections.get(1).getSectionname();

                        }

                        if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                        selectedCount.clear();
                        boxcount.clear();
                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mmini_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    } else if (sections.size() >= 3) {

                        position = 2;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);
                        mbox_list.setVisibility(View.GONE);
                        mmini_list.setVisibility(View.VISIBLE);
                        mpetite_list.setVisibility(View.GONE);
                        visible = true;

                        Log.i("TAG", "sectionname7 " + sections.get(2).getSectionname());
                        if (sections.get(2).getSectionname().equals("Box")) {
                            sectionname = sections.get(2).getSectionname();

                        }

                        if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                        selectedCount.clear();
                        boxcount.clear();
                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mmini_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    }
                }

                break;

            case R.id.petite:

                finalItemCOunt = 0;
                finalitemcount1 = 0;
                if (finalcount) {
                    for (Integer integer : counts) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                } else {
                    for (Integer integer : selectedCountIndividual) {
                        finalItemCOunt = finalItemCOunt + integer;
                    }
                    for (Integer integer : selectedCount) {
                        finalitemcount1 = finalitemcount1 + integer;
                    }
                }

                if (finalItemCOunt > 0 || finalitemcount1 > 0) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("You haven't finished your order yet. Do you want to leave without finishing? ")
                            .setCancelable(false)
                            .setPositiveButton("Leave order", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    visible = true;
                                    itemQty.clear();
                                    itemfinalQty.clear();
                                    itemfinalprice.clear();
                                    selectedCountIndividual.clear();
                                    counts.clear();
                                    indivialcount.clear();
                                    boxcount.clear();
                                    subitemQty.clear();
                                    SectionRecyclerViewAdapter.Qty = 0;

//                pcs_layout.setVisibility(View.INVISIBLE);
                                    petite.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                                    individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                                    box.setBackgroundColor(getResources().getColor(R.color.blue_color));

                                    try {

                                        PastriesSelection.orderPrice.setText("-");
                                        PastriesSelection.orderQuantity.setText("-");
                                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (sections.size() == 1) {

                                        position = 0;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);
                                        mbox_list.setVisibility(View.GONE);
                                        mmini_list.setVisibility(View.GONE);
                                        mpetite_list.setVisibility(View.VISIBLE);


                                        Log.i("TAG", "sectionname8 " + sections.get(0).getSectionname());
                                        if (sections.get(0).getSectionname().equals("Box")) {
                                            sectionname = sections.get(0).getSectionname();

                                        }

                                        if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mpetite_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    } else if (sections.size() == 2) {

                                        position = 1;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);

                                        mbox_list.setVisibility(View.GONE);

                                        mmini_list.setVisibility(View.GONE);

                                        mpetite_list.setVisibility(View.VISIBLE);


                                        Log.i("TAG", "sectionname9 " + sections.get(1).getSectionname());
                                        if (sections.get(1).getSectionname() == "Box") {
                                            sectionname = sections.get(1).getSectionname();

                                        }

                                        if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mpetite_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    } else if (sections.size() == 3) {

                                        position = 2;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);

                                        mbox_list.setVisibility(View.GONE);

                                        mmini_list.setVisibility(View.GONE);

                                        mpetite_list.setVisibility(View.VISIBLE);


                                        if (sections.get(2).getSectionname().equals("Box")) {
                                            sectionname = sections.get(2).getSectionname();

                                        }

                                        if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mpetite_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();

                                    } else if (sections.size() == 4) {

                                        position = 3;

                                        pcs_layout.setVisibility(View.VISIBLE);
                                        mindividual_list.setVisibility(View.GONE);

                                        mbox_list.setVisibility(View.GONE);

                                        mmini_list.setVisibility(View.GONE);

                                        mpetite_list.setVisibility(View.VISIBLE);


                                        Log.i("TAG", "sectionname10 " + sections.get(3).getSectionname());
                                        if (sections.get(3).getSectionname().equals("Box")) {
                                            sectionname = sections.get(3).getSectionname();

                                        }

                                        if (sections.get(3).getChildItems().get(0).getBuildingType().equals("1")) {

                                            mbox_layout.setVisibility(View.GONE);
                                            qty_plus.setVisibility(View.GONE);
                                            qty_mins.setVisibility(View.GONE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.VISIBLE);
                                            mGridView.setVisibility(View.GONE);
                                            maddmore.setVisibility(View.VISIBLE);
                                            maddmore1.setVisibility(View.GONE);

                                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                                selectedCountIndividual.add(0);
                                                counts.add(0);
                                                indivialcount.add(0);
                                            }

                                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                                            mGridView2.setAdapter(mItemAdapter);

                                        } else if (sections.get(3).getChildItems().get(0).getBuildingType().equals("2")) {

                                            mbox_layout.setVisibility(View.VISIBLE);
                                            qty_plus.setVisibility(View.VISIBLE);
                                            qty_mins.setVisibility(View.VISIBLE);
                                            mGridView1.setVisibility(View.GONE);
                                            mGridView2.setVisibility(View.GONE);
                                            mGridView.setVisibility(View.VISIBLE);
                                            maddmore.setVisibility(View.GONE);
                                            maddmore1.setVisibility(View.VISIBLE);
                                        }

                                        selectedCount.clear();
                                        boxcount.clear();
                                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                                        mpetite_list.setAdapter(mDropDownAdapter);

                                        mDropDownAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                    alertDialogBuilder
                            .setNegativeButton("Continue order", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();


                } else {

                    visible = true;
                    itemQty.clear();
                    itemfinalQty.clear();
                    itemfinalprice.clear();
                    selectedCountIndividual.clear();
                    counts.clear();
                    indivialcount.clear();
                    boxcount.clear();
                    subitemQty.clear();
                    SectionRecyclerViewAdapter.Qty = 0;

//                pcs_layout.setVisibility(View.INVISIBLE);
                    petite.setBackgroundColor(getResources().getColor(R.color.brown_color1));
                    individual.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    mini.setBackgroundColor(getResources().getColor(R.color.blue_color));
                    box.setBackgroundColor(getResources().getColor(R.color.blue_color));

                    try {

                        PastriesSelection.orderPrice.setText("-");
                        PastriesSelection.orderQuantity.setText("-");
                        PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (sections.size() == 1) {

                        position = 0;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);
                        mbox_list.setVisibility(View.GONE);
                        mmini_list.setVisibility(View.GONE);
                        mpetite_list.setVisibility(View.VISIBLE);


                        Log.i("TAG", "sectionname8 " + sections.get(0).getSectionname());
                        if (sections.get(0).getSectionname().equals("Box")) {
                            sectionname = sections.get(0).getSectionname();

                        }

                        if (sections.get(0).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(0).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                        selectedCount.clear();
                        boxcount.clear();
                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mpetite_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    } else if (sections.size() == 2) {

                        position = 1;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);

                        mbox_list.setVisibility(View.GONE);

                        mmini_list.setVisibility(View.GONE);

                        mpetite_list.setVisibility(View.VISIBLE);


                        Log.i("TAG", "sectionname9 " + sections.get(1).getSectionname());
                        if (sections.get(1).getSectionname() == "Box") {
                            sectionname = sections.get(1).getSectionname();

                        }

                        if (sections.get(1).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(1).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                        selectedCount.clear();
                        boxcount.clear();
                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mpetite_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    } else if (sections.size() == 3) {

                        position = 2;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);

                        mbox_list.setVisibility(View.GONE);

                        mmini_list.setVisibility(View.GONE);

                        mpetite_list.setVisibility(View.VISIBLE);


                        if (sections.get(2).getSectionname().equals("Box")) {
                            sectionname = sections.get(2).getSectionname();

                        }

                        if (sections.get(2).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(2).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                        selectedCount.clear();
                        boxcount.clear();
                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mpetite_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();

                    } else if (sections.size() == 4) {

                        position = 3;

                        pcs_layout.setVisibility(View.VISIBLE);
                        mindividual_list.setVisibility(View.GONE);

                        mbox_list.setVisibility(View.GONE);

                        mmini_list.setVisibility(View.GONE);

                        mpetite_list.setVisibility(View.VISIBLE);


                        Log.i("TAG", "sectionname10 " + sections.get(3).getSectionname());
                        if (sections.get(3).getSectionname().equals("Box")) {
                            sectionname = sections.get(3).getSectionname();

                        }

                        if (sections.get(3).getChildItems().get(0).getBuildingType().equals("1")) {

                            mbox_layout.setVisibility(View.GONE);
                            qty_plus.setVisibility(View.GONE);
                            qty_mins.setVisibility(View.GONE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.VISIBLE);
                            mGridView.setVisibility(View.GONE);
                            maddmore.setVisibility(View.VISIBLE);
                            maddmore1.setVisibility(View.GONE);

                            for (int k = 0; k < sections.get(position).getChildItems().size(); k++) {
                                selectedCountIndividual.add(0);
                                counts.add(0);
                                indivialcount.add(0);
                            }

                            mItemAdapter = new SectionRecyclerViewAdapter(getApplicationContext(), title, sections, position, language, PastriesSelection.this);
                            mGridView2.setAdapter(mItemAdapter);

                        } else if (sections.get(3).getChildItems().get(0).getBuildingType().equals("2")) {

                            mbox_layout.setVisibility(View.VISIBLE);
                            qty_plus.setVisibility(View.VISIBLE);
                            qty_mins.setVisibility(View.VISIBLE);
                            mGridView1.setVisibility(View.GONE);
                            mGridView2.setVisibility(View.GONE);
                            mGridView.setVisibility(View.VISIBLE);
                            maddmore.setVisibility(View.GONE);
                            maddmore1.setVisibility(View.VISIBLE);
                        }

                        selectedCount.clear();
                        boxcount.clear();
                        mDropDownAdapter = new DropDownListAdapter(getApplicationContext(), sections, position, language, PastriesSelection.this);
                        mpetite_list.setAdapter(mDropDownAdapter);

                        mDropDownAdapter.notifyDataSetChanged();
                    }
                }
                break;

//            case R.id.pcs_6_box:
//                pcs_layout.setVisibility(View.INVISIBLE);
//                boxName.setText("6pcs Box");
//                boxSize = 6;
//                mNumBox.setText("0/"+boxSize);
//                totalCount = 0;
//                Boxitems.clear();
//                mBoxGridView.setNumColumns(3);
//                mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, PastriesSelection.this);
//                mBoxAdapter.notifyDataSetChanged();
//                break;

//            case R.id.pcs_12_box:
//                pcs_layout.setVisibility(View.INVISIBLE);
//                boxName.setText("12pcs Box");
//                boxSize = 12;
//                mNumBox.setText("0/"+boxSize);
//                Boxitems.clear();
//                totalCount = 0;
//                mBoxGridView.setNumColumns(6);
//                mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, PastriesSelection.this);
//                mBoxAdapter.notifyDataSetChanged();
//                break;
//
//            case R.id.pcs_24_box:
//                pcs_layout.setVisibility(View.INVISIBLE);
//                boxName.setText("24pcs Box");
//                boxSize = 24;
//                mNumBox.setText("0/"+boxSize);
//                totalCount = 0;
//                Boxitems.clear();
//                mBoxGridView.setNumColumns(12);
//                mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), Boxitems, boxSize, language, PastriesSelection.this);
//                mBoxAdapter.notifyDataSetChanged();
//                break;
        }
    }

//    public void add() {
//
//
//        for (int k = 0; k < sections.size(); k++) {
//            for (int l = 0; l < sections.get(k).getChildItems().size(); l++) {
//                Log.i("TAG", "building type " + sections.get(k).getChildItems().get(l).getBuildingType());
//                if (sections.get(k).getChildItems().get(l).getBuildingType().equals("2")) {
//                    Log.i("TAG", "building type1 " + sections.get(k).getChildItems().get(l).getBuildingType());
//
//                    if (Qty == 1) {
//                        orderprice = Float.parseFloat(sections.get(k).getChildItems().get(l).getPriceList().get(0).getPrice());
//                    }
//                    Log.i("TAG", "order price " + orderprice);
//                    Log.i("TAG", "catid" + catId);
//
//                    String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "0";
//                    if (boxpos != null && boxpos.size() > 0) {
//                        for (int i = 0; i < boxpos.size(); i++) {
//                            if (addl_ids != null && addl_ids.length() > 0) {
//
//                                addl_ids = addl_ids + "," + sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemid();
//                                addl_name = addl_name + "," + sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName();
//                                addl_name_ar = addl_name_ar + "," + sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName_Ar();
//                                addl_image = addl_image + "," + sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemimage();
////                                addl_price = addl_price + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(i))).getsub();
//                                int count = 0;
//                                for (int j = 0; j < subitemQty.size(); j++) {
//                                    if (subitemQty.get(j).equals(sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName())) {
//                                        count = count + 1;
//                                    }
//                                }
//                                addl_qty = addl_qty + "," + Integer.toString(count);
//
//                            } else {
//                                addl_ids = sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemid();
//                                addl_name = sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName();
//                                addl_name_ar = sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemName_Ar();
//                                addl_image = sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubItemimage();
////                                addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
//                                int count = 0;
//                                for (int j = 0; j < subitemQty.size(); j++) {
//                                    if (subitemQty.get(j).equals(sections.get(k).getChildItems().get(l).getSubItems().get(Integer.parseInt(boxpos.get(i))).getSubitemName())) {
//                                        count = count + 1;
//                                    }
//                                }
//                                addl_qty = Integer.toString(count);
//                            }
//                        }
//                    }
//
//                    Log.i("TAG", "subitemid " + addl_ids);
//                    Log.i("TAG", "subitemname " + addl_name);
//                    Log.i("TAG", "subitemcount " + addl_qty);
//
//                    HashMap<String, String> values = new HashMap<>();
//                    values.put("itemId", sections.get(k).getChildItems().get(l).getItemId());
//                    values.put("itemTypeId", sections.get(k).getChildItems().get(l).getPriceList().get(0).getSize());
//                    values.put("subCategoryId", sections.get(k).getChildItems().get(l).getSubCatId());
//                    values.put("additionals", "");
//                    values.put("qty", String.valueOf(Qty));
//                    values.put("price", sections.get(k).getChildItems().get(l).getPriceList().get(0).getPrice());
//                    values.put("additionalsPrice", "");
//                    values.put("additionalsTypeId", "");
//                    values.put("totalAmount", String.valueOf(orderprice));
//                    values.put("comment", "");
//                    values.put("status", "1");
//                    values.put("creationDate", "14/07/2015");
//                    values.put("modifiedDate", "14/07/2015");
//                    values.put("categoryId", String.valueOf(catId));
//                    values.put("itemName", sections.get(k).getChildItems().get(l).getItemName());
//                    values.put("itemNameAr", sections.get(k).getChildItems().get(l).getItemNameAr());
//                    values.put("image", sections.get(k).getChildItems().get(l).getImage());
//                    values.put("item_desc", sections.get(k).getChildItems().get(l).getDescription());
//                    values.put("item_desc_Ar", sections.get(k).getChildItems().get(l).getDescriptionAr());
//                    values.put("sub_itemName", addl_name);
//                    values.put("sub_itemName_Ar", addl_name_ar);
//                    values.put("sub_itemImage", addl_image);
//                    values.put("sub_item_id", addl_ids);
//                    values.put("sub_item_count", addl_qty);
//                    values.put("ItemType", "");
//                    values.put("sizename", sections.get(k).getChildItems().get(l).getPriceList().get(0).getSizeid());
//
//                    myDbHelper.insertOrder(values);
//
//                    Log.i("TAG", "item name list" + sections.get(k).getChildItems().get(l).getItemName());
//                    Log.i("TAG", "order price " + orderprice);
//
//                    Intent a = new Intent(PastriesSelection.this, CategoriesListActivity.class);
//                    startActivity(a);
//                    finish();
//                }
//            }
//        }
//    }


    @Override
    public boolean onDrag(View v, DragEvent event) {
        final int action = event.getAction();
        // Handles each of the expected events
        switch (action) {

            case DragEvent.ACTION_DRAG_STARTED:

                // Returns false. During the current drag and drop operation, this View will
                // not receive events again until ACTION_DRAG_ENDED is sent.
                return true;

            case DragEvent.ACTION_DRAG_ENTERED:
//                Log.i("TAG","drap entered ACTION_DRAG_ENTERED");

                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
//                Log.i("TAG","drap entered ACTION_DRAG_LOCATION");
                // Ignore the event
                return true;

            case DragEvent.ACTION_DRAG_EXITED:

                return true;

            case DragEvent.ACTION_DROP:

                totalCount = 0;
                for (Integer i : PastriesSelection.selectedCount) {
                    totalCount = totalCount + i;
                }

                totalCount = totalCount + 1;
                if (totalCount <= boxSize) {
                    int localCount = PastriesSelection.selectedCount.get(pos);
                    PastriesSelection.selectedCount.set(pos, (localCount + 1));
                    PastriesSelection.boxcount.set(pos, (localCount + 1));
                    selectedItems.add(sections.get(position).getChildItems().get(value1).getSubItems().get(pos));
//                    mNumBox.setText(totalCount + "/" + boxSize);

                    for (int j = 0; j < main_list.size(); j++) {
                        maincatid = String.valueOf(catId);
                    }

                    mBoxAdapter = new PastriesBoxItemsAdapter(getApplicationContext(), maincatid, sectionname, selectedItems, position, value1, boxSize, language, PastriesSelection.this);
                    mBoxGridView.setAdapter(mBoxAdapter);
                    mBoxAdapter.notifyDataSetChanged();
                    mSubItemAdapter.notifyDataSetChanged();
                } else {
                    String message;
//                    if(language.equalsIgnoreCase("En")) {
                    message = "You are done with maximum " + boxSize + " " + getIntent().getStringExtra("title") + " per box, to order more use other box.";
//                    }
//                    else{
//                        message = "في حال انتهائك من طلب 9 أسياخ ، لو رغبت في المزيد عليك استخدام مربع آخر";
//                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));
//        if(language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("BAKERY & Co.");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(message)
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
                // Returns true. DragEvent.getResult() will return true.
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                mBoxAdapter.notifyDataSetChanged();
                // returns true; the value is ignored.
                return true;

            // An unknown action type was received.
            default:
                Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                break;
        }
        return false;
    }
//    public void refreshData(){
//        mAdapter.notifyDataSetChanged();
//    }

    public void Noofboxes() {

        if (no_of_items.equals("1")) {

            mBoxGridView.setNumColumns(1);
            rowSize = 1;

        } else if (no_of_items.equals("5")) {

            mBoxGridView.setNumColumns(5);
            rowSize = 1;

        } else if (no_of_items.equals("6")) {

            mBoxGridView.setNumColumns(3);
            rowSize = 2;

        } else if (no_of_items.equals("10")) {

            mBoxGridView.setNumColumns(5);
            rowSize = 2;

        } else if (no_of_items.equals("12")) {

            mBoxGridView.setNumColumns(6);
            rowSize = 2;

        } else if (no_of_items.equals("14")) {

            mBoxGridView.setNumColumns(7);
            rowSize = 2;

        } else if (no_of_items.equals("18")) {

            mBoxGridView.setNumColumns(9);
            rowSize = 2;

        } else if (no_of_items.equals("20")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 2;

        } else if (no_of_items.equals("24")) {

            mBoxGridView.setNumColumns(12);
            rowSize = 2;

        } else if (no_of_items.equals("25")) {

            mBoxGridView.setNumColumns(5);
            rowSize = 5;

        } else if (no_of_items.equals("30")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 3;

        } else if (no_of_items.equals("35")) {

            mBoxGridView.setNumColumns(7);
            rowSize = 5;

        } else if (no_of_items.equals("36")) {

            mBoxGridView.setNumColumns(9);
            rowSize = 4;

        } else if (no_of_items.equals("40")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 4;

        } else if (no_of_items.equals("48")) {

            mBoxGridView.setNumColumns(12);
            rowSize = 4;

        } else if (no_of_items.equals("50")) {

            mBoxGridView.setNumColumns(10);
            rowSize = 5;

        } else if (no_of_items.equals("60")) {

            mBoxGridView.setNumColumns(12);
            rowSize = 5;

        } else if (no_of_items.equals("70")) {

            mBoxGridView.setNumColumns(14);
            rowSize = 5;

        } else if (no_of_items.equals("75")) {

            mBoxGridView.setNumColumns(15);
            rowSize = 5;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        if (itemfinalQty.size() == 0) {
//            if (sections.get(position).getChildItems().get(value1).getBuildingType() == "1") {
//                orderPrice.setText("0.00");
//                orderQuantity.setText("0");
//
//            }
//        } else if (subitemfinalQty.size() == 0) {
//
//            if (sections.get(position).getChildItems().get(value1).getBuildingType() == "2") {
//                orderPrice.setText(sections.get(position).getChildItems().get(value1).getPriceList().get(0).getPrice());
//                orderQuantity.setText(Qty);
//
//            }
//        }
        Log.i("TAG1", "onResume: " + itemfinalQty.size());


        if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("2")) {
            if (pastries) {
                mSubItemAdapter.notifyDataSetChanged();
            }
            if (pastries_box) {
                mBoxAdapter.notifyDataSetChanged();
            }
        } else if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("1")) {
            if (section){
                mItemAdapter.notifyDataSetChanged();
            }
        }


//        try {
//            DecimalFormat decimalFormat = new DecimalFormat("0.00");
//            if (Qty == 0) {
//                PastriesSelection.orderPrice.setText("-");
//                PastriesSelection.orderQuantity.setText("-");
//                PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//            } else {
//                PastriesSelection.orderPrice.setText("" + decimalFormat.format(orderPrice));
//                PastriesSelection.orderQuantity.setText("" + Qty);
//                PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//            }
//
//            Log.i("TAG", "order price " + orderPrice);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        if (sections.get(position).getChildItems().get(value1).getBuildingType().equals("3")) {
            if (catId == 2) {
                mCakeAdapter.notifyDataSetChanged();
            }
        }

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
//                     set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

//                     set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                } else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(PastriesSelection.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("BAKERY & Co.");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a = new Intent(PastriesSelection.this, CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
                                        PastriesSelection.orderQuantity.setText("-");
                                        PastriesSelection.orderPrice.setText("-");
                                        PastriesSelection.mcount_basket.setText("0");
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بيكري آند كومباني");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
//                                    CheckoutActivity.orderQuantity.setText("0");
//                                    CheckoutActivity.orderPrice.setText("0SR");
//                                    CheckoutActivity.mcount_basket.setText("0");
//                                        CategoriesListActivity.orderQuantity.setText("0");
//                                        CategoriesListActivity.orderPrice.setText("0.00SR");
//                                        CategoriesListActivity.mcount_basket.setText("0");
                                        PastriesSelection.orderQuantity.setText("-");
                                        PastriesSelection.orderPrice.setText("-");
                                        PastriesSelection.mcount_basket.setText("0");
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a = new Intent(PastriesSelection.this, CheckoutActivity.class);
                                        startActivity(a);
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

    }
}
