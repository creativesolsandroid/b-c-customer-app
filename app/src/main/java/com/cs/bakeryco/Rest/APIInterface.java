package com.cs.bakeryco.Rest;

import com.cs.bakeryco.model.InsertOrderList;
import com.cs.bakeryco.model.ItemsList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("OrderDetails/InsertOrder")
    Call<InsertOrderList> getinsertorder(@Body RequestBody body);

    @GET("ItemDetails/GetSubCategoryItemsDetailswithbanners")
    Call<ItemsList> getItemsList();
}
