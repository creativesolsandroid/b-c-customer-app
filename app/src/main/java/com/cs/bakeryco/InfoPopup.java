package com.cs.bakeryco;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cs.bakeryco.activities.PastriesSelection;
import com.cs.bakeryco.adapters.SectionRecyclerViewAdapter;
import com.cs.bakeryco.adapters.ViewPagerAdapter;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubItems;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.bakeryco.activities.PastriesSelection.boxSize;
import static com.cs.bakeryco.activities.PastriesSelection.boxcount;
import static com.cs.bakeryco.activities.PastriesSelection.boxpos;
import static com.cs.bakeryco.activities.PastriesSelection.indivialcount;
import static com.cs.bakeryco.activities.PastriesSelection.subitemQty;
import static com.cs.bakeryco.activities.PastriesSelection.totalCount;
import static com.cs.bakeryco.activities.PastriesSelection.visible;

/**
 * Created by CS on 4/18/2018.
 */

public class InfoPopup extends Activity {

//    ImageView mitem_image;
//    TextView mitem_desc, mclose_popup, itemname;

    String Image, desc, mitemname, desc_ar, mitemname_ar, cat_name;

    ArrayList<Section> sections = new ArrayList<>();
    int pos, value, value1;
    int qty = 0;

    Bitmap icon;

    ViewPagerAdapter mAdapter;
    LinearLayout mseekbar_layout;
    RelativeLayout msecond_layout;

    int localCountplus;


    ViewPager viewPager;
    TextView mcancel, mcat_name, mview_page_count, mitem_name, mitem_desc, madd;
    ImageView mshare, mitem_image;

    TextView mbox_pos, mbox_max_value;

    DataBaseHelper myDbHelper;

    ImageView minus, plus;
    EditText itemQty;

    int In_totalCount;

//    TabLayout tab;

    SeekBar seekBar;

    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.pop_info);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.pop_info_arabic);
        }

        viewPager = findViewById(R.id.viewpager);
        mcancel = findViewById(R.id.cancel);
        mcat_name = findViewById(R.id.cat_name);
        mshare = findViewById(R.id.share_icon);
        mitem_name = findViewById(R.id.item_name);
        mitem_desc = findViewById(R.id.item_desc);
        mview_page_count = findViewById(R.id.view_pager_count);
        madd = findViewById(R.id.add);
        seekBar = findViewById(R.id.seekbar);
        mbox_max_value = findViewById(R.id.box_max_value);
        mbox_pos = findViewById(R.id.box_pos);
        mseekbar_layout = findViewById(R.id.seekbar_layout);
        msecond_layout = findViewById(R.id.second);

//        tab = findViewById(R.id.tab_layout);

        minus = findViewById(R.id.donut_minus);
        plus = findViewById(R.id.donut_plus);
        itemQty = findViewById(R.id.donut_qty);

        sections = (ArrayList<Section>) getIntent().getSerializableExtra("section");
        pos = getIntent().getIntExtra("pos", 0);
        value = getIntent().getIntExtra("value", 0);
        value1 = getIntent().getIntExtra("value1", 0);
        qty = getIntent().getIntExtra("qty", 0);
        cat_name = getIntent().getStringExtra("cat_name");
        desc = getIntent().getStringExtra("desc");
        mitemname = getIntent().getStringExtra("itemname");
        desc_ar = getIntent().getStringExtra("desc_ar");
        mitemname_ar = getIntent().getStringExtra("itemname_ar");

        Log.i("TAG", "image " + Image);
        Log.i("TAG", "desc " + qty);


        myDbHelper = new DataBaseHelper(InfoPopup.this);

//        itemQty.setText(String.valueOf(qty));

//        if (getIntent().getStringExtra("info").equals("box")) {
//            for (int k = 0; k < sections.get(pos).getChildItems().get(value).getSubItems().size(); k++) {
////            selectedCountIndividual.add(0);
//                boxcount.add(0);
//            }
//        }else {
//            for (int k = 0; k < sections.get(pos).getChildItems().size(); k++) {
//            selectedCountIndividual.add(0);
//                if (!indivialname.contains(sections.get(pos).getChildItems().get(k).getSubItems().get())) {
//                    indivialname.add(sections.get(pos).getChildItems().get(k).getItemName());
//                    indivialcount.add(0);
//                }
//            }
//        }


        if (getIntent().getStringExtra("info").equals("box")) {
            mseekbar_layout.setVisibility(View.VISIBLE);
            seekBar.setMax(boxSize);

            mbox_max_value.setText("" + boxSize);

        } else {
            mseekbar_layout.setVisibility(View.GONE);
//            RelativeLayout relativeLayout = new RelativeLayout(this);
//// Create LayoutParams for it // Note 200 200 is width, height in pixels
//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//// Align bottom-right, and add bottom-margin
//            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//            params.bottomMargin = 10;
//
//            relativeLayout.setLayoutParams(params);
//            msecond_layout.addView(relativeLayout);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)msecond_layout.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//            params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);

            msecond_layout.setLayoutParams(params);
        }

        mcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mcat_name.setText(cat_name);

        if (language.equalsIgnoreCase("En")) {
            mitem_name.setText(mitemname);
            mitem_desc.setText(desc);
        } else {
            mitem_name.setText(mitemname_ar);
            mitem_desc.setText(desc_ar);
        }

//        mAdapter = new ViewPagerAdapter(InfoPopup.this, cat_name, sections, value, value1, language, pos, getIntent().getStringExtra("info"), InfoPopup.this);
//        viewPager.setAdapter(mAdapter);
//        tab.setupWithViewPager(viewPager,true);
        viewPager.setCurrentItem(pos);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {

                Log.e("TAG", "currentpos: " + position);

                if (getIntent().getStringExtra("info").equals("box")) {

                    if (language.equalsIgnoreCase("En")) {

                        mitem_name.setText(sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                        mitem_desc.setText(sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemDesc());
                        int maxsize = sections.get(value).getChildItems().get(value1).getSubItems().size();
                        mview_page_count.setText(+position + 1 + "/" + maxsize);

                        mshare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Uri imageUri = Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage());

                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_TEXT,
                                        "Backery & Company \n " + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName() + "\n " + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemDesc() + "\n Know More : http://onelink.to/5wsjta");

//                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                        icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
//                        try {
//                            f.createNewFile();
//                            FileOutputStream fo = new FileOutputStream(f);
//                            fo.write(bytes.toByteArray());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage()));
//                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                                intent.putExtra(Intent.EXTRA_STREAM, imageUri);

                                startActivity(Intent.createChooser(intent, "Share"));

                            }
                        });

                        itemQty.setText("" + PastriesSelection.boxcount.get(position));
//                        In_totalCount = 0;
//                        for (Integer i : PastriesSelection.boxcount) {
//                            In_totalCount = In_totalCount + i;
//                        }
//
////                        In_totalCount = In_totalCount + 1;
//                        In_totalCount = In_totalCount + 1;
//                        if (In_totalCount <= boxSize) {
//                            mbox_pos.setText("" + In_totalCount);
//                            seekBar.setProgress(In_totalCount);
//                        }
                        Log.i("TAG", "onPageScrolled: " + In_totalCount);

                        plus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (getIntent().getStringExtra("info").equals("box")) {

                                    if (visible) {

                                        showErrorDialog("Please select the size to proceed with item selection");

                                    } else {

                                        In_totalCount = 0;
                                        for (Integer i : PastriesSelection.boxcount) {
                                            In_totalCount = In_totalCount + i;
                                        }

                                        In_totalCount = In_totalCount + 1;
                                        if (In_totalCount <= boxSize) {

                                            localCountplus = PastriesSelection.boxcount.get(position);
                                            PastriesSelection.boxcount.set(position, (localCountplus + 1));
                                            itemQty.setText("" + (localCountplus + 1));
                                            itemQty.setSelection(itemQty.length());
//                                            AddItems(position, (localCountplus + 1));
                                            qty = In_totalCount;
                                            mbox_pos.setText("" + qty);
                                            seekBar.setProgress(qty);


                                        } else {
                                            if (language.equals("En")) {
                                                showErrorDialog("You are done with maximum " + PastriesSelection.boxSize + " " + PastriesSelection.title + " per box, to order more use other box.");
                                            } else {
                                                showErrorDialog("Please enter less than or equals to box size");
                                            }
                                        }
                                    }

                                }

                            }
                        });

                        minus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (visible) {

                                    showErrorDialog("Please select the size to proceed with item selection");
                                } else {
                                    if (itemQty.length() > 0) {
                                        if (Integer.parseInt(itemQty.getText().toString()) > 0) {
                                            int localCount = boxcount.get(position);
                                            boxcount.set(position, (localCount - 1));
                                            itemQty.setText("" + (localCount - 1));
                                            itemQty.setSelection(itemQty.length());
//                                            AddItems(this.getAdapterPosition(), (localCount - 1));
                                            Log.i("TAG", "onClick: " + localCount);
                                        }
                                    }
                                }
                            }
                        });

                        madd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                for (int i = 0; i < boxcount.size(); i++){

                                    PastriesSelection.selectedCount.set(i,boxcount.get(i));
                                    AddItems(i,boxcount.get(i));

                                }

                                totalCount = In_totalCount;

                                finish();
                            }
                        });


                    } else if (language.equalsIgnoreCase("Ar")) {

                        mitem_name.setText(sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemName_Ar());
                        mitem_desc.setText(sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemDesc_Ar());
                        int maxsize = sections.get(value).getChildItems().get(value1).getSubItems().size();
                        mview_page_count.setText(+position + 1 + "/" + maxsize);

                        mshare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Uri imageUri = Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage());

                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_TEXT,
                                        "بيكري & كومباني \n " + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemName_Ar() + "\n " + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemDesc_Ar() + "\n Know More : http://onelink.to/5wsjta");

//                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                        icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
//                        try {
//                            f.createNewFile();
//                            FileOutputStream fo = new FileOutputStream(f);
//                            fo.write(bytes.toByteArray());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage()));
//                                intent.setType("image/*");
//                                intent.putExtra(Intent.EXTRA_STREAM, imageUri);

                                startActivity(Intent.createChooser(intent, "Share"));

                            }
                        });

                        itemQty.setText("" + PastriesSelection.boxcount.get(position));
//                        In_totalCount = 0;
//                        for (Integer i : PastriesSelection.boxcount) {
//                            In_totalCount = In_totalCount + i;
//                        }
//
////                        In_totalCount = In_totalCount + 1;
//                        In_totalCount = In_totalCount + 1;
//                        if (In_totalCount <= boxSize) {
//                            mbox_pos.setText("" + In_totalCount);
//                            seekBar.setProgress(In_totalCount);
//                        }
                        Log.i("TAG", "onPageScrolled: " + In_totalCount);

                        plus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (getIntent().getStringExtra("info").equals("box")) {

                                    if (visible) {

                                        showErrorDialog("Please select the size to proceed with item selection");

                                    } else {

                                        In_totalCount = 0;
                                        for (Integer i : PastriesSelection.boxcount) {
                                            In_totalCount = In_totalCount + i;
                                        }

                                        In_totalCount = In_totalCount + 1;
                                        if (In_totalCount <= boxSize) {

                                            localCountplus = PastriesSelection.boxcount.get(position);
                                            PastriesSelection.boxcount.set(position, (localCountplus + 1));
                                            itemQty.setText("" + (localCountplus + 1));
                                            itemQty.setSelection(itemQty.length());
//                                            AddItems(position, (localCountplus + 1));
                                            qty = In_totalCount;
                                            mbox_pos.setText("" + qty);
                                            seekBar.setProgress(qty);


                                        } else {
                                            if (language.equals("En")) {
                                                showErrorDialog("You are done with maximum " + PastriesSelection.boxSize + " " + PastriesSelection.title + " per box, to order more use other box.");
                                            } else {
                                                showErrorDialog("Please enter less than or equals to box size");
                                            }
                                        }
                                    }

                                }

                            }
                        });

                        minus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (visible) {

                                    showErrorDialog("Please select the size to proceed with item selection");
                                } else {
                                    if (itemQty.length() > 0) {
                                        if (Integer.parseInt(itemQty.getText().toString()) > 0) {
                                            int localCount = boxcount.get(position);
                                            boxcount.set(position, (localCount - 1));
                                            itemQty.setText("" + (localCount - 1));
                                            itemQty.setSelection(itemQty.length());
//                                            AddItems(this.getAdapterPosition(), (localCount - 1));
                                            Log.i("TAG", "onClick: " + localCount);
                                        }
                                    }
                                }
                            }
                        });

                        madd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                for (int i = 0; i < boxcount.size(); i++){

                                    PastriesSelection.selectedCount.set(i,boxcount.get(i));
                                    AddItems(i,boxcount.get(i));

                                }

                                totalCount = In_totalCount;

                                finish();
                            }
                        });

                    }

                } else if (getIntent().getStringExtra("info").equals("indivial")) {

                    if (language.equalsIgnoreCase("En")) {

                        mitem_name.setText(sections.get(value).getChildItems().get(position).getItemName());
                        mitem_desc.setText(sections.get(value).getChildItems().get(position).getDescription());
                        int maxsize = sections.get(value).getChildItems().size();
                        mview_page_count.setText(+position + 1 + "/" + maxsize);

                        mshare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

//                                Uri imageUri = Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(position).getImage());

                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_TEXT,
                                        "Backery & Company \n " + sections.get(value).getChildItems().get(position).getItemName() + "\n " + sections.get(value).getChildItems().get(position).getDescription() + "\n Know More : http://onelink.to/5wsjta");

//                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                        icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
//                        try {
//                            f.createNewFile();
//                            FileOutputStream fo = new FileOutputStream(f);
//                            fo.write(bytes.toByteArray());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage()));
//                                intent.setType("image/*");
//                                intent.putExtra(Intent.EXTRA_STREAM, imageUri);

                                startActivity(Intent.createChooser(intent, "Share"));

                            }
                        });

//                        if (PastriesSelection.selectedCountIndividual.get(position) != 0) {
//                            if (PastriesSelection.selectedCountIndividual.get(position) <= indivialcount.get(position) && indivialcount.get(position) != 0 || selectedCountIndividual.get(position) >= indivialcount.get(position) && indivialcount.get(position) != 0) {
//                                indivialcount.set(position, PastriesSelection.selectedCountIndividual.get(position));
                                itemQty.setText("" + indivialcount.get(position));
//                            }
//                        }


                        plus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (getIntent().getStringExtra("info").equals("indivial")) {

                                    int localCountplus;
//                                    if (finalcount){
//                                        localCountplus = PastriesSelection.counts.get(position);
//                                    }else {
                                        localCountplus = indivialcount.get(position);
//                                    }

                                    if (localCountplus < 9999) {
//                                        if (finalcount) {
//                                            PastriesSelection.counts.set(position, (localCountplus + 1));
//                                        }else {
                                            indivialcount.set(position, (localCountplus + 1));
//                                        }
                                        itemQty.setText("" + (localCountplus + 1));
                                        itemQty.setSelection(itemQty.getText().toString().length());
                                        qty = localCountplus + 1;
//                                        AddItems(position, (localCountplus + 1));
                                    }
                                }

                            }
                        });

                        minus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (getIntent().getStringExtra("info").equals("indivial")) {

                                    if (Integer.parseInt(itemQty.getText().toString()) > 0) {
                                        int localCount1;
//                                        if (finalcount) {
//                                            localCount1 = PastriesSelection.counts.get(this.getAdapterPosition());
//                                            PastriesSelection.counts.set(this.getAdapterPosition(), (localCount1 - 1));
//                                        }else {
                                        localCount1 = indivialcount.get(position);
//                                            PastriesSelection.selectedCountIndividual.set(this.getAdapterPosition(), (localCount1 - 1));
                                        PastriesSelection.indivialcount.set(position, (localCount1 - 1));
//                                        }
                                        itemQty.setText("" + (localCount1 - 1));
                                        itemQty.setSelection(itemQty.getText().toString().length());
//                                        AddItems(this.getAdapterPosition(), (localCount1 - 1));
                                        qty = localCount1 - 1;
                                    }
                                }

                            }
                        });



                        Log.i("TAG", "onPageScrolled: " + indivialcount);
                        Log.i("TAG", "onPageScrolled: " + indivialcount.size());

                        madd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                for (int i = 0; i < indivialcount.size(); i++){

                                    PastriesSelection.selectedCountIndividual.set(i,indivialcount.get(i));
                                    AddItems(i,indivialcount.get(i));

                                }

                                finish();
                            }
                        });



                    } else if (language.equalsIgnoreCase("Ar")) {

                        mitem_name.setText(sections.get(value).getChildItems().get(position).getItemNameAr());
                        mitem_desc.setText(sections.get(value).getChildItems().get(position).getDescriptionAr());
                        int maxsize = sections.get(value).getChildItems().size();
                        mview_page_count.setText(+position + 1 + "/" + maxsize);

                        mshare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

//                                Uri imageUri = Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(position).getImage());
//
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_TEXT,
                                        "بيكري & كومباني \n " + sections.get(value).getChildItems().get(position).getItemNameAr() + "\n " + sections.get(value).getChildItems().get(position).getDescriptionAr() + "\n Know More : http://onelink.to/5wsjta");

//                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                        icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//                        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
//                        try {
//                            f.createNewFile();
//                            FileOutputStream fo = new FileOutputStream(f);
//                            fo.write(bytes.toByteArray());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Constants.IMAGE_URL + sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubItemimage()));
//                                intent.setType("image/*");
//                                intent.putExtra(Intent.EXTRA_STREAM, imageUri);
//                                Log.e("TAG", "onClick: " + imageUri );

                                startActivity(Intent.createChooser(intent, "Share"));

                            }
                        });

                        itemQty.setText("" + indivialcount.get(position));
//                            }
//                        }


                        plus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (getIntent().getStringExtra("info").equals("indivial")) {

                                    int localCountplus;
//                                    if (finalcount){
//                                        localCountplus = PastriesSelection.counts.get(position);
//                                    }else {
                                    localCountplus = indivialcount.get(position);
//                                    }

                                    if (localCountplus < 9999) {
//                                        if (finalcount) {
//                                            PastriesSelection.counts.set(position, (localCountplus + 1));
//                                        }else {
                                        indivialcount.set(position, (localCountplus + 1));
//                                        }
                                        itemQty.setText("" + (localCountplus + 1));
                                        itemQty.setSelection(itemQty.getText().toString().length());
                                        qty = localCountplus + 1;
//                                        AddItems(position, (localCountplus + 1));
                                    }
                                }

                            }
                        });

                        minus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (getIntent().getStringExtra("info").equals("indivial")) {

                                    if (Integer.parseInt(itemQty.getText().toString()) > 0) {
                                        int localCount1;
//                                        if (finalcount) {
//                                            localCount1 = PastriesSelection.counts.get(this.getAdapterPosition());
//                                            PastriesSelection.counts.set(this.getAdapterPosition(), (localCount1 - 1));
//                                        }else {
                                        localCount1 = indivialcount.get(position);
//                                            PastriesSelection.selectedCountIndividual.set(this.getAdapterPosition(), (localCount1 - 1));
                                        PastriesSelection.indivialcount.set(position, (localCount1 - 1));
//                                        }
                                        itemQty.setText("" + (localCount1 - 1));
                                        itemQty.setSelection(itemQty.getText().toString().length());
//                                        AddItems(this.getAdapterPosition(), (localCount1 - 1));
                                        qty = localCount1 - 1;
                                    }
                                }

                            }
                        });



                        Log.i("TAG", "onPageScrolled: " + indivialcount);
                        Log.i("TAG", "onPageScrolled: " + indivialcount.size());

                        madd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                for (int i = 0; i < indivialcount.size(); i++){

                                    PastriesSelection.selectedCountIndividual.set(i,indivialcount.get(i));
                                    AddItems(i,indivialcount.get(i));

                                }

                                finish();
                            }
                        });


                    }

                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        Log.e("TAG", "currentpos: " + viewPager.getVerticalScrollbarPosition());
    }

    private void AddItems(final int position, final int quantity) {

        if (getIntent().getStringExtra("info").equals("indivial")) {

        int localSize = PastriesSelection.itemQty.size();
        ArrayList<String> localArray = new ArrayList<>();
        localArray.addAll(PastriesSelection.itemQty);
        for (int i = 0; i < localSize; i++) {
            if (localArray.get(i).equals(sections.get(value).getChildItems().get(position).getItemId())) {
                SectionRecyclerViewAdapter.Qty = SectionRecyclerViewAdapter.Qty - 1;
                SectionRecyclerViewAdapter.orderPrice = SectionRecyclerViewAdapter.orderPrice - Float.parseFloat(sections.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());
                PastriesSelection.itemQty.remove(sections.get(value).getChildItems().get(position).getItemId());
            } else {
//            Do nothing
            }
        }

        for (int j = 0; j < quantity; j++) {
            SectionRecyclerViewAdapter.Qty = SectionRecyclerViewAdapter.Qty + 1;
            SectionRecyclerViewAdapter.orderPrice = SectionRecyclerViewAdapter.orderPrice + Float.parseFloat(sections.get(value).getChildItems().get(position).getPriceList().get(0).getPrice());
            PastriesSelection.itemQty.add(sections.get(value).getChildItems().get(position).getItemId());

            Log.i("TAG", "AddItems: " + SectionRecyclerViewAdapter.Qty);
//            plus.setClickable(true);
//            int qty = Integer.parseInt(mitemQty.getText().toString());
//            if (qty == 9999) {
//                plus.setClickable(false);
//            }
        }


        PastriesSelection.itemfinalQty.add(String.valueOf(SectionRecyclerViewAdapter.Qty));
        PastriesSelection.itemfinalprice.add(String.valueOf(SectionRecyclerViewAdapter.orderPrice));

//        Log.i("TAG1", "AddItems: " + PastriesSelection.itemfinalQty.size());
         Log.i("TAG1", "onResume: "+PastriesSelection.itemQty.size());
        if (PastriesSelection.itemfinalQty.size() == 0) {

            SectionRecyclerViewAdapter.orderPrice = 0;
            SectionRecyclerViewAdapter.Qty = 0;
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                if (SectionRecyclerViewAdapter.Qty == 0) {
                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } else {
                    PastriesSelection.orderPrice.setText("" + decimalFormat.format(SectionRecyclerViewAdapter.orderPrice) + " SR");
                    PastriesSelection.orderQuantity.setText("" + SectionRecyclerViewAdapter.Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                if (SectionRecyclerViewAdapter.Qty == 0) {
                    PastriesSelection.orderPrice.setText("-");
                    PastriesSelection.orderQuantity.setText("-");
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                } else {
                    PastriesSelection.orderPrice.setText("" + decimalFormat.format(SectionRecyclerViewAdapter.orderPrice) + " SR");
                    PastriesSelection.orderQuantity.setText("" + SectionRecyclerViewAdapter.Qty);
                    PastriesSelection.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


//        madd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {

//                PastriesSelection.selectedCount.set(position, (localCountplus + 1));
//                    AddItems(position, (localCountplus + 1));

//                    int localSize = PastriesSelection.itemQty.size();
//                    ArrayList<String> localArray = new ArrayList<>();
//                    localArray.addAll(PastriesSelection.itemQty);
//                    for (int i = 0; i < localSize; i++) {
//
//                    }

        }else {
            int localSize = PastriesSelection.selectedItems.size();
            ArrayList<SubItems> localArray = new ArrayList<>();
            localArray.addAll(PastriesSelection.selectedItems);
            for (int i = 0; i < localSize; i++) {
                if (localArray.get(i).equals(sections.get(value).getChildItems().get(value1).getSubItems().get(position))) {
                    PastriesSelection.selectedItems.remove(sections.get(value).getChildItems().get(value1).getSubItems().get(position));
                    subitemQty.remove(sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                    String pos = Integer.toString(position);
                    boxpos.remove(pos);
                    Log.i("TAG", "box pos remove " + boxpos);
                    Log.i("TAG", "subitemqty remove " + subitemQty);
                } else {
//            Do nothing
                }
            }

            for (int j = 0; j < quantity; j++) {
                PastriesSelection.selectedItems.add(sections.get(value).getChildItems().get(value1).getSubItems().get(position));
                subitemQty.add(sections.get(value).getChildItems().get(value1).getSubItems().get(position).getSubitemName());
                String pos = Integer.toString(position);
                if (boxpos != null && !boxpos.contains(pos)) {
                    boxpos.add(pos);
                    Log.i("TAG", "box pos " + boxpos);
                    Log.i("TAG", "subitemqty " + subitemQty);
                }
//           else {
//                boxpos.remove(pos);
//            }
            }
        }

//                finish();

//            }
//        });

    }

    public void showErrorDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(InfoPopup.this, android.R.style.Theme_Material_Light_Dialog));

        alertDialogBuilder.setTitle("BAKERY & Co.");

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
