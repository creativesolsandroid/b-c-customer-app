package com.cs.bakeryco;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.activities.AboutUs;
import com.cs.bakeryco.activities.EditProfile;
import com.cs.bakeryco.activities.FavoriteOrderActivity;
import com.cs.bakeryco.activities.LanguageActivity;
import com.cs.bakeryco.activities.LoginActivity;
import com.cs.bakeryco.activities.LoginRegistration;
import com.cs.bakeryco.activities.OrderActivity;
import com.cs.bakeryco.activities.SplashActivity;
import com.cs.bakeryco.fragments.MainFragment;
import com.cs.bakeryco.fragments.MoreFragment;
import com.cs.bakeryco.fragments.OrderFragment;
import com.cs.bakeryco.model.ItemsList;
import com.google.android.gms.maps.model.LatLng;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView prof_name;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    DrawerListAdapter adapter;
    private String[] mSidemenuTitles;
    private int[] mSidemenuIcons;
    private ListView mDrawerList;
//    private ImageView mTitleTV;
//    TextView langAr, langEng;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    Double lat, longi;

    private LinearLayout mDrawerLinear;
    private String mTitle, mAppTitle;
    FragmentManager fragmentManager = getSupportFragmentManager();
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences languagePrefs, userPrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    SharedPreferences.Editor userPrefEditor;

    DataBaseHelper myDbHelper;

    CircleImageView profile_pic;
    TextView user_name;


    String mLoginStatus;
    String profile;
    TextView cs, app_version;
    LinearLayout cs_website;

    ArrayList<ItemsList.Items> itemsArrayList = new ArrayList<>();
    ArrayList<ItemsList.Banners> bannerArrayList = new ArrayList<>();

    Boolean menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        profile = userPrefs.getString("user_profile", null);

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_main1);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_main1_ar);
        }

        myDbHelper = new DataBaseHelper(MainActivity.this);

        itemsArrayList = SplashActivity.itemsArrayList;
        bannerArrayList = SplashActivity.bannersArrayList;

        Log.i("TAG", "itemssize: " + itemsArrayList.size());
        Log.i("TAG", "bannerssize: " + bannerArrayList.size());

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                }
            } else {
                getGPSCoordinates();
            }
        } else {
            getGPSCoordinates();
        }

        mAppTitle = (String) getTitle();
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
//        langAr = (TextView) findViewById(R.id.language_arabic);
//        langEng = (TextView) findViewById(R.id.language_eng);
        mLoginStatus = userPrefs.getString("login_status", "");
        if (mLoginStatus.equalsIgnoreCase("loggedin")) {
            if (language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"Home", "Menu", "Favourite Orders",
                        "More", "About Us", "Change Language", "Log Out" };
//            langAr.setVisibility(View.VISIBLE);
//            langEng.setVisibility(View.GONE);
            } else if (language.equalsIgnoreCase("Ar")) {
                mSidemenuTitles = new String[]{"الرئيسية", "القائمة", "المفضلة",
                        "المزيد", "عنا", "تغيير اللغة", "الخروج" };
//            langAr.setVisibility(View.GONE);
//            langEng.setVisibility(View.VISIBLE);
            }
        } else {
            if (language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"Home", "Menu", "Favourite Orders",
                        "More", "About Us", "Change Language", "Log In"};
//            langAr.setVisibility(View.VISIBLE);
//            langEng.setVisibility(View.GONE);
            } else if (language.equalsIgnoreCase("Ar")) {

                mSidemenuTitles = new String[]{"الرئيسية", "القائمة", "المفضلة",
                        "المزيد", "عنا", "تغيير اللغة", "دخول"};
//            langAr.setVisibility(View.GONE);
//            langEng.setVisibility(View.VISIBLE);
            }
        }
//        mSidemenuTitles1 = new String[] { "Main", "Store", "Menu", "Account",
//                "Share Our App","Rate Our App" };
        mSidemenuIcons = new int[]{R.drawable.menu_main, R.drawable.menu_order, R.drawable.menu_more, R.drawable.menu_share, R.drawable.menu_rating};
//        mTitle = mSidemenuTitles1[0];
//        mTitle = "Oregano Pi";
        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                mSidemenuTitles, mSidemenuIcons, language);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLinear = (LinearLayout) findViewById(R.id.left_drawer);

        cs_website = findViewById(R.id.cs_website);

        cs = findViewById(R.id.cs);
        app_version = findViewById(R.id.app_version);

        profile_pic = findViewById(R.id.profile_pic);

        user_name = findViewById(R.id.username);

        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Profile();

            }
        });

        mLoginStatus = userPrefs.getString("login_status", "");
        if (mLoginStatus.equalsIgnoreCase("loggedin")) {
            if (profile != null) {
                try {


                    JSONObject property = new JSONObject(profile);
                    JSONObject userObjuect = property.getJSONObject("profile");

//                String parts[] = userObjuect.getString("phone").split("-");

//                userId = userObjuect.getString("userId");
                    user_name.setText("" + userObjuect.getString("fullName"));


                } catch (JSONException e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }

            }
        } else {

            if (language.equalsIgnoreCase("En")) {
                user_name.setText("Sign in");
            } else {
                user_name.setText("الدخول");
            }

        }

//        mTitleTV = (ImageView) findViewById(R.id.header_title) ;
        configureToolbar();
        configureDrawer();
        mDrawerList.setAdapter(adapter);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        app_version.setText("App version " + version);

        cs.setPaintFlags(cs.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


//        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();


        try {
            menu = getIntent().getBooleanExtra("menu", false);
        } catch (Exception e) {
            menu = false;
            e.printStackTrace();
        }

        Log.i("TAG", "menu: " + menu);

        if (menu) {

            Fragment newFragment = new OrderFragment();
//            Bundle args = new Bundle();
//            args.putSerializable("itemArraylist", itemsArrayList);
//            args.putSerializable("bannersArraylist", bannerArrayList);
//            newFragment.setArguments(args);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, newFragment).commit();

        } else {

            Fragment newFragment = new MainFragment();
//            Bundle args = new Bundle();
//            args.putSerializable("itemArraylist", itemsArrayList);
//            args.putSerializable("bannersArraylist", bannerArrayList);
//            newFragment.setArguments(args);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, newFragment).commit();

        }

        try {
            int start = getIntent().getIntExtra("startWith", -1);
            if (start != -1) {
                selectItem(start);
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

//        if(language.equalsIgnoreCase("En")){
//            mTitleTV.setText("Bakery & Co.");
//        }else if(language.equalsIgnoreCase("Ar")) {
//            mTitleTV.setText("بيكري & كومباني");
//        }

//        langAr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                languagePrefsEditor.putString("language","Ar");
//                languagePrefsEditor.commit();
//                recreate();
//            }
//        });
//
//        langEng.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                languagePrefsEditor.putString("language","En");
//                languagePrefsEditor.commit();
//                recreate();
//            }
//        });

    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(MainActivity.this);
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
//                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(MainActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                } else {
                    Toast.makeText(MainActivity.this, "Location permission denied, unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void configureToolbar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mainToolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        mTitleTV.setText(mSidemenuTitles1[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (language.equalsIgnoreCase("En")) {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);

                    } else {
                        mDrawerLayout.openDrawer(GravityCompat.START);
                    }
                } else if (language.equalsIgnoreCase("Ar")) {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                        mDrawerLayout.closeDrawer(GravityCompat.END);

                    } else {
                        mDrawerLayout.openDrawer(GravityCompat.END);
                    }
                }


//                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
//                    mDrawerLayout.closeDrawer(GravityCompat.START);
//
//                } else {
//                    mDrawerLayout.openDrawer(GravityCompat.START);
//                }
            }
        });
    }

    public void menuClick() {
        if (language.equalsIgnoreCase("En")) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);

            } else {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        } else {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                mDrawerLayout.closeDrawer(GravityCompat.END);

            } else {
                mDrawerLayout.openDrawer(GravityCompat.END);
            }
        }
    }

    public void configureDrawer() {
        // Configure drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_closed) {

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
//                mTitleTV.setText(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
//                mTitleTV.setText(mAppTitle);
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }


    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            view.setSelected(true);
            selectItem(position);
        }
    }

    public void Profile() {

        mLoginStatus = userPrefs.getString("login_status", "");
        if (mLoginStatus.equalsIgnoreCase("loggedin")) {
            Fragment newFragment = new EditProfile();

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, newFragment).commit();
        } else {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        if (language.equalsIgnoreCase("En")) {

            mDrawerLayout.closeDrawer(GravityCompat.START);

        } else {

            mDrawerLayout.closeDrawer(GravityCompat.END);

        }

    }

    public void selectItem(int position) {

        // update the main content by replacing fragments
        switch (position) {
            case 0:
                if (language.equalsIgnoreCase("En")) {
//                    langAr.setVisibility(View.VISIBLE);
//                    langEng.setVisibility(View.GONE);
//                    mTitleTV.setText("Bakery & Co");
                } else if (language.equalsIgnoreCase("Ar")) {
//                    langAr.setVisibility(View.GONE);
//                    langEng.setVisibility(View.VISIBLE);
//                    mTitleTV.setText("بيكري & كومباني");
                }

                mDrawerList.setItemChecked(position, true);
                if (language.equalsIgnoreCase("En")) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                }
                Fragment unitsFragment = new MainFragment();
//                Bundle args = new Bundle();
//                args.putSerializable("itemArraylist", itemsArrayList);
//                args.putSerializable("bannersArraylist", bannerArrayList);
//
//                unitsFragment.setArguments(args);
                fragmentManager.beginTransaction().replace(R.id.content_frame, unitsFragment).commit();
//
//                AnalyticsManager.sendScreenView("Units Screen");
                break;

            case 1:
//                langAr.setVisibility(View.GONE);
//                langEng.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
//                    mTitleTV.setText("Order");
                } else if (language.equalsIgnoreCase("Ar")) {
//                    mTitleTV.setText("الطلب");
                }
                mDrawerList.setItemChecked(position, true);
                if (language.equalsIgnoreCase("En")) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                }
                Fragment orderFragment = new OrderFragment();
//                Bundle args1 = new Bundle();
//                args1.putSerializable("itemArraylist", itemsArrayList);
//                args1.putSerializable("bannersArraylist", bannerArrayList);
//
//                orderFragment.setArguments(args1);
                fragmentManager.beginTransaction().replace(R.id.content_frame, orderFragment).commit();
                break;

//            case 2:
//                Intent cateringIntent = new Intent(MainActivity.this, CateringActivity.class);
//                startActivity(cateringIntent);
//                break;
//
//            case 3:
//                Intent i = new Intent(MainActivity.this, TrackOrderActivity.class);
//                startActivity(i);
//                break;
            case 2:
//                langAr.setVisibility(View.GONE);
//                langEng.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
//                    mTitleTV.setText("More");
                } else if (language.equalsIgnoreCase("Ar")) {
//                    mTitleTV.setText("المزيد");
                }
                mDrawerList.setItemChecked(position, true);
                if (language.equalsIgnoreCase("En")) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                }

                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Fragment colorFragment = new FavoriteOrderActivity();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, colorFragment).commit();
                } else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                }


//                AnalyticsManager.sendScreenView("Color Coding Screen");
                break;

            case 3:

//                mTitle = mAppTitle;
                //mDrawerList.setItemChecked(position, true);
                if (language.equalsIgnoreCase("En")) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                }


                Fragment colorFragment = new MoreFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, colorFragment).commit();

//                new Handler().postDelayed(new Runnable() {
//
//                    // Using handler with postDelayed called runnable run method
//
//                    @Override
//                    public void run() {
//                        Intent intent = new Intent(Intent.ACTION_SEND);
//                        intent.setType("text/plain");
//                        intent.putExtra(Intent.EXTRA_TEXT,
//                                "https://play.google.com/store/apps/details?id=com.cs.bakeryco&hl=en");
//                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//                                "Check out Bakery & Company App !");
//                        startActivity(Intent.createChooser(intent, "Share"));
//                    }
//                }, 200);
                break;

            case 4:
//                mTitle = mAppTitle;
                //mDrawerList.setItemChecked(position, true);
                if (language.equalsIgnoreCase("En")) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                }

                Fragment colorFragment1 = new AboutUs();
                fragmentManager.beginTransaction().replace(R.id.content_frame, colorFragment1).commit();

//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.bakeryco")));
//
//                new Handler().postDelayed(new Runnable() {
//
//                    // Using handler with postDelayed called runnable run method
//
//                    @Override
//                    public void run() {
////                        AppRater.app_launched(MainActivity.this);
//                    }
//                }, 200);
                break;

            case 5:

                //                mTitle = mAppTitle;
                //mDrawerList.setItemChecked(position, true);
                if (language.equalsIgnoreCase("En")) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                }

                Fragment changelanguage = new LanguageActivity();
                fragmentManager.beginTransaction().replace(R.id.content_frame, changelanguage).commit();

//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.bakeryco")));
//
//                new Handler().postDelayed(new Runnable() {
//
//                    // Using handler with postDelayed called runnable run method
//
//                    @Override
//                    public void run() {
////                        AppRater.app_launched(MainActivity.this);
//                    }
//                }, 200);

                break;

            case 6:

                mDrawerList.setItemChecked(position, true);
                if (language.equalsIgnoreCase("En")) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.closeDrawer(GravityCompat.END);

                }

                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {

                    AlertDialog customDialog = null;
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout;
                    if (language.equalsIgnoreCase("En")) {
                        layout = R.layout.alert_dialog;
                    } else {
                        layout = R.layout.alert_dialog_arabic;
                    }
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);
                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);
//                desc.setText("Do you want to logout from this account?");
                    if (language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.app_name));
                        desc.setText("Do you want to logout from this account?");
                    } else {
                        title.setText(getResources().getString(R.string.app_name_ar));
                        desc.setText("هل انت متأكد من تسجيل الخروج؟");
                    }
                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    final AlertDialog finalCustomDialog = customDialog;
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            userPrefEditor.clear();
                            userPrefEditor.commit();
                            myDbHelper.deleteOrderTable();
                            if (language.equalsIgnoreCase("En")) {
                                mSidemenuTitles = new String[]{"Home", "Menu", "Favourite Orders",
                                        "More", "About Us", "Change Language", "Log In"};
//            langAr.setVisibility(View.VISIBLE);
//            langEng.setVisibility(View.GONE);
                            } else if (language.equalsIgnoreCase("Ar")) {

                                mSidemenuTitles = new String[]{"الرئيسية", "القائمة", "المفضلة",
                                        "المزيد", "عنا", "تغيير اللغة", "دخول"};
//            langAr.setVisibility(View.GONE);
//            langEng.setVisibility(View.VISIBLE);
                            }

                            finish();
                            Intent a = new Intent(MainActivity.this, MainActivity.class);
                            startActivity(a);


//                        ((MainActivity) getActivity()).selectItem(0);
                            finalCustomDialog.dismiss();
                        }
                    });
                    final AlertDialog finalCustomDialog1 = customDialog;
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finalCustomDialog1.dismiss();
                        }
                    });
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;
                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                } else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

                break;

            default:
                break;
        }

//		if (fragment != null)
//			extracted(position, fragment);
//		else {
//			// error in creating fragment
//			Log.e("MainActivity", "Error in creating fragment");
//		}

    }

    @Override
    public void onResume() {
        super.onResume();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        profile = userPrefs.getString("user_profile", null);
        mLoginStatus = userPrefs.getString("login_status", "");
        if (mLoginStatus.equalsIgnoreCase("loggedin")) {

            if (profile != null) {
                try {


                    JSONObject property = new JSONObject(profile);
                    JSONObject userObjuect = property.getJSONObject("profile");

//                String parts[] = userObjuect.getString("phone").split("-");

//                userId = userObjuect.getString("userId");
                    user_name.setText("" + userObjuect.getString("fullName"));


                } catch (JSONException e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }

            }

                if (language.equalsIgnoreCase("En")) {
                    mSidemenuTitles = new String[]{"Home", "Menu", "Favourite Orders",
                            "More", "About Us", "Change Language", "Log Out"};
//            langAr.setVisibility(View.VISIBLE);
//            langEng.setVisibility(View.GONE);
                } else if (language.equalsIgnoreCase("Ar")) {
                    mSidemenuTitles = new String[]{"الرئيسية", "القائمة", "المفضلة",
                            "المزيد", "عنا", "تغيير اللغة", "الخروج"};
//            langAr.setVisibility(View.GONE);
//            langEng.setVisibility(View.VISIBLE);
                }

        } else {

            if (language.equalsIgnoreCase("En")) {
                mSidemenuTitles = new String[]{"Home", "Menu", "Favourite Orders",
                        "More", "About Us", "Change Language", "Log In"};
//            langAr.setVisibility(View.VISIBLE);
//            langEng.setVisibility(View.GONE);
            } else if (language.equalsIgnoreCase("Ar")) {

                mSidemenuTitles = new String[]{"الرئيسية", "القائمة", "المفضلة",
                        "المزيد", "عنا", "تغيير اللغة", "دخول"};
//            langAr.setVisibility(View.GONE);
//            langEng.setVisibility(View.VISIBLE);
            }

            if (language.equalsIgnoreCase("En")) {
                user_name.setText("Sign in");
            } else {
                user_name.setText("الدخول");
            }
        }

        adapter = new DrawerListAdapter(this, R.layout.drawer_list_item,
                mSidemenuTitles, mSidemenuIcons, language);
        mDrawerList.setAdapter(adapter);



//        try {
//            if (menu) {
//                Fragment newFragment = new OrderFragment();
//                Bundle args = new Bundle();
//                args.putSerializable("itemArraylist", itemsArrayList);
//                args.putSerializable("bannersArraylist", bannerArrayList);
//                newFragment.setArguments(args);
//
//                FragmentManager fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().replace(R.id.content_frame, newFragment).commit();
//
//            } else {
//
//                Fragment newFragment = new MainFragment();
//                Bundle args = new Bundle();
//                args.putSerializable("itemArraylist", itemsArrayList);
//                args.putSerializable("bannersArraylist", bannerArrayList);
//                newFragment.setArguments(args);
//
//                FragmentManager fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().replace(R.id.content_frame, newFragment).commit();
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (language.equalsIgnoreCase("En")) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            } else if (!doubleBackToExitPressedOnce) {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press BACK again to exit.", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
            }
        } else {

            if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                mDrawerLayout.closeDrawer(GravityCompat.END);
            } else if (!doubleBackToExitPressedOnce) {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press BACK again to exit.", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
            }

        }


    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


}
