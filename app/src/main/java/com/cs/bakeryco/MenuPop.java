package com.cs.bakeryco;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.bakeryco.adapters.MenuPopUpGridAdapter;
import com.cs.bakeryco.model.Items;
import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.MainCategories;
import com.cs.bakeryco.model.Price;
import com.cs.bakeryco.model.Section;
import com.cs.bakeryco.model.SubCategories;
import com.cs.bakeryco.model.SubItems;
import com.cs.bakeryco.widgets.NetworkUtil;
import com.cs.bakeryco.widgets.SpacesItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MenuPop extends Activity implements View.OnClickListener {

    SharedPreferences languagePrefs;
    String language;
    TextView cake, pastries, sandwich, macarons, bread, offers, cancel;
    RecyclerView sub_cat_grid;
    ArrayList<ItemsList.Items> mainitem = new ArrayList<>();
    int catId;
    int position = 0;
    String response;
    MenuPopUpGridAdapter mSubMenuAdapter;
    ImageView menu_popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
//        if (language.equalsIgnoreCase("En")) {
        setContentView(R.layout.menu_pop_up);
//        } else if (language.equalsIgnoreCase("Ar")) {
//            setContentView(R.layout.pop_info_arabic);
//        }

        mainitem = (ArrayList<ItemsList.Items>) getIntent().getSerializableExtra("itemsArraylist");

        cancel = findViewById(R.id.cancel);
        cake = findViewById(R.id.cake);
        pastries = findViewById(R.id.pastries);
        sandwich = findViewById(R.id.sandwich);
        macarons = findViewById(R.id.macaron);
        bread = findViewById(R.id.bread);
        offers = findViewById(R.id.offer);
        sub_cat_grid = findViewById(R.id.sub_cat_grid);
        menu_popup = findViewById(R.id.menu_icon);

        sub_cat_grid.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        sub_cat_grid.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


//        new GetCategoryItems().execute(Constants.CATEGORY_ITEMS_URL);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        menu_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
        sub_cat_grid.setAdapter(mSubMenuAdapter);
        mSubMenuAdapter.notifyDataSetChanged();

        cake.setOnClickListener(this);
        pastries.setOnClickListener(this);
        sandwich.setOnClickListener(this);
        macarons.setOnClickListener(this);
        bread.setOnClickListener(this);
        offers.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.pastries:

                position = 1;

                mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
                sub_cat_grid.setAdapter(mSubMenuAdapter);

                break;

            case R.id.cake:

                position = 0;

                mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
                sub_cat_grid.setAdapter(mSubMenuAdapter);

                break;

            case R.id.sandwich:

                position = 4;

                mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
                sub_cat_grid.setAdapter(mSubMenuAdapter);

                break;
            case R.id.macaron:

                position = 2;

                mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
                sub_cat_grid.setAdapter(mSubMenuAdapter);

                break;

            case R.id.bread:

                position = 3;

                mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
                sub_cat_grid.setAdapter(mSubMenuAdapter);

                break;

            case R.id.offer:

                position = 5;

                mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
                sub_cat_grid.setAdapter(mSubMenuAdapter);

                break;
        }

    }

//    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String networkStatus;
//        ProgressDialog dialog;
//
//        @Override
//        protected void onPreExecute() {
//            mainitem.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(MenuPop.this);
//            dialog = ProgressDialog.show(MenuPop.this, "",
//                    "Loading items...");
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(MenuPop.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    if (result.equals("")) {
//                        Toast.makeText(MenuPop.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        try {
//                            JSONObject jo = new JSONObject(result);
//                            JSONArray ja = jo.getJSONArray("Success");
//
//                            for (int i = 0; i < ja.length(); i++) {
//
//                                MainCategories maincat = new MainCategories();
//                                ArrayList<SubCategories> subcatList = new ArrayList<>();
//
//                                JSONObject jo1 = ja.getJSONObject(i);
//
//                                maincat.setMaincatid(jo1.getString("MainCategoryId"));
//                                maincat.setMaincatname(jo1.getString("MainCategoryName"));
//                                maincat.setMaincatname_ar(jo1.getString("MainCategoryName_Ar"));
//                                maincat.setMaincatdesc(jo1.getString("MainCategoryDesc"));
//                                maincat.setMaincatdesc_ar(jo1.getString("MainCategoryDesc_Ar"));
//                                maincat.setMainimg(jo1.getString("Simg"));
//
//                                JSONArray ja1 = jo1.getJSONArray("SubCategoryItems");
//
//                                for (int k = 0; k < ja1.length(); k++) {
//
//                                    SubCategories subcat = new SubCategories();
//                                    ArrayList<Section> sectionList = new ArrayList<>();
//
//                                    JSONObject jo2 = ja1.getJSONObject(k);
//                                    subcat.setCatId(jo2.getString("SubCategoryId"));
//                                    subcat.setSubCat(jo2.getString("SubCategoryName"));
//                                    subcat.setSubCatAr(jo2.getString("SubCategoryName_Ar"));
//                                    subcat.setDescription(jo2.getString("SubCategoryDesc"));
//                                    subcat.setDescriptionAr(jo2.getString("SubCategoryDesc_Ar"));
//                                    subcat.setImages(jo2.getString("SubCategoryImage"));
//                                    subcat.setSubCatSeq(jo2.getString("SubCategorySeq"));
//                                    subcat.setMaincatid(jo2.getString("MainCategoryId"));
//
//
//                                    JSONArray ja2 = jo2.getJSONArray("Sections");
//
//                                    for (int l = 0; l < ja2.length(); l++) {
//
//                                        Section section = new Section();
//                                        ArrayList<Items> itemlist = new ArrayList<>();
//
//                                        JSONObject jo3 = ja2.getJSONObject(l);
//                                        section.setSectionid(jo3.getString("SectionId"));
//                                        section.setSectionname(jo3.getString("SectionName"));
//                                        section.setSectionname_ar(jo3.getString("SectionName_Ar"));
//                                        section.setSectionsubcat(jo3.getString("SectionSubCategory"));
//
//                                        JSONArray ja3 = jo3.getJSONArray("Items");
//
//                                        for (int m = 0; m < ja3.length(); m++) {
//
//                                            Items item = new Items();
//                                            ArrayList<Price> priceList = new ArrayList<>();
//                                            ArrayList<SubItems> subitemsList = new ArrayList<>();
//
//                                            JSONObject jo4 = ja3.getJSONObject(m);
//                                            item.setItemId(jo4.getString("ItemId"));
//                                            item.setSubCatId(jo4.getString("SubCategoryId"));
//                                            item.setSectionid(jo4.getString("SectionId"));
//                                            item.setItemName(jo4.getString("ItemName"));
//                                            item.setItemNameAr(jo4.getString("ItemName_Ar"));
//                                            item.setDescription(jo4.getString("ItemDesc"));
//                                            item.setDescriptionAr(jo4.getString("ItemDesc_Ar"));
//                                            item.setImage(jo4.getString("ItemImage"));
//                                            item.setItemtypeIds(jo4.getString("ItemTypeIds"));
//                                            item.setItemseq(jo4.getString("ItemSequence"));
//                                            item.setBuildingType(jo4.getString("BindingType"));
//                                            item.setIsdeliver(jo4.getString("IsDeliver"));
//                                            item.setNo_of_items(jo4.getString("NoOfItems"));
//                                            item.setDisplayname(jo4.getString("DisplayName"));
//                                            item.setDisplayname_ar(jo4.getString("DisplayName_Ar"));
//
//                                            JSONArray ja4 = jo4.getJSONArray("Price");
//
//                                            for (int n = 0; n < ja4.length(); n++) {
//                                                Price price = new Price();
//
//                                                JSONObject jo5 = ja4.getJSONObject(n);
//                                                price.setPrice(jo5.getString("price"));
//                                                price.setSizeid(jo5.getString("sizeId"));
//                                                price.setSize(jo5.getString("size"));
//                                                price.setSizeName_Ar(jo5.getString("SizeName_Ar"));
//                                                price.setItemsizedesc(jo5.getString("ItemSizeDescription"));
//                                                price.setItemSizeDescription_Ar(jo5.getString("ItemSizeDescription_Ar"));
//                                                price.setPriceId(jo5.getString("priceId"));
//
//                                                Log.i("TAG", "desc " + jo5.getString("ItemSizeDescription"));
//
//                                                priceList.add(price);
//                                            }
//
//                                            JSONArray ja5 = jo4.getJSONArray("SubItems");
//
//                                            try {
//                                                for (int o = 0; o < ja5.length(); o++) {
//                                                    SubItems subItems = new SubItems();
//
//                                                    JSONObject jo6 = ja5.getJSONObject(o);
//                                                    subItems.setItemid(jo6.getString("ItemId"));
//                                                    subItems.setSubitemid(jo6.getString("SubItemId"));
//                                                    subItems.setSubitemName(jo6.getString("SubItemName"));
//                                                    subItems.setSubItemName_Ar(jo6.getString("SubItemName_Ar"));
//                                                    subItems.setSubItemDesc(jo6.getString("SubItemDesc"));
//                                                    subItems.setSubItemDesc_Ar(jo6.getString("SubItemDesc_Ar"));
//                                                    subItems.setSubItemimage(jo6.getString("SubItemImage"));
//                                                    subItems.setSubItemseq(jo6.getString("SubItemSequence"));
//                                                    subItems.setSubItemIsdeliver(jo6.getString("SubItemIsDeliver"));
//
//                                                    subitemsList.add(subItems);
//
//                                                    Log.i("TAG", "subitem " + subitemsList.size());
//
//                                                }
//                                            } catch (JSONException e) {
//                                                e.printStackTrace();
//                                            }
//
//                                            item.setSubItems(subitemsList);
//                                            item.setPriceList(priceList);
//                                            itemlist.add(item);
//                                        }
//                                        section.setChildItems(itemlist);
//                                        sectionList.add(section);
//                                    }
//                                    subcat.setSections(sectionList);
//                                    subcatList.add(subcat);
//
//                                }
//                                maincat.setSubCategories(subcatList);
//                                mainitem.add(maincat);
//                                Log.e("TAG", "maincat " + mainitem.size());
//                            }
//
//                            Log.i("TAG", "position " + position);
//                            Log.i("TAG", "maincat " + mainitem);
//
//
//
//                            mSubMenuAdapter = new MenuPopUpGridAdapter(MenuPop.this, mainitem, position, language);
//                            sub_cat_grid.setAdapter(mSubMenuAdapter);
//                            mSubMenuAdapter.notifyDataSetChanged();
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            } else {
//                Toast.makeText(MenuPop.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }
////            mAdapter.notifyDataSetChanged();
//
////            if(subCatList.size()>0) {
////                mSubMenuAdapter = new SubMenuAdapter(MenuPop.this, subCatList, language);
////                mGridView.setAdapter(mSubMenuAdapter);
////                mSubMenuAdapter.notifyDataSetChanged();
////            }
////            int count =  mAdapter.getGroupCount();
////            for (int i = 0; i <count ; i++) {
////                mExpListView.collapseGroup(i);
////            }
//            super.onPostExecute(result);
//        }
//    }
}
