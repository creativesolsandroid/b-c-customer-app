package com.cs.bakeryco;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.cs.bakeryco.model.ItemsList;
import com.cs.bakeryco.model.MainCategories;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 09-06-2016.
 */
public class Constants {

//    static String live_url = "http://www.ircfood.com";
////    static String live_url = "http://85.194.94.241/OreganoServices";
//
//    public static String CATEGORY_ITEMS_URL = live_url+"/api/SubCategoryApi/";
//    public static String ADDITIONALS_URL = live_url+"/Api/ModifierInfoList/";
//    public static String STORES_URL = live_url+"/api/StoreInformationApi/";
//    public static String LOGIN_URL = live_url+"/api/VerifyUserCredentialsApi/";
//    public static String REGISTRATION_URL = live_url+"/api/RegistrationApi";
//    public static String INSERT_ORDER_URL = live_url+"/api/OrderDetailsApi";
//    public static String ORDER_HISTORY_URL = live_url+"/api/OrderTrackApi/";
//    public static String SAVE_ADDRESS_URL = live_url+"/api/RegistrationApi/";
//    public static String TRACK_ORDER_STEPS_URL = live_url+"/api/OrderTrackApi?orderId=";
//    public static String SAVED_ADDRESS_URL = live_url+"/api/RegistrationApi/";
//    public static String GET_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
//    public static String DELETE_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
//    public static String INSERT_FAVORITE_ORDER_URL = live_url+"/api/FaviorateOrderApi?OrderId=";
//    public static String CHANGE_PASSWORD_URL = live_url+"/api/VerifyUserCredentialsApi/";
//    public static String ORDERD_DETAILS_URL = live_url+"/api/OrderDetailsApi?OrderId=";
//    public static String VERIFY_RANDOM_NUMBER_URL = live_url+"/api/RegistrationApi?MobileNo=";
//    public static String GET_SAVED_CARDS_URL = live_url+"/api/CreditCardDetails?userId=";
//    public static String SAVE_CARD_DETAILS_URL = live_url+"/api/CreditCardDetails?userId=";
//    public static String FORGOT_PASSWORD_URL = live_url+"/api/VerifyUserCredentialsApi?UsrName=";
//    public static String GET_CURRENT_TIME_URL = live_url+"/api/GetCurrentTime";
//    public static String EDIT_ADDRESS_URL = live_url+"/api/RegistrationApi";
//    public static String GET_MESSAGES_URL = live_url+"/api/PushMsg?userid=";
//    public static String GET_OFFERS_URL = live_url+"/api/OfferDetails?offerId=";
//    public static String UPDATE_MESSAGE_URL = live_url+"/api/PushMsg";
//    public static String UPDATE_PROFILE_URL = live_url+"/api/VerifyUserCredentialsApi";
//    public static String GET_BANNERS_URL = live_url+"/api/OfferDetails";
//    public static String GET_PROMOS_URL = live_url+"/api/OreganoPromotions?userId=";
//    public static String LIVE_TRACKING_URL = live_url+"/api/OrderTrackApi?OrderId=";
//    public static String DELETE_ORDER_FROM_HISTORY = live_url + "OrderDetailsApi?Delete_OrderID=";

//    static String local_url = "http://csadms.com/bncservices";
    static String local_url = "http://bakeryandcompany.com";

    public static String CATEGORY_ITEMS_URL = local_url + "/api/ItemDetails/GetSubCategoryItemsDetails";
    public static String LOGIN_URL = local_url + "/api/RegistrationApi/Signin/";
    public static String REGISTRATION_URL = local_url + "/api/RegistrationApi/RegisterUser";
    public static String INSERT_ORDER_URL = local_url + "/api/OrderDetails/InsertOrder";
    public static String ORDER_HISTORY_URL = local_url + "/api/OrderDetails/GetOrderHistory?userId=";
    public static String SAVE_ADDRESS_URL = local_url + "/api/RegistrationApi/SaveUserAddress";
    public static String TRACK_ORDER_STEPS_URL = local_url + "/api/OrderDetails/TrackOrder?orderId=";
    public static String SAVED_ADDRESS_URL = local_url + "/api/RegistrationApi/GetAddressDetails?userid=";
    public static String DELETE_ADDRESS_URL = local_url + "/api/RegistrationApi/DeleteUserAddress?addressid=";
    public static String GET_FAVORITE_ORDERS_URL = local_url + "/api/OrderDetails/GetFaviorateOrder?userid=";
    public static String DELETE_FAVORITE_ORDERS_URL = local_url + "/api/OrderDetails/DeleteFaviorateOrder?orderId=";
    public static String INSERT_FAVORITE_ORDER_URL = local_url + "/api/OrderDetails/InsertFaviorateOrder?orderId=";
    public static String CHANGE_PASSWORD_URL = local_url + "/api/RegistrationApi/ChangePassword/";
    public static String ORDERD_DETAILS_URL = local_url + "/api/OrderDeils/GetOrderDetails?userId=";
    public static String VIEW_ORDERD_DETAILS_URL = local_url + "/api/OrderDetails/GetOrderDetails?userId=";
    public static String VERIFY_RANDOM_NUMBER_URL = local_url + "/api/RegistrationApi/VerifyMobileNo?MobileNo=";
    public static String VERIFY_MOBILE = local_url + "/api/RegistrationApi/CheckMobileNo?MobileNo=";
    public static String GET_SAVED_CARDS_URL = local_url + "/api/CreditCardDetails?userId=";
    public static String SAVE_CARD_DETAILS_URL = local_url + "/api/CreditCardDetails?userId=";
    public static String FORGOT_PASSWORD_URL = local_url + "/api/RegistrationApi/ForgetPassword?Username=";
    public static String GET_CURRENT_TIME_URL = local_url + "/api/CurrentTime/GetCurrentTime";
    public static String EDIT_ADDRESS_URL = local_url + "/api/RegistrationApi/UpdateUserAddress";
    public static String GET_MESSAGES_URL = local_url + "/api/PushMsg?userid=";
    public static String GET_OFFERS_URL = local_url + "/api/OfferDetails?offerId=";
    public static String UPDATE_MESSAGE_URL = local_url + "/OreganoServices/api/PushMsg";
    public static String UPDATE_PROFILE_URL = local_url + "/api/RegistrationApi/UpdateUserProfile";
    public static String GET_BANNERS_URL = local_url + "/api/OfferDetails";
    public static String LIVE_TRACKING_URL = local_url + "/api/OrderDetails/GetOrderLocation?orderId=";
    public static String CORPORATE_ORDER_URL = local_url + "/api/OrderDetails/InsertCorporateRequest";
    public static String CANCEL_ORDER_URL = local_url + "/api/OrderDetails/CancelOrder";
    public static String SEND_OTP = local_url + "/api/RegistrationApi/SendOTP?userid=";
    public static String STORES_URL = "http://bakeryandcompany.com/api/StoreInformation/GetStoreDetails?day=";
    public static String DELETE_ORDER_FROM_HISTORY = local_url + "/api/OrderDetails/DeleteOrder?orderId=";
    public static String RATING_URL = local_url + "/api/OrderDetails/PostInsertRating";
    public static String GET_PROMOS_URL = local_url + "/api/Promotions/GetPromotions?userId=";

    public static String IMAGE_URL = "http://bakeryandcompany.com/images/";
    public static String string = "R.drawable.sandwich";
//    public static String IMAGE_URL = "http://csadms.com/bncservices/images/";

    public static String ORDER_TYPE = "";
    public static String COMMENTS = "";
    public static int address_id = 0;
    public static float delivery_dist = 0;
    public static String address = "", address_name = "";
    public static Double user_lat, user_longi, store_lat = 24.533966, store_longi = 46.899342;
    public static boolean coupon = false;
    public static String coupon_name, coupon_price;
    static String language;

    static SharedPreferences languagePrefs;

    public static DecimalFormat decimalFormat = new DecimalFormat("0");
    public static DecimalFormat decimalFormat1 = new DecimalFormat("0.00");
    public static String Country_Code = "+966 ";

    public static ArrayList<ItemsList.Items> ItemsArrayList = new ArrayList<>();
    public static ArrayList<ItemsList.Banners> BannersArrayList = new ArrayList<>();

    public static void showOneButtonAlertDialog(String descriptionStr, Activity context) {
        AlertDialog customDialog = null;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Material_Light_Dialog));
        languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            // set title
            alertDialogBuilder.setTitle("BAKERY & Co.");

            // set dialog message
            alertDialogBuilder
                    .setMessage(descriptionStr)
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
        } else if (language.equalsIgnoreCase("Ar")) {
            // set title
            alertDialogBuilder.setTitle(context.getResources().getString(R.string.app_name_ar));

            // set dialog message
            alertDialogBuilder
                    .setMessage(descriptionStr)
                    .setCancelable(false)
                    .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
        }

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public class LocationConstants {
        public static final int SUCCESS_RESULT = 0;

        public static final int FAILURE_RESULT = 1;

        public static final String PACKAGE_NAME = "com.cs.bakeryco.";

        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

        public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

        public static final String LOCATION_DATA_AREA = PACKAGE_NAME + ".LOCATION_DATA_AREA";
        public static final String LOCATION_DATA_CITY = PACKAGE_NAME + ".LOCATION_DATA_CITY";
        public static final String LOCATION_DATA_STREET = PACKAGE_NAME + ".LOCATION_DATA_STREET";

    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static Typeface getTypeFace(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "helvetica.ttf");
        return typeface;
    }

    public static void requestEditTextFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static ArrayList<MainCategories> mainCategories = new ArrayList<>();

    public static String convertToArabic(String value) {
        String newValue = (value
                .replaceAll("١", "1").replaceAll("٢", "2")
                .replaceAll("٣", "3").replaceAll("٤", "4")
                .replaceAll("٥", "5").replaceAll("٦", "6")
                .replaceAll("٧", "7").replaceAll("٨", "8")
                .replaceAll("٩", "9").replaceAll("٠", "0")
                .replaceAll("٫", "."));
        return newValue;
    }
}
