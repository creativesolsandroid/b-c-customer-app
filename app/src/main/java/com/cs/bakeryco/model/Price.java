package com.cs.bakeryco.model;

import java.io.Serializable;

/**
 * Created by CS on 23-09-2016.
 */
public class Price implements Serializable{
    String price, size, SizeName_Ar, priceId, sizeid, itemsizedesc, ItemSizeDescription_Ar;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSizeName_Ar() {
        return SizeName_Ar;
    }

    public void setSizeName_Ar(String sizeName_Ar) {
        SizeName_Ar = sizeName_Ar;
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getSizeid() {
        return sizeid;
    }

    public void setSizeid(String sizeid) {
        this.sizeid = sizeid;
    }

    public String getItemsizedesc() {
        return itemsizedesc;
    }

    public void setItemsizedesc(String itemsizedesc) {
        this.itemsizedesc = itemsizedesc;
    }

    public String getItemSizeDescription_Ar() {
        return ItemSizeDescription_Ar;
    }

    public void setItemSizeDescription_Ar(String itemSizeDescription_Ar) {
        ItemSizeDescription_Ar = itemSizeDescription_Ar;
    }
}
