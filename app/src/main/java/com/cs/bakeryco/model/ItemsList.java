package com.cs.bakeryco.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class ItemsList {

    @Expose
    @SerializedName("Success")
    private Success Success;

    public Success getSuccess() {
        return Success;
    }

    public void setSuccess(Success Success) {
        this.Success = Success;
    }

    public static class Success {
        @Expose
        @SerializedName("Banners")
        private ArrayList<Banners> Banners;
        @Expose
        @SerializedName("Items")
        private ArrayList<Items> Items;

        public ArrayList<Banners> getBanners() {
            return Banners;
        }

        public void setBanners(ArrayList<Banners> Banners) {
            this.Banners = Banners;
        }

        public ArrayList<Items> getItems() {
            return Items;
        }

        public void setItems(ArrayList<Items> Items) {
            this.Items = Items;
        }
    }

    public static class Banners implements Serializable {
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;
        @Expose
        @SerializedName("BannerImage")
        private String BannerImage;
        @Expose
        @SerializedName("BannerName")
        private String BannerName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public String getBannerImage() {
            return BannerImage;
        }

        public void setBannerImage(String BannerImage) {
            this.BannerImage = BannerImage;
        }

        public String getBannerName() {
            return BannerName;
        }

        public void setBannerName(String BannerName) {
            this.BannerName = BannerName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Items implements Serializable {
        @Expose
        @SerializedName("SubCategoryItems")
        private ArrayList<SubCategoryItems> SubCategoryItems;
        @Expose
        @SerializedName("Simg")
        private String Simg;
        @Expose
        @SerializedName("MainCategoryDesc_Ar")
        private String MainCategoryDesc_Ar;
        @Expose
        @SerializedName("MainCategoryDesc")
        private String MainCategoryDesc;
        @Expose
        @SerializedName("MainCategoryName_Ar")
        private String MainCategoryName_Ar;
        @Expose
        @SerializedName("MainCategoryName")
        private String MainCategoryName;
        @Expose
        @SerializedName("MainCategoryId")
        private int MainCategoryId;

        public ArrayList<SubCategoryItems> getSubCategoryItems() {
            return SubCategoryItems;
        }

        public void setSubCategoryItems(ArrayList<SubCategoryItems> SubCategoryItems) {
            this.SubCategoryItems = SubCategoryItems;
        }

        public String getSimg() {
            return Simg;
        }

        public void setSimg(String Simg) {
            this.Simg = Simg;
        }

        public String getMainCategoryDesc_Ar() {
            return MainCategoryDesc_Ar;
        }

        public void setMainCategoryDesc_Ar(String MainCategoryDesc_Ar) {
            this.MainCategoryDesc_Ar = MainCategoryDesc_Ar;
        }

        public String getMainCategoryDesc() {
            return MainCategoryDesc;
        }

        public void setMainCategoryDesc(String MainCategoryDesc) {
            this.MainCategoryDesc = MainCategoryDesc;
        }

        public String getMainCategoryName_Ar() {
            return MainCategoryName_Ar;
        }

        public void setMainCategoryName_Ar(String MainCategoryName_Ar) {
            this.MainCategoryName_Ar = MainCategoryName_Ar;
        }

        public String getMainCategoryName() {
            return MainCategoryName;
        }

        public void setMainCategoryName(String MainCategoryName) {
            this.MainCategoryName = MainCategoryName;
        }

        public int getMainCategoryId() {
            return MainCategoryId;
        }

        public void setMainCategoryId(int MainCategoryId) {
            this.MainCategoryId = MainCategoryId;
        }
    }

    public static class SubCategoryItems implements Serializable {
        @Expose
        @SerializedName("Sections")
        private ArrayList<Sections> Sections;
        @Expose
        @SerializedName("MainCategoryId")
        private int MainCategoryId;
        @Expose
        @SerializedName("SubCategorySeq")
        private int SubCategorySeq;
        @Expose
        @SerializedName("SubCategoryBackgroundImage")
        private String SubCategoryBackgroundImage;
        @Expose
        @SerializedName("SubCategoryImage")
        private String SubCategoryImage;
        @Expose
        @SerializedName("SubCategoryDesc_Ar")
        private String SubCategoryDesc_Ar;
        @Expose
        @SerializedName("SubCategoryName_Ar")
        private String SubCategoryName_Ar;
        @Expose
        @SerializedName("SubCategoryDesc")
        private String SubCategoryDesc;
        @Expose
        @SerializedName("SubCategoryName")
        private String SubCategoryName;
        @Expose
        @SerializedName("SubMainCategoryId")
        private int SubMainCategoryId;
        @Expose
        @SerializedName("SubCategoryId")
        private int SubCategoryId;

        public ArrayList<Sections> getSections() {
            return Sections;
        }

        public void setSections(ArrayList<Sections> Sections) {
            this.Sections = Sections;
        }

        public int getMainCategoryId() {
            return MainCategoryId;
        }

        public void setMainCategoryId(int MainCategoryId) {
            this.MainCategoryId = MainCategoryId;
        }

        public int getSubCategorySeq() {
            return SubCategorySeq;
        }

        public void setSubCategorySeq(int SubCategorySeq) {
            this.SubCategorySeq = SubCategorySeq;
        }

        public String getSubCategoryBackgroundImage() {
            return SubCategoryBackgroundImage;
        }

        public void setSubCategoryBackgroundImage(String SubCategoryBackgroundImage) {
            this.SubCategoryBackgroundImage = SubCategoryBackgroundImage;
        }

        public String getSubCategoryImage() {
            return SubCategoryImage;
        }

        public void setSubCategoryImage(String SubCategoryImage) {
            this.SubCategoryImage = SubCategoryImage;
        }

        public String getSubCategoryDesc_Ar() {
            return SubCategoryDesc_Ar;
        }

        public void setSubCategoryDesc_Ar(String SubCategoryDesc_Ar) {
            this.SubCategoryDesc_Ar = SubCategoryDesc_Ar;
        }

        public String getSubCategoryName_Ar() {
            return SubCategoryName_Ar;
        }

        public void setSubCategoryName_Ar(String SubCategoryName_Ar) {
            this.SubCategoryName_Ar = SubCategoryName_Ar;
        }

        public String getSubCategoryDesc() {
            return SubCategoryDesc;
        }

        public void setSubCategoryDesc(String SubCategoryDesc) {
            this.SubCategoryDesc = SubCategoryDesc;
        }

        public String getSubCategoryName() {
            return SubCategoryName;
        }

        public void setSubCategoryName(String SubCategoryName) {
            this.SubCategoryName = SubCategoryName;
        }

        public int getSubMainCategoryId() {
            return SubMainCategoryId;
        }

        public void setSubMainCategoryId(int SubMainCategoryId) {
            this.SubMainCategoryId = SubMainCategoryId;
        }

        public int getSubCategoryId() {
            return SubCategoryId;
        }

        public void setSubCategoryId(int SubCategoryId) {
            this.SubCategoryId = SubCategoryId;
        }
    }

    public static class Sections implements Serializable {
        @Expose
        @SerializedName("Items")
        private ArrayList<Item> Items;
        @Expose
        @SerializedName("SectionName")
        private String SectionName;
        @Expose
        @SerializedName("SectionName_Ar")
        private String SectionName_Ar;
        @Expose
        @SerializedName("SectionSubCategory")
        private int SectionSubCategory;
        @Expose
        @SerializedName("SectionId")
        private int SectionId;

        public ArrayList<Item> getItems() {
            return Items;
        }

        public void setItems(ArrayList<Item> Items) {
            this.Items = Items;
        }

        public String getSectionName() {
            return SectionName;
        }

        public void setSectionName(String SectionName) {
            this.SectionName = SectionName;
        }

        public String getSectionName_Ar() {
            return SectionName_Ar;
        }

        public void setSectionName_Ar(String SectionName_Ar) {
            this.SectionName_Ar = SectionName_Ar;
        }

        public int getSectionSubCategory() {
            return SectionSubCategory;
        }

        public void setSectionSubCategory(int SectionSubCategory) {
            this.SectionSubCategory = SectionSubCategory;
        }

        public int getSectionId() {
            return SectionId;
        }

        public void setSectionId(int SectionId) {
            this.SectionId = SectionId;
        }
    }

    public static class Item implements Serializable {
        @Expose
        @SerializedName("SubItems")
        private ArrayList<SubItems> SubItems;
        @Expose
        @SerializedName("Price")
        private ArrayList<Price> Price;
        @Expose
        @SerializedName("NoOfItems")
        private int NoOfItems;
        @Expose
        @SerializedName("DisplayName_Ar")
        private String DisplayName_Ar;
        @Expose
        @SerializedName("DisplayName")
        private String DisplayName;
        @Expose
        @SerializedName("IsDeliver")
        private boolean IsDeliver;
        @Expose
        @SerializedName("BindingType")
        private int BindingType;
        @Expose
        @SerializedName("ItemSequence")
        private int ItemSequence;
        @Expose
        @SerializedName("ItemTypeIds")
        private String ItemTypeIds;
        @Expose
        @SerializedName("ItemImage")
        private String ItemImage;
        @Expose
        @SerializedName("ItemDesc_Ar")
        private String ItemDesc_Ar;
        @Expose
        @SerializedName("ItemDesc")
        private String ItemDesc;
        @Expose
        @SerializedName("ItemName_Ar")
        private String ItemName_Ar;
        @Expose
        @SerializedName("ItemName")
        private String ItemName;
        @Expose
        @SerializedName("SectionId")
        private int SectionId;
        @Expose
        @SerializedName("SubCategoryId")
        private int SubCategoryId;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;

        public ArrayList<SubItems> getSubItems() {
            return SubItems;
        }

        public void setSubItems(ArrayList<SubItems> SubItems) {
            this.SubItems = SubItems;
        }

        public ArrayList<Price> getPrice() {
            return Price;
        }

        public void setPrice(ArrayList<Price> Price) {
            this.Price = Price;
        }

        public int getNoOfItems() {
            return NoOfItems;
        }

        public void setNoOfItems(int NoOfItems) {
            this.NoOfItems = NoOfItems;
        }

        public String getDisplayName_Ar() {
            return DisplayName_Ar;
        }

        public void setDisplayName_Ar(String DisplayName_Ar) {
            this.DisplayName_Ar = DisplayName_Ar;
        }

        public String getDisplayName() {
            return DisplayName;
        }

        public void setDisplayName(String DisplayName) {
            this.DisplayName = DisplayName;
        }

        public boolean getIsDeliver() {
            return IsDeliver;
        }

        public void setIsDeliver(boolean IsDeliver) {
            this.IsDeliver = IsDeliver;
        }

        public int getBindingType() {
            return BindingType;
        }

        public void setBindingType(int BindingType) {
            this.BindingType = BindingType;
        }

        public int getItemSequence() {
            return ItemSequence;
        }

        public void setItemSequence(int ItemSequence) {
            this.ItemSequence = ItemSequence;
        }

        public String getItemTypeIds() {
            return ItemTypeIds;
        }

        public void setItemTypeIds(String ItemTypeIds) {
            this.ItemTypeIds = ItemTypeIds;
        }

        public String getItemImage() {
            return ItemImage;
        }

        public void setItemImage(String ItemImage) {
            this.ItemImage = ItemImage;
        }

        public String getItemDesc_Ar() {
            return ItemDesc_Ar;
        }

        public void setItemDesc_Ar(String ItemDesc_Ar) {
            this.ItemDesc_Ar = ItemDesc_Ar;
        }

        public String getItemDesc() {
            return ItemDesc;
        }

        public void setItemDesc(String ItemDesc) {
            this.ItemDesc = ItemDesc;
        }

        public String getItemName_Ar() {
            return ItemName_Ar;
        }

        public void setItemName_Ar(String ItemName_Ar) {
            this.ItemName_Ar = ItemName_Ar;
        }

        public String getItemName() {
            return ItemName;
        }

        public void setItemName(String ItemName) {
            this.ItemName = ItemName;
        }

        public int getSectionId() {
            return SectionId;
        }

        public void setSectionId(int SectionId) {
            this.SectionId = SectionId;
        }

        public int getSubCategoryId() {
            return SubCategoryId;
        }

        public void setSubCategoryId(int SubCategoryId) {
            this.SubCategoryId = SubCategoryId;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }

    public static class SubItems implements Serializable {
        @Expose
        @SerializedName("SubItemIsDeliver")
        private boolean SubItemIsDeliver;
        @Expose
        @SerializedName("SubItemSequence")
        private int SubItemSequence;
        @Expose
        @SerializedName("SubItemImage")
        private String SubItemImage;
        @Expose
        @SerializedName("SubItemDesc_Ar")
        private String SubItemDesc_Ar;
        @Expose
        @SerializedName("SubItemDesc")
        private String SubItemDesc;
        @Expose
        @SerializedName("SubItemName_Ar")
        private String SubItemName_Ar;
        @Expose
        @SerializedName("SubItemName")
        private String SubItemName;
        @Expose
        @SerializedName("SubItemId")
        private int SubItemId;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;

        public boolean getSubItemIsDeliver() {
            return SubItemIsDeliver;
        }

        public void setSubItemIsDeliver(boolean SubItemIsDeliver) {
            this.SubItemIsDeliver = SubItemIsDeliver;
        }

        public int getSubItemSequence() {
            return SubItemSequence;
        }

        public void setSubItemSequence(int SubItemSequence) {
            this.SubItemSequence = SubItemSequence;
        }

        public String getSubItemImage() {
            return SubItemImage;
        }

        public void setSubItemImage(String SubItemImage) {
            this.SubItemImage = SubItemImage;
        }

        public String getSubItemDesc_Ar() {
            return SubItemDesc_Ar;
        }

        public void setSubItemDesc_Ar(String SubItemDesc_Ar) {
            this.SubItemDesc_Ar = SubItemDesc_Ar;
        }

        public String getSubItemDesc() {
            return SubItemDesc;
        }

        public void setSubItemDesc(String SubItemDesc) {
            this.SubItemDesc = SubItemDesc;
        }

        public String getSubItemName_Ar() {
            return SubItemName_Ar;
        }

        public void setSubItemName_Ar(String SubItemName_Ar) {
            this.SubItemName_Ar = SubItemName_Ar;
        }

        public String getSubItemName() {
            return SubItemName;
        }

        public void setSubItemName(String SubItemName) {
            this.SubItemName = SubItemName;
        }

        public int getSubItemId() {
            return SubItemId;
        }

        public void setSubItemId(int SubItemId) {
            this.SubItemId = SubItemId;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }
    }

    public static class Price implements Serializable {
        @Expose
        @SerializedName("ItempriceId")
        private int ItempriceId;
        @Expose
        @SerializedName("priceId")
        private int priceId;
        @Expose
        @SerializedName("ItemSizeDescription_Ar")
        private String ItemSizeDescription_Ar;
        @Expose
        @SerializedName("ItemSizeDescription")
        private String ItemSizeDescription;
        @Expose
        @SerializedName("SizeName_Ar")
        private String SizeName_Ar;
        @Expose
        @SerializedName("size")
        private String size;
        @Expose
        @SerializedName("sizeId")
        private int sizeId;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("Calories")
        private String Calories;



        public int getItempriceId() {
            return ItempriceId;
        }

        public void setItempriceId(int ItempriceId) {
            this.ItempriceId = ItempriceId;
        }

        public int getPriceId() {
            return priceId;
        }

        public void setPriceId(int priceId) {
            this.priceId = priceId;
        }

        public String getItemSizeDescription_Ar() {
            return ItemSizeDescription_Ar;
        }

        public void setItemSizeDescription_Ar(String ItemSizeDescription_Ar) {
            this.ItemSizeDescription_Ar = ItemSizeDescription_Ar;
        }

        public String getItemSizeDescription() {
            return ItemSizeDescription;
        }

        public void setItemSizeDescription(String ItemSizeDescription) {
            this.ItemSizeDescription = ItemSizeDescription;
        }

        public String getSizeName_Ar() {
            return SizeName_Ar;
        }

        public void setSizeName_Ar(String SizeName_Ar) {
            this.SizeName_Ar = SizeName_Ar;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public int getSizeId() {
            return sizeId;
        }

        public void setSizeId(int sizeId) {
            this.sizeId = sizeId;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCalories() {
            return Calories;
        }

        public void setCalories(String calories) {
            Calories = calories;
        }
    }
}
