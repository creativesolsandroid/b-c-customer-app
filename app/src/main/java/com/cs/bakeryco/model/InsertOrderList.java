package com.cs.bakeryco.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsertOrderList {


    @Expose
    @SerializedName("Success")
    private String Success = "";
    @Expose
    @SerializedName("Failure")
    private String Failure = "";

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String Success) {
        this.Success = Success;
    }

    public String getFailure() {
        return Failure;
    }

    public void setFailure(String failure) {
        Failure = failure;
    }
}
