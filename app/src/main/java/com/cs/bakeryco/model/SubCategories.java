package com.cs.bakeryco.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SKT on 15-01-2016.
 */
public class SubCategories implements Serializable{
    String subCatId, catId, subCat, description, images, subCatAr, descriptionAr, image, subCatSeq, maincatid;
    ArrayList<Section> sections;


    public String getSubCatId() {
        return subCatId;
    }

    public String getSubCatSeq() {
        return subCatSeq;
    }

    public void setSubCatSeq(String subCatSeq) {
        this.subCatSeq = subCatSeq;
    }

    public String getMaincatid() {
        return maincatid;
    }

    public void setMaincatid(String maincatid) {
        this.maincatid = maincatid;
    }

    public ArrayList<Section> getSections() {
        return sections;
    }

    public void setSections(ArrayList<Section> sections) {
        this.sections = sections;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getSubCat() {
        return subCat;
    }

    public void setSubCat(String subCat) {
        this.subCat = subCat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getSubCatAr() {
        return subCatAr;
    }

    public void setSubCatAr(String subCatAr) {
        this.subCatAr = subCatAr;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
