package com.cs.bakeryco.model;

import java.io.Serializable;

/**
 * Created by CS on 28-02-2017.
 */

public class Rating implements Serializable {

    String ratingId,comment,comment_ar,rating;

    public String getRatingId() {
        return ratingId;
    }

    public void setRatingId(String ratingId) {
        this.ratingId = ratingId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_ar() {
        return comment_ar;
    }

    public void setComment_ar(String comment_ar) {
        this.comment_ar = comment_ar;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
