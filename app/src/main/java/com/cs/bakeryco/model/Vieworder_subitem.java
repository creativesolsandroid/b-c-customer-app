package com.cs.bakeryco.model;

public class Vieworder_subitem {

    String subitemid, subitemname, subitemname_ar, subitemqty;

    public String getSubitemid() {
        return subitemid;
    }

    public void setSubitemid(String subitemid) {
        this.subitemid = subitemid;
    }

    public String getSubitemname() {
        return subitemname;
    }

    public void setSubitemname(String subitemname) {
        this.subitemname = subitemname;
    }

    public String getSubitemname_ar() {
        return subitemname_ar;
    }

    public void setSubitemname_ar(String subitemname_ar) {
        this.subitemname_ar = subitemname_ar;
    }

    public String getSubitemqty() {
        return subitemqty;
    }

    public void setSubitemqty(String subitemqty) {
        this.subitemqty = subitemqty;
    }
}
