package com.cs.bakeryco.model;

public class Track_subitems {

    String SubItem, SubItemqty;

    public String getSubItem() {
        return SubItem;
    }

    public void setSubItem(String subItem) {
        SubItem = subItem;
    }

    public String getSubItemqty() {
        return SubItemqty;
    }

    public void setSubItemqty(String subItemqty) {
        SubItemqty = subItemqty;
    }
}
