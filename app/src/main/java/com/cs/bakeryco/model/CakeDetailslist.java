package com.cs.bakeryco.model;

import java.io.Serializable;

/**
 * Created by CS on 4/1/2018.
 */

public class CakeDetailslist implements Serializable {

    String price, sizeId, size, cakedesc, priceid, itemid;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCakedesc() {
        return cakedesc;
    }

    public void setCakedesc(String cakedesc) {
        this.cakedesc = cakedesc;
    }

    public String getPriceid() {
        return priceid;
    }

    public void setPriceid(String priceid) {
        this.priceid = priceid;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }
}
