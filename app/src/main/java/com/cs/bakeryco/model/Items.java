package com.cs.bakeryco.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SKT on 15-01-2016.
 */
public class Items implements Serializable{
    String itemId, sectionid, subCatId, itemNo, itemName, description, images, itemNameAr, descriptionAr, image, itemtypeIds, itemseq, buildingType, Isdeliver, displayname, displayname_ar, no_of_items;

    ArrayList<Price> priceList;

    ArrayList<SubItems> subItems;

    public ArrayList<SubItems> getSubItems() {
        return subItems;
    }

    public void setSubItems(ArrayList<SubItems> subItems) {
        this.subItems = subItems;
    }

    public ArrayList<Price> getPriceList() {
        return priceList;
    }

    public void setPriceList(ArrayList<Price> priceList) {
        this.priceList = priceList;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemtypeIds() {
        return itemtypeIds;
    }

    public void setItemtypeIds(String itemtypeIds) {
        this.itemtypeIds = itemtypeIds;
    }

    public String getSectionid() {
        return sectionid;

    }

    public void setSectionid(String sectionid) {
        this.sectionid = sectionid;
    }

    public String getItemseq() {
        return itemseq;
    }

    public void setItemseq(String itemseq) {
        this.itemseq = itemseq;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getIsdeliver() {
        return Isdeliver;
    }

    public void setIsdeliver(String isdeliver) {
        Isdeliver = isdeliver;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getDisplayname_ar() {
        return displayname_ar;
    }

    public void setDisplayname_ar(String displayname_ar) {
        this.displayname_ar = displayname_ar;
    }

    public String getNo_of_items() {
        return no_of_items;
    }

    public void setNo_of_items(String no_of_items) {
        this.no_of_items = no_of_items;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
