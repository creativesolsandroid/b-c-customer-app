package com.cs.bakeryco.widgets;

/**
 * Created by CS on 4/17/2018.
 */
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.GridView;

public class CustomGridView1 extends GridView {

    public CustomGridView1(Context context) {
        super(context);
    }

    public CustomGridView1(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomGridView1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev){
        // Called when a child does not want this parent and its ancestors to intercept touch events.
        requestDisallowInterceptTouchEvent(true);
        return super.onTouchEvent(ev);
    }
}
